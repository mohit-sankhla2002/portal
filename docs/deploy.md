# Deployment

## Production

The instructions below describe a containerized deployment with `docker compose`.
Kubernetes deployment has been dropped.

### Overview

When Portal is running, a number of internal services need to be running as
well:

- `db` the [postgreSQL](https://www.postgresql.org/) database backend.
- `web` the Django backend which runs the whole Portal, depends on `db`.
- `frontend` is displaying the GUI in the browser. When starting up, various
  elements first need to be built, using `npm`.
- `traefik` responsible for handling certificates (for https) and load
  balancing, routes requests either to the `web` backend or to `nginx`.
- `nginx` http webserver, responsible for serving static files (i.e. all files
  found under `/static`).
- `redis` broker for celery tasks, see below.
- `web-celery-beat` tasks that run periodically, very much like cronjobs.
- `web-celery` handles asynchronous tasks or tasks that need longer time to
  complete.
- `fluent-bit` pre-processes `web` log files, sends them to the
  [data prepper](https://opensearch.org/docs/latest/data-prepper/index/) server,
  which finally ingests them into [OpenSearch](https://opensearch.org), where
  the log files can be easily accessed. `fluent-bit` is a replacement of
  `filebeat` which we used in earlier versions.

In addition, portal relies on _external services_:

- `keycloak` identity broker. During login, a user is redirected to the
  configured identity provider, e.g. to SWITCH edu-ID or a local LDAP. Returns
  the relevant user claims (user data) back to the portal.
- `smtp server` to send out notification emails to the user.

### Traefik configuration

- make sure you have a server certificate in `.pem` format (usual `.crt`
  format doesn't work)

### Keycloak configuration

**Authentication mechanics:**

- User visits the Portal, e.g. `portal.dcc.sib.swiss/`
- Some JavaScript detects that the user is not authenticated yet
- User is redirected to `portal.dcc.sib.swiss/auth/oidc/authenticate/`
- Django redirects user further to Keycloak at `login.biomedit.ch`
- Keycloak uses its own workflow to display a page to sign in with SWITCH
  edu-ID or to authenticate against an LDAP, etc.
- In case of SWITCH edu-ID, users are taken to `login.eduid.ch`, where they
  needs to enter username/password as well as the TOTP or SMS-code (2nd factor
  authentication).
- After successful authentication, users are redirected to keycloak together
  with account information (SAML2 token, because openID Connect is not yet
  fully supported by SWITCH).
- User object in Keycloak is internally being created/updated.
- Keycloak redirects user to the portal, sending an authorization token (JWT).

**Keycloak configuration with LDAP:**

- Configure your **local keycloak instance** to use a local LDAP server as an
  authentication backend:
  - First, make sure you are _not_ working in the `Master` realm. If you see
    `Master` on top of the left column, click on the dropdown menu and on
    `Create Realm`
  - Name the realm `portal`, click on `create`.
  - Choose `User federation` and `Add new provider` to add a new LDAP server
  - Define the proxy user (i.e. a user with higher privileges) for Bind DN and
    Bind credentials.
  - Make sure you define sensible values for searching and updating. For
    example, in ActiveDirectory the UUID LDAP attribute is typically
    `objectGUID` which never changes, in OpenLDAP it is better to use
    `uidNumber` or a similiar attribute that uniquely identifies an entry.
    The username attribute in AD is called `sAMAccountName`, in OpenLDAP it
    is `uid`.
  - After testing and saving the LDAP configuration, switch to the `Mappers`
    tab to configure the attribute mapping. Use the `user-attribute-ldap-mapper`
    mapper type.
  - Keycloak maps various LDAP attributes (in brackets) to internal attributes,
    so called User Model Attribute. Examples:
    - email (mail)
    - firstName (givenName)
    - lastName (sn)
    - username (uid)
  - To test the mapping, switch to the Users tab and search for an existing
    user.
  - You can add any additional mappings as you like – just remember the correct
    writing of the internal User Model Attribute when you later define a client
    and mapper.
  - Go to the Clients tab, click on Import client and use
    [this template file](keycloak-client-example.json) to import a client.
  - Click on the newly imported portal client, switch to Credentials and
    _regenerate_ the client secret.
  - Copy the client secret and paste it in `web/.config.json` in `client_secret`
    (see also below).

### Configure `web/.config.json`

- Use `/web/.config.json.sample` as template.
- The `client_id` attribute is equal to the `Client ID` in the Clients tab of
  Keycloak (e.g. `portal`).
- The `client_secret` is equal to the `Client secret` in Clients tab,
  Credentials. Do not forget to _regenerate the secret_ in Keycloak after
  importing the client template (see above).
- `config_url` attribute: in the Keycloak Realm Settings tab, click on the
  OpenID Endpoint Configuration link (should display a configuration in JSON
  format). Copy the URL.

### Configure `frontend/.env`

- Use `frontend/.env.sample` as template
- Adjust `NEXT_PUBLIC_CONTACT_EMAIL`
- Adjust `NEXT_PUBLIC_KEY_SERVER`

### Configure `.env`

- Change the `SECRET_KEY` (you can use any online Django secret generator)
- Change `ALLOWED_HOSTS` and `SERVICE_HOSTNAME` according to your hostname
- Change `DB_PASS`, `POSTGRES_PASSWORD`
- Adjust `LOGSTASH_HOST`, if you run a logstash server
- Adjust `DJANGO_SUPERUSER_PASSWORD`, `DJANGO_SUPERUSER_USERNAME`,
  `DJANGO_SUPERUSER_EMAIL`.

### Run your application

This command will build and run the Portal application in production.
Add `-d` to daemonize it:

```bash
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/fluent-bit.yml -f docker-compose.prod.yml up --build
```

- Access service at your domain name.

Note: the build command creates two images, one for `frontend` and one for
`web`. Their registry prefixes are defined by the `REGISTRY` environment
variable (see `.env.sample`).

### Running without traefik / fluent-bit

If you have a system with pre deployed traefik, like
[here](https://framagit.org/oxyta.net/proxyta.net), then just omit
`-f compose/traefik.yml` in all `docker compose` invocations.

Similarly, if you don't need fluent-bit, simply omit the
`compose/fluent-bit.yml` file.

## Production-like local deployment

You can set up a local environment resembling production by using
`docker-compose.local-prod.yml` instead of `docker-compose.prod.yml`.
The `local-prod` deployment runs over plain HTTP and exposes the service
monitoring system at <http://localhost:8080>

```bash
docker compose -f docker-compose.yml -f compose/traefik.yml -f compose/fluent-bit.yml -f docker-compose.local-prod.yml up --build
```
