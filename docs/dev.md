# Development

Requirements:

- Python == 3.11
- NodeJS >= 16
- Docker Engine >= 17.04.0

## Testing

For detailed instructions on testing Portal see [testing](test.md).

## Local deployment

### Initial setup

- Install [nodejs](https://nodejs.org).
- Install backend: see [instructions below](#Setting up the backend).
- Install frontend: see [`../frontend/README.md`](frontend/README.md).
- Start the backend virtualenv: `source venv/bin/activate`.
- Start the backend: `cd web && python3 ./manage.py runserver localhost:8000`.
- Start the front-end: `cd frontend & npm run start`.
  If this fails, try to re-run `npm install`.
- The portal front-end should now be up on `localhost:3000`.
- Go to the backend admin `localhost:8000/backend/admin/`,
  where you can now add nodes and users.

### Setting up the backend

- Copy `.env.sample` to `.env` and edit the values to match your application.
- Enter the the `web` directory: `cd web`.
- Copy `.config.toml.sample` to `.config.toml` and edit the settings to
  match the OIDC identity provider service configuration:
  - Replace `client_secret` in the `oidc: client_secret` entry with the
    keycloak client OIDC secret token. This is the token that is used to
    authenticate with the keycloak service.
  - If you intend to use the test keycloak instance, replace `auth` with
    `keycloak-test` in the `config_url`.
  - The length of the local_username can optionally be configured in your
    local `.config.toml` file, by adding this inside the "user" object:
    `"local_username": {"min_length": 6, "max_length":64}` (the maximum length
    allowed by the database is 64).
- Create and activate a python3 `venv`:
  `python3 -m venv ./venv && source venv/bin/activate`.
- Install requirements: `pip install -r requirements-dev.txt`.
- If you intend to write new tests, to get code completion in your IDE install
  the test requirements: `pip install -r requirements-test.txt`.
  Note that this is not necessary for running tests.
- Run database migrations `python3 ./manage.py migrate`.
- Create an admin user for the local backend: `python3 ./manage.py createsuperuser`.
  This is the user needed for `localhost:8000/backend/admin/`.
- Run the service `./manage.py runserver localhost:8000`.
- The backend service can be accessed at `localhost:8000/backend/`.
- The admin panel can be accessed at `localhost:8000/backend/admin/`.

**NOTE:** to reset/delete the database associated with the portal, delete the
file `web/db.sqlite3`, then run `python3 ./manage.py migrate`.

### Setting-up the frontend

- Go to the `frontend` directory.
- Copy `.env.sample` to `.env` and edit the values to match your application.
- Run:

  ```bash
  npm install
  npm start
  ```

A new browser tab should automatically open at `localhost:3000`

Detailed instructions can be found in [frontend documentation](frontend/README.md).

### Starting the portal service

- Start the backend service

  ```bash
  cd web
  ./manage.py runserver localhost:8000
  ```

- Start the frontend service

  ```bash
  cd frontend
  npm start
  ```

## Docker-based development

```bash
docker compose -f docker-compose.local-dev.yml up --build
```

- This will start the frontend on [localhost:3000](localhost:3000) and the
  backend on [localhost:8000](localhost:8000)
- During the build process, a local `admin` superuser will be created in the
  postgres database, with password `admin`
- A database dump can be placed in `compose/postgres/portal.sql` (edit of
  `compose/postgres/Dockerfile` to copy this file is also needed). When the
  database has not been created yet, this db dump will be loaded.
- To reset the database, delete the `portal_db_data` docker volume and
  rerun the `docker compose` command above.
- Log in as admin/admin via the backend:
  [http://localhost:8000/backend/admin/](http://localhost:8000/backend/admin/)
- Visit the frontend on [localhost:3000](localhost:3000)
- To use federated identity authentication via OpenID Connect, you'll still
  have to create a `web/.config.toml` using the `web/.config.toml.sample`
  template.

### Run the frontend typescript checks

```bash
docker exec -it portal-frontend-1 npm run ts-check
```

### Run the frontend UI tests

```bash
docker exec -it portal-frontend-1 npm run test
```

### Run the backend tests

For more explanations and examples, see **Running backend tests** below.

```bash
docker exec -it portal-web-1 tox -e py311 -- -n 8 projects/tests/views
docker exec -it portal-web-1 tox -e py311 -- projects/tests/views/test_project.py --pdb
docker exec -it portal-web-1 tox -e py311 -- projects/tests/views/test_project.py -k test_view_users --pdb
```

When you run this command for the first time, `tox` will create a virtual
environment in `web/.tox` and install all the dependencies, which will take a
while. Subsequent runs are much faster.

### Debugging portal backend with docker compose

Unfortunately, you cannot directly set a `breakpoint()` or `import pdb; pdb.set_trace()`.
Such a command would result in an error like this:

```bash
portal-web-1        | (Pdb)
portal-web-1        | Unhandled exception in thread started by...
...
portal-web-1        | bdb.BdbQuit
```

Here we describe how this problem can be circumvented. Once you started portal
with the command described above.

```bash
docker compose -f docker-compose.local-dev.yml up --build
```

you should see (in another terminal) all the running containers:

```bash
docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED        STATUS         PORTS                    NAMES
39ff16d71145   ...               "docker-entrypoint.s…"   45 hours ago   Up 2 minutes   0.0.0.0:3000->3000/tcp   portal-frontend-1
38ba6d953723   ...               "/entrypoint /start"     45 hours ago   Up 2 minutes   0.0.0.0:8000->8000/tcp   portal-web-1
32d7678270cb   ...               "docker-entrypoint.s…"   45 hours ago   Up 2 minutes   0.0.0.0:5432->5432/tcp   portal-db-1
```

1. Remove docker container.

```bash
docker rm -f portal-web-1
```

2. Run Django with service-ports.

```bash
docker compose -f docker-compose.local-dev.yml run --rm --service-ports web
```

Now you can insert a `breakpoint()` in your code, the Django backand will automatically restart and the breakpoint will
work as expected!

### Update `package.json` and `package-lock.json` using docker compose

- Stop the containers.
- Un-comment these two lines in `docker-compose.local-dev.yml` in the
  `frontend` section:
  - `- ./frontend/package.json:/usr/app/package.json`
  - `- ./frontend/package-lock.json:/usr/app/package-lock.json`
- Start `docker compose -f docker-compose.local-dev.yml up --build`.
- Update `package.json` on the host and change the versions you want to change.
- Run `docker exec -it portal-frontend-1 npm install` to update the node
  packages. It will update `package-lock.json` on the host as well.
- Stop the containers.
- Re-comment the two lines in `docker-compose.local-dev.yml` in the `frontend`
  section:
  - `#- ./frontend/package.json:/usr/app/package.json`
  - `#- ./frontend/package-lock.json:/usr/app/package-lock.json`
- Commit both `package.json` and `package-lock.json`.

## Automated code generation

When changing the API or database models it's necessary to generate code that
reflects those changes.

### Generating the backend API in the frontend based on the backend

Every time you change something in the backend that involves changes in the
API, you also need to re-generate the backend API in the frontend.

- Re-generate the backend API in the frontend:

  ```bash
  cd frontend
  npm run generate-api
  ```

- Commit the changes inside `frontend/src/api/generated` in the same commit as
  your change in the backend.

## Using Danger

The project uses [Danger JS](https://danger.systems/js/) to automate some code
review chores.

The following setup is specific to **GitLab** and will change if your
repository is hosted by another service.

1. From the project's home page, navigate to **Settings** -> **Access Tokens**.
2. Create an access token with **Reporter** role and **read/write** API scope.
3. Copy the newly created access token - it will not be possible to access it
   again after leaving the page.
4. Navigate to **Settings** -> **CI/CD**.
5. Expand the **Variables** section.
6. Create a variable named `DANGER_GITLAB_API_TOKEN` and set the access token as
   its value.
7. Create a variable named `TECHNICAL_COORDINATOR` and set the username of your
   team's technical coordinator as its value. A comma separated list of
   usernames will also work, in case you want more than one person to be
   notified. Make sure the variable is not protected, so that it is visible on
   feature branches.
8. Optionally, create a variable named
   `TECHNICAL_COORDINATOR_NOTIFICATION_LABEL`. Its value should be the name of
   the label that if present should trigger the notification to the technical
   coordinator. If you don't do this, the default `UX/UI` label will be used.

## CI/CD

This project uses GitLab CI/CD for testing, building, and releasing new
versions of the software. The CI jobs runs on both public (provided by
GitLab) and private (self-hosted) runners. Since the public runners are
limited to 1 CPU, we run heavy jobs that can make use of multiple CPUs
on private runners. We currently use `docker` and `shell` types of runners
(see [executors](https://docs.gitlab.com/runner/executors/)). Heavy jobs
should be marked with the `parallel` tag (see the `tags` sections in
`.gitlab-ci.yml`).

In order to register a new private runner follow the
[official instructions](https://docs.gitlab.com/runner/register/).
Note:

- If you want to share the same runner for multiple projects register it as a
  [group runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners).
- Make sure that the host machine has sufficient CPU and memory resources to
  efficiently run the desired jobs.
- Add the required tag (e.g. `parallel`) during the runner registration.
- Runners do **not** limit the number of concurrent jobs that they handle.
  In order to not exhaust the available resources on the host machine you
  should set a limit on concurrent jobs
  ([runners section of config.toml](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section)).
  You can also set the maximum number of concurrent jobs across all registered
  runners on a given
  host ([global section of config.toml](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section)).

## OAUTH

This project may act as [OIDC](https://en.wikipedia.org/wiki/OpenID) provider.
Associated configuration section from `web/.config.toml` looks as following:

```toml
[oauth]
active = true
issuer = "http://localhost:8000/oauth"
expiration_time_seconds = 3_600
alg = "RS256"

  [oauth.jwt_key]
  public_path = "/tmp/public.pem"
  private_path = "/tmp/private.pem"

```

In order to use **OIDC**, you will need a
[TSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) key pair:

```bash
openssl genrsa -out private.pem 4096                 # To generate a new private key.
openssl rsa -in private.pem -pubout -out public.pem  # To generate a public key out of the private one.
```

After generation, just specify the full path to the keys in the above
configuration.
