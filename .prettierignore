# NOTE: pretty-quick handles paths differently from prettier inside of the `.prettierignore` file!
#
# Prettier:
# Evaluates the paths defined here relative from the path of where prettier is executed (working directory)
# So if this file includes `src/api/generated` and we run prettier inside the `frontend` folder, then
# `frontend/src/api/generated` will be ignored.
#
# pretty-quick:
# Evaluates the paths defined here relative from the path of the project's root
# So if this file includes `src/api/generated` and we run pretty-quick inside the `frontend` folder, then
# `src/api/generated` will be ignored.
#
# To avoid issues with relative paths, use a syntax like `/**/src/api/generated` here as long as the path is unique
#
# See https://github.com/azz/pretty-quick/issues/95 for details


# package.json is formatted by package managers, so we ignore it here
package.json
package-lock.json

# other generated files
/**/src/api/generated

# ignore backend files
/**/web

# binary files
*.jpg
*.png
*.gif

# cannot be formatted by Prettier
*.conf
*.py
*.toml
*.txt
.gitignore
.gitmessage
.prettierignore

# don't format csv files
*.csv

# .gitignore files:
# Created by .ignore support plugin (hsz.mobi)
### Emacs template
# -*- mode: gitignore; -*-
*~
\#*\#
/.emacs.desktop
/.emacs.desktop.lock
*.elc
auto-save-list
tramp
.\#*

# Org-mode
.org-id-locations
*_archive

# flymake-mode
*_flymake.*

# eshell files
/eshell/history
/eshell/lastdir

# elpa packages
/elpa/

# reftex files
*.rel

# AUCTeX auto folder
/auto/

# cask packages
.cask/
dist/

# Flycheck
flycheck_*.el

# server auth directory
/server/

# projectiles files
.projectile

# directory configuration
.dir-locals.el

# network security
/network-security.data


### Node template
# Logs
logs
*.log
npm-debug.log*
yarn-debug.log*
yarn-error.log*
lerna-debug.log*

# Diagnostic reports (https://nodejs.org/api/report.html)
report.[0-9]*.[0-9]*.[0-9]*.[0-9]*.json

# Runtime data
pids
*.pid
*.seed
*.pid.lock

# Coverage directory used by tools like istanbul
coverage
*.lcov

# Compiled binary addons (https://nodejs.org/api/addons.html)
build/Release

# Dependency directories
node_modules/
jspm_packages/

# TypeScript cache
*.tsbuildinfo

# Optional npm cache directory
.npm

# Optional eslint cache
.eslintcache

# Optional REPL history
.node_repl_history

# Output of 'npm pack'
*.tgz

# Yarn Integrity file
.yarn-integrity

# dotenv environment variables file
.env
.env.test
.env_dev
*.env

# Stores VSCode versions used for testing VSCode extensions
.vscode-test

# yarn v2
.yarn/cache
.yarn/unplugged
.yarn/build-state.yml
.yarn/install-state.gz
.pnp.*

### macOS template
# General
.DS_Store
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon

# Thumbnails
._*

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk

### Windows template
# Windows thumbnail cache files
Thumbs.db
Thumbs.db:encryptable
ehthumbs.db
ehthumbs_vista.db

# Dump file
*.stackdump

# Folder config file
[Dd]esktop.ini

# Recycle Bin used on file shares
$RECYCLE.BIN/

# Windows Installer files
*.cab
*.msi
*.msix
*.msm
*.msp

# Windows shortcuts
*.lnk

### JetBrains template
# Covers JetBrains IDEs: IntelliJ, RubyMine, PhpStorm, AppCode, PyCharm, CLion, Android Studio, WebStorm and Rider
# Reference: https://intellij-support.jetbrains.com/hc/en-us/articles/206544839

.idea
*.iml

# File-based project format
*.iws

# IntelliJ
out/

# mpeltonen/sbt-idea plugin
.idea_modules/

# JIRA plugin
atlassian-ide-plugin.xml

### NextJS

# dependencies
/frontend/node_modules
/frontend/.pnp
.pnp.js

# testing
/coverage

# next.js
/frontend/.next/
/frontend/out/

# production
/frontend/build

# misc
*.pem
*.ico

# local env files
.env.local
.env.development.local
.env.test.local
.env.production.local

# vercel
.vercel

### Custom

CHANGELOG.md

# ts-coverage
coverage-ts

# schema generation
schema.yaml

# ignore files
.dockerignore
.eslintignore

# sample files
.env.sample

# shell scripts
*.sh

# local folders
/.git/
/venv/

# no need to format a json example
keycloak-client-example.json