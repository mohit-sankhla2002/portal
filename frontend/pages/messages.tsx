import React, { ReactElement } from 'react';
import {
  MessageTable,
  MessageTableRefresh,
} from '../src/components/message/MessageTable';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { appName } from './_app';

export const MessagePage = (): ReactElement => {
  return (
    <PageBase
      appName={appName}
      title={'Messages'}
      content={MessageTable}
      toolbar={MessageTableRefresh}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
      I18nNamespace.MESSAGE,
    ])),
  },
});

export default MessagePage;
