import React, { ReactElement } from 'react';
import { DataTransferOverviewTable } from '../../src/components/dataTransfer/DataTransferOverviewTable';
import { I18nNamespace } from '../../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { appName } from '../_app';

export const DataTransferPage = (): ReactElement => {
  return (
    <PageBase
      appName={appName}
      title={'Data Transfers'}
      content={DataTransferOverviewTable}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.DATA_TRANSFER_APPROVALS,
      I18nNamespace.LIST,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default DataTransferPage;
