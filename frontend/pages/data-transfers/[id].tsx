import React, { ReactElement } from 'react';
import { useRouter } from 'next/router';
import {
  DataTransferOverviewTable,
  DataTransferOverviewTableProps,
} from '../../src/components/dataTransfer/DataTransferOverviewTable';
import { PageBase } from '@biomedit/next-widgets';
import { appName } from '../_app';

export { getStaticProps } from './index';

export const DataTransferPageWithSelection = (): ReactElement => {
  const router = useRouter();
  const props: DataTransferOverviewTableProps = {};
  const selected = router.query.id;

  if (typeof selected === 'string') props.selected = selected;

  return (
    <PageBase
      appName={appName}
      title={'Data Transfers'}
      content={DataTransferOverviewTable}
      props={props}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default DataTransferPageWithSelection;
