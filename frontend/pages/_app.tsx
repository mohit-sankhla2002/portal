import type { AppProps } from 'next/app';
import React, { ReactElement, useEffect, useMemo, useState } from 'react';
import { batch, Provider, useDispatch, useSelector } from 'react-redux';
import { CHECK_AUTH, LOAD_MESSAGES } from '../src/actions/actionTypes';
import { UserProfileDialog } from '../src/components/UserProfileDialog';
import { contactEmail, version } from '../src/config';
import { RootState, wrapper } from '../src/store';
import { appWithTranslation, Trans } from 'next-i18next';
import { useRouter } from 'next/router';
import nextI18NextConfig from '../next-i18next.config.js';
import {
  BaseReducerState,
  Container,
  EmailLink,
  ErrorBoundary,
  Footer,
  Header,
  isBetweenIgnoreYear,
  isClient,
  logger,
  LogListener,
  Menu,
  Nav,
  NavItem,
  offLog,
  onLog,
  requestAction,
  SkipLink,
  ThemeProvider,
  ToastBar,
} from '@biomedit/next-widgets';
import { winstonLogger } from '../src/logger';
import { StructureItem, useStructureItems } from '../src/structure';
import { supportedBrowsers } from '../src/supportedBrowsers';
import isbot from 'isbot';
import { I18nNamespace } from '../src/i18n';
import { Box } from '@mui/material';
import Head from 'next/head';
import { AuthState } from '../src/reducers/auth';
import { Message, MessageStatusEnum } from '../src/api/generated';
import { IdRequired } from '../src/global';
import { any, PagePermissions } from '../src/permissions';

export const unsupportedBrowserLogMessage = 'Browser is not supported!';
export const appName = 'BioMedIT Portal';

type MenuItem = Partial<Menu> & Pick<StructureItem, 'title' | 'permission'>;

export const useMenuItems = (
  pagePermissions: PagePermissions,
  hasUnreadMessages = false,
  isChristmas = false,
) => {
  const structureItems = useStructureItems(hasUnreadMessages, isChristmas);
  const menuItems: MenuItem[] = structureItems
    .filter(({ permission }) => !permission || any(pagePermissions[permission]))
    .map(({ title, menu, permission }) => ({ title, permission, ...menu }));

  return menuItems;
};

export function App({ children }: { children? }): ReactElement {
  // See https://github.com/vercel/next.js/discussions/17443
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  const isSupportedBrowser: boolean | undefined = useMemo(() => {
    if (isClient()) {
      const userAgent = (window.navigator || navigator).userAgent;
      if (isbot(userAgent)) {
        // prevent bots from crawling contact email address
        return true;
      }
      return supportedBrowsers.test(userAgent);
    } else {
      return undefined;
    }
  }, []);

  const dispatch = useDispatch();

  const { isFetching, isAuthenticated, pagePermissions } = useSelector<
    RootState,
    AuthState
  >((state) => state.auth);
  const { itemList } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Message>>
  >((state) => state.messages);
  const isChristmas = useMemo(() => isBetweenIgnoreYear(12, 1, 12, 31), []);

  const hasUnreadMessages = useMemo(
    () =>
      !!itemList.filter((x: Message) => x.status === MessageStatusEnum.U)
        .length,
    [itemList],
  );

  const menuItems = useMenuItems(
    pagePermissions,
    hasUnreadMessages,
    isChristmas,
  );

  const { pathname } = useRouter();
  useEffect(() => {
    if (isSupportedBrowser && !isAuthenticated) {
      batch(() => {
        dispatch(requestAction(CHECK_AUTH));
        dispatch(requestAction(LOAD_MESSAGES));
      });
    }
  }, [dispatch, isAuthenticated, isSupportedBrowser]);

  useEffect(() => {
    const log = winstonLogger();
    const listener: LogListener = (data) => {
      log.log(data);
    };
    onLog(listener);
    return () => {
      offLog(listener);
    };
  }, []);

  const log = logger('App');

  if (mounted && !isSupportedBrowser) {
    log.warn({ message: unsupportedBrowserLogMessage });
    return (
      <Trans
        i18nKey={`${I18nNamespace.COMMON}:unsupportedBrowser`}
        values={{ contactEmail }}
        components={{
          email: <EmailLink email={contactEmail ?? ''} />,
          ulist: <ul />,
          litem: <li />,
          break: <br />,
        }}
      />
    );
  }

  const subjectPrefix = `${appName}: `;

  return (
    <ErrorBoundary
      subjectPrefix={subjectPrefix}
      contactEmail={contactEmail ?? ''}
    >
      <ThemeProvider>
        {mounted && <SkipLink />}
        <div>
          <ToastBar
            subjectPrefix={subjectPrefix}
            contactEmail={contactEmail ?? ''}
          />
          {isAuthenticated && !isFetching && (
            <Box display="flex">
              <UserProfileDialog />
              <Nav>
                <nav>
                  {menuItems.map((menuItem) => (
                    <NavItem
                      selected={menuItem.link == pathname}
                      href={menuItem.link ?? '#'}
                      key={'MenuItem-' + menuItem.title}
                      primary={menuItem.title}
                      icon={menuItem.icon}
                    />
                  ))}
                </nav>
                <Footer
                  logoSrc="/sib_logo.svg"
                  appName={appName}
                  logoText="SIB Logo"
                  logoWidth="52"
                  logoHeight="40"
                  logoHref="https://www.sib.swiss/"
                  version={version}
                />
              </Nav>
              <Container maxWidth={false}>
                <>
                  <header>
                    <Header
                      logoSrc="/biomedit_logo.svg"
                      logoWidth="313"
                      logoHeight="80"
                      logoText="BioMedIT Logo"
                      logoHref="https://www.biomedit.ch/"
                      priority={true}
                    />
                  </header>
                  <main id="main-content">{children}</main>
                </>
              </Container>
            </Box>
          )}
        </div>
      </ThemeProvider>
    </ErrorBoundary>
  );
}

const AppWrapper = ({ Component, ...rest }: AppProps): ReactElement => {
  const { store, props } = wrapper.useWrappedStore(rest);
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/favicon.ico" />
        <title>{appName}</title>
      </Head>
      <Provider store={store}>
        <App>
          <Component {...props.pageProps} />
        </App>
      </Provider>
    </>
  );
};

export default appWithTranslation(AppWrapper, nextI18NextConfig);
