import React, { ReactElement } from 'react';
import { Administration } from '../../src/components/admin/Administration';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../../src/i18n';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { appName } from '../_app';

export const AdministrationPage = (): ReactElement => {
  return (
    <PageBase
      appName={appName}
      title={'Administration'}
      content={Administration}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.ADMINISTRATION,
      I18nNamespace.BOOLEAN,
      I18nNamespace.COMMON,
      I18nNamespace.DATA_PROVIDER,
      I18nNamespace.FLAG_LIST,
      I18nNamespace.GROUP_LIST,
      I18nNamespace.GROUP_MANAGE_FORM,
      I18nNamespace.LIST,
      I18nNamespace.NODE,
      I18nNamespace.PGP_KEY_INFO_LIST,
      I18nNamespace.USER_INFO,
      I18nNamespace.USER_LIST,
      I18nNamespace.USER_MANAGE_FORM,
      I18nNamespace.USER_TEXT_FIELD,
    ])),
  },
});

export default AdministrationPage;
