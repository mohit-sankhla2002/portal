import React, { ReactElement } from 'react';
import {
  Administration,
  AdministrationProps,
} from '../../src/components/admin/Administration';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { getStaticProps as staticProps } from './index';
import { useRouter } from 'next/router';
import { appName } from '../_app';

export const AdministrationPageWithSelection = (): ReactElement => {
  const router = useRouter();
  const props: AdministrationProps = {};
  const path = router.query.path;

  if (Array.isArray(path)) {
    if (path.length > 1) {
      props.entityCode = path[1];
    }
    if (path.length > 0) {
      props.tabCode = path[0];
    }
  }

  return (
    <PageBase
      appName={appName}
      title={'Administration'}
      content={Administration}
      props={props}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = staticProps;

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default AdministrationPageWithSelection;
