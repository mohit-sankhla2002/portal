import React, { ReactElement } from 'react';
import { ContactUs } from '../src/components/ContactUs';
import { I18nNamespace } from '../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { appName } from './_app';

const ContactUsPage = (): ReactElement => {
  return (
    <PageBase appName={appName} title={'Contact Us'} content={ContactUs} />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.CONTACT_US,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default ContactUsPage;
