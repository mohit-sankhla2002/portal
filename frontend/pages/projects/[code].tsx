import React, { ReactElement } from 'react';
import { useRouter } from 'next/router';
import { PageBase } from '@biomedit/next-widgets';
import {
  ProjectList,
  ProjectListProps,
} from '../../src/components/projectList/ProjectList';
import { appName } from '../_app';

export { getStaticProps } from './index';

export const ProjectPageWithSelection = (): ReactElement => {
  const router = useRouter();
  const props: ProjectListProps = {};
  const selected = router.query.code;

  if (typeof selected === 'string') {
    props.selected = selected;
  }

  return (
    <PageBase
      appName={appName}
      title={'Projects'}
      content={ProjectList}
      props={props}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default ProjectPageWithSelection;
