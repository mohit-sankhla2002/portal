import React, { ReactElement } from 'react';
import { FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { I18nNamespace } from '../src/i18n';
import { Profile } from '../src/components/Profile';
import { appName } from './_app';

export const ProfilePage = (): ReactElement => {
  return <PageBase appName={appName} title={'Profile'} content={Profile} />;
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default ProfilePage;
