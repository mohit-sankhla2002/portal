/**
 * Clones an array as fast as possible, retaining references of the array's values.
 * @param a The array to clone. Must be defined.
 * @returns A copy of the array.
 */
export function arrayFastClone<T>(a: T[]): T[] {
  return a.slice(0, a.length);
}
