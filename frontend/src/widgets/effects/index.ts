/*
 Copyright 2020 Nurjin Jafar
 Copyright 2020 Nordeck IT + Consulting GmbH.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
/**
 * Source: https://github.com/matrix-org/matrix-react-sdk/blob/develop/src/effects/index.ts
 * Significant changes were made.
 */

import { ConfettiOptions } from './confetti';
import { FireworksOptions } from './fireworks';
import { HeartOptions } from './hearts';
import { RainfallOptions } from './rainfall';
import { SnowfallOptions } from './snowfall';
import { SpaceInvadersOptions } from './spaceinvaders';

export enum EffectCommand {
  CONFETTI = 'confetti',
  FIREWORKS = 'fireworks',
  HEARTS = 'hearts',
  RAINFALL = 'rainfall',
  SNOWFALL = 'snowfall',
  SPACEINVADERS = 'spaceinvaders',
}

/**
 * This configuration define effects that can be triggered
 */
export const effects: Record<EffectCommand, Record<string, unknown>> = {
  [EffectCommand.CONFETTI]: {
    maxCount: 150,
    speed: 3,
    frameInterval: 15,
    alpha: 1.0,
    gradient: false,
  } as ConfettiOptions,
  [EffectCommand.FIREWORKS]: {
    maxCount: 500,
    gravity: 0.05,
  } as FireworksOptions,
  [EffectCommand.SPACEINVADERS]: {
    maxCount: 50,
    gravity: 0.005,
  } as SpaceInvadersOptions,
  [EffectCommand.SNOWFALL]: {
    maxCount: 200,
    gravity: 0.05,
    maxDrift: 5,
  } as SnowfallOptions,
  [EffectCommand.HEARTS]: {} as HeartOptions,
  [EffectCommand.RAINFALL]: {} as RainfallOptions,
};
