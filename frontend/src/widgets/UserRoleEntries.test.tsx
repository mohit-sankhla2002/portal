import React, { ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import { Description } from '@biomedit/next-widgets';
import { I18nNamespace } from '../i18n';
import { useUserRoleEntries } from './UserRoleEntries';

const MockComponent = ({ roles }): ReactElement => {
  const entries = useUserRoleEntries(roles, I18nNamespace.COMMON);
  return <Description entries={entries} labelWidth={300} />;
};

const roles = {
  foo: [
    {
      username: 'brian',
      firstName: 'Brian',
      lastName: 'Cohen',
      email: 'brian@example.org',
      authorized: true,
    },
  ],
  bar: [
    {
      username: 'chuck',
      firstName: 'Chuck',
      lastName: 'Norris',
      email: 'chuck@example.org',
      authorized: false,
    },
  ],
  baz: [],
};

describe('UserRoleEntries', () => {
  it('should return an empty table if nothing is passed in', () => {
    render(<MockComponent roles={{}} />);
    expect(screen.queryAllByRole('row')).toHaveLength(0);
  });

  it('should return table with `n` rows when `n` entries are passed', async () => {
    render(<MockComponent roles={roles} />);
    expect(await screen.findAllByRole('row')).toHaveLength(2);
  });

  it('should render verified icon only for authorized users', async () => {
    render(<MockComponent roles={roles} />);

    expect(
      await screen.findAllByLabelText('common:authorizedTooltip'),
    ).toHaveLength(1);
  });
});
