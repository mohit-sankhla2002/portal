import React, { ReactElement, useMemo } from 'react';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import VerifiedIcon from '@mui/icons-material/Verified';
import { Box, Typography } from '@mui/material';
import { EmailLink, FormattedItemField, Tooltip } from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { getFullName } from '../utils';
import { GroupUsersInner, ProjectUsersInner } from '../api/generated';

export function addMultiValueField(
  fieldName: string,
  fieldValues: Array<string | ReactElement> | undefined,
  infoBox: FormattedItemField[],
) {
  if (!!fieldValues && fieldValues.length) {
    infoBox.push({
      caption: fieldName,
      component: (
        <div>
          {fieldValues.map((field) => {
            const key = typeof field === 'string' ? field : field.key;
            return (
              <Typography key={key} variant={'body1'}>
                {field}
              </Typography>
            );
          })}
        </div>
      ),
    });
  }
}

export type UserType =
  | (GroupUsersInner & {
      authorized?: boolean;
    })
  | ProjectUsersInner;
export type RoleMap = { [name: string]: UserType[] };

export function useUserRoleEntries<T extends RoleMap>(
  roles: T,
  translation: I18nNamespace,
): FormattedItemField[] {
  const { t } = useTranslation([translation]);

  return useMemo((): FormattedItemField[] => {
    const fields: FormattedItemField[] = [];
    const userNameAndEmailLink = (role: string, u: UserType) => (
      // `email` is usually specified
      <Box component="span" key={role + '-' + u.username}>
        {u?.firstName || u?.lastName ? getFullName(u) : u.username}
        <EmailLink email={u.email ?? ''}>
          <MailOutlineIcon sx={{ marginLeft: 1, verticalAlign: 'bottom' }} />
        </EmailLink>
        {u.authorized && (
          <Tooltip title={t(`${translation}:authorizedTooltip`)}>
            <VerifiedIcon
              color={'success'}
              sx={{ verticalAlign: 'bottom' }}
              aria-label={t(`${translation}:authorizedTooltip`)}
            />
          </Tooltip>
        )}
      </Box>
    );
    for (const role in roles) {
      const users = roles[role];
      if (role && users && users.length > 0) {
        addMultiValueField(
          t(`${translation}:role.${role}`),
          users.map((user) => userNameAndEmailLink(role, user)),
          fields,
        );
      }
    }
    return fields;
  }, [t, translation, roles]);
}
