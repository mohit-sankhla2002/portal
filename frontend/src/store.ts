import {
  applyMiddleware,
  legacy_createStore,
  Store,
  StoreEnhancer,
} from 'redux';
import { composeWithDevToolsLogOnlyInProduction } from '@redux-devtools/extension';
import createSagaMiddleware from 'redux-saga';
import { reducers } from './reducers';
import { rootSaga } from './actions/sagas';
import { Context, createWrapper } from 'next-redux-wrapper';

export type RootState = ReturnType<typeof reducers>;

export function makeStore(
  context?: Context,
  preloadedState?: RootState,
): Store {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [sagaMiddleware];

  const enhancers: StoreEnhancer = composeWithDevToolsLogOnlyInProduction(
    applyMiddleware(...middlewares),
  );

  const store = preloadedState
    ? legacy_createStore(reducers, preloadedState, enhancers)
    : legacy_createStore(reducers, enhancers);

  sagaMiddleware.run(rootSaga);

  return store;
}

export function getInitialState(): RootState {
  // @ts-expect-error an action which isn't defined in any reducer was chosen on purpose here, to make sure no reducers get triggered
  return reducers(undefined, { type: 'NOOP' });
}

export const wrapper = createWrapper<Store<RootState>>(makeStore);
