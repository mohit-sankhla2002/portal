import { emailChoices, isUserInfo } from './utils';

describe('isUserInfo', () => {
  it.each`
    userOrInfo             | expected
    ${undefined}           | ${false}
    ${{}}                  | ${false}
    ${{ manages: {} }}     | ${true}
    ${{ permissions: {} }} | ${true}
    ${{ ipAddress: {} }}   | ${true}
  `(
    'Should return $expected if userOrInfo is $userOrInfo',
    ({ userOrInfo, expected }) => {
      expect(isUserInfo(userOrInfo)).toBe(expected);
    },
  );
});

describe('emailChoices', () => {
  it.each`
    userInfo                              | expected
    ${{}}                                 | ${[]}
    ${{ email: 'chuck.norris@email.com' }} | ${[
  {
    label: 'chuck.norris@email.com',
    value: 'chuck.norris@email.com',
  },
]}
    ${{
  email: 'chuck.norris@email.com',
  profile: { emails: 'chuck.norris@email.ch,chuck.norris@gmail.com' },
}} | ${[
  { label: 'chuck.norris@email.ch', value: 'chuck.norris@email.ch' },
  {
    label: 'chuck.norris@gmail.com',
    value: 'chuck.norris@gmail.com',
  },
]}
    ${{ profile: { emails: 'a@a.com,b@b.com,a@a.com' } }} | ${[
  {
    label: 'a@a.com',
    value: 'a@a.com',
  },
  { label: 'b@b.com', value: 'b@b.com' },
]}
    ${{ profile: { emails: undefined } }} | ${[]}
    ${{ email: undefined }}               | ${[]}
  `(
    `Should return '$expected' if userInfo is '$userInfo'`,
    ({ userInfo, expected }) => {
      expect(emailChoices(userInfo)).toStrictEqual(expected);
    },
  );
});
