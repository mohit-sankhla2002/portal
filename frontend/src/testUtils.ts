import path from 'path';
import fs from 'fs';

/**
 * Returns the contents of of the file {@code filename} in the folder `test-data` as a string.
 * @param filename of the file of which to return the content as string
 */
export function getTestFileAsString(filename: string): string {
  let p = filename;
  try {
    const srcPath = path.resolve(__dirname);
    p = `${srcPath}/../test-data/${filename}`;
    return fs.readFileSync(p, 'utf8');
  } catch (e) {
    throw new Error(`Test file with filename: ${p} not found!`);
  }
}
