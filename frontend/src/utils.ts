import { ProjectUsersInner, User, Userinfo } from './api/generated';
import { uniq } from 'lodash';

type UserProfileType = User | Userinfo | null;
type UserType = UserProfileType | ProjectUsersInner;
type EmailChoice = { label: string; value: string };

export const getFullName = (user: UserType) => {
  const firstName = user?.firstName;
  const lastName = user?.lastName;
  return (
    (firstName || lastName) &&
    (firstName ? firstName + ' ' : '') + (lastName ? lastName : '')
  );
};

export function getAffiliation(user: UserProfileType): string | undefined {
  return (user?.profile?.affiliation || '-').replace(',', ', ');
}

export function isUserInfo(
  userOrInfo: User | Userinfo,
): userOrInfo is Userinfo {
  return (
    !!userOrInfo &&
    ('manages' in userOrInfo ||
      'permissions' in userOrInfo ||
      'ipAddress' in userOrInfo)
  );
}

export function emailChoices(userInfo: UserProfileType): EmailChoice[] {
  if (!userInfo) {
    return [];
  }
  const emails =
    userInfo?.profile?.emails?.split(',') ||
    (userInfo.email && [userInfo.email]) ||
    [];
  return uniq(emails).map((email) => {
    return {
      label: email,
      value: email,
    };
  });
}
