/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UniqueUser
 */
export interface UniqueUser {
    /**
     * 
     * @type {number}
     * @memberof UniqueUser
     */
    readonly id?: number;
    /**
     * 
     * @type {string}
     * @memberof UniqueUser
     */
    username?: string;
    /**
     * 
     * @type {string}
     * @memberof UniqueUser
     */
    email?: string | null;
}

/**
 * Check if a given object implements the UniqueUser interface.
 */
export function instanceOfUniqueUser(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function UniqueUserFromJSON(json: any): UniqueUser {
    return UniqueUserFromJSONTyped(json, false);
}

export function UniqueUserFromJSONTyped(json: any, ignoreDiscriminator: boolean): UniqueUser {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'username': !exists(json, 'username') ? undefined : json['username'],
        'email': !exists(json, 'email') ? undefined : json['email'],
    };
}

export function UniqueUserToJSON(value?: UniqueUser | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'username': value.username,
        'email': value.email,
    };
}

