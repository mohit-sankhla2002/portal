/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Feed
 */
export interface Feed {
    /**
     * 
     * @type {Date}
     * @memberof Feed
     */
    readonly created?: Date;
    /**
     * 
     * @type {string}
     * @memberof Feed
     */
    label: FeedLabelEnum;
    /**
     * 
     * @type {string}
     * @memberof Feed
     */
    title?: string;
    /**
     * 
     * @type {string}
     * @memberof Feed
     */
    message?: string;
    /**
     * 
     * @type {Date}
     * @memberof Feed
     */
    fromDate?: Date;
}


/**
 * @export
 */
export const FeedLabelEnum = {
    INFO: 'INFO',
    WARN: 'WARN'
} as const;
export type FeedLabelEnum = typeof FeedLabelEnum[keyof typeof FeedLabelEnum];


/**
 * Check if a given object implements the Feed interface.
 */
export function instanceOfFeed(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "label" in value;

    return isInstance;
}

export function FeedFromJSON(json: any): Feed {
    return FeedFromJSONTyped(json, false);
}

export function FeedFromJSONTyped(json: any, ignoreDiscriminator: boolean): Feed {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'created': !exists(json, 'created') ? undefined : (new Date(json['created'])),
        'label': json['label'],
        'title': !exists(json, 'title') ? undefined : json['title'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'fromDate': !exists(json, 'from_date') ? undefined : (new Date(json['from_date'])),
    };
}

export function FeedToJSON(value?: Feed | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'label': value.label,
        'title': value.title,
        'message': value.message,
        'from_date': value.fromDate === undefined ? undefined : (value.fromDate.toISOString()),
    };
}

