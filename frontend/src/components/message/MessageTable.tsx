import React, { ReactElement, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DELETE_MESSAGE, LOAD_MESSAGES } from '../../actions/actionTypes';
import { Message, MessageStatusEnum } from '../../api/generated';
import { IdRequired } from '../../global';
import {
  BaseReducerState,
  Button,
  ColoredStatus,
  EnhancedTable,
  formatDate,
  requestAction,
  Status,
  useDialogState,
} from '@biomedit/next-widgets';
import { MessageDetail } from './MessageDetail';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import AutorenewIcon from '@mui/icons-material/Autorenew';
import truncate from 'lodash/truncate';
import { Typography } from '@mui/material';
import { RootState } from '../../store';
import { createColumnHelper } from '@tanstack/react-table';

function fmtMsgByStatus(
  text: string | null | undefined,
  status: MessageStatusEnum,
) {
  if (!text) {
    return;
  }
  if (status === MessageStatusEnum.R) {
    return text;
  }
  return (
    <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
      {text}
    </Typography>
  );
}

export const MessageTableRefresh = (): ReactElement => {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.MESSAGE]);
  const dispatch = useDispatch();
  return (
    <Button
      onClick={() => {
        dispatch(requestAction(LOAD_MESSAGES));
      }}
      variant="outlined"
      color="primary"
      startIcon={<AutorenewIcon />}
      aria-label={
        t(`${I18nNamespace.MESSAGE}:actionButtons.refresh`) +
        ' ' +
        t(`${I18nNamespace.COMMON}:models.message_plural`)
      }
    >
      {t(`${I18nNamespace.MESSAGE}:actionButtons.refresh`)}
    </Button>
  );
};
export const MessageTable = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.MESSAGE);
  const dispatch = useDispatch();

  const { itemList, isFetching, isSubmitting } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Message>>
  >((state) => state.messages);

  const loadItems = useCallback(() => {
    dispatch(requestAction(LOAD_MESSAGES));
  }, [dispatch]);

  const columns = useMemo(() => {
    const columnHelper = createColumnHelper<Required<Message>>();
    const statuses: Status<MessageStatusEnum>[] = [
      {
        text: t('status.read') as string,
        value: MessageStatusEnum.R,
      },
      {
        color: 'info',
        text: t('status.unread') as string,
        value: MessageStatusEnum.U,
      },
    ];
    return [
      columnHelper.accessor('title', {
        header: t('columns.title'),
        sortingFn: 'alphanumeric',
        cell: (props) =>
          fmtMsgByStatus(props.getValue(), props.row.original.status),
      }),
      columnHelper.accessor('body', {
        header: t('columns.body'),
        sortingFn: 'alphanumeric',
        cell: (props) =>
          fmtMsgByStatus(
            truncate(props.getValue(), { length: 100, separator: ' ' }),
            props.row.original.status,
          ),
      }),
      columnHelper.accessor('created', {
        id: 'date',
        header: t('columns.created'),
        sortingFn: 'datetime',
        cell: (props) =>
          fmtMsgByStatus(
            formatDate(props.getValue(), true),
            props.row.original.status,
          ),
      }),
      columnHelper.accessor('status', {
        header: t('columns.status'),
        cell: (props) =>
          ColoredStatus<MessageStatusEnum>({
            value: props.getValue(),
            statuses,
          }),
      }),
    ];
  }, [t]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_MESSAGE, { id }));
    },
    [dispatch],
  );

  const { item, setItem, onClose, open } = useDialogState<Message>();

  return (
    <>
      <MessageDetail message={item} onClose={onClose} open={open} />
      <EnhancedTable<Required<Message>>
        emptyMessage={t('emptyMessage') as string}
        columns={columns}
        itemList={itemList}
        loadItems={loadItems}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        onRowClick={setItem}
        canEdit={true}
        onEdit={setItem}
        canDelete={true}
        onDelete={deleteItem}
      />
    </>
  );
};
