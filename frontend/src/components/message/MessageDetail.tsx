import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import {
  ButtonBox,
  CancelLabel,
  DeleteDialog,
  Dialog,
  FabButton,
  Markdown,
  requestAction,
} from '@biomedit/next-widgets';
import { Message, MessageStatusEnum } from '../../api/generated';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  CALL,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
} from '../../actions/actionTypes';
import { RootState } from '../../store';

export type MessageDetailProps = {
  message: Message | null;
  open: boolean;
  onClose: () => void;
};

export const MessageDetail = ({
  message,
  open,
  onClose,
}: MessageDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.MESSAGE]);
  const dispatch = useDispatch();
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.messages.isSubmitting,
  );
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const isMessageRead = useCallback(
    () => message?.status === MessageStatusEnum.R,
    [message],
  );

  const [messageRead, setMessageRead] = useState<boolean>(isMessageRead());

  const changeMessageStatus = useCallback(
    (messageId: number | undefined, status: MessageStatusEnum) => {
      if (messageId) {
        dispatch(
          requestAction(
            UPDATE_MESSAGE,
            {
              id: String(messageId),
              message: { status },
            },
            {
              type: CALL,
              callback: () => {
                setMessageRead(status === MessageStatusEnum.R);
              },
            },
          ),
        );
      }
    },
    [dispatch],
  );

  useEffect(() => {
    if (!isMessageRead()) {
      changeMessageStatus(message?.id, MessageStatusEnum.R);
    } else {
      setMessageRead(isMessageRead());
    }
  }, [message, isMessageRead, changeMessageStatus]);

  const onDeleteDialogClose = () => {
    setOpenDeleteDialog(false);
    onClose();
  };

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={message?.title}
      text={<Markdown text={message?.body || ''} openLinksInNewTab />}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <DeleteDialog
        key={'delete-dialog-' + message?.id}
        title={t(`${I18nNamespace.MESSAGE}:deleteDialog.title`)}
        text={t(`${I18nNamespace.MESSAGE}:deleteDialog.text`)}
        open={openDeleteDialog}
        itemName={t(`${I18nNamespace.COMMON}:models.message_plural`) as string}
        isSubmitting={isSubmitting}
        onDelete={(isConfirmed) => {
          if (isConfirmed) {
            dispatch(
              requestAction(
                DELETE_MESSAGE,
                {
                  id: String(message?.id),
                },
                {
                  type: CALL,
                  callback: onDeleteDialogClose,
                },
              ),
            );
          } else {
            setOpenDeleteDialog(false);
          }
        }}
      />
      <ButtonBox sx={{ mt: 4 }}>
        {!messageRead && (
          <FabButton
            key={'mark-read-button-' + message?.id}
            aria-label={
              t(`${I18nNamespace.MESSAGE}:actionButtons.markRead`) as string
            }
            title={
              t(`${I18nNamespace.MESSAGE}:actionButtons.markRead`) as string
            }
            size="small"
            icon="markEmailRead"
            onClick={() =>
              changeMessageStatus(message?.id, MessageStatusEnum.R)
            }
          />
        )}
        {messageRead && (
          <FabButton
            key={'mark-unread-button-' + message?.id}
            aria-label={
              t(`${I18nNamespace.MESSAGE}:actionButtons.markUnread`) as string
            }
            title={
              t(`${I18nNamespace.MESSAGE}:actionButtons.markUnread`) as string
            }
            size="small"
            icon="markEmailUnread"
            onClick={() =>
              changeMessageStatus(message?.id, MessageStatusEnum.U)
            }
          />
        )}
        <FabButton
          key={'delete-button-' + message?.id}
          aria-label={
            t(`${I18nNamespace.MESSAGE}:actionButtons.delete`) as string
          }
          title={t(`${I18nNamespace.MESSAGE}:actionButtons.delete`) as string}
          size="small"
          icon="del"
          onClick={() => setOpenDeleteDialog(true)}
        />
      </ButtonBox>
    </Dialog>
  );
};
