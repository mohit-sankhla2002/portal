import { DefaultRootState, useSelector } from 'react-redux';
import { createSelector } from 'reselect';

export const isStaff = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.staff;

export const isNodeAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeAdmin;

export const isNodeViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeViewer;

export const isDataProviderAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderAdmin;

export const isDataProviderViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderViewer;

export const isGroupManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.groupManager;

export const isDataManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataManager;

export const canAddDataTransfer = createSelector(
  isStaff,
  isDataManager,
  (staff, dataManager) => staff || dataManager,
);

export const canSeeUsersTab = createSelector(
  isStaff,
  isNodeAdmin,
  (staff, nodeAdmin) => staff || nodeAdmin,
);

export const canSeeNodeTab = createSelector(
  isStaff,
  isNodeAdmin,
  isNodeViewer,
  (staff, nodeAdmin, nodeViewer) => staff || nodeAdmin || nodeViewer,
);

export const canSeeDataProviderTab = createSelector(
  isStaff,
  isDataProviderAdmin,
  isDataProviderViewer,
  isNodeAdmin,
  isNodeViewer,
  (staff, dataProviderAdmin, dataProviderViewer, nodeAdmin, nodeViewer) =>
    staff || dataProviderAdmin || dataProviderViewer || nodeAdmin || nodeViewer,
);

export const canSeeGroupTab = createSelector(
  isStaff,
  isGroupManager,
  (staff, groupManager) => staff || groupManager,
);

export const canAddProjects = (state: DefaultRootState): boolean =>
  state.auth.pagePermissions.project.add;

export function useParamSelector(selector, ...params) {
  return useSelector((state) => selector(state, ...params));
}
