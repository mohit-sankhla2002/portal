import { CustomAffiliation, Userinfo } from '../api/generated';
import {
  affiliationFieldCaption,
  displayNameFieldCaption,
  usernameFieldCaption,
  emailFieldCaption,
  ipAddressFieldCaption,
  localUsernameFieldCaption,
  Profile,
  useUserInfoFields,
} from './Profile';
import { render, renderHook, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore, RootState } from '../store';
import React from 'react';
import { initialState } from '../reducers/auth';
import {
  DeepWriteable,
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { backend } from '../api/api';
import { rest } from 'msw';

describe('Profile', () => {
  const ip = '127.0.0.1';

  const getUser = (): Userinfo => {
    return {
      username: '63457666153@eduid.ch',
      email: 'chuck.norris@roundhouse.kick',
      firstName: 'Chuck',
      lastName: 'Norris',
      profile: {
        affiliation: '',
        localUsername: 'cnorris',
        displayName: 'Chuck Norris (chuck.norris@roundhouse.kick)',
        displayId: 'ID: 63457666153',
        uid: 1000000,
        gid: 1000000,
        namespace: 'ch',
        displayLocalUsername: 'ch_cnorris',
      },
      flags: [],
      id: 2,
      ipAddress: ip,
      permissions: {
        manager: true,
        staff: true,
        dataManager: true,
        groupManager: true,
        nodeAdmin: false,
        nodeViewer: false,
        dataProviderAdmin: false,
        dataProviderViewer: false,
        hasProjects: true,
      },
    };
  };

  let user: Userinfo;
  const customAffiliations: CustomAffiliation[] = [
    {
      id: 1,
      code: 'ABC',
      name: 'Custom Affiliation 1',
    },
    {
      id: 2,
      code: 'XYZ',
      name: 'Custom Affiliation 2',
    },
  ];

  beforeEach(() => {
    user = getUser();
  });

  describe('useUserInfoFields', () => {
    const basicUserInfoFieldAmount = 6;

    it('should contain field captions in the right order', () => {
      const { result } = renderHook(() =>
        useUserInfoFields(user, customAffiliations),
      );
      expect(result.current.map((field) => field.caption)).toStrictEqual([
        displayNameFieldCaption,
        usernameFieldCaption,
        localUsernameFieldCaption,
        emailFieldCaption,
        affiliationFieldCaption,
        ipAddressFieldCaption,
      ]);
    });

    it('should contain the correct field values', () => {
      const { result } = renderHook(() =>
        useUserInfoFields(user, customAffiliations),
      );
      expect(result.current.map((field) => field.component)).toStrictEqual([
        'Chuck Norris',
        '63457666153@eduid.ch',
        'cnorris',
        'chuck.norris@roundhouse.kick',
        '-',
        '127.0.0.1',
      ]);
    });

    it('should return only basic user info fields if "manages" is "undefined"', () => {
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an empty object', () => {
      user.manages = {};
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an object with only empty arrays', () => {
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should also return one flag field if user has flags', () => {
      const { result } = renderHook(() =>
        useUserInfoFields({
          ...user,
          flags: ['funwithflags', 'anotheflag'],
        }),
      );
      expect(result.current).toHaveLength(basicUserInfoFieldAmount + 1);
    });
  });

  describe('Component', () => {
    const server: MockServer = setupMockApi();
    const listCustomAffiliations = `${backend}custom-affiliations/`;
    let verifier;

    let state: DeepWriteable<RootState>;

    const setupServer = (): void => {
      server.use(
        rest.get(listCustomAffiliations, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
      );
    };

    beforeEach(() => {
      setupServer();
      verifier = new RequestVerifier(server);
      state = {
        ...getInitialState(),
        auth: {
          ...initialState,
          user: getUser(),
        },
      };
    });

    afterEach(() => {
      verifier.assertCalled(listCustomAffiliations, 1);
    });

    it('should display caption and component of fields', async () => {
      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <Profile />
        </Provider>,
      );

      await screen.findByText(ipAddressFieldCaption);
      await screen.findByText(ip);
    });

    it('should not display fields that are undefined', async () => {
      delete state.auth.user?.ipAddress;

      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <Profile />
        </Provider>,
      );

      expect(screen.queryByText(ipAddressFieldCaption)).not.toBeInTheDocument();
      expect(screen.queryByText(ip)).not.toBeInTheDocument();
    });
  });
});
