import {
  CustomAffiliation,
  DataProvider,
  Flag,
  Group,
  Node,
  OAuth2Client,
  Permission,
  User,
} from '../api/generated';
import { DeepRequired } from 'utility-types';
import { Choice, useChoices } from '@biomedit/next-widgets';

export const displayName = (user: User): string => {
  if (user.profile && user.profile.displayName) {
    return user.profile.displayName;
  }
  const name =
    user.firstName || user.lastName
      ? `${user.firstName || ''} ${user.lastName || ''}`.trim()
      : user.username;
  const email = user.email && `(${user.email})`;
  return `${name || ''} ${email || ''}`.trim();
};

export const useUserChoices = (users: DeepRequired<User>[]): Choice[] =>
  useChoices(users, 'id', displayName);

export const useNodeChoices = (nodes: Node[]): Choice[] =>
  useChoices<Node>(nodes, 'code', 'name');

export const useOAuth2ClientChoices = (
  oAuth2Clients: OAuth2Client[],
): Choice[] => {
  const label = (oAuth2Client: OAuth2Client): string => {
    return `${oAuth2Client.clientName} (${oAuth2Client.redirectUri})`;
  };
  return useChoices<OAuth2Client>(oAuth2Clients, 'id', label);
};

export const useFlagChoices = (flags: Flag[]): Choice[] =>
  useChoices<Flag>(flags, 'code', 'code');

export const useGroupChoices = (groups: Group[]): Choice[] =>
  useChoices<Group>(groups, 'name', 'name');

export const useCustomAffiliationChoices = (
  affiliations: CustomAffiliation[],
): Choice[] => useChoices<CustomAffiliation>(affiliations, 'id', 'code');

export const useDataProviderChoices = (
  dataProviders: DataProvider[],
): Choice[] => {
  const label = (dataProvider: DataProvider): string => {
    return `${dataProvider.code} (${dataProvider.name})`;
  };
  return useChoices<DataProvider>(dataProviders, 'code', label);
};

export const usePermissionChoices = (permissions: Permission[]): Choice[] =>
  useChoices<Permission>(permissions, 'id', 'name');
