import React, { ReactElement, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  CheckboxField,
  FormDialog,
  LabelledField,
  requestAction,
  SelectField,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { UPDATE_USER_PROFILE } from '../actions/actionTypes';
import { FormProvider } from 'react-hook-form';
import Alert from '@mui/material/Alert';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';
import { styled } from '@mui/material/styles';
import { RootState } from '../store';
import { AuthState } from '../reducers/auth';
import { FormFieldsContainer } from './FormFieldsContainer';
import { affiliationConsentRequired } from '../config';
import { emailChoices } from '../utils';

type FormData = {
  localUsername: string;
  affiliationConsent: boolean;
  email: string;
};

const StyledCheckboxField = styled(CheckboxField)(({ theme }) => ({
  marginTop: theme.spacing(1),
}));

export const dialogTitle = 'userProfileDialog.title';
export const usernameLabel = 'userProfileDialog.usernameLabel';
export const affiliationConsentLabel =
  'userProfileDialog.affiliationConsentLabel';
export const emailLabel = 'userProfileDialog.emailLabel';

export const UserProfileDialog = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const dispatch = useDispatch();

  const {
    user: userInfo,
    isSubmitting,
    isExistingUsername,
    invalidMsg,
  } = useSelector<RootState, AuthState>((state) => state.auth);

  const form = useEnhancedForm<FormData>();

  const [invalidUsername, setInvalidUsername] = useState<string | undefined>();

  const usernameMissing = useMemo(
    () => !userInfo?.profile?.localUsername,
    [userInfo],
  );
  const affiliationConsentMissing = useMemo(
    () => affiliationConsentRequired && !userInfo?.profile?.affiliationConsent,
    [userInfo],
  );
  const emailMissing = useMemo(() => !userInfo?.email, [userInfo]);

  const usernameField = 'localUsername';
  const affiliationConsentField = 'affiliationConsent';
  const emailField = 'email';

  const watchUsername = form.watch(usernameField);

  const submit = ({ localUsername, affiliationConsent, email }: FormData) => {
    setInvalidUsername(undefined);
    if (localUsername || affiliationConsent || email) {
      dispatch(
        requestAction(UPDATE_USER_PROFILE, {
          user: {
            profile: {
              localUsername,
              affiliationConsent,
            },
            email,
          },
          id: String(userInfo?.id),
        }),
      );
    }
  };

  const fieldError = useMemo(() => {
    // `invalidUsername` NOT set (is reset in submit call)
    if (!invalidUsername && (isExistingUsername || !!invalidMsg)) {
      setInvalidUsername(watchUsername);
    }
    // Input has changed and does NOT equal `invalidUsername`
    if (invalidUsername && watchUsername !== invalidUsername) {
      return;
    }
    if (isExistingUsername) {
      return {
        type: 'validate',
        message: t('userProfileDialog.validationUsernameAlreadyExists'),
      };
    }
    if (invalidMsg) {
      return {
        type: 'validate',
        message: invalidMsg,
      };
    }
  }, [t, invalidMsg, isExistingUsername, watchUsername, invalidUsername]);

  const choices = useMemo(() => emailChoices(userInfo), [userInfo]);

  return (
    <FormProvider {...form}>
      <FormDialog<FormData>
        title={t(dialogTitle)}
        open={usernameMissing || affiliationConsentMissing || emailMissing}
        onSubmit={submit}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          {usernameMissing && (
            <>
              <Alert
                severity="warning"
                sx={{ marginBottom: 2, marginRight: 0 }}
              >
                {t('userProfileDialog.warning')}
              </Alert>
              <LabelledField
                type="text"
                name={usernameField}
                label={t(usernameLabel)}
                fieldError={fieldError}
                required={usernameMissing}
                autofocus
                fullWidth
              />
            </>
          )}
          {emailMissing && !!userInfo?.profile?.emails && (
            <SelectField
              name={emailField}
              label={t(emailLabel)}
              choices={choices}
              populateIfSingleChoice
              required={emailMissing && !!userInfo?.profile?.emails}
              helperText={t('userProfileDialog.emailHelperText')}
              fullWidth
            />
          )}
          {affiliationConsentMissing && (
            <StyledCheckboxField
              name={affiliationConsentField}
              label={t(affiliationConsentLabel)}
              required={affiliationConsentMissing}
            />
          )}
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
