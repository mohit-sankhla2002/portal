import { styled } from '@mui/material/styles';
import {
  ipAddress,
  LabelledArrayField,
  maxValue,
  minValue,
  required,
  MultilineField,
} from '@biomedit/next-widgets';
import React, { ReactElement } from 'react';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';

export const arrayName = 'ipAddressRanges';

const StyledLabelledArrayField = styled(LabelledArrayField)(({ theme }) => ({
  width: '30%',
  marginRight: theme.spacing(1),
}));

export const IpRangeList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.IP_RANGE_LIST);

  const renderChild = (field: Record<string, string>, index: number) => (
    <>
      <StyledLabelledArrayField
        field={field}
        index={index}
        type="text"
        label={t('captions.ipAddress')}
        validations={[required, ipAddress]}
        arrayName={arrayName}
        fieldName="ipAddress"
      />
      <StyledLabelledArrayField
        field={field}
        index={index}
        type="number"
        label={t('captions.mask')}
        validations={[required, minValue(0), maxValue(32)]}
        arrayName={arrayName}
        fieldName="mask"
      />
    </>
  );

  return <MultilineField arrayName={arrayName} renderChild={renderChild} />;
};
