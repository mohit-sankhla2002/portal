import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import {
  destinationLabel,
  nameLabel,
  preProcessProject,
  projectCodeLabel,
  ProjectForm,
  hasValidRoles,
  convertPermissions,
} from './ProjectForm';
import { Provider } from 'react-redux';
import React from 'react';
import { getInitialState, makeStore } from '../../store';
import {
  ConfirmLabel,
  mockConsoleError,
  mockConsoleWarn,
  MockServer,
  resBody,
  resJson,
  setInputValue,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import {
  Project,
  ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { AuthState } from '../../reducers/auth';
import {
  createBatch,
  createCustomAffiliation,
  createNode,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../../factories';

const users = [
  {
    username: '63457666153@eduid.ch',
    email: 'chuck.norris@roundhouse.kick',
    firstName: 'Chuck',
    lastName: 'Norris',
    displayName: 'Chuck Norris (chuck.norris@roundhouse.kick)',
    affiliation: '',
    affiliationId: '',
    id: 2,
    roles: [ProjectUsersInnerRolesEnum.PL],
  },
];
describe('ProjectForm', () => {
  describe('preProcessProject', () => {
    it('should transform object with permissions into array of permission keys', () => {
      expect(
        preProcessProject({
          users: [
            {
              roles: {
                PL: true,
                PM: true,
                DM: false,
                USER: true,
              },
            },
          ],
        }),
      ).toEqual({
        users: [
          {
            roles: ['PL', 'PM', 'USER'],
          },
        ],
      });
    });
  });

  describe('Component', () => {
    const enterText = (
      inputs: HTMLInputElement[],
      name: string,
      value: string,
    ) => {
      const nameField = inputs.find((el) => el.name === name);
      if (!nameField) {
        throw new Error(`No input field could be found for '${name}'`);
      }
      setInputValue(nameField, value);
    };

    const server: MockServer = setupMockApi();

    const node1Name = 'Node';
    const node1Code = 'node';
    const node2Name = 'Other';
    const userNode = createNode({ code: node1Code, name: node1Name });

    const nodes = [
      createNode({ code: node1Code, name: node1Name }),
      createNode({ code: 'other', name: node2Name }),
    ];

    beforeEach(() => {
      server.use(
        rest.get(`${backend}nodes/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, nodes)),
        ),
        rest.get(`${backend}users/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
        rest.get(`${backend}identity/group/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
        rest.get(`${backend}custom-affiliations`, (req, res, ctx) =>
          res(
            resJson(ctx),
            resBody(ctx, createBatch(createCustomAffiliation, 2)),
          ),
        ),
      );
    });

    afterEach(() => {
      // Reset any runtime handlers tests may use.
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    it('should display an error when ID and name are identical (case-sensitive)', async () => {
      const project: Omit<Project, 'code' | 'name'> = {
        destination: node1Code,
        users: [
          {
            id: 1,
            roles: [ProjectUsersInnerRolesEnum.PL],
          },
        ],
      };
      render(
        <Provider store={makeStore()}>
          <ProjectForm
            project={project}
            close={() => {
              return;
            }}
          />
        </Provider>,
      );
      const inputs = screen.getAllByRole('textbox') as HTMLInputElement[];
      const sameValue = 'du';
      enterText(inputs, 'name', sameValue);
      enterText(inputs, 'code', sameValue);
      // Ensure that the node is already selected
      await screen.findByText(node1Name);
      fireEvent.click(screen.getByText(ConfirmLabel.SAVE));
      await screen.findByText(`Must not be the same as ${nameLabel}`);
      await screen.findByText(`Must not be the same as ${projectCodeLabel}`);
    }, 10000);

    const renderForm = (
      authState: Record<'auth', AuthState>,
      nodeName: string,
      destination = true,
      users: Array<ProjectUsersInner> = [],
      isEdit = false,
      expirationDate: Date | undefined = undefined,
    ) =>
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...authState,
          })}
        >
          <ProjectForm
            project={{
              id: isEdit ? 1 : undefined,
              users: users,
              destination: nodes.find((node) => node.name === nodeName)?.code,
              permissions: {
                edit: {
                  destination: destination,
                  expirationDate: true,
                },
              },
              expirationDate,
            }}
            close={jest.fn()}
          />
        </Provider>,
      );

    it.each`
      authState                               | selectableNodes           | nonSelectableNodes | description
      ${createStaffAuthState()}               | ${[node1Name, node2Name]} | ${[]}              | ${'allow destination to be any of the nodes for staff'}
      ${createNodeAdminAuthState([userNode])} | ${[node1Name]}            | ${[node2Name]}     | ${'only allow destination to be one of the nodes the user is node admin of'}
    `(
      'should $description',
      async ({ authState, selectableNodes, nonSelectableNodes }) => {
        const consoleSpy = mockConsoleWarn();

        for (const selectableNode of selectableNodes) {
          const { findByText } = renderForm(
            { auth: authState },
            selectableNode,
          );
          await findByText(selectableNode);
          expect(consoleSpy).not.toHaveBeenCalled();
        }

        for (const nonSelectableNode of nonSelectableNodes) {
          const { queryByText, findAllByText } = renderForm(
            { auth: authState },
            nonSelectableNode,
          );
          await findAllByText(destinationLabel);
          expect(queryByText(nonSelectableNode)).not.toBeInTheDocument();
        }

        if (nonSelectableNodes.length) {
          await waitFor(() => {
            const warnings = consoleSpy.mock.calls;
            expect(warnings).toHaveLength(nonSelectableNodes.length * 2);
            const expectedWarning =
              'MUI: You have provided an out-of-range value';
            for (const warning of warnings) {
              expect(warning[0]).toContain(expectedWarning);
            }
          });
        } else {
          expect(consoleSpy).not.toHaveBeenCalled();
        }
      },
    );

    it('should show destination node when editing a project the node admin is NOT associated with but is project leader of', async () => {
      const { findByText } = renderForm(
        { auth: createNodeAdminAuthState() },
        node2Name,
        false,
        users,
        true,
      );
      await findByText(node2Name);
    });

    it('should display the right expiration date, which can be cleared', async () => {
      renderForm(
        { auth: createStaffAuthState() },
        node2Name,
        true,
        users,
        true,
        new Date('2023-03-03'),
      );
      mockConsoleError();
      const expirationDate = screen.getByRole('textbox', {
        name: 'captions.expirationDate',
      });
      expect(expirationDate).not.toHaveDisplayValue(['']);
      // 'Choose date' button to open date picker should be available
      const openCalendar = screen.getByRole('button', {
        name: /Mar 3, 2023/,
      });
      fireEvent.click(openCalendar);
      // Ensure calendar is open
      const clearButton = screen.getByRole('button', {
        name: 'Clear',
      });
      fireEvent.click(clearButton);
      // Displayed value has been reset
      expect(expirationDate).toHaveDisplayValue(['']);
    });

    it.each`
      users                                                                                         | expected | description
      ${[{ roles: [ProjectUsersInnerRolesEnum.PM] }, { roles: [ProjectUsersInnerRolesEnum.USER] }]} | ${false} | ${'PL is missing'}
      ${[{ roles: [ProjectUsersInnerRolesEnum.PL] }, { roles: [] }]}                                | ${false} | ${'a user has no role'}
      ${[{ roles: [ProjectUsersInnerRolesEnum.PL] }, { roles: [ProjectUsersInnerRolesEnum.USER] }]} | ${true}  | ${'all users have a role, including one PL'}
    `('should return $expected when $description', ({ users, expected }) => {
      // @ts-expect-error not relevant for logic to be tested
      const result = hasValidRoles(convertPermissions({ users: users }));
      expect(result).toBe(expected);
    });
  });
});
