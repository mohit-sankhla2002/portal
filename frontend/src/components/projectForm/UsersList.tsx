import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import React, { ReactElement } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TooltipText,
  UserChip,
} from '@biomedit/next-widgets';
import {
  ProjectPermissionsEditUsersEnum,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { UserTextField } from '../UserTextField';
import { ConvertedProjectUser, useRoles, useTooltips } from './UsersListHooks';
import { RoleTableCell } from './RoleTableCell';

interface FormValues {
  users: Array<ConvertedProjectUser>;
}

type UserListProps = {
  permissions?: Array<ProjectPermissionsEditUsersEnum>;
};

const isRoleSelected = (
  field: ConvertedProjectUser,
  role: ProjectUsersInnerRolesEnum,
): boolean => !!field?.roles?.[role];

export const UsersList = ({ permissions }: UserListProps): ReactElement => {
  const arrayName = 'users';

  const { control, clearErrors } = useFormContext<FormValues>();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
    keyName: 'key',
  });

  function hasRole(
    role: ProjectUsersInnerRolesEnum | ProjectPermissionsEditUsersEnum,
  ): boolean {
    return !!permissions?.includes(role as ProjectPermissionsEditUsersEnum);
  }

  const tooltips = useTooltips();
  const roles = useRoles();

  return (
    <div>
      <UserTextField
        addUser={(user: ConvertedProjectUser) => {
          if (!fields.some((field) => String(field.id) === String(user.id))) {
            append(user);
          }
        }}
        name={arrayName}
      />
      {fields.length > 0 && (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>User</TableCell>
              {roles.map((permission) => (
                <TableCell key={permission}>
                  <TooltipText text={permission} title={tooltips[permission]} />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {fields.map((field, index) => {
              const displayName = field?.['displayName']
                ? field?.['displayName']
                : field?.['username'];

              //
              // Make sure only users can be deleted which only have selected roles the current user is allowed to change.
              //
              // Example: if the current user is permissions manager, they cannot change whether users are project leaders.
              // This means they should also not be able to delete any users from the list which are project leaders,
              // else they would also remove the project leader's roles by doing so.
              //
              const canDelete = roles.every((role) =>
                isRoleSelected(field, role) ? hasRole(role) : true,
              );

              const onDelete = canDelete
                ? () => {
                    clearErrors('users');
                    remove(index);
                  }
                : undefined;

              return (
                <TableRow key={field.key}>
                  <TableCell>
                    <Controller
                      control={control}
                      name={`${arrayName}.${index}.displayName`}
                      defaultValue={displayName}
                      render={() => (
                        <UserChip user={displayName} onDelete={onDelete} />
                      )}
                    />
                  </TableCell>
                  {roles.map((role) => {
                    return (
                      <RoleTableCell
                        name={arrayName}
                        key={role}
                        index={index}
                        field={field}
                        disabled={!hasRole(role)}
                        fieldName={`roles.${role}`}
                      />
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )}
    </div>
  );
};
