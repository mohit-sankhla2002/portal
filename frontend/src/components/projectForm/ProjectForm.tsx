import React, { ReactElement, useCallback, useEffect, useMemo } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { Overwrite, Required } from 'utility-types';
import {
  AutocompleteField,
  BaseReducerState,
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  SelectField,
  useChoices,
  useEnhancedForm,
  useFormDialog,
} from '@biomedit/next-widgets';
import { FormProvider } from 'react-hook-form';
import {
  ADD_PROJECT,
  CALL,
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_GROUPS,
  LOAD_NODES,
  updateProjectAction,
} from '../../actions/actionTypes';
import {
  Group,
  GroupProfileRoleEnum,
  Node,
  Project,
  ProjectPermissionsEditUsersEnum,
  ProjectUsersInnerRolesEnum,
  User,
} from '../../api/generated';
import { arrayName, IpRangeList } from './IpRangeList';
import { generatedBackendApi } from '../../api/api';
import { useNodeChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { CircularProgress } from '@mui/material';
import { RootState } from '../../store';
import { IdRequired } from '../../global';
import { reduce } from 'lodash';
import { ConvertedProjectUser } from './UsersListHooks';
import { UsersList } from './UsersList';

type ConvertedProject = Overwrite<
  UiProject,
  {
    users: ConvertedProjectUser[];
  }
>;

export function preProcessProject(project: ConvertedProject): UiProject {
  // This is to convert `{role1: true, role2: false}` into `[role1]`
  const { users = [], ...rest } = project;
  return {
    ...rest,
    users: users.map(({ roles, ...userContent }) => ({
      roles: reduce(
        roles,
        (result, value, key) => {
          if (value) {
            result.push(ProjectUsersInnerRolesEnum[key]);
          }
          return result;
        },
        [] as ProjectUsersInnerRolesEnum[],
      ),
      ...userContent,
    })),
  };
}

export function convertPermissions(project: UiProject): ConvertedProject {
  // This is to convert `[perm1, perm2]` into `{perm1: true, perm2: true}`
  const { users, ...rest } = project;
  return {
    ...rest,
    users: users?.map(({ roles, ...userContent }) => ({
      roles: Object.fromEntries(roles.map((r) => [r, true])),
      ...userContent,
    })),
  };
}

type ProjectFormProps = {
  project: UiProject;
  close: () => void;
};

type UiProject = Required<Partial<Project>, 'users'>;

export function useProjectForm() {
  const newProject: ConvertedProject = {
    permissions: {
      edit: { users: Object.values(ProjectPermissionsEditUsersEnum) },
    },
    users: [],
  };
  const { closeFormDialog, data, ...rest } = useFormDialog<UiProject>(
    preProcessProject(newProject),
  );
  const projectForm = data && (
    <ProjectForm project={data} close={closeFormDialog} />
  );
  return {
    projectForm,
    ...rest,
  };
}

export const nameLabel = 'captions.name';
export const projectCodeLabel = 'captions.code';
export const destinationLabel = 'captions.destination';
export const legalSupportContactLabel = 'captions.legalSupportContact';
export const legalApprovalGroupLabel = 'captions.legalApprovalGroup';
export const expirationDateLabel = 'captions.expirationDate';

export function hasValidRoles(project: Project): boolean {
  return (
    project?.users?.every((u) => Object.values(u.roles).some((role) => role)) &&
    project?.users?.some((u) => u.roles[ProjectUsersInnerRolesEnum.PL])
  );
}

export function ProjectForm({
  project,
  close,
}: ProjectFormProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.PROJECT_FORM);

  const dispatch = useDispatch();

  const initialValues = convertPermissions(project);

  const { isSubmitting, isFetching: isFetchingProject } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Project>>
  >((state) => state.project);

  const { itemList: nodes, isFetching: isFetchingNodes } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Project>>
  >((state) => state.nodes);

  const users = useSelector<RootState, Array<IdRequired<User>>>(
    (state) => state.users.itemList,
  );
  const nodeAdminNodes = useSelector<
    RootState,
    Array<IdRequired<Node>> | undefined
  >((state) => state.auth.user?.manages?.nodeAdmin);

  const { itemList: legalApprovalGroups, isFetching: isFetchingGroups } =
    useSelector<RootState, BaseReducerState<IdRequired<Group>>>(
      (state) => state.groups,
    );

  const isEdit = !!project.id;
  const isCreate = !isEdit;

  const isDisabled = useCallback(
    (fieldName: string): boolean =>
      !(isCreate || (isEdit && !!project.permissions?.edit?.[fieldName])),
    [isCreate, isEdit, project.permissions?.edit],
  );
  const isNameDisabled = useMemo(() => isDisabled('name'), [isDisabled]);
  const isDestDisabled = useMemo(() => isDisabled('destination'), [isDisabled]);
  const isLegalSupportContactDisabled = useMemo(
    () => isDisabled('legalSupportContact'),
    [isDisabled],
  );
  const isIpEnabled = useMemo(() => !isDisabled(arrayName), [isDisabled]);
  const isLegalApprovalGroupDisabled = useMemo(
    () => isDisabled('legalApprovalGroup'),
    [isDisabled],
  );
  const isExpirationDateDisabled = useMemo(
    () => isDisabled('expirationDate'),
    [isDisabled],
  );

  const nodeChoices = useNodeChoices(
    nodeAdminNodes?.length && !isDestDisabled ? nodeAdminNodes : nodes,
  );

  const legalApprovalGroupChoices = useChoices(
    legalApprovalGroups,
    'id',
    'name',
  );

  const form = useEnhancedForm<Project | ConvertedProject>({
    defaultValues: initialValues,
  });

  const { setError } = form;

  const handleSubmit = (project) => {
    if (
      // Otherwise it is a `dayjs` object
      typeof project.expirationDate === 'string' &&
      project.expirationDate.trim() === ''
    ) {
      project.expirationDate = null;
    }

    // Dispatch if all users have a role AND we have at least one PL
    if (hasValidRoles(project)) {
      const processedProject = preProcessProject(project);
      const onSuccessAction = { type: CALL, callback: close };
      if (project.id) {
        dispatch(
          updateProjectAction(processedProject as Project, onSuccessAction),
        );
      } else {
        dispatch(
          requestAction(
            ADD_PROJECT,
            { project: processedProject as Project },
            onSuccessAction,
          ),
        );
      }
    } else {
      setError('users', {
        message: t('invalidRolesError') as string,
      });
    }
  };

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_NODES));
      dispatch(requestAction(LOAD_GROUPS, { role: GroupProfileRoleEnum.ELSI }));
      dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
    });
  }, [dispatch]);

  if (isFetchingProject || !initialValues || !users) {
    return <CircularProgress />;
  }

  const nameValue = form.getValues().name;
  const projectCodeValue = form.getValues().code;

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueProject({ uniqueProject: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={project?.name ?? t('newProject')}
        open={!!project}
        onSubmit={handleSubmit}
        onClose={close}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
        noValidate
      >
        <HiddenField name="id" initialValues={initialValues} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            unique={uniqueCheck}
            label={t(projectCodeLabel)}
            initialValues={initialValues}
            validations={idValidations(t(nameLabel) as string, nameValue)}
            disabled={isEdit}
            fullWidth
          />
          <LabelledField
            name="name"
            type="text"
            unique={uniqueCheck}
            label={t(nameLabel)}
            validations={nameValidations(
              t(projectCodeLabel) as string,
              projectCodeValue,
            )}
            initialValues={initialValues}
            disabled={isNameDisabled}
            fullWidth
          />
          <SelectField
            name="destination"
            label={t(destinationLabel)}
            initialValues={initialValues}
            isLoading={isFetchingNodes}
            required
            choices={nodeChoices}
            disabled={isDestDisabled}
          />
          <LabelledField
            name="expirationDate"
            type="date"
            label={t(expirationDateLabel)}
            initialValues={initialValues}
            InputLabelProps={{ shrink: true }}
            disabled={isExpirationDateDisabled}
            format="DD.MM.YYYY"
            fullWidth
          />
          <LabelledField
            name="legalSupportContact"
            type="email"
            label={t(legalSupportContactLabel)}
            initialValues={initialValues}
            validations={[email]}
            disabled={isLegalSupportContactDisabled}
            fullWidth
          />
          <AutocompleteField
            name="legalApprovalGroup"
            label={t(legalApprovalGroupLabel)}
            initialValues={initialValues}
            isLoading={isFetchingGroups}
            choices={legalApprovalGroupChoices}
            disabled={isLegalApprovalGroupDisabled}
            width="100%"
          />
        </FormFieldsContainer>
        <h4>{t('subtitles.users')}</h4>
        <UsersList permissions={project?.permissions?.edit?.users} />
        {isIpEnabled && (
          <>
            <h4>{t('subtitles.ipRanges')}</h4>
            <IpRangeList />
          </>
        )}
      </FormDialog>
    </FormProvider>
  );
}
