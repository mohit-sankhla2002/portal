import { useMemo } from 'react';
import {
  ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  affiliationFieldCaption,
  displayNameFieldCaption,
  emailFieldCaption,
  usernameFieldCaption,
} from '../Profile';
import { getFullName } from '../../utils';
import { Overwrite } from 'utility-types';

export type ConvertedProjectUser = Overwrite<
  ProjectUsersInner,
  {
    roles: Partial<Record<ProjectUsersInnerRolesEnum, boolean>>;
  }
>;

export const useTooltips = (): Record<ProjectUsersInnerRolesEnum, string> => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  return useMemo(() => {
    return {
      [ProjectUsersInnerRolesEnum.PL]: t('roles.PL'),
      [ProjectUsersInnerRolesEnum.PM]: t('roles.PM'),
      [ProjectUsersInnerRolesEnum.DM]: t('roles.DM'),
      [ProjectUsersInnerRolesEnum.USER]: t('roles.USER'),
    };
  }, [t]);
};

export const useRoles = (): ProjectUsersInnerRolesEnum[] =>
  useMemo(() => Object.values(ProjectUsersInnerRolesEnum).reverse(), []);

export const useUserEntries = (user: ConvertedProjectUser | null) => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  return useMemo(
    () => [
      { caption: t(displayNameFieldCaption), component: getFullName(user) },
      {
        caption: t(usernameFieldCaption),
        component: user?.username,
      },
      {
        caption: t(emailFieldCaption),
        component: user?.email,
      },
      {
        caption: t(affiliationFieldCaption),
        component: user?.affiliation,
      },
    ],
    [t, user],
  );
};
