import React, { ReactElement } from 'react';
import {
  CheckboxArrayField,
  EnhancedArrayFieldProps,
  HiddenArrayField,
  TableCell,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { ProjectUsersInnerRolesEnum } from '../../api/generated';
import { useFormContext } from 'react-hook-form';
import { FieldPath } from 'react-hook-form/dist/types/path';
import { FieldValues } from 'react-hook-form/dist/types';
import { $Values } from 'utility-types';

type RoleTableCellProps<T extends FieldValues> = {
  name: FieldPath<T>;
  index: number;
  field: $Values<Pick<EnhancedArrayFieldProps, 'field'>>;
  disabled: boolean;
  fieldName: string;
};

export function RoleTableCell<T extends FieldValues>({
  name,
  field,
  index,
  disabled,
  fieldName,
}: RoleTableCellProps<T>): ReactElement {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  const { clearErrors, getValues } = useFormContext<T>();

  const onChange = () => {
    const projectLeader = getValues()[name].find(
      (user) => user.roles?.[ProjectUsersInnerRolesEnum.PL],
    );
    if (projectLeader) {
      clearErrors(name);
    }
  };

  return (
    <TableCell>
      <HiddenArrayField
        arrayName={name}
        fieldName="id"
        index={index}
        field={field}
      />
      <CheckboxArrayField
        arrayName={name}
        fieldName={fieldName}
        index={index}
        field={field}
        disabled={disabled}
        disabledTitle={t('disabledTitle')}
        onChange={onChange}
      />
    </TableCell>
  );
}
