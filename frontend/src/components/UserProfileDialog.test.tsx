import { expectAllToBeInTheDocument } from '@biomedit/next-widgets';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { getInitialState, makeStore } from '../store';
import {
  affiliationConsentLabel,
  dialogTitle,
  usernameLabel,
  UserProfileDialog,
  emailLabel,
} from './UserProfileDialog';
import { createAuthState, createUserinfo } from '../../factories';

describe('UserProfileDialog', () => {
  describe('Component', () => {
    const username = 'chucknorris';
    const emails = 'chuck.norris@email.com';

    it.each`
      localUsername | affiliationConsent | affiliationConsentRequired | email        | isDialogOpen | isPromptedUsername | isPromptedAffiliation | isPromptedEmail
      ${undefined}  | ${false}           | ${false}                   | ${undefined} | ${true}      | ${true}            | ${false}              | ${true}
      ${undefined}  | ${true}            | ${false}                   | ${undefined} | ${true}      | ${true}            | ${false}              | ${true}
      ${username}   | ${false}           | ${false}                   | ${undefined} | ${true}      | ${false}           | ${false}              | ${true}
      ${username}   | ${true}            | ${false}                   | ${undefined} | ${true}      | ${false}           | ${false}              | ${true}
      ${undefined}  | ${false}           | ${true}                    | ${undefined} | ${true}      | ${true}            | ${true}               | ${true}
      ${undefined}  | ${true}            | ${true}                    | ${undefined} | ${true}      | ${true}            | ${false}              | ${true}
      ${username}   | ${false}           | ${true}                    | ${undefined} | ${true}      | ${false}           | ${true}               | ${true}
      ${username}   | ${true}            | ${true}                    | ${undefined} | ${true}      | ${false}           | ${false}              | ${true}
      ${undefined}  | ${false}           | ${false}                   | ${emails}    | ${true}      | ${true}            | ${false}              | ${false}
      ${undefined}  | ${true}            | ${false}                   | ${emails}    | ${true}      | ${true}            | ${false}              | ${false}
      ${username}   | ${false}           | ${false}                   | ${emails}    | ${false}     | ${false}           | ${false}              | ${false}
      ${username}   | ${true}            | ${false}                   | ${emails}    | ${false}     | ${false}           | ${false}              | ${false}
      ${undefined}  | ${false}           | ${true}                    | ${emails}    | ${true}      | ${true}            | ${true}               | ${false}
      ${undefined}  | ${true}            | ${true}                    | ${emails}    | ${true}      | ${true}            | ${false}              | ${false}
      ${username}   | ${false}           | ${true}                    | ${emails}    | ${true}      | ${false}           | ${true}               | ${false}
      ${username}   | ${true}            | ${true}                    | ${emails}    | ${false}     | ${false}           | ${false}              | ${false}
    `(
      'should show user profile dialog ($isDialogOpen), ' +
        'include a field to enter a username ($isPromptedUsername) and ' +
        'include a field to choose an email ($isPromptedEmail) and ' +
        'ask for affiliation consent ($isPromptedAffiliation), ' +
        'when localUsername is $localUsername, ' +
        'email is $email, ' +
        'affiliationConsent is $affiliationConsent and ' +
        'affiliationConsentRequired is $affiliationConsentRequired',
      ({
        localUsername,
        affiliationConsent,
        affiliationConsentRequired,
        email,
        isDialogOpen,
        isPromptedUsername,
        isPromptedAffiliation,
        isPromptedEmail,
      }) => {
        jest.mock('../config', () => {
          const originalModule = jest.requireActual('../config');

          return {
            __esModule: true,
            ...originalModule,
            affiliationConsentRequired: affiliationConsentRequired,
          };
        });

        const authState = createAuthState({
          invalidMsg: null,
          user: createUserinfo({
            email: email,
            profile: {
              localUsername: localUsername,
              affiliationConsent: affiliationConsent,
              emails: emails,
            },
          }),
        });

        render(
          <Provider
            store={makeStore(undefined, {
              ...getInitialState(),
              auth: authState,
            })}
          >
            <UserProfileDialog />
          </Provider>,
        );

        [
          { label: dialogTitle, flag: isDialogOpen },
          { label: usernameLabel, flag: isPromptedUsername },
          { label: affiliationConsentLabel, flag: isPromptedAffiliation },
          { label: emailLabel, flag: isPromptedEmail },
        ].forEach(({ label, flag }) => {
          expectAllToBeInTheDocument(screen.queryAllByText(label), flag);
        });
      },
    );
  });
});
