import { PgpKeyInfoStatusEnum } from '../../api/generated';
import { allowedPgpKeyInfoStatusEnumSubset } from './PgpKeyInfoManageForm';

describe('allowedPgpKeyInfoStatusEnumSubset', () => {
  const allowedEnumSubsetFromPending = {
    [PgpKeyInfoStatusEnum.APPROVED]: PgpKeyInfoStatusEnum.APPROVED,
    [PgpKeyInfoStatusEnum.REJECTED]: PgpKeyInfoStatusEnum.REJECTED,
  };
  const allowedEnumSubsetFromApproved = {
    [PgpKeyInfoStatusEnum.APPROVAL_REVOKED]:
      PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
  };

  it.each`
    currentStatus                            | expectedEnumSubset
    ${PgpKeyInfoStatusEnum.PENDING}          | ${allowedEnumSubsetFromPending}
    ${PgpKeyInfoStatusEnum.APPROVED}         | ${allowedEnumSubsetFromApproved}
    ${PgpKeyInfoStatusEnum.APPROVAL_REVOKED} | ${{}}
    ${PgpKeyInfoStatusEnum.DELETED}          | ${{}}
    ${PgpKeyInfoStatusEnum.REJECTED}         | ${{}}
    ${PgpKeyInfoStatusEnum.KEY_REVOKED}      | ${{}}
    ${undefined}                             | ${{}}
  `(
    'Should return $expectedEnumSubset if current status is $currentStatus',
    function ({ currentStatus, expectedEnumSubset }) {
      expect(allowedPgpKeyInfoStatusEnumSubset(currentStatus)).toStrictEqual(
        expectedEnumSubset,
      );
    },
  );
});
