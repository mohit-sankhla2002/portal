import {
  Group,
  GroupProfileRoleEnum,
  GroupUsersInner,
} from '../../api/generated';

export const groupsToRoleUsers = (
  groups: Group[],
  role: GroupProfileRoleEnum,
): GroupUsersInner[] =>
  groups.find((group) => group.profile?.role == role)?.users || [];
