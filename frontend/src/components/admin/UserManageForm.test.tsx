import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import { UserForm } from './UserManageForm';
import { User } from '../../api/generated';
import {
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { createStaffAuthState } from '../../../factories';

describe('UserManageForm', () => {
  const listFlags = `${backend}flags/`;
  const listGroups = `${backend}identity/group/`;
  const listCustomAffiliations = `${backend}custom-affiliations/`;

  const staffAuthState = createStaffAuthState();
  const differentUser: User = {
    ...staffAuthState.user,
    username: 'differentUser',
  };

  const server: MockServer = setupMockApi();

  beforeEach(() => {
    server.use(
      rest.get(listFlags, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [])),
      ),
      rest.get(listGroups, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [])),
      ),
      rest.get(listCustomAffiliations, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [])),
      ),
    );
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    editedUser             | canDisable | description
    ${staffAuthState.user} | ${false}   | ${'prevent the currently logged in user from inactivating themself'}
    ${differentUser}       | ${true}    | ${'allow the currently logged in user to inactivate a different user'}
  `('should $description', async ({ editedUser, canDisable }) => {
    const verifier = new RequestVerifier(server);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: staffAuthState,
        })}
      >
        <UserForm user={editedUser} onClose={jest.fn()} />
      </Provider>,
    );

    const isActiveCheckbox = await screen.findByRole('checkbox');
    expect(isActiveCheckbox).toBeInTheDocument();
    if (canDisable) {
      expect(isActiveCheckbox).toBeEnabled();
    } else {
      expect(isActiveCheckbox).toBeDisabled();
    }

    verifier.assertCount(3);
    verifier.assertCalled(listFlags, 1);
    verifier.assertCalled(listGroups, 1);
    verifier.assertCalled(listCustomAffiliations, 1);
  });
});
