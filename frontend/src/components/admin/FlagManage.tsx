import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_FLAGS } from '../../actions/actionTypes';
import { Flag } from '../../api/generated';
import { IdRequired } from '../../global';
import { useFlagColumns, useFlagForm } from './AdministrationHooks';
import { isStaff } from '../selectors';
import { I18nNamespace } from '../../i18n';
import { useTranslation } from 'next-i18next';
import {
  BaseReducerState,
  EnhancedTable,
  requestAction,
} from '@biomedit/next-widgets';
import { RootState } from '../../store';

export const FlagManageList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.FLAG_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: flags,
  } = useSelector<RootState, BaseReducerState<IdRequired<Flag>>>(
    (state) => state.flags,
  );
  const staff = useSelector(isStaff);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_FLAGS));
  }, [dispatch]);

  const { openFormDialog, flagForm } = useFlagForm();

  const columns = useFlagColumns();
  return (
    <EnhancedTable<Required<Flag>>
      itemList={flags}
      columns={columns}
      canAdd={staff}
      canEdit={staff}
      canDelete={false} // it's forbidden to delete flags
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      form={flagForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      addButtonLabel={t(`${I18nNamespace.FLAG_LIST}:addButton`) as string}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.flag_plural`),
        }) as string
      }
      inline
    />
  );
};
