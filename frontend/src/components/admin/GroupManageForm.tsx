import React, { ReactElement, useEffect, useState } from 'react';
import {
  DataProvider,
  Group,
  GroupProfileRoleEnum,
  GroupUsersInner,
  User,
} from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import {
  AutocompleteField,
  BaseReducerState,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  LabelledField,
  nameValidations,
  requestAction,
  useEnhancedForm,
  useEnumChoices,
} from '@biomedit/next-widgets';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  ADD_GROUP,
  CALL,
  LOAD_PERMISSIONS,
  UPDATE_GROUP,
} from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { usePermissionChoices } from '../choice';
import { PermissionsObjectList } from './PermissionsObjectList';
import { generatedBackendApi } from '../../api/api';
import { isStaff } from '../selectors';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { RootState } from '../../store';
import { IdRequired } from '../../global';
import { UserTextField } from '../UserTextField';
import { Chip, Stack } from '@mui/material';
import { styled } from '@mui/material/styles';

const StyledFieldset = styled('fieldset')(({ theme }) => ({
  width: '100%',
  marginBottom: theme.spacing(5),
  marginInlineStart: 0,
  borderRadius: theme.shape.borderRadius,
  border: '1px',
  borderColor: theme.palette.grey[400],
  borderStyle: 'solid',
  fontSize: '0.75rem',
  minHeight: theme.spacing(17),
}));

const StyledLegend = styled('legend')(({ theme }) => ({
  paddingInlineStart: '0.3rem',
  paddingInlineEnd: '0.3rem',
  color: theme.palette.text.secondary,
}));

type GroupFormProps = {
  group: Partial<Group>;
  onClose: () => void;
};

export const GroupForm = ({ group, onClose }: GroupFormProps): ReactElement => {
  const usersFieldName = 'users';
  const dispatch = useDispatch();
  const { t } = useTranslation([
    I18nNamespace.GROUP_LIST,
    I18nNamespace.GROUP_MANAGE_FORM,
  ]);

  const isEdit = !!group.id;

  const { isFetching: isFetchingPermissions, itemList: permissions } =
    useSelector<RootState, BaseReducerState<IdRequired<DataProvider>>>(
      (state) => state.permissions,
    );

  const staff = useSelector(isStaff);
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.groups.isSubmitting,
  );
  const permissionChoices = usePermissionChoices(permissions);

  const form = useEnhancedForm<Group>({ defaultValues: group });

  const [users, setUsers] = useState<GroupUsersInner[]>(group.users || []);

  function submit(activeGroup: Group) {
    const onSuccessAction = { type: CALL, callback: onClose };
    activeGroup.users = users;
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_GROUP,
          {
            id: String(activeGroup.id),
            group: activeGroup,
          },
          onSuccessAction,
        ),
      );
    } else {
      dispatch(
        requestAction(ADD_GROUP, { group: activeGroup }, onSuccessAction),
      );
    }
  }

  const handleDelete = (user: User) => () => {
    setUsers(users?.filter((u) => u.id != user.id));
  };

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_PERMISSIONS));
    });
  }, [dispatch, group.id]);

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueGroup({ uniqueGroup: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={t(
          `${I18nNamespace.GROUP_MANAGE_FORM}:title.${
            isEdit ? 'edit' : 'create'
          }`,
        )}
        open={!!group}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: group.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={group}
            fullWidth
            disabled={!staff}
          />
          <LabelledField
            name="profile.description"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.description`)}
            initialValues={group}
            fullWidth
            disabled={!staff}
            multiline
            rows={3}
            sx={{ marginBottom: 5 }}
          />
          <AutocompleteField
            name="profile.role"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.role`)}
            initialValues={group}
            disabled={!staff || isEdit}
            width="100%"
            choices={useEnumChoices(GroupProfileRoleEnum)}
          />
          <StyledFieldset>
            <StyledLegend>
              {t(`${I18nNamespace.COMMON}:models.user_plural`)}
            </StyledLegend>
            <UserTextField
              name={usersFieldName}
              addUser={(user) => {
                setUsers([...users, user as GroupUsersInner]);
              }}
            />
            <Stack direction="row" spacing={1}>
              {users?.map((user) => (
                <Chip
                  key={user.id}
                  label={user.displayName}
                  onDelete={handleDelete(user)}
                />
              ))}
            </Stack>
          </StyledFieldset>
          <AutocompleteField
            name="permissions"
            label={t(
              `${I18nNamespace.GROUP_MANAGE_FORM}:captions.modelPermissions`,
            )}
            initialValues={group}
            choices={permissionChoices}
            isLoading={isFetchingPermissions}
            multiple
            width="100%"
            disabled={!staff}
          />
        </FormFieldsContainer>
        <h4>
          {t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.objectPermissions`)}
        </h4>
        <PermissionsObjectList />
      </FormDialog>
    </FormProvider>
  );
};
