import { FlagManageList } from './FlagManage';
import { backend } from '../../api/api';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { makeStore } from '../../store';
import React from 'react';
import { rest } from 'msw';
import {
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { createFlag } from '../../../factories';

describe('FlagManageList.fetchFlags', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it('should display empty list', async () => {
    server.use(
      rest.get(`${backend}flags/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [])),
      ),
    );
    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>,
    );
    // Empty message (NOT i18n'ed)
    expect(await screen.findByText('list:emptyMessage')).toBeInTheDocument();
  });

  it('should list the flags', async () => {
    server.use(
      rest.get(`${backend}flags/`, (req, res, ctx) =>
        res(
          resJson(ctx),
          resBody(ctx, [
            createFlag({ code: 'flag_1' }),
            createFlag({ code: 'flag_2' }),
          ]),
        ),
      ),
    );
    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>,
    );
    // Table header (1x), plus flag rows (2x), plus table footer (1x)
    expect(await screen.findAllByRole('row')).toHaveLength(4);
    expect(await screen.findByText('flag_1')).toBeInTheDocument();
    expect(await screen.findByText('flag_2')).toBeInTheDocument();
  });
});
