import React, { ReactElement, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import { PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { CALL, UPDATE_PGP_KEY_INFO } from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  ConfirmLabel,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
} from '@biomedit/next-widgets';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { RootState } from '../../store';

const PendingPgpKeyInfoStatusEnum = {
  APPROVED: PgpKeyInfoStatusEnum.APPROVED,
  REJECTED: PgpKeyInfoStatusEnum.REJECTED,
};

const ApprovedPgpKeyInfoStatusEnum = {
  APPROVAL_REVOKED: PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
};

export const allowedPgpKeyInfoStatusEnumSubset = (
  currentStatus: PgpKeyInfoStatusEnum | undefined,
) => {
  switch (currentStatus) {
    case PgpKeyInfoStatusEnum.PENDING:
      return PendingPgpKeyInfoStatusEnum;
    case PgpKeyInfoStatusEnum.APPROVED:
      return ApprovedPgpKeyInfoStatusEnum;
    default:
      return {};
  }
};

type PgpKeyInfoManageFormProps = {
  pgpKeyInfo: Partial<PgpKeyInfo>;
  onClose: () => void;
};

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.PGP_KEY_INFO_LIST]);
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.pgpKeyInfo.isSubmitting,
  );
  const dispatch = useDispatch();
  const form = useEnhancedForm<PgpKeyInfo>();
  const pgpKeyInfoStatusEnumSubset = useMemo(
    () => allowedPgpKeyInfoStatusEnumSubset(pgpKeyInfo.status),
    [pgpKeyInfo.status],
  );

  function submit(formPgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        UPDATE_PGP_KEY_INFO,
        {
          id: String(pgpKeyInfo.id),
          pgpKeyInfo: formPgpKeyInfo,
        },
        onSuccessAction,
      ),
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={`Key: ${pgpKeyInfo?.fingerprint}`}
        open={!!pgpKeyInfo}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <HiddenField name="id" initialValues={{ id: pgpKeyInfo.id }} />
          <SelectField
            name="status"
            label={t(`${I18nNamespace.PGP_KEY_INFO_LIST}:columns.status`)}
            required
            choices={useEnumChoices(pgpKeyInfoStatusEnumSubset)}
            populateIfSingleChoice
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
