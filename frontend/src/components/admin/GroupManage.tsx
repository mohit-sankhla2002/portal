import React, { ReactElement, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DELETE_GROUP, LOAD_GROUPS } from '../../actions/actionTypes';
import { Group } from '../../api/generated';
import { IdRequired } from '../../global';
import { useGroupColumns, useGroupForm } from './AdministrationHooks';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { isGroupManager, isStaff } from '../selectors';
import {
  BaseReducerState,
  EnhancedTable,
  requestAction,
} from '@biomedit/next-widgets';
import { RootState } from '../../store';

export const GroupList = (): ReactElement => {
  const { t } = useTranslation([I18nNamespace.GROUP_LIST, I18nNamespace.LIST]);

  const {
    itemList: groups,
    isFetching,
    isSubmitting,
  } = useSelector<RootState, BaseReducerState<IdRequired<Group>>>(
    (state) => state.groups,
  );
  const groupManager = useSelector(isGroupManager);
  const staff = useSelector(isStaff);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_GROUPS, { managed: !staff }));
  }, [staff, dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_GROUP, { id }));
    },
    [dispatch],
  );

  const { openFormDialog, groupForm } = useGroupForm();

  const getDeleteConfirmationText = useCallback(
    (item: Required<Group>) => 'Delete ' + item.name,
    [],
  );

  const columns = useGroupColumns();
  return (
    <EnhancedTable<Required<Group>>
      itemList={groups}
      columns={columns}
      canAdd={staff}
      canEdit={staff || groupManager}
      canDelete={staff}
      onAdd={openFormDialog}
      onEdit={openFormDialog}
      onDelete={deleteItem}
      form={groupForm}
      isFetching={isFetching}
      getDeleteConfirmationText={getDeleteConfirmationText}
      isSubmitting={isSubmitting}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.group_plural`),
        }) as string
      }
      addButtonLabel={t(`${I18nNamespace.GROUP_LIST}:addButton`) as string}
      inline
    />
  );
};
