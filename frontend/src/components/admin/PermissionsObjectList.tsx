import { useFormContext } from 'react-hook-form';
import { styled } from '@mui/material/styles';
import {
  AutocompleteArrayField,
  BaseReducerState,
  Choice,
  MultilineField,
  SelectedChoice,
} from '@biomedit/next-widgets';
import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { usePermissionChoices } from '../choice';
import { generatedBackendApi } from '../../api/api';
import { produce } from 'immer';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { isStaff } from '../selectors';
import { DataProvider, GroupPermissionsObjectInner } from '../../api/generated';
import { RootState } from '../../store';
import { IdRequired } from '../../global';

type AvailableObjects = Record<string, Choice[]>;
type AvailableObjectsIsFetching = Record<string, boolean>;
type ResetToEmpty = Record<string, boolean>;

const StyledAutocompleteArrayField = styled(AutocompleteArrayField)(
  ({ theme }) => ({
    marginBottom: 'auto',
    marginRight: theme.spacing(1),
  }),
);

export const PermissionsObjectList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.GROUP_MANAGE_FORM,
  ]);
  const arrayName = 'permissionsObject';

  const { getValues, watch, reset } = useFormContext();

  const { isFetching: isFetchingPermissions, itemList: permissions } =
    useSelector<RootState, BaseReducerState<IdRequired<DataProvider>>>(
      (state) => state.permissions,
    );

  const permissionChoices = usePermissionChoices(permissions);
  const staff = useSelector(isStaff);

  const watchPermission = watch(arrayName);

  const [availableObjects, setAvailableObjects] = useState<AvailableObjects>(
    {},
  );
  const [availableObjectsIsFetching, setAvailableObjectsIsFetching] =
    useState<AvailableObjectsIsFetching>({});
  const [resetToEmpty, setResetToEmpty] = useState<ResetToEmpty>({});

  const fetchPermissions = useCallback(
    (permIds: Array<GroupPermissionsObjectInner['permission']>) => {
      if (permIds.length === 0) {
        return;
      }
      const newAvailableObjectsIsFetching = produce(
        availableObjectsIsFetching,
        (draft: AvailableObjectsIsFetching) => {
          permIds.forEach((p) => {
            draft[p] = true;
          });
        },
      );
      setAvailableObjectsIsFetching(newAvailableObjectsIsFetching);

      generatedBackendApi
        .listObjectByPermissionViewSets({ permId: permIds })
        .then((result) => {
          setAvailableObjects(
            produce(availableObjects, (draft: AvailableObjects) => {
              for (const perm in result) {
                draft[perm] = result[perm].map((obj) => ({
                  key: obj.id,
                  value: obj.id,
                  label: obj.name,
                }));
              }
            }),
          );
        })
        .finally(() => {
          setAvailableObjectsIsFetching(
            produce(
              newAvailableObjectsIsFetching,
              (draft: AvailableObjectsIsFetching) => {
                permIds.forEach((p) => {
                  draft[p] = false;
                });
              },
            ),
          );
        });
    },
    [availableObjects, availableObjectsIsFetching],
  );

  useEffect(() => {
    const permissionsToFetch = watchPermission.map((row) => row.permission);
    fetchPermissions(permissionsToFetch);
    // eslint-disable-next-line react-hooks/exhaustive-deps -- only run on mount
  }, []);

  useEffect(() => {
    if (!isFetchingPermissions) {
      // new data is available, which could change `defaultValue` of `objects` fields
      // see https://github.com/react-hook-form/react-hook-form/issues/1558#issuecomment-623196472
      reset();
    }
  }, [isFetchingPermissions, reset]);

  const renderChild = (field: Record<string, string>, index: number) => {
    const permission = getValues()[arrayName]?.[index]?.['permission'];
    const objectsChoices = availableObjects[permission] ?? [];
    return (
      <>
        <StyledAutocompleteArrayField
          field={field}
          index={index}
          label={t(`${I18nNamespace.COMMON}:models.permission`)}
          arrayName={arrayName}
          fieldName="permission"
          required
          choices={permissionChoices}
          isLoading={isFetchingPermissions}
          width="50%"
          disabled={!staff}
          onChange={(choice: SelectedChoice) => {
            fetchPermissions(choice ? [Number((choice as Choice).value)] : []);
            setResetToEmpty(
              produce(resetToEmpty, (draft: ResetToEmpty) => {
                draft[field.key] = true;
              }),
            );
          }}
          inputSize="medium"
        />
        <StyledAutocompleteArrayField
          field={field}
          index={index}
          label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.object`)}
          arrayName={arrayName}
          fieldName="objects"
          required
          multiple
          choices={objectsChoices}
          isLoading={availableObjectsIsFetching[permission]}
          width="50%"
          disabled={!staff}
          resetToEmpty={resetToEmpty[field.key] ?? false}
          onChange={() => {
            setResetToEmpty(
              produce(resetToEmpty, (draft: ResetToEmpty) => {
                draft[field.key] = false;
              }),
            );
          }}
          inputSize="medium"
        />
      </>
    );
  };

  return (
    <MultilineField
      arrayName={arrayName}
      addButtonDisabled={!staff}
      deleteButtonDisabled={!staff}
      renderChild={renderChild}
    />
  );
};
