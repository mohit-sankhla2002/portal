import React from 'react';
import {
  MockServer,
  setupMockApi,
  resBody,
  resJson,
} from '@biomedit/next-widgets';
import { screen, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { PgpKeyInfoList } from './PgpKeyInfoManage';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { createPgpKeyInfo, createStaffAuthState } from '../../../factories';
import { PgpKeyInfoStatusEnum } from '../../../src/api/generated';

describe('PgpKeyInfoManage', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  beforeEach(() => {
    server.use(
      rest.get(`${backend}pgpkey/`, (req, res, ctx) =>
        res(
          resJson(ctx),
          resBody(ctx, [
            createPgpKeyInfo({ status: PgpKeyInfoStatusEnum.PENDING }),
            createPgpKeyInfo({ status: PgpKeyInfoStatusEnum.REJECTED }),
          ]),
        ),
      ),
    );

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createStaffAuthState(),
        })}
      >
        <PgpKeyInfoList />
      </Provider>,
    );
  });

  it('Should only show edit button for keys with allowedPgpKeyInfoStatusEnumSubset', async function () {
    const editButtonLabel = 'Edit row';

    expect(await screen.findAllByLabelText(editButtonLabel)).toHaveLength(1);
  });
});
