import React, { useMemo } from 'react';
import {
  DataProvider,
  Flag,
  Group,
  Node,
  NodeNodeStatusEnum,
  PgpKeyInfo,
  PgpKeyInfoStatusEnum,
  User,
} from '../../api/generated';
import { DataProviderForm } from './DataProviderManageForm';
import { NodeForm } from './NodeManageForm';
import { FlagManageForm } from './FlagManageForm';
import truncate from 'lodash/truncate';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { GroupForm } from './GroupManageForm';
import { Required } from 'utility-types';
import {
  ColoredStatus,
  formatDate,
  useLocaleSortingFn,
  Status,
  useFormDialog,
} from '@biomedit/next-widgets';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';
import { createColumnHelper } from '@tanstack/react-table';

export const useUserColumns = () => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.BOOLEAN,
  ]);
  const localeSortingFn = useLocaleSortingFn<Required<User>>();

  return useMemo(() => {
    const columnHelper = createColumnHelper<Required<User>>();
    const statuses: Status<boolean>[] = [
      {
        color: 'success',
        value: true,
        text: t(`${I18nNamespace.BOOLEAN}:true`),
      },
      {
        color: 'error',
        value: false,
        text: t(`${I18nNamespace.BOOLEAN}:false`),
      },
    ];
    return [
      columnHelper.accessor('firstName', {
        header: t('columns.firstName'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('lastName', {
        header: t('columns.lastName'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('email', {
        header: t('columns.email'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('username', {
        header: t('columns.username'),
        sortingFn: 'alphanumeric',
      }),
      // lastLogin might be null, and if that happens datetime sorting won't work
      columnHelper.accessor((user) => user.lastLogin ?? new Date(0), {
        id: 'lastLogin',
        header: t('columns.lastLogin'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.row.original.lastLogin, true) ?? '',
      }),
      columnHelper.accessor(
        (value) => {
          return t(`${I18nNamespace.BOOLEAN}:${value.isActive}`);
        },
        {
          id: 'isActive',
          header: t('columns.isActive'),
          sortingFn: 'alphanumeric',
          cell: ({ row: { original } }) => {
            return ColoredStatus<boolean>({
              value: original.isActive,
              statuses,
            });
          },
        },
      ),
    ];
  }, [t, localeSortingFn]);
};

export const useFlagColumns = () => {
  const columnHelper = createColumnHelper<Required<Flag>>();
  const { t } = useTranslation(I18nNamespace.FLAG_LIST);
  return useMemo(() => {
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('description', {
        header: t('columns.description'),
        cell: (props) =>
          truncate(props.getValue(), {
            length: 100,
            separator: ' ',
          }),
      }),
      columnHelper.accessor('users', {
        header: t('columns.users'),
        cell: (props) => props.getValue().length,
      }),
    ];
  }, [t, columnHelper]);
};

export const useGroupColumns = () => {
  const columnHelper = createColumnHelper<Required<Group>>();
  const { t } = useTranslation(I18nNamespace.GROUP_LIST);
  return useMemo(() => {
    return [
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('users', {
        header: t('columns.users'),
        cell: (props) => props.getValue().length,
      }),
    ];
  }, [t, columnHelper]);
};

export const useNodeColumns = () => {
  const columnHelper = createColumnHelper<Required<Node>>();
  const { t } = useTranslation(I18nNamespace.NODE);
  return useMemo(() => {
    const statuses: Status<NodeNodeStatusEnum>[] = [
      {
        color: 'success',
        value: NodeNodeStatusEnum.ON,
      },
      {
        color: 'error',
        value: NodeNodeStatusEnum.OFF,
      },
    ];
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('nodeStatus', {
        header: t('columns.nodeStatus'),
        cell: (props) =>
          ColoredStatus<NodeNodeStatusEnum>({
            value: props.getValue(),
            statuses,
          }),
      }),
    ];
  }, [t, columnHelper]);
};

enum DataProviderStatusEnum {
  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
}

export function getDataProviderColoredStatus(enabled: boolean | undefined) {
  const statuses: Status<DataProviderStatusEnum>[] = [
    {
      color: 'success',
      value: DataProviderStatusEnum.ENABLED,
    },
    {
      color: 'error',
      value: DataProviderStatusEnum.DISABLED,
    },
  ];
  return ColoredStatus<DataProviderStatusEnum>({
    value: enabled
      ? DataProviderStatusEnum.ENABLED
      : DataProviderStatusEnum.DISABLED,
    statuses,
  });
}

export const useDataProviderColumns = () => {
  const columnHelper = createColumnHelper<Required<DataProvider>>();
  const { t } = useTranslation(I18nNamespace.DATA_PROVIDER);
  return useMemo(() => {
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('node', {
        header: t('columns.node'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('enabled', {
        header: t('columns.enabled'),
        cell: (props) => getDataProviderColoredStatus(props.getValue()),
      }),
    ];
  }, [t, columnHelper]);
};

export const useFlagForm = () => {
  const newFlag = {
    code: undefined,
    description: undefined,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Flag>>(newFlag);
  const flagForm = data && (
    <FlagManageForm flag={data} onClose={closeFormDialog} />
  );
  return {
    flagForm,
    ...rest,
  };
};

export function useNodeForm() {
  const newNode = {
    code: undefined,
    name: undefined,
    nodeStatus: NodeNodeStatusEnum.ON,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Node>>(newNode);
  const nodeForm = data && <NodeForm node={data} onClose={closeFormDialog} />;
  return {
    nodeForm,
    ...rest,
  };
}

export function useDataProviderForm() {
  const newDataProvider = {
    code: undefined,
    name: undefined,
    description: '',
    enabled: false,
    node: '',
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<DataProvider>>(newDataProvider);
  const dataProviderForm = data && (
    <DataProviderForm dataProvider={data} onClose={closeFormDialog} />
  );
  return {
    dataProviderForm,
    ...rest,
  };
}

export function useGroupForm() {
  const newGroup = {
    name: undefined,
    users: [],
    permissions: [],
    permissionsObject: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Group>>(newGroup);
  const groupForm = data && (
    <GroupForm group={data} onClose={closeFormDialog} />
  );
  return {
    groupForm,
    ...rest,
  };
}

export const usePgpKeyInfoForm = () => {
  const newPgpKeyInfo = {
    id: undefined,
    status: PgpKeyInfoStatusEnum.PENDING,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<PgpKeyInfo>>(newPgpKeyInfo);
  const pgpKeyInfoForm = data && (
    <PgpKeyInfoManageForm pgpKeyInfo={data} onClose={closeFormDialog} />
  );
  return {
    pgpKeyInfoForm,
    ...rest,
  };
};
