import React, { ReactElement, useEffect, useMemo } from 'react';
import {
  BaseReducerState,
  CancelLabel,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
  requestAction,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { CircularProgress } from '@mui/material';
import { GroupProfileRoleEnum, Node, OAuth2Client } from '../../api/generated';
import { I18nNamespace } from '../../i18n';
// Do NOT shorten this import!
import { RoleMap, useUserRoleEntries } from '../../widgets/UserRoleEntries';
import { batch, useDispatch, useSelector } from 'react-redux';
import { LOAD_GROUPS, LOAD_OAUTH2_CLIENTS } from '../../actions/actionTypes';
import { groupsToRoleUsers } from './utils';
import { RootState } from '../../store';
import { GroupState } from '../../reducers/group';
import { IdRequired } from '../../global';
import { httpsDtrEnabled } from '../../config';

export type NodeDetailProps = {
  node: Node;
  open: boolean;
  onClose: () => void;
};

export const NodeDetail = ({
  node,
  open,
  onClose,
}: NodeDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE]);
  const { itemList: groups, isFetching } = useSelector<RootState, GroupState>(
    (store) => store.groups,
  );
  const { itemList: clients } = useSelector<
    RootState,
    BaseReducerState<IdRequired<OAuth2Client>>
  >((store) => store.clients);
  const dispatch = useDispatch();

  useEffect(() => {
    if (node.id) {
      batch(() => {
        dispatch(
          requestAction(LOAD_GROUPS, {
            roleEntityId: String(node.id),
            roleEntityType: 'node',
          }),
        );
        dispatch(requestAction(LOAD_OAUTH2_CLIENTS));
      });
    }
  }, [node, dispatch]);

  const roles: RoleMap = useMemo(() => {
    return {
      manager: groupsToRoleUsers(groups, GroupProfileRoleEnum.NM),
      admin: groupsToRoleUsers(groups, GroupProfileRoleEnum.NA),
      viewer: groupsToRoleUsers(groups, GroupProfileRoleEnum.NV),
    };
  }, [groups]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const detailFields = [
      Field({
        caption: t(`${I18nNamespace.NODE}:columns.code`),
        getProperty: (node: Node) => node.code,
        key: 'code',
      }),
      Field({
        caption: t(`${I18nNamespace.NODE}:columns.name`),
        getProperty: (node: Node) => node.name,
        key: 'name',
      }),
      Field({
        caption: t(`${I18nNamespace.NODE}:columns.nodeStatus`),
        getProperty: (node: Node) => node.nodeStatus,
        key: 'nodeStatus',
      }),
      Field({
        caption: t(
          `${I18nNamespace.NODE}:columns.ticketingSystemEmail`,
        ) as string,
        getProperty: (node: Node) => node.ticketingSystemEmail,
        key: 'ticketingSystemEmail',
      }),
    ];
    const httpsDtrFields = httpsDtrEnabled
      ? [
          Field({
            caption: t(
              `${I18nNamespace.NODE}:columns.objectStorageUrl`,
            ) as string,
            getProperty: (node: Node) => node.objectStorageUrl,
            key: 'objectStorageUrl',
          }),
          Field({
            caption: t(`${I18nNamespace.NODE}:columns.oauth2Client`),
            getProperty: (node: Node) => {
              const client = clients.find(
                (client: OAuth2Client) => client.id === node.oauth2Client,
              );
              return client
                ? `${client.clientName} (${client.redirectUri})`
                : '';
            },
            key: 'client',
          }),
        ]
      : [];
    const model: ListModel<Node> = {
      fields: [...detailFields, ...httpsDtrFields],
    };
    return formatItemFields<Node>(model, node);
  }, [t, node, clients]);

  const userRoleEntries = useUserRoleEntries(roles, I18nNamespace.NODE);

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={node.name}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <Description entries={detailEntries} labelWidth={300} />
      {isFetching ? (
        <CircularProgress />
      ) : (
        <Description entries={userRoleEntries} labelWidth={300} />
      )}
    </Dialog>
  );
};
