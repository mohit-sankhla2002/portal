import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_USERS } from '../../actions/actionTypes';
import { User } from '../../api/generated';
import { generatedBackendApi } from '../../api/api';

import {
  BaseReducerState,
  EnhancedTable,
  getFilenameWithTimestamp,
  requestAction,
  ToastBarSnackBar,
  ToastSeverity,
  useDialogState,
  useFormDialog,
} from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { useUserColumns } from './AdministrationHooks';
import { FormUser, UserForm } from './UserManageForm';
import { FormAddLocalUser, UserAddLocalForm } from './UserAddLocalForm';
import { isStaff } from '../selectors';
import { Trans, useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { RootState } from '../../store';
import { UserDetail } from './UserDetail';

export const UserList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    itemList: users,
    isFetching,
    isSubmitting,
  } = useSelector<RootState, BaseReducerState<IdRequired<User>>>(
    (state) => state.users,
  );

  const staff = useSelector(isStaff);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_USERS, { includeInactive: true }));
  }, [dispatch]);

  const { closeFormDialog, data, openFormDialog } =
    useFormDialog<FormUser>(undefined);
  const {
    closeFormDialog: closeAddLocalFormDialog,
    data: addLocalData,
    openFormDialog: openAddLocalFormDialog,
  } = useFormDialog<FormAddLocalUser>({});
  const userForm = useMemo(() => {
    if (data) {
      return <UserForm user={data} onClose={closeFormDialog} />;
    }
    if (addLocalData) {
      return (
        <UserAddLocalForm
          user={addLocalData}
          onClose={closeAddLocalFormDialog}
        />
      );
    }
  }, [data, closeFormDialog, addLocalData, closeAddLocalFormDialog]);

  const columns = useUserColumns();

  const exportUsersCsv = useCallback(() => {
    setExporting(true);
    const link = document.createElement('a');
    link.target = '_blank';
    link.download = getFilenameWithTimestamp('users', '.csv');
    generatedBackendApi
      .listExportUsersCSVs()
      .then((text) => {
        link.href = window.URL.createObjectURL(
          new Blob([text], {
            type: 'text/csv',
          }),
        );
        link.click();
        URL.revokeObjectURL(link.href);
      })
      .finally(() => setExporting(false));
  }, []);

  const { item, setItem, onClose, open } = useDialogState<User>();
  const [exporting, setExporting] = useState<boolean>(false);

  return (
    <>
      {item && <UserDetail user={item} onClose={onClose} open={open} />}
      <ToastBarSnackBar severity={ToastSeverity.SUCCESS} open={exporting}>
        <span>
          <Trans
            i18nKey={t(`${I18nNamespace.USER_LIST}:exporting`)}
            components={{
              break: <br />,
            }}
          />
        </span>
      </ToastBarSnackBar>
      <EnhancedTable<Required<User>>
        itemList={users}
        columns={columns}
        canAdd={staff}
        canEdit={staff}
        canDelete={false} // it's forbidden to delete users
        onAdd={openAddLocalFormDialog}
        onRowClick={setItem}
        onEdit={openFormDialog}
        canExport={true}
        isExporting={exporting}
        onExport={exportUsersCsv}
        form={userForm}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.user_plural`),
        })}
        addButtonLabel={t(`${I18nNamespace.USER_LIST}:addButton`)}
        inline
      />
    </>
  );
};
