import React, { ReactElement } from 'react';
import {
  ConfirmLabel,
  email,
  FormDialog,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { User } from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { ADD_LOCAL_USER, CALL } from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { RootState } from '../../store';

export type FormAddLocalUser = Pick<
  Partial<User>,
  'firstName' | 'lastName' | 'email'
>;

type UserFormProps = {
  user: FormAddLocalUser;
  onClose: () => void;
};

export const UserAddLocalForm = ({
  user,
  onClose,
}: UserFormProps): ReactElement => {
  const dispatch = useDispatch();

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.users.isSubmitting,
  );

  const form = useEnhancedForm<User>();

  function submit(user: User) {
    dispatch(
      requestAction(
        ADD_LOCAL_USER,
        { user },
        { type: CALL, callback: onClose },
      ),
    );
  }

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueUser({ uniqueUser: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title="Add local user"
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <LabelledField
            name="firstName"
            type="text"
            label="First name"
            fullWidth
            validations={required}
          />
          <LabelledField
            name="lastName"
            type="text"
            label="Last name"
            fullWidth
            validations={required}
          />
          <LabelledField
            name="email"
            type="email"
            label="Email address"
            validations={[required, email]}
            unique={uniqueCheck}
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
