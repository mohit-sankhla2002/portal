import React, { ReactElement, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LOAD_DATA_PROVIDERS } from '../../actions/actionTypes';
import { DataProvider } from '../../api/generated';
import { IdRequired } from '../../global';
import {
  useDataProviderColumns,
  useDataProviderForm,
} from './AdministrationHooks';
import { canSeeNodeTab, isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  BaseReducerState,
  EnhancedTable,
  requestAction,
  useDialogState,
} from '@biomedit/next-widgets';
import { DataProviderDetail } from './DataProviderDetail';
import { RootState } from '../../store';
import { AuthState } from '../../reducers/auth';

export declare type DataProviderListProps = {
  selected?: string;
};

export const DataProviderList = (
  props: DataProviderListProps,
): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_PROVIDER,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: dataProviders,
  } = useSelector<RootState, BaseReducerState<IdRequired<DataProvider>>>(
    (state) => state.dataProvider,
  );
  const staff = useSelector(isStaff);
  const staffOrNodePersonnel = useSelector(canSeeNodeTab);
  const auth = useSelector<RootState, AuthState>((state) => state.auth);
  const username = auth.user?.username;
  const dataProviderAdminFor = auth.user?.manages?.dataProviderAdmin?.map(
    (dp) => dp.id,
  );
  const canEdit = useCallback(
    (dp) => {
      return (
        staff ||
        (dp && dataProviderAdminFor
          ? dataProviderAdminFor.includes(dp.id)
          : false)
      );
    },
    [staff, dataProviderAdminFor],
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (staffOrNodePersonnel) {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    } else {
      dispatch(requestAction(LOAD_DATA_PROVIDERS, { username }));
    }
  }, [dispatch, username, staffOrNodePersonnel]);

  const { openFormDialog, dataProviderForm } = useDataProviderForm();
  const { item, setItem, onClose, open } = useDialogState<DataProvider>();
  const columns = useDataProviderColumns();

  useEffect(() => {
    if (props.selected) {
      const dataTransferSelected = dataProviders.find(
        (dataProvider) => dataProvider.code === props.selected,
      );
      if (dataTransferSelected) {
        setItem(dataTransferSelected);
      }
    }
  }, [dataProviders, props.selected, setItem]);

  return (
    <>
      {item && <DataProviderDetail dp={item} onClose={onClose} open={open} />}

      <EnhancedTable<Required<DataProvider>>
        itemList={dataProviders}
        columns={columns}
        canAdd={staff}
        canEdit={canEdit}
        canDelete={false} // it's forbidden to delete data providers
        onEdit={openFormDialog}
        onAdd={openFormDialog}
        onRowClick={setItem}
        addButtonLabel={t(`${I18nNamespace.DATA_PROVIDER}:addButton`) as string}
        form={dataProviderForm}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.dataProvider_plural`),
          }) as string
        }
        inline
      />
    </>
  );
};
