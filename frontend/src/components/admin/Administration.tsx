import React, { ReactElement, ReactNode, useMemo } from 'react';
import { UserList } from './UserManage';
import { GroupList } from './GroupManage';
import { DataProviderList } from './DataProviderManage';
import { FlagManageList } from './FlagManage';
import { NodeList } from './NodeManage';
import { useSelector } from 'react-redux';
import {
  canSeeDataProviderTab,
  canSeeGroupTab,
  canSeeNodeTab,
  canSeeUsersTab,
  isStaff,
} from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { PgpKeyInfoList } from './PgpKeyInfoManage';
import { TabItem, TabPane } from '@biomedit/next-widgets';

export declare type AdministrationProps = {
  tabCode?: string;
  entityCode?: string;
  children?: ReactNode;
};

export const Administration = (props: AdministrationProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.ADMINISTRATION);
  const staff = useSelector(isStaff);
  const showUsersTab = useSelector(canSeeUsersTab);
  const showNodeTab = useSelector(canSeeNodeTab);
  const showDataProviderTab = useSelector(canSeeDataProviderTab);
  const showGroupTab = useSelector(canSeeGroupTab);

  const tabItems = useMemo(() => {
    const items: TabItem[] = [];
    if (showUsersTab) {
      items.push({
        id: 'users',
        label: t('tabs.users'),
        content: <UserList />,
      });
    }
    if (showGroupTab) {
      items.push({
        id: 'groups',
        label: t('tabs.groups'),
        content: <GroupList />,
      });
    }
    if (showNodeTab) {
      items.push({
        id: 'nodes',
        label: t('tabs.nodes'),
        content: <NodeList />,
      });
    }
    if (showDataProviderTab) {
      items.push({
        id: 'dataProviders',
        label: t('tabs.dataProviders'),
        content: <DataProviderList selected={props.entityCode} />,
      });
    }
    if (staff) {
      items.push({
        id: 'flags',
        label: t('tabs.flags'),
        content: <FlagManageList />,
      });
    }
    if (staff) {
      items.push({
        id: 'keys',
        label: t('tabs.pgpKeyInfos'),
        content: <PgpKeyInfoList />,
      });
    }
    return items;
  }, [
    staff,
    showGroupTab,
    showNodeTab,
    showDataProviderTab,
    showUsersTab,
    t,
    props.entityCode,
  ]);

  return (
    <TabPane
      id="Administration"
      label={t('label')}
      model={tabItems}
      selected={props.tabCode}
    />
  );
};
