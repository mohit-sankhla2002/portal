import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import {
  expectQueryParameter,
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { GroupForm } from './GroupManageForm';
import {
  createBatch,
  createGroup,
  createObjectByPermission,
  createPermission,
  createPermissionObject,
  createSnakeCased,
  createStaffAuthState,
  createUser,
} from '../../../factories';
import { GroupUsersInner } from '../../api/generated';

describe('GroupManageForm', () => {
  const listObjectByPermissionParam = 'perm_id';
  const viewNodePermissionId = 44;
  const userId = 3;
  const groupId = 5;
  const listPermissions = `${backend}identity/permission/`;
  let listObjectByPermission: string;
  const timeout = 10000;

  const server: MockServer = setupMockApi();
  const permissionName = 'Can view node';
  const permissionObjectName = 'Node 1 (node_1)';
  const user = createSnakeCased(createUser)({
    id: userId,
  }) as GroupUsersInner;

  const createServer = (
    delayListUsers: number,
    delayListPerm: number,
    delayListObjectByPerm: number,
  ): void => {
    listObjectByPermission = `${backend}identity/object_by_permission/`;
    const permission = createPermission({
      id: viewNodePermissionId,
      name: permissionName,
    });
    const permissions = [permission, ...createBatch(createPermission, 9)];

    server.use(
      rest.get(listPermissions, (req, res, ctx) =>
        res(ctx.delay(delayListPerm), resJson(ctx), resBody(ctx, permissions)),
      ),
      rest.get(listObjectByPermission, (req, res, ctx) => {
        expectQueryParameter(
          req,
          listObjectByPermissionParam,
          String(viewNodePermissionId),
        );
        return res(
          ctx.delay(delayListObjectByPerm),
          resJson(ctx),
          resBody(
            ctx,
            createObjectByPermission(permission.id, [
              createPermissionObject({
                id: 1,
                name: permissionObjectName,
              }),
            ]),
          ),
        );
      }),
    );

    // msw only needs the path
    listObjectByPermission += `?${listObjectByPermissionParam}=${viewNodePermissionId}`;
  };

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    delayListUsers | delayListPerm | delayListObjectByPerm
    ${0}           | ${0}          | ${0}
    ${500}         | ${500}        | ${500}
    ${2000}        | ${2000}       | ${2000}
    ${2000}        | ${0}          | ${0}
    ${0}           | ${2000}       | ${0}
    ${0}           | ${0}          | ${2000}
    ${2000}        | ${2000}       | ${0}
    ${0}           | ${2000}       | ${2000}
    ${2000}        | ${0}          | ${2000}
  `(
    'should contain both permission and object when ' +
      'call to list users is delayed by $delayListUsers ms and ' +
      'call to list permissions is delayed by $delayListPerm ms and ' +
      'call to list object by permission is delayed by $delayListObjectByPerm ms',
    async ({ delayListUsers, delayListPerm, delayListObjectByPerm }) => {
      createServer(delayListUsers, delayListPerm, delayListObjectByPerm);
      const verifier = new RequestVerifier(server);

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createStaffAuthState(),
          })}
        >
          <GroupForm
            group={createGroup({
              permissionsObject: [
                {
                  permission: viewNodePermissionId,
                  objects: [1],
                },
              ],
              users: [user],
              id: groupId,
            })}
            onClose={jest.fn()}
          />
        </Provider>,
      );
      // Wait for all API calls to be finished (no more spinners are visible, text-/combobox all visible)
      await waitFor(
        () => {
          const boxes = screen.getAllByRole('combobox');
          expect(boxes).toHaveLength(4);
          expect(screen.getAllByRole('textbox')).toHaveLength(3);
          expect(boxes[2]).toHaveValue(permissionName);
        },
        { timeout },
      );
      await screen.findByText(permissionObjectName);

      verifier.assertCount(2);
      verifier.assertCalled(listPermissions, 1);
      verifier.assertCalled(listObjectByPermission, 1);
    },
    timeout,
  );
});
