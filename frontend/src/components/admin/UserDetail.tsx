import React, { ReactElement, useEffect, useState } from 'react';
import {
  CancelLabel,
  Description,
  Dialog,
  formatDate,
  requestAction,
} from '@biomedit/next-widgets';
import { useUserInfoFields } from '../Profile';
import { useTranslation } from 'next-i18next';
import { CustomAffiliation, User } from '../../api/generated';
import {
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_FLAGS,
  LOAD_GROUPS,
} from '../../actions/actionTypes';
import { I18nNamespace } from '../../i18n';
import { batch, useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { generatedBackendApi } from '../../api/api';

export type UserDetailProps = {
  user: User;
  open: boolean;
  onClose: () => void;
};

export const UserDetail = ({
  user,
  open,
  onClose,
}: UserDetailProps): ReactElement => {
  const dispatch = useDispatch();

  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.USER_MANAGE_FORM,
  ]);

  const customAffiliations = useSelector<RootState, Array<CustomAffiliation>>(
    (state) => state.customAffiliation.itemList,
  );
  const noProjectsPlaceholder = t(
    `${I18nNamespace.USER_MANAGE_FORM}:noProjectsPlaceholder`,
  );
  const [projects, setProjects] = useState<string>(noProjectsPlaceholder);
  const userInfoEntries = useUserInfoFields(user, customAffiliations);

  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_LIST}:columns.lastLogin`) as string,
    component: formatDate(user.lastLogin, true),
  });

  useEffect(() => {
    generatedBackendApi
      .listProjects({
        username: user.username,
        ordering: 'name',
      })
      .then((userProjects) => {
        setProjects(
          userProjects?.length
            ? userProjects.map((project) => project.code).join(', ')
            : noProjectsPlaceholder,
        );
      });
  }, [noProjectsPlaceholder, user.username, projects]);

  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.projects`) as string,
    component: projects,
  });

  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.flags`) as string,
    component: user.flags?.length
      ? user.flags?.map((f) => f).join(', ')
      : t(`${I18nNamespace.USER_MANAGE_FORM}:noFlagsPlaceholder`),
  });

  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.groups`) as string,
    component: user.groups?.length
      ? user.groups?.map((g) => g).join(', ')
      : t(`${I18nNamespace.USER_MANAGE_FORM}:noGroupsPlaceholder`),
  });

  userInfoEntries.push({
    caption: t(`${I18nNamespace.USER_LIST}:columns.isActive`) as string,
    component: t(`${I18nNamespace.BOOLEAN}:${user.isActive}`),
  });

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_FLAGS));
      dispatch(requestAction(LOAD_GROUPS));
      dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
    });
  }, [dispatch, user.username]);

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={`User ${user.id}`}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <Description entries={userInfoEntries} labelWidth="25%" />
    </Dialog>
  );
};
