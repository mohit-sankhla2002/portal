import { render, renderHook, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import { Group, Permission } from '../../api/generated';
import {
  expectQueryParameter,
  initialBaseReducerState,
  MockServer,
  RequestVerifier,
  resBody,
  resJson,
  setupMockApi,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { FormProvider } from 'react-hook-form';
import { PermissionsObjectList } from './PermissionsObjectList';
import {
  createBatch,
  createGroup,
  createObjectByPermission,
  createPermission,
  createPermissionObject,
  createStaffAuthState,
} from '../../../factories';
import { IdRequired } from '../../global';

describe('PermissionsObjectList', () => {
  const listObjectByPermissionParam = 'perm_id';
  const viewNodePermissionId = 44;
  let listObjectByPermission;
  const permissionObjectName = 'Node 1 (node_1)';

  const server: MockServer = setupMockApi();

  const createServer = (delayListObjectByPerm: number): void => {
    listObjectByPermission = `${backend}identity/object_by_permission/`;

    server.use(
      rest.get(listObjectByPermission, (req, res, ctx) => {
        expectQueryParameter(
          req,
          listObjectByPermissionParam,
          String(viewNodePermissionId),
        );
        return res(
          ctx.delay(delayListObjectByPerm),
          resJson(ctx),
          resBody(
            ctx,
            createObjectByPermission(viewNodePermissionId, [
              createPermissionObject({
                id: 1,
                name: permissionObjectName,
              }),
            ]),
          ),
        );
      }),
    );

    // msw only needs the path
    listObjectByPermission += `?${listObjectByPermissionParam}=${viewNodePermissionId}`;
  };

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    delayListObjectByPerm
    ${0}
    ${500}
    ${2000}
  `(
    'should contain both permission and object when ' +
      'call to list object by permission is delayed by $delayListObjectByPerm ms',
    async ({ delayListObjectByPerm }) => {
      createServer(delayListObjectByPerm);
      const verifier = new RequestVerifier(server);

      const { result } = renderHook(() =>
        useEnhancedForm<Group>({
          defaultValues: createGroup({
            permissionsObject: [
              { permission: viewNodePermissionId, objects: [1] },
            ],
          }),
        }),
      );
      const form = result.current;
      const permissionName = 'Can view node';

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createStaffAuthState(),
            permissions: {
              ...initialBaseReducerState<IdRequired<Permission>>(),
              itemList: [
                createPermission({
                  id: viewNodePermissionId,
                  name: permissionName,
                }),
                ...createBatch(createPermission, 9),
              ],
            },
          })}
        >
          <FormProvider {...form}>
            <PermissionsObjectList />
          </FormProvider>
        </Provider>,
      );

      // wait for all API calls to be finished (no more spinners are visible, comboboxes all visible)
      const comboboxes = await waitFor(
        () => {
          const boxes = screen.getAllByRole('combobox');
          expect(boxes).toHaveLength(2);
          return boxes;
        },
        { timeout: 5000 },
      );

      expect(comboboxes[0]).toHaveValue(permissionName);
      await screen.findByText(permissionObjectName);

      verifier.assertCount(1);
      verifier.assertCalled(listObjectByPermission, 1);
    },
  );
});
