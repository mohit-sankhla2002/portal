import React from 'react';
import {
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { render, screen, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { backend } from '../../api/api';
import { DataProviderList } from './DataProviderManage';
import { DataProviderDetail } from './DataProviderDetail';
import {
  createDataProvider,
  createDataProviderAdminAuthState,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../../factories';

jest.mock('./DataProviderDetail', () => {
  const originalModule = jest.requireActual('./DataProviderDetail');

  return {
    __esModule: true,
    ...originalModule,
    DataProviderDetail: jest.fn((props) => `${props?.dp.id}`),
  };
});

describe('DataProviderList', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it('should display empty list', async () => {
    server.use(
      rest.get(`${backend}data-provider/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [])),
      ),
    );
    render(
      <Provider store={makeStore()}>
        <DataProviderList />
      </Provider>,
    );
    await screen.findByText('list:emptyMessage');
  });

  const userDataProvider = createDataProvider({ id: 1, code: 'exists' });
  const otherDataProvider = createDataProvider({ id: 2 });
  const dataProviderAuthState = createDataProviderAdminAuthState([
    userDataProvider,
  ]);

  it.each`
    userState                     | userType                 | expectedRows | expectedEditButtons
    ${createStaffAuthState()}     | ${'staff'}               | ${2}         | ${2}
    ${createNodeAdminAuthState()} | ${'node admin'}          | ${2}         | ${0}
    ${dataProviderAuthState}      | ${'data provider admin'} | ${1}         | ${1}
  `(
    'should list $expectedRows data providers and $expectedEditButtons edit buttons if user is $userType',
    async ({ userState, expectedRows, expectedEditButtons }) => {
      server.use(
        rest.get(`${backend}data-provider/`, (req, res, ctx) => {
          return res(
            resJson(ctx),
            resBody(
              ctx,
              req.url.searchParams.get('username') ===
                dataProviderAuthState.user?.username
                ? [userDataProvider]
                : [userDataProvider, otherDataProvider],
            ),
          );
        }),
      );
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: userState,
          })}
        >
          <DataProviderList />
        </Provider>,
      );
      // Table header (1x), plus user rows, plus table footer (1x)
      expect(await screen.findAllByRole('row')).toHaveLength(expectedRows + 2);
      // Only privileged users should be able to edit rows
      await waitFor(() =>
        expect(
          screen.queryAllByRole('button', {
            name: 'Edit dataProvider:addButton',
          }),
        ).toHaveLength(expectedEditButtons),
      );
    },
  );

  afterEach(() => {
    jest.clearAllMocks();
  });

  it.each`
    props                             | output       | description
    ${{}}                             | ${undefined} | ${'nothing selected'}
    ${{ selected: 'exists' }}         | ${'1'}       | ${'existing object'}
    ${{ selected: 'does_not_exist' }} | ${undefined} | ${'non-existing object'}
  `('$description', async ({ props, output }) => {
    server.use(
      rest.get(`${backend}data-provider/`, (req, res, ctx) => {
        return res(
          resJson(ctx),
          resBody(ctx, [userDataProvider, otherDataProvider]),
        );
      }),
    );
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createStaffAuthState(),
        })}
      >
        <DataProviderList {...props} />
      </Provider>,
    );
    await waitFor(() => {
      if (output) {
        expect(DataProviderDetail).toHaveReturnedWith(output);
      } else {
        expect(DataProviderDetail).not.toHaveReturned();
      }
    });
  });
});
