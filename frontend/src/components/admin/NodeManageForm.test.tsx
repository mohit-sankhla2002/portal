import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import React from 'react';
import {
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { rest } from 'msw';
import { backend } from '../../api/api';
import { NodeForm } from './NodeManageForm';
import {
  createAuthState,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../../factories';

jest.mock('../../config', () => {
  const originalModule = jest.requireActual('../../config');

  return {
    __esModule: true,
    ...originalModule,
    httpsDtrEnabled: true,
  };
});

describe('NodeManageForm', () => {
  const server: MockServer = setupMockApi();

  beforeEach(() => {
    server.use(
      rest.post(`${backend}node/unique/`, (req, res, ctx) => {
        return res(resJson(ctx), resBody(ctx, {}));
      }),
      rest.get(`${backend}oauth2-clients/`, (req, res, ctx) => {
        return res(resJson(ctx), resBody(ctx, []));
      }),
    );
  });

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  it.each`
    authState                     | enabled  | userType
    ${createStaffAuthState()}     | ${true}  | ${'Portal admin'}
    ${createNodeAdminAuthState()} | ${true}  | ${'node admin'}
    ${createAuthState()}          | ${false} | ${'regular user'}
  `(
    'Should allow ($enabled) to edit the OAuth2 client field if the user is a $userType',
    async ({ authState, enabled }) => {
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: authState,
          })}
        >
          <NodeForm node={{}} onClose={jest.fn()} />
        </Provider>,
      );

      const clientField = await screen.findByLabelText(
        'node:columns.oauth2Client',
      );
      expect(clientField).toBeInTheDocument();
      if (!enabled) {
        expect(clientField).toHaveAttribute('aria-disabled');
      } else {
        expect(clientField).not.toHaveAttribute('aria-disabled');
      }
    },
  );
});
