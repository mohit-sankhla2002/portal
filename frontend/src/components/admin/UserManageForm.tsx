import React, { ReactElement, useEffect } from 'react';
import { Required } from 'utility-types';
import {
  AutocompleteField,
  BaseReducerState,
  CheckboxField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { CustomAffiliation, Flag, Group, User } from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  CALL,
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_FLAGS,
  LOAD_GROUPS,
  UPDATE_USER,
} from '../../actions/actionTypes';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  useCustomAffiliationChoices,
  useFlagChoices,
  useGroupChoices,
} from '../choice';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { RootState } from '../../store';
import { IdRequired } from '../../global';

export type FormUser = Required<Partial<User>, 'id'>;

type UserFormProps = {
  user: FormUser;
  onClose: () => void;
};

export const UserForm = ({ user, onClose }: UserFormProps): ReactElement => {
  const dispatch = useDispatch();
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.USER_MANAGE_FORM,
  ]);

  const { isFetching: isFetchingFlags, itemList: flags } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Flag>>
  >((state) => state.flags);

  const { isFetching: isFetchingGroups, itemList: groups } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Group>>
  >((state) => state.groups);

  const {
    isFetching: isFetchingCustomAffiliations,
    itemList: customAffiliations,
  } = useSelector<RootState, BaseReducerState<IdRequired<CustomAffiliation>>>(
    (state) => state.customAffiliation,
  );

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.users.isSubmitting,
  );
  const currentUsername = useSelector<RootState, string | undefined>(
    (state) => state.auth.user?.username,
  );

  const form = useEnhancedForm<User>();

  function submit(formUser: User) {
    dispatch(
      requestAction(
        UPDATE_USER,
        { user: formUser, id: String(formUser.id) },
        { type: CALL, callback: onClose },
      ),
    );
  }

  useEffect(() => {
    batch(() => {
      dispatch(requestAction(LOAD_FLAGS));
      dispatch(requestAction(LOAD_GROUPS));
      dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
    });
  }, [dispatch]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={t(`${I18nNamespace.USER_MANAGE_FORM}:title`)}
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: user.id }} />
        <FormFieldsContainer>
          <AutocompleteField
            name="profile.customAffiliation"
            label={t(`${I18nNamespace.USER_INFO}:custom_affiliation`)}
            initialValues={user}
            choices={useCustomAffiliationChoices(customAffiliations)}
            isLoading={isFetchingCustomAffiliations}
            width="100%"
          />
          <SelectField
            name="flags"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.flags`)}
            initialValues={user}
            multiple
            choices={useFlagChoices(flags)}
            isLoading={isFetchingFlags}
          />
          <SelectField
            name="groups"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.groups`)}
            initialValues={user}
            multiple
            choices={useGroupChoices(groups)}
            isLoading={isFetchingGroups}
          />
          <CheckboxField
            name="isActive"
            label={t(`${I18nNamespace.USER_LIST}:columns.isActive`)}
            initialValues={user}
            disabled={currentUsername === user.username}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
