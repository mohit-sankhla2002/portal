import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import {
  BaseReducerState,
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  required,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
  httpsUrl,
} from '@biomedit/next-widgets';
import { Node, NodeNodeStatusEnum, OAuth2Client } from '../../api/generated';
import {
  ADD_NODE,
  CALL,
  UPDATE_NODE,
  LOAD_OAUTH2_CLIENTS,
} from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { RootState } from '../../store';
import { isNodeAdmin, isStaff } from '../selectors';
import { httpsDtrEnabled } from '../../config';
import { IdRequired } from '../../global';
import { useOAuth2ClientChoices } from '../choice';

type NodeFormProps = {
  node: Partial<Node>;
  onClose: () => void;
};

export const NodeForm = ({ node, onClose }: NodeFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE]);

  const isEdit = !!node.id;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_OAUTH2_CLIENTS));
  }, [dispatch]);

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.nodes.isSubmitting,
  );

  const staff = useSelector(isStaff);
  const nodeAdmin = useSelector(isNodeAdmin);
  const { itemList: clients } = useSelector<
    RootState,
    BaseReducerState<IdRequired<OAuth2Client>>
  >((store) => store.clients);

  const form = useEnhancedForm<Node>();

  function submit(node: Node) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_NODE,
          {
            id: String(node.id),
            node,
          },
          onSuccessAction,
        ),
      );
    } else {
      dispatch(
        requestAction(
          ADD_NODE,
          {
            node,
          },
          onSuccessAction,
        ),
      );
    }
  }

  const uniqueCheck = async (value) =>
    generatedBackendApi.uniqueNode({ uniqueNode: value });

  const clientsChoices = useOAuth2ClientChoices(clients);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Node: ${node?.code}` : 'New Node'}
        open={!!node}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: node.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.NODE}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
            disabled={isEdit}
          />
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.NODE}:columns.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
          />
          <SelectField
            name="nodeStatus"
            label={t(`${I18nNamespace.NODE}:columns.nodeStatus`)}
            initialValues={node}
            required
            choices={useEnumChoices(NodeNodeStatusEnum)}
          />
          <LabelledField
            name="ticketingSystemEmail"
            type="email"
            label={t(`${I18nNamespace.NODE}:columns.ticketingSystemEmail`)}
            validations={[required, email]}
            initialValues={node}
            fullWidth
          />
          {httpsDtrEnabled && (
            <>
              <LabelledField
                name="objectStorageUrl"
                type="text"
                validations={[httpsUrl]}
                label={t(`${I18nNamespace.NODE}:columns.objectStorageUrl`)}
                initialValues={node}
                fullWidth
                disabled={!(staff || nodeAdmin)}
              />
              <SelectField
                name="oauth2Client"
                label={t(`${I18nNamespace.NODE}:columns.oauth2Client`)}
                initialValues={node}
                fullWidth
                disabled={!(staff || nodeAdmin)}
                choices={clientsChoices}
              />
            </>
          )}
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
