import {
  Group,
  GroupProfileRoleEnum,
  GroupUsersInner,
} from '../../api/generated';
import { groupsToRoleUsers } from './utils';

describe('groupsToRoleUsers', () => {
  const users: GroupUsersInner[] = [{ id: 1 }, { id: 2 }, { id: 3 }];
  // Define some groups as input for the method to test
  const groups: Group[] = [
    {
      name: 'Node Viewer',
      profile: { role: GroupProfileRoleEnum.NV },
      users: [],
    },
    {
      name: 'Node Admin',
      profile: { role: GroupProfileRoleEnum.NA },
      // This group has two users
      users: users.slice(0, 2),
    },
  ];

  it.each`
    groups
    ${[]}
  `('Should return an empty array if groups is $groups', ({ groups }) => {
    expect(groupsToRoleUsers(groups, GroupProfileRoleEnum.NA)).toStrictEqual(
      [],
    );
  });

  // Parameterized test
  it.each`
    role                       | expected
    ${GroupProfileRoleEnum.NV} | ${[]}
    ${GroupProfileRoleEnum.NA} | ${users.slice(0, 2)}
    ${GroupProfileRoleEnum.NM} | ${[]}
  `('Should return $expected if given role is $role', ({ role, expected }) => {
    expect(groupsToRoleUsers(groups, role)).toStrictEqual(expected);
  });
});
