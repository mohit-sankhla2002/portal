import React, { ReactElement, useEffect, useMemo } from 'react';
import {
  BaseReducerState,
  CancelLabel,
  Description,
  Dialog,
  Field,
  formatItemFields,
  FormattedItemField,
  ListModel,
  requestAction,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { CircularProgress } from '@mui/material';
import { DataProvider, Group, GroupProfileRoleEnum } from '../../api/generated';
import { I18nNamespace } from '../../i18n';
import { RoleMap, useUserRoleEntries } from '../../widgets/UserRoleEntries';
import { getDataProviderColoredStatus } from './AdministrationHooks';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { IdRequired } from '../../global';
import { LOAD_GROUPS } from '../../actions/actionTypes';
import { groupsToRoleUsers } from './utils';

export type DataProviderDetailProps = {
  dp: DataProvider;
  open: boolean;
  onClose: () => void;
};

export const DataProviderDetail = ({
  dp,
  open,
  onClose,
}: DataProviderDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.DATA_PROVIDER]);
  const { itemList: groups, isFetching } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Group>>
  >((store) => store.groups);
  const dispatch = useDispatch();

  useEffect(() => {
    if (dp?.id) {
      dispatch(
        requestAction(LOAD_GROUPS, {
          roleEntityId: String(dp.id),
          roleEntityType: 'dataprovider',
        }),
      );
    }
  }, [dp, dispatch]);

  const roles: RoleMap = useMemo(() => {
    return {
      technicalAdmin: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPTA),
      viewer: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPW),
      coordinator: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPC),
      dataEngineer: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPDE),
      securityOfficer: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPSO),
      manager: groupsToRoleUsers(groups, GroupProfileRoleEnum.DPM),
    };
  }, [groups]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataProvider> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.code`) as string,
          getProperty: (dp: DataProvider) => dp.code,
          key: 'code',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.name`) as string,
          getProperty: (dp: DataProvider) => dp.name,
          key: 'name',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.node`) as string,
          getProperty: (dp: DataProvider) => dp.node,
          key: 'node',
        }),
        Field({
          caption: t(
            `${I18nNamespace.DATA_PROVIDER}:columns.enabled`,
          ) as string,
          getProperty: (dp: DataProvider) =>
            getDataProviderColoredStatus(dp.enabled),
          key: 'enabled',
        }),
      ],
    };
    return formatItemFields<DataProvider>(model, dp);
  }, [t, dp]);

  const userRoleEntries = useUserRoleEntries(
    roles,
    I18nNamespace.DATA_PROVIDER,
  );

  return (
    <Dialog
      open={open}
      isSubmitting={false}
      fullWidth={true}
      maxWidth={'lg'}
      title={dp.name}
      cancelLabel={CancelLabel.CLOSE}
      onClose={onClose}
    >
      <Description entries={detailEntries} labelWidth={300} />
      {isFetching ? (
        <CircularProgress />
      ) : (
        <Description entries={userRoleEntries} labelWidth={300} />
      )}
    </Dialog>
  );
};
