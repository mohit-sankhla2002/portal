import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestAction, TooltipText } from '@biomedit/next-widgets';
import { LOAD_DATA_PROVIDERS } from '../../actions/actionTypes';
import { RootState } from '../../store';
import { IdRequired } from '../../global';
import { DataProvider } from '../../api/generated';

type DataProviderFieldProps = { code?: string };

export function DataProviderField({
  code,
}: DataProviderFieldProps): ReactElement {
  const dataProviders = useSelector<RootState, Array<IdRequired<DataProvider>>>(
    (state) => state.dataProvider.itemList,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (!dataProviders.length) {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    }
  }, [dataProviders, dispatch]);

  const dataProvider = dataProviders.find(
    (dataProvider) => dataProvider.code === code,
  );
  if (dataProvider) {
    return <TooltipText title={dataProvider.name} text={dataProvider.code} />;
  } else {
    return <>{code}</>;
  }
}
