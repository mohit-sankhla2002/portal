import { DataTransfer } from '../../api/generated';
import React, { ReactElement, useMemo } from 'react';

const useName = (
  requestorFirstName: string | undefined,
  requestorLastName: string | undefined,
): string => {
  return useMemo<string>(
    () => `${requestorFirstName} ${requestorLastName}`,
    [requestorFirstName, requestorLastName],
  );
};

export function RequestorField({
  requestorFirstName,
  requestorLastName,
  requestorEmail,
  requestorDisplayId,
}: Pick<
  DataTransfer,
  | 'requestorFirstName'
  | 'requestorLastName'
  | 'requestorEmail'
  | 'requestorDisplayId'
>): ReactElement {
  return (
    <span>
      {useName(requestorFirstName, requestorLastName)}
      {requestorDisplayId ? ` (${requestorDisplayId})` : ''} ({requestorEmail})
    </span>
  );
}

export function RequestorTableField({
  requestorFirstName,
  requestorLastName,
}: Pick<
  DataTransfer,
  'requestorFirstName' | 'requestorLastName'
>): ReactElement {
  return <span>{useName(requestorFirstName, requestorLastName)}</span>;
}
