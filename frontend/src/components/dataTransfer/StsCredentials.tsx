import {
  CopyToClipboardPasswordField,
  CopyToClipboardSnackbar,
  CopyToClipboardTextField,
  Description,
  Field,
  formatDate,
  formatItemFields,
  FormattedItemField,
  ListModel,
  LoadingButton,
  ToastBarSnackBar,
  ToastSeverity,
  useCopyToClipboardState,
} from '@biomedit/next-widgets';
import { Box, Button, ButtonGroup } from '@mui/material';
import { useTranslation } from 'next-i18next';
import React, { ReactElement, useCallback, useMemo, useState } from 'react';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import {
  Credentials,
  FetchError,
  ResponseError,
  S3Opts,
} from '../../api/generated';

export type CredentialsWithOpts = Credentials & S3Opts;

type StsCredentialsProps = {
  setCredentialsWithOpts: (credentialsWithOpts: CredentialsWithOpts) => void;
  credentialsWithOpts: CredentialsWithOpts;
};

export const StsCredentials = ({
  credentialsWithOpts,
  setCredentialsWithOpts,
}: StsCredentialsProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  const [isFetchingCredentials, setIsFetchingCredentials] =
    useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const fetchCredentials = useCallback(() => {
    setIsFetchingCredentials(true);
    Promise.all([
      generatedBackendApi.s3optsS3Opts({
        id: credentialsWithOpts.dtr.toString(),
      }),
      generatedBackendApi.createCredentials({
        credentials: credentialsWithOpts,
      }),
    ])
      .then(([s3opts, credentials]) => {
        setCredentialsWithOpts({ ...credentials, ...s3opts });
      })
      .catch((error: ResponseError | FetchError | Error) => {
        // 'ResponseError'
        if ('response' in error) {
          const response = error.response;
          const contentType = response.headers
            .get('content-type')
            ?.toLowerCase();
          if (contentType && contentType === 'application/json') {
            response.json().then((json) => {
              setErrorMessage(json['detail']);
            });
          } else {
            setErrorMessage(response.statusText);
          }
        } else {
          // 'FetchError' | 'Error'
          setErrorMessage(error.message);
        }
      })
      .finally(() => {
        setIsFetchingCredentials(false);
      });
  }, [credentialsWithOpts, setCredentialsWithOpts]);

  const onErrorClose = () => {
    setErrorMessage(null);
  };

  const { open, onClose, onClick } = useCopyToClipboardState();

  const entries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<CredentialsWithOpts> = {
      fields: [
        Field({
          caption: t('columns.accessKeyId'),
          key: 'accessKeyId',
          getProperty: (credentials: CredentialsWithOpts) =>
            credentials.accessKeyId,
          render: (accessKeyId: string | undefined) => (
            <CopyToClipboardTextField
              value={accessKeyId}
              id="accessKeyId"
              label="Access Key ID"
              fullWidth
            />
          ),
        }),
        Field({
          caption: t('columns.secretAccessKey'),
          key: 'secretAccessKey',
          getProperty: (credentials: CredentialsWithOpts) =>
            credentials.secretAccessKey,
          render: (secretAccessKey: string | undefined) => (
            <CopyToClipboardPasswordField
              value={secretAccessKey}
              id="secretAccessKey"
              label="Secret Access Key"
              fullWidth
            />
          ),
        }),
        Field({
          caption: t('columns.sessionToken'),
          key: 'sessionToken',
          getProperty: (credentials: CredentialsWithOpts) =>
            credentials.sessionToken,
          render: (sessionToken: string | undefined) => (
            <CopyToClipboardTextField
              value={sessionToken}
              id="sessionToken"
              label="Session Token"
              fullWidth
            />
          ),
        }),
        Field({
          caption: t('columns.expirationDatetime'),
          key: 'expiration',
          getProperty: (credentials: CredentialsWithOpts) =>
            formatDate(credentials.expiration, true, true),
        }),
      ],
    };
    return formatItemFields<CredentialsWithOpts>(model, credentialsWithOpts);
  }, [t, credentialsWithOpts]);

  return (
    <>
      <CopyToClipboardSnackbar open={open} onClose={onClose} />
      <ToastBarSnackBar
        severity={ToastSeverity.ERROR}
        open={errorMessage !== null}
        onClose={onErrorClose}
      >
        <span>{errorMessage}</span>
      </ToastBarSnackBar>
      <Box sx={{ display: 'flex', flexDirection: 'column' }} maxWidth={'70%'}>
        <Description
          entries={entries}
          labelWidth={200}
          styleRules={{
            table: {
              marginBottom: '2rem',
            },
            caption: {
              verticalAlign: 'middle',
            },
          }}
        />
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}
        >
          <ButtonGroup>
            <Button
              onClick={() => onClick(JSON.stringify(credentialsWithOpts))}
              variant="outlined"
              color="primary"
              arial-label={t('copyAll')}
              disabled={credentialsWithOpts.expiration == null}
              title={t('copyAllTitle') as string}
            >
              {t('copyAll')}
            </Button>
            <LoadingButton
              type="button"
              onClick={fetchCredentials}
              color="primary"
              variant="outlined"
              loading={isFetchingCredentials}
            >
              {t('fetchCredentials')}
            </LoadingButton>
          </ButtonGroup>
        </Box>
      </Box>
    </>
  );
};
