import { addExcelSeparatorHeader } from './DataTransferExport';

describe('DataTransferExport', () => {
  describe('addExcelSeparatorHeader', () => {
    it('should add line with excel separator header before actual csv content', () => {
      expect(addExcelSeparatorHeader('Some Text\non a new line.')).toBe(
        'sep=,\nSome Text\non a new line.',
      );
    });
  });
});
