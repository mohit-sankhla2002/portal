import React, { ReactElement, useCallback } from 'react';
import { ButtonGroup, Grid, Stack, Typography } from '@mui/material';
import { I18nNamespace } from '../../i18n';
import { ColoredStatus, formatDate } from '@biomedit/next-widgets';
import Button from '@mui/material/Button';
import { useTranslation } from 'next-i18next';
import { useApprovalStatus } from './DataTransferHooks';
import {
  ApprovalInstitutionEnum,
  DataTransferDataProviderApprovalsInner,
  DataTransferDataProviderApprovalsInnerStatusEnum,
  DataTransferGroupApprovalsInner,
  DataTransferGroupApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInner,
  DataTransferNodeApprovalsInnerStatusEnum,
} from '../../api/generated';
import camelCase from 'lodash/camelCase';

export type ApprovalDialogState = {
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
  action: ApprovalAction;
};
export type DataTransferStatus =
  | DataTransferNodeApprovalsInnerStatusEnum
  | DataTransferDataProviderApprovalsInnerStatusEnum
  | DataTransferGroupApprovalsInnerStatusEnum;

export enum ApprovalAction {
  ACCEPT,
  REJECT,
}

type DataTransferApprovalsStackProps = {
  approvals: Array<DataTransferApproval>;
  approvalInstitution: ApprovalInstitutionEnum;
  setApprovalState: (approvalState: ApprovalDialogState) => void;
  waitingStatus: DataTransferStatus;
};

export type DataTransferApproval =
  | DataTransferNodeApprovalsInner
  | DataTransferDataProviderApprovalsInner
  | DataTransferGroupApprovalsInner;

export const getTitle = (
  approval: DataTransferApproval,
  approvalInstitution: ApprovalInstitutionEnum,
) => {
  const approvalType = camelCase(approvalInstitution);
  const name = approval[approvalType].name;
  if ('code' in approval[approvalType]) {
    return `${name} (${approval[approvalType].code})`;
  }
  return name;
};

export const DataTransferApprovalsStack = ({
  approvals,
  approvalInstitution,
  setApprovalState,
  waitingStatus,
}: DataTransferApprovalsStackProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_TRANSFER,
    I18nNamespace.DATA_TRANSFER_APPROVALS,
  ]);

  const sort = useCallback(
    (approvals) =>
      [...approvals].sort((a, b) => {
        const institutionA = a[camelCase(approvalInstitution)];
        const institutionB = b[camelCase(approvalInstitution)];
        return (institutionA ? institutionA.name.toLowerCase() : '') >
          (institutionB ? institutionB.name.toLowerCase() : '')
          ? 1
          : -1;
      }),
    [approvalInstitution],
  );

  const statuses = useApprovalStatus();

  const makeButton = useCallback(
    (approval) => {
      const canTakeAction =
        approval.canApprove && approval.status === waitingStatus;
      return (
        <>
          {canTakeAction && (
            <ButtonGroup
              sx={{
                maxHeight: '40px',
                marginTop: '7px',
              }}
              variant="outlined"
              aria-label="outlined primary button group"
            >
              <Button
                onClick={() => {
                  setApprovalState({
                    approval,
                    approvalInstitution,
                    action: ApprovalAction.REJECT,
                  });
                }}
              >
                {t('actionButtons.reject')}
              </Button>
              <Button
                onClick={() =>
                  setApprovalState({
                    approval,
                    approvalInstitution,
                    action: ApprovalAction.ACCEPT,
                  })
                }
              >
                {t('actionButtons.approve')}
              </Button>
            </ButtonGroup>
          )}
          {!canTakeAction && (
            <ColoredStatus value={approval.status} statuses={statuses} />
          )}
        </>
      );
    },
    [approvalInstitution, setApprovalState, statuses, t, waitingStatus],
  );

  return (
    <Stack direction="column" spacing={1}>
      <Typography variant="subtitle1">
        {t(`${I18nNamespace.COMMON}:models.${camelCase(approvalInstitution)}`)}{' '}
        {t(`${I18nNamespace.COMMON}:models.approval_plural`)}
      </Typography>
      {sort(approvals).map((approval) => {
        return (
          <Grid container key={approval.id}>
            <Grid item xs={2}>
              <Typography marginTop={0.75}>
                {getTitle(approval, approvalInstitution)}
              </Typography>
            </Grid>
            <Grid
              item
              xs={4}
              sx={{ display: 'flex', justifyContent: 'flex-end' }}
            >
              {makeButton(approval)}
            </Grid>
            <Grid item xs={2}>
              {approval.status !== 'W' && (
                <Typography color="darkgray" marginLeft={6} marginTop={0.75}>
                  {formatDate(approval.changeDate, true)}
                </Typography>
              )}
            </Grid>
            <Grid item xs={4}>
              <Typography color="darkgray" marginLeft={6} marginTop={0.75}>
                {approval.rejectionReason}
              </Typography>
            </Grid>
          </Grid>
        );
      })}
    </Stack>
  );
};
