import { rest } from 'msw';
import {
  mockConsoleError,
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { backend } from '../../api/api';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { makeStore } from '../../store';
import { StsCredentials } from './StsCredentials';
import { Credentials, S3Opts } from '../../api/generated';
import { mapKeys, snakeCase } from 'lodash';

const credentials: Credentials = {
  accessKeyId: 'WMJDW12LT58IPQYQ31TD',
  secretAccessKey: 'hu98Hpvn+Hm3vaVf96ws+4KX60WmaxuJmrC+3LDK',
  sessionToken: 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9',
  expiration: new Date('2021-03-31T14:00:00.000Z'),
  dtr: 1,
};
const s3opts: S3Opts = {
  bucket: 'my-bucket',
  endpoint: 'http://localhost:9000/',
};
describe('StsCredentials', () => {
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  describe('Display the error(s)', () => {
    it("should display `ResponseError`'s detail", async () => {
      const detail = 'That did not work!';
      server.use(
        rest.get(`${backend}data-transfer/1/s3opts`, (req, res, ctx) => {
          return res(resJson(ctx), resBody(ctx, s3opts));
        }),
        rest.post(`${backend}sts/`, (req, res, ctx) => {
          return res(ctx.status(500), ctx.json({ detail }));
        }),
      );

      render(
        <Provider store={makeStore()}>
          <StsCredentials
            credentialsWithOpts={{ dtr: 1 }}
            setCredentialsWithOpts={jest.fn()}
          />
        </Provider>,
      );

      const fetchCredentialsBtn = await screen.findByText('fetchCredentials');
      fireEvent.click(fetchCredentialsBtn);

      await screen.findByText(detail);
    });

    it('should display a message for non-`Response` errors', async () => {
      mockConsoleError();

      server.use(
        rest.post(`${backend}sts/`, () => {
          throw new Error('Oops! Something went terribly wrong.');
        }),
      );

      render(
        <Provider store={makeStore()}>
          <StsCredentials
            credentialsWithOpts={{ dtr: 1 }}
            setCredentialsWithOpts={jest.fn()}
          />
        </Provider>,
      );

      const fetchCredentialsBtn = await screen.findByText('fetchCredentials');
      fireEvent.click(fetchCredentialsBtn);

      await screen.findByText(
        'The request failed and the interceptors did not return an alternative response',
      );
    });
  });

  describe('Fetch credentials', () => {
    beforeAll(() => {
      server.use(
        rest.get(`${backend}data-transfer/1/s3opts`, (req, res, ctx) => {
          return res(resJson(ctx), resBody(ctx, s3opts));
        }),
        rest.post(`${backend}sts/`, (req, res, ctx) => {
          const body = mapKeys(credentials, (v, k) => snakeCase(k));
          return res(resJson(ctx), resBody(ctx, body));
        }),
      );
    });
    it('should fetch credentials', async () => {
      const setCredentialsWithOpts = jest.fn();

      render(
        <Provider store={makeStore()}>
          <StsCredentials
            credentialsWithOpts={{ dtr: 1 }}
            setCredentialsWithOpts={setCredentialsWithOpts}
          />
        </Provider>,
      );

      const fetchCredentialsBtn = await screen.findByText('fetchCredentials');
      fireEvent.click(fetchCredentialsBtn);

      await waitFor(() => {
        expect(setCredentialsWithOpts).toHaveBeenCalledWith({
          ...credentials,
          ...s3opts,
        });
      });
    });
  });
});
