import { Required } from 'utility-types';
import {
  DataTransfer,
  DataTransferStatusEnum,
  Project,
} from '../../api/generated';
import { useMemo } from 'react';
import {
  ColoredStatus,
  formatDate,
  useLocaleSortingFn,
  Status,
} from '@biomedit/next-widgets';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { RootState } from '../../store';
import { createSelector } from 'reselect';
import { canSeeDataProviderTab, useParamSelector } from '../selectors';
import { DataProviderField } from './DataProviderField';
import { RequestorTableField } from './RequestorField';
import { createColumnHelper, FilterFn } from '@tanstack/react-table';
import { useSelector } from 'react-redux';

/**
 * '-1' (standing for 'unlimited'/'infinity') gets converted into 'Number.MAX_SAFE_INTEGER'
 * for sorting purposes.
 * We can not use 'infinity'. Otherwise addition (in sorting) will not work.
 */

export const useApprovalStatus = () => {
  const { t } = useTranslation([I18nNamespace.DATA_TRANSFER]);

  return useMemo<Status<string>[]>(
    () => [
      {
        color: 'info',
        text: t(`actionButtons.W`).toUpperCase(),
        value: 'W',
      },
      {
        color: 'success',
        text: t(`actionButtons.A`).toUpperCase(),
        value: 'A',
      },
      {
        color: 'error',
        text: t(`actionButtons.R`).toUpperCase(),
        value: 'R',
      },
      {
        text: t(`actionButtons.B`).toUpperCase(),
        value: 'B',
      },
    ],
    [t],
  );
};

export const useDataTransferStatus = () =>
  useMemo<Status<DataTransferStatusEnum>[]>(
    () => [
      {
        color: 'info',
        value: DataTransferStatusEnum.INITIAL,
      },
      {
        color: 'success',
        value: DataTransferStatusEnum.AUTHORIZED,
      },
      {
        value: DataTransferStatusEnum.EXPIRED,
      },
      {
        color: 'error',
        value: DataTransferStatusEnum.UNAUTHORIZED,
      },
    ],
    [],
  );

export const useDataTransferColumns = (withProjectName: boolean) => {
  const columnHelper = createColumnHelper<Required<DataTransfer>>();
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  const statuses = useDataTransferStatus();
  const localeSortingFn = useLocaleSortingFn<Required<DataTransfer>>();
  const canSeeDataProviderField = useSelector(canSeeDataProviderTab);

  return useMemo(() => {
    const columns = [
      columnHelper.accessor('id', {
        header: t('columns.transferId'),
      }),
      columnHelper.accessor('dataProvider', {
        header: t('columns.dataProvider'),
        sortingFn: 'alphanumeric',
        cell: (props) =>
          canSeeDataProviderField
            ? DataProviderField({ code: props.getValue() })
            : props.getValue(),
      }),
      columnHelper.accessor('requestorName', {
        header: t('columns.requestor'),
        sortingFn: localeSortingFn,
        cell: ({ row: { original } }) => RequestorTableField(original),
      }),
      columnHelper.accessor('purpose', {
        header: t('columns.purpose'),
      }),
      columnHelper.accessor('changeDate', {
        header: t('columns.changeDate'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true) ?? '',
      }),
      columnHelper.accessor('status', {
        header: t('columns.status'),
        cell: (props) =>
          ColoredStatus({
            value: props.getValue(),
            statuses: statuses,
          }),
      }),
    ];

    const projectNameColumns = withProjectName
      ? [
          columnHelper.accessor('projectName', {
            header: t('columns.projectName'),
            sortingFn: 'alphanumeric',
          }),
        ]
      : [];

    return [...projectNameColumns, ...columns];
  }, [
    t,
    withProjectName,
    columnHelper,
    statuses,
    localeSortingFn,
    canSeeDataProviderField,
  ]);
};
export const useGlobalDataTransferFilter =
  (): FilterFn<Required<DataTransfer>> => (row, columnId, filterValue) => {
    const filterOnColumns = () => {
      switch (columnId) {
        case 'id':
        case 'maxPackages':
          return row.getValue(columnId) == filterValue;
        default:
          return String(row.getValue(columnId))
            .toLowerCase()
            .includes(String(filterValue).toLowerCase());
      }
    };

    const filterOnDataPackage = () =>
      row.original.packages.some((dataPackage) =>
        String(dataPackage.fileName)
          .toLowerCase()
          .includes(String(filterValue).toLowerCase()),
      );

    return filterOnColumns() || filterOnDataPackage();
  };

const getDataTransfers = (state: RootState) => state.dataTransfers.itemList;
const getDtrIdParam = (_, dtrId): number => dtrId;
const getProjectParam = (_, prj): Project => prj;
const findDataTransferById = createSelector(
  getDataTransfers,
  getDtrIdParam,
  (dataTransfers: DataTransfer[], dtrId: number) =>
    dataTransfers.find((dataTransfer) => dataTransfer.id === dtrId),
);
const filterDataTransfersByProject = createSelector(
  getDataTransfers,
  getProjectParam,
  (dataTransfers: DataTransfer[], prj: Project) =>
    dataTransfers.filter((dtr) => dtr.project === prj.id),
);

export const useDataTransfer = (dtrId: number): DataTransfer => {
  return useParamSelector(findDataTransferById, dtrId);
};

export const useProjectDataTransfers = (project: Project) => {
  return useParamSelector(filterDataTransfersByProject, project);
};
