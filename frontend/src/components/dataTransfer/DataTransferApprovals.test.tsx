import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { Provider } from 'react-redux';
import { getInitialState, makeStore } from '../../store';
import { DataTransferApprovals } from './DataTransferApprovals';
import {
  DataTransferDataProviderApprovalsInnerStatusEnum,
  DataTransferGroupApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInnerStatusEnum,
  DataTransferPurposeEnum,
} from '../../api/generated';
import {
  createDataTransfer,
  createDataTransferDataProviderApproval,
  createDataTransferGroupApproval,
  createDataTransferNodeApproval,
} from '../../../factories';

describe('DataTransferApprovals', function () {
  it.each`
    isRejectionForm | buttonLabel                | expected
    ${false}        | ${'actionButtons.approve'} | ${'dataTransferApprovals:declarations.title'}
    ${true}         | ${'actionButtons.reject'}  | ${'dataTransferApprovals:rejectDataTransfer.text'}
  `(
    'Should show the form for approving data transfer',
    async function ({ buttonLabel, expected }) {
      const dataTransfer = createDataTransfer({
        purpose: DataTransferPurposeEnum.PRODUCTION,
        nodeApprovals: [
          createDataTransferNodeApproval({
            canApprove: true,
            status: DataTransferNodeApprovalsInnerStatusEnum.W,
          }),
        ],
        dataProviderApprovals: [
          createDataTransferDataProviderApproval({
            canApprove: true,
            status: DataTransferDataProviderApprovalsInnerStatusEnum.W,
          }),
        ],
        groupApprovals: [
          createDataTransferGroupApproval({
            canApprove: true,
            status: DataTransferGroupApprovalsInnerStatusEnum.W,
          }),
        ],
      });

      render(
        <Provider store={makeStore(undefined, getInitialState())}>
          <DataTransferApprovals dtr={dataTransfer} />
        </Provider>,
      );

      const buttons = screen.getAllByText(buttonLabel);
      expect(buttons).toHaveLength(3);

      fireEvent.click(buttons[0]);
      expect(await screen.findByText(expected)).toBeInTheDocument();
    },
  );
});
