import {
  CheckboxField,
  FormDialog,
  HiddenField,
  requestAction,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import React, { ReactElement, useCallback, useMemo, useState } from 'react';
import {
  Approval,
  ApprovalInstitutionEnum,
  ApprovalStatusEnum,
  DataTransfer,
  DataTransferNodeApprovalsInner,
  DataTransferNodeApprovalsInnerTypeEnum,
  DataTransferPurposeEnum,
} from '../../api/generated';
import { Stack, Typography } from '@mui/material';
import { CALL, UPDATE_APPROVAL } from '../../actions/actionTypes';
import { useDispatch, useSelector } from 'react-redux';
import { FormProvider } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import TextField from '@mui/material/TextField';
import { DataTransferApproval, getTitle } from './DataTransferApprovalsStack';
import camelCase from 'lodash/camelCase';
import { RootState } from '../../store';

type DataTransferApprovalFormProps = {
  dtr: DataTransfer;
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
  onClose: () => void;
  open: boolean;
  isRejectionForm: boolean;
};

type TitleProps = {
  dtr: DataTransfer;
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
};

export const Title = ({
  dtr,
  approval,
  approvalInstitution,
}: TitleProps): ReactElement => {
  return (
    <>
      <Typography variant="h5" display={'inline'}>
        {`${dtr.id} - ${dtr.projectName}`}
      </Typography>
      <Typography display={'inline'} pl={5}>
        {getTitle(approval, approvalInstitution)}
      </Typography>
    </>
  );
};

export const DataTransferApprovalForm = ({
  dtr,
  approval,
  approvalInstitution,
  onClose,
  open,
  isRejectionForm,
}: DataTransferApprovalFormProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_TRANSFER_APPROVALS,
    I18nNamespace.DATA_TRANSFER,
  ]);
  const dispatch = useDispatch();
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.dataTransfers.isSubmitting,
  );
  const form = useEnhancedForm<Approval>();
  const [rejectionReason, setRejectionReason] = useState<string>();

  const submit = useCallback(
    (updatedApproval: Approval) => {
      const onSuccessAction = {
        type: CALL,
        callback: onClose,
      };
      dispatch(
        requestAction(
          UPDATE_APPROVAL,
          {
            id: String(updatedApproval.id),
            approval: updatedApproval,
          },
          onSuccessAction,
        ),
      );
    },
    [onClose, dispatch],
  );

  const reject = useCallback(
    (approval: DataTransferApproval) => {
      const onSuccessAction = {
        type: CALL,
        callback: onClose,
      };
      dispatch(
        requestAction(
          UPDATE_APPROVAL,
          {
            id: String(approval?.id),
            approval: {
              id: approval?.id,
              institution: approvalInstitution,
              status: ApprovalStatusEnum.R,
              rejectionReason: rejectionReason,
            },
          },
          onSuccessAction,
        ),
      );
    },
    [dispatch, approvalInstitution, onClose, rejectionReason],
  );

  const checkboxes = useMemo(() => {
    const names =
      approvalInstitution === 'group' ? [] : ['technicalMeasuresInPlace'];
    if (dtr.purpose === DataTransferPurposeEnum.PRODUCTION) {
      names.push('existingLegalBasis');
    }
    if (approval && approvalInstitution === 'node') {
      const nodeApprovalType = (approval as DataTransferNodeApprovalsInner)
        .type;
      if (nodeApprovalType === DataTransferNodeApprovalsInnerTypeEnum.H) {
        names.push('projectSpaceReady');
      }
    }
    return names;
  }, [dtr.purpose, approval, approvalInstitution]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={
          <Title
            dtr={dtr}
            approval={approval}
            approvalInstitution={approvalInstitution}
          />
        }
        onSubmit={!isRejectionForm ? submit : () => reject(approval)}
        onClose={() => {
          onClose();
          form.reset();
        }}
        open={open}
        confirmLabel={
          (!isRejectionForm
            ? t(`${I18nNamespace.DATA_TRANSFER}:actionButtons.approve`)
            : t(
                `${I18nNamespace.DATA_TRANSFER}:actionButtons.reject`,
              )) as string
        }
        isSubmitting={isSubmitting}
      >
        {!isRejectionForm && (
          <>
            <HiddenField name="id" initialValues={{ id: approval.id }} />
            <HiddenField
              name="status"
              initialValues={{ status: ApprovalStatusEnum.A }}
            />
            <HiddenField
              name="institution"
              initialValues={{
                institution: approvalInstitution,
              }}
            />
            <Typography variant="h5">
              {t(`${I18nNamespace.DATA_TRANSFER_APPROVALS}:declarations.title`)}
            </Typography>
            <Stack>
              {checkboxes.map((name) => {
                return (
                  <CheckboxField
                    key={name}
                    name={name}
                    label={t(
                      `${
                        I18nNamespace.DATA_TRANSFER_APPROVALS
                      }:declarations.${camelCase(approvalInstitution)}.${name}`,
                    )}
                    required
                  />
                );
              })}
            </Stack>
          </>
        )}

        {isRejectionForm && (
          <>
            <Typography variant="h5">
              {t(
                `${I18nNamespace.DATA_TRANSFER_APPROVALS}:rejectDataTransfer.title`,
              )}
            </Typography>
            <br />
            <Typography>
              {t(
                `${I18nNamespace.DATA_TRANSFER_APPROVALS}:rejectDataTransfer.text`,
              )}
            </Typography>
            <TextField
              autoFocus={true}
              onChange={(event) => {
                setRejectionReason(event.target.value);
              }}
              required
              inputProps={{ maxLength: 512 }}
              size="small"
              variant="outlined"
              fullWidth
            />
          </>
        )}
      </FormDialog>
    </FormProvider>
  );
};
