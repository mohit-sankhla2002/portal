import React, { ReactElement, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DataTransfer, Project } from '../../api/generated';
import { DELETE_DATA_TRANSFER } from '../../actions/actionTypes';
import { useDataTransferForm } from './DataTransferForm';
import {
  BaseReducerState,
  EnhancedTable,
  requestAction,
  useDialogState,
} from '@biomedit/next-widgets';
import { IdRequired } from '../../global';
import { Required } from 'utility-types';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
  useProjectDataTransfers,
} from './DataTransferHooks';
import { DataTransferDetail } from './DataTransferDetail';
import { canAddDataTransfer, isStaff } from '../selectors';
import { RootState } from '../../store';

export const dtrTableTitle = 'Data Transfers';
export const dtrTableAddButtonLabel = 'Data Transfer';

export const DataTransferTable = (
  project: IdRequired<Project>,
): ReactElement => {
  const { isFetching, isSubmitting } = useSelector<
    RootState,
    BaseReducerState<IdRequired<DataTransfer>>
  >((state) => state.dataTransfers);

  const dataTransfers = useProjectDataTransfers(project);
  const staff = useSelector(isStaff);
  const canAdd = useSelector(canAddDataTransfer) && !project.archived;

  const dispatch = useDispatch();
  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_DATA_TRANSFER, { id }));
    },
    [dispatch],
  );

  const { openFormDialog, dtrForm } = useDataTransferForm(project.id);

  const columns = useDataTransferColumns(false);

  const { item, setItem, onClose, open } = useDialogState<DataTransfer>();

  const globalFilterFn = useGlobalDataTransferFilter();

  const getDeleteConfirmationText = useCallback(
    (item: Required<DataTransfer>) => 'Delete ' + String(item.id),
    [],
  );

  return (
    <>
      {item && item.id && (
        <DataTransferDetail dtrId={item.id} onClose={onClose} open={open} />
      )}
      <EnhancedTable<Required<DataTransfer>>
        itemList={dataTransfers}
        columns={columns}
        title={dtrTableTitle}
        canAdd={canAdd}
        canEdit={staff}
        canDelete={staff}
        onAdd={openFormDialog}
        onEdit={openFormDialog}
        onRowClick={setItem}
        form={dtrForm}
        onDelete={deleteItem}
        addButtonLabel={dtrTableAddButtonLabel}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        globalFilterFn={globalFilterFn}
        getDeleteConfirmationText={getDeleteConfirmationText}
        inline
      />
    </>
  );
};
