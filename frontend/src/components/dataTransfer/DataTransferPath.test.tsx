import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import { DataTransferPath } from './DataTransferPath';
import { getInitialState, makeStore, RootState } from '../../store';

describe('DataTransferPath', () => {
  describe('DataTransferPath', () => {
    const initialState: RootState = {
      ...getInitialState(),
    };
    const store = makeStore(undefined, initialState);

    const transferPath = [
      'Data Provider',
      'Transfer Node',
      'Hosting Node',
      'Project',
    ];

    it('should display path for a given DTR', async () => {
      render(
        <Provider store={store}>
          <DataTransferPath transferPath={transferPath} />
        </Provider>,
      );

      for (const step of transferPath) {
        await screen.findByText(step);
      }
    });
  });
});
