import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { Required } from 'utility-types';
import {
  AutocompleteField,
  BaseReducerState,
  Button,
  CheckboxField,
  ConfirmLabel,
  FixedChildrenHeight,
  FormDialog,
  grey,
  HiddenField,
  LabelledField,
  requestAction,
  SelectField,
  ToggleButton,
  ToggleButtonGroup,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
} from '@biomedit/next-widgets';
import {
  ADD_DATA_TRANSFER,
  CALL,
  CLEAR_PGP_KEY_INFOS,
  LOAD_DATA_PROVIDERS,
  LOAD_PGP_KEY_INFOS,
  LOAD_USERS,
  UPDATE_DATA_TRANSFER,
} from '../../actions/actionTypes';
import {
  DataProvider,
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListUsersRoleEnum,
  PgpKeyInfoStatusEnum,
  User,
} from '../../api/generated';
import { FormProvider } from 'react-hook-form';
import { TestId } from '../../testId';
import { isStaff } from '../selectors';
import { useDataProviderChoices, useUserChoices } from '../choice';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { CircularProgress, Link, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { RootState } from '../../store';
import { IdRequired } from '../../global';

export enum MaxPackagesToggleState {
  UNLIMITED = -1,
  ONE_PACKAGE = 1,
}

export type UiDataTransfer = Required<
  Partial<DataTransfer>,
  'maxPackages' | 'purpose' | 'project'
>;

export type DataTransferFormProps = {
  transfer: UiDataTransfer;
  close: () => void;
};

export function useDataTransferForm(projectId: number | undefined = undefined) {
  const newDtr =
    projectId !== undefined
      ? {
          maxPackages: MaxPackagesToggleState.ONE_PACKAGE,
          project: +projectId,
          purpose: DataTransferPurposeEnum.PRODUCTION,
        }
      : undefined;
  const { closeFormDialog, data, ...rest } =
    useFormDialog<UiDataTransfer>(newDtr);
  const dtrForm = data && (
    <DataTransferForm transfer={data} close={closeFormDialog} />
  );
  return {
    dtrForm,
    ...rest,
  };
}

export const DataTransferForm = ({
  transfer,
  close,
}: DataTransferFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);

  const isEdit = !!transfer.id;

  const dispatch = useDispatch();

  const [requestorDataLoaded, setRequestorDataLoaded] =
    useState<boolean>(false);

  const { isFetching: isFetchingDataProvider, itemList: dataProviders } =
    useSelector<RootState, BaseReducerState<IdRequired<DataProvider>>>(
      (state) => state.dataProvider,
    );
  const { isFetching: isFetchingUsers, itemList: users } = useSelector<
    RootState,
    BaseReducerState<IdRequired<User>>
  >((state) => state.users);

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.project.isSubmitting,
  );

  const requestorFieldVisible = useSelector(isStaff);
  const userChoices = useUserChoices(users);
  const dataProviderChoices = useDataProviderChoices(
    dataProviders.filter((dp) => dp.coordinators?.length && dp.enabled),
  );
  const dataTransferPurposeChoices = useEnumChoices(DataTransferPurposeEnum);
  const dataTransferStatusChoices = useEnumChoices(DataTransferStatusEnum);

  const form = useEnhancedForm<DataTransfer>({ defaultValues: transfer });

  const { reset, getValues, watch } = form;

  function submit(dataTransfer: DataTransfer) {
    const onSuccessAction = { type: CALL, callback: close };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_DATA_TRANSFER,
          {
            id: String(dataTransfer.id),
            dataTransfer,
          },
          onSuccessAction,
        ),
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_TRANSFER,
          {
            dataTransfer,
          },
          onSuccessAction,
        ),
      );
    }
  }

  const watchRequestor = watch('requestor');

  useEffect(() => {
    batch(() => {
      if (requestorFieldVisible) {
        dispatch(
          requestAction(
            LOAD_USERS,
            {
              projectId: transfer.project,
              role: ListUsersRoleEnum.DM,
            },
            { type: CALL, callback: () => setRequestorDataLoaded(true) },
          ),
        );
      } else {
        // Load all current user's approved keys
        dispatch(
          requestAction(
            LOAD_PGP_KEY_INFOS,
            {
              status: PgpKeyInfoStatusEnum.APPROVED,
            },
            { type: CALL, callback: () => setRequestorDataLoaded(true) },
          ),
        );
      }
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    });
  }, [
    transfer.project,
    dispatch,
    requestorFieldVisible,
    setRequestorDataLoaded,
  ]);

  useEffect(() => {
    const requestorUser = users.find((user) => user.id === watchRequestor);

    if (requestorFieldVisible && watchRequestor && requestorUser) {
      dispatch(
        requestAction(LOAD_PGP_KEY_INFOS, {
          status: PgpKeyInfoStatusEnum.APPROVED,
          username: requestorUser.username,
        }),
      );
    } else {
      dispatch({ type: CLEAR_PGP_KEY_INFOS });
    }
  }, [dispatch, watchRequestor, users, requestorFieldVisible]);

  const watchPurpose = watch('purpose');
  const legalBasisRequired = useMemo(
    () => watchPurpose === DataTransferPurposeEnum.PRODUCTION,
    [watchPurpose],
  );

  const [maxPackagesToggleState, setMaxPackagesToggleState] =
    useState<MaxPackagesToggleState>(transfer.maxPackages);

  const handleMaxPackageStateToggled = useCallback(
    (_event, newToggleState) => {
      if (newToggleState !== null) {
        // update max packages on switching between "unlimited" and "one package"
        const formValues = getValues();
        formValues.maxPackages = newToggleState;
        reset(formValues);

        // update toggle
        setMaxPackagesToggleState(newToggleState);
      }
    },
    [setMaxPackagesToggleState, getValues, reset],
  );

  const checkForKeys = useCallback(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS));
  }, [dispatch]);

  if (!users) {
    return <CircularProgress />;
  }

  // in edit mode, requestor's keys are shown instead
  if (requestorDataLoaded && !requestorFieldVisible) {
    const docsLink = (link: string, text: string): ReactElement => {
      return (
        <Link href={link} target={'_blank'} rel={'noopener noreferrer'}>
          {text}
        </Link>
      );
    };

    const StyledTypography = styled(Typography)(({ theme }) => ({
      marginBottom: theme.spacing(1),
    }));

    return (
      <>
        <StyledTypography>{t('noKeys.intro')}</StyledTypography>
        <StyledTypography>
          {t('noKeys.upload.helper')}
          {docsLink(t('noKeys.upload.link'), t('noKeys.upload.text'))}
        </StyledTypography>
        <StyledTypography>
          {t('noKeys.create.helper')}
          {docsLink(t('noKeys.create.link'), t('noKeys.create.text'))}
        </StyledTypography>
        <Button color="primary" variant="outlined" onClick={checkForKeys}>
          {t('noKeys.button')}
        </Button>
      </>
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        title={isEdit ? `Data Transfer #${transfer?.id}` : 'New Data Transfer'}
        open={!!transfer && requestorDataLoaded}
        isSubmitting={isSubmitting}
        onSubmit={submit}
        onClose={close}
        confirmLabel={ConfirmLabel.SAVE}
      >
        <HiddenField
          name="project"
          initialValues={{ project: transfer.project }}
        />
        <HiddenField name="id" initialValues={{ id: transfer.id }} />
        <ToggleButtonGroup
          value={maxPackagesToggleState}
          exclusive
          onChange={handleMaxPackageStateToggled}
          sx={{ my: 3 }}
        >
          {[
            MaxPackagesToggleState.UNLIMITED,
            MaxPackagesToggleState.ONE_PACKAGE,
          ].map((enumItem) => {
            return (
              <ToggleButton key={enumItem} value={enumItem}>
                {t('maxPackages', { context: String(enumItem) })}
              </ToggleButton>
            );
          })}
        </ToggleButtonGroup>
        <HiddenField
          name="maxPackages"
          initialValues={{
            maxPackages: maxPackagesToggleState,
          }}
        />
        <FixedChildrenHeight>
          <SelectField
            name="dataProvider"
            label="Data Provider"
            initialValues={transfer}
            required
            isLoading={isFetchingDataProvider}
            choices={dataProviderChoices}
            fullWidth={true}
            helperText={t('helperText')}
            populateIfSingleChoice
          />
          <SelectField
            name="purpose"
            label="Purpose"
            initialValues={transfer}
            required
            choices={dataTransferPurposeChoices}
            disabled={isEdit}
            fullWidth={true}
          />
          {isEdit && (
            <SelectField
              name="status"
              label="Status"
              initialValues={transfer}
              required
              choices={dataTransferStatusChoices}
              fullWidth={true}
            />
          )}
          {requestorFieldVisible && (
            <AutocompleteField
              name="requestor"
              label="Requestor"
              initialValues={transfer}
              required
              testId={TestId.REQUESTOR}
              choices={userChoices}
              isLoading={isFetchingUsers}
              width="100%"
            />
          )}
          {!isEdit && (
            <CheckboxField
              name="dataSelectionConfirmation"
              label={t('dataSelectionConfirmation')}
              initialValues={transfer}
              required
            />
          )}
          <div>
            <Typography
              sx={legalBasisRequired ? { mb: 2 } : { mb: 2, color: grey[400] }}
            >
              {t('legalBasisHelp')}
            </Typography>
            <LabelledField
              name="legalBasis"
              type="text"
              label={t('legalBasis')}
              initialValues={transfer}
              // Do NOT use `validations={legalBasisRequired ? required : undefined}` as it does NOT work
              required={legalBasisRequired}
              disabled={!legalBasisRequired}
              fullWidth
            />
          </div>
        </FixedChildrenHeight>
      </FormDialog>
    </FormProvider>
  );
};
