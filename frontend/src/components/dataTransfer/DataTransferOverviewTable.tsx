import React, {
  ReactElement,
  ReactNode,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  DELETE_DATA_TRANSFER,
  LOAD_DATA_TRANSFERS,
} from '../../actions/actionTypes';
import { DataTransfer, User } from '../../api/generated';
import { useDataTransferForm } from './DataTransferForm';
import {
  BaseReducerState,
  EnhancedTable,
  requestAction,
  useDialogState,
} from '@biomedit/next-widgets';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
} from './DataTransferHooks';
import { DataTransferDetail } from './DataTransferDetail';
import { isStaff } from '../selectors';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { RootState } from '../../store';
import { IdRequired } from '../../global';
import { exportDtrsCsv } from './DataTransferExport';

export declare type DataTransferOverviewTableProps = {
  children?: ReactNode;
  selected?: string;
};

export const DataTransferOverviewTable = (
  props: DataTransferOverviewTableProps,
): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.DATA_TRANSFER,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: dataTransfers,
  } = useSelector<RootState, BaseReducerState<IdRequired<User>>>(
    (state) => state.dataTransfers,
  );

  const staff = useSelector(isStaff);
  const canEdit = useCallback(
    (item?: Required<DataTransfer>) => staff && !item?.projectArchived,
    [staff],
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_DATA_TRANSFERS));
  }, [dispatch]);

  const deleteItem = useCallback(
    (id) => {
      dispatch(requestAction(DELETE_DATA_TRANSFER, { id }));
    },
    [dispatch],
  );

  const { openFormDialog, dtrForm } = useDataTransferForm();

  const columns = useDataTransferColumns(true);

  const { item, setItem, onClose, open } = useDialogState<DataTransfer>();

  useEffect(() => {
    if (props.selected) {
      // Passing `props.selected` causes a TypeScript error
      const selected = props.selected;
      const dataTransferSelected = dataTransfers.find(
        (x) => x.id === parseInt(selected),
      );
      if (dataTransferSelected) {
        setItem(dataTransferSelected);
      }
    }
  }, [props.selected, setItem, dataTransfers]);

  const globalFilterFn = useGlobalDataTransferFilter();

  const getDeleteConfirmationText = useCallback(
    (item: Required<DataTransfer>) => 'Delete ' + String(item.id),
    [],
  );

  const [exporting, setExporting] = useState<boolean>(false);

  const onExport = useCallback(() => {
    setExporting(true);
    exportDtrsCsv(dataTransfers);
    setExporting(false);
  }, [dataTransfers]);

  return (
    <>
      {item && item.id && (
        <DataTransferDetail dtrId={item.id} onClose={onClose} open={open} />
      )}
      <EnhancedTable<Required<DataTransfer>>
        itemList={dataTransfers}
        columns={columns}
        canAdd={false} // it should only be possible to add data transfers from the projects tabs
        canEdit={canEdit}
        canDelete={staff}
        onEdit={openFormDialog}
        onRowClick={setItem}
        form={dtrForm}
        onDelete={deleteItem}
        isFetching={isFetching}
        getDeleteConfirmationText={getDeleteConfirmationText}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
          }) as string
        }
        globalFilterFn={globalFilterFn}
        globalFilterHelperText={
          t(`${I18nNamespace.DATA_TRANSFER}:globalFilterHelperText`) as string
        }
        canExport={true}
        isExporting={exporting}
        onExport={onExport}
        inline
      />
    </>
  );
};
