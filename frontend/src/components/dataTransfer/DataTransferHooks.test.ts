import {
  createColumnHelper,
  createTable,
  getCoreRowModel,
} from '@tanstack/react-table';
import { DataTransfer } from '../../api/generated';
import { useGlobalDataTransferFilter } from './DataTransferHooks';

const IRRELEVANT = 'id';

describe('useGlobalDataTransferFilter', () => {
  it.each`
    columnId         | filterValue | expected
    ${'id'}          | ${10}       | ${true}
    ${'id'}          | ${'10'}     | ${true}
    ${'id'}          | ${11}       | ${false}
    ${'maxPackages'} | ${-1}       | ${true}
    ${'projectName'} | ${'proj'}   | ${true}
    ${'projectName'} | ${'foo'}    | ${false}
    ${IRRELEVANT}    | ${'ABCDEF'} | ${false}
    ${IRRELEVANT}    | ${'FEDCBA'} | ${false}
    ${IRRELEVANT}    | ${'.tar'}   | ${true}
    ${IRRELEVANT}    | ${'.zip'}   | ${false}
  `(
    'Should return $expected for column $columnId if filterValue is $filterValue',
    ({ columnId, filterValue, expected }) => {
      const globalFilter = useGlobalDataTransferFilter();
      const columnHelper = createColumnHelper<Required<DataTransfer>>();
      const row = createTable({
        data: [
          {
            id: 10,
            maxPackages: -1,
            projectName: 'Project',
            // @ts-expect-error not relevant for logic to be tested
            packages: [{ fileName: 'package.tar' }],
          },
        ],
        state: {},
        onStateChange: () => void 0,
        columns: [
          columnHelper.accessor('id', {}),
          columnHelper.accessor('maxPackages', {}),
          columnHelper.accessor('projectName', {}),
        ],
        getCoreRowModel: getCoreRowModel(),
      }).getRow('0');

      expect(globalFilter(row, columnId, filterValue, () => void 0)).toBe(
        expected,
      );
    },
  );
});
