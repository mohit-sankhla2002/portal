import { styled } from '@mui/material/styles';
import { FixedChildrenHeight } from '@biomedit/next-widgets';

export const FormFieldsContainer = styled(FixedChildrenHeight)(({ theme }) => ({
  marginTop: theme.spacing(1),
}));
