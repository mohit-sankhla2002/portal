import React, { useMemo } from 'react';
import {
  ColoredStatus,
  useLocaleSortingFn,
  Status,
  useFormDialog,
} from '@biomedit/next-widgets';
import { PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';
import { createColumnHelper } from '@tanstack/react-table';

export const usePgpKeyInfoColumns = () => {
  const columnHelper = createColumnHelper<Required<PgpKeyInfo>>();
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_LIST);
  const localeSortingFn = useLocaleSortingFn<Required<PgpKeyInfo>>();

  return useMemo(() => {
    const statuses: Status<PgpKeyInfoStatusEnum>[] = [
      {
        color: 'info',
        value: PgpKeyInfoStatusEnum.PENDING,
      },
      {
        color: 'success',
        value: PgpKeyInfoStatusEnum.APPROVED,
      },
      {
        color: 'error',
        value: PgpKeyInfoStatusEnum.REJECTED,
      },
      {
        value: PgpKeyInfoStatusEnum.DELETED,
      },
      {
        value: PgpKeyInfoStatusEnum.KEY_REVOKED,
      },
      {
        value: PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
      },
    ];

    return [
      columnHelper.accessor('fingerprint', {
        header: t('columns.fingerprint'),
      }),
      columnHelper.accessor('keyUserId', {
        header: t('columns.keyUserId'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('keyEmail', {
        header: t('columns.keyEmail'),
      }),
      columnHelper.accessor('status', {
        header: t('columns.status'),
        cell: (props) =>
          ColoredStatus<PgpKeyInfoStatusEnum>({
            value: props.getValue(),
            statuses,
          }),
      }),
    ];
  }, [t, columnHelper, localeSortingFn]);
};

export function usePgpKeyInfoManageForm() {
  const newPgpKeyInfo: Partial<PgpKeyInfo> = { fingerprint: undefined };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<PgpKeyInfo>>(newPgpKeyInfo);
  const pgpKeyInfoManageForm = data && (
    <PgpKeyInfoManageForm pgpKeyInfo={data} onClose={closeFormDialog} />
  );
  return {
    pgpKeyInfoManageForm,
    ...rest,
  };
}
