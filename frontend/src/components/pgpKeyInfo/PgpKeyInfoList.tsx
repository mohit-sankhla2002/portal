import React, { ReactElement, useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  CALL,
  LOAD_PGP_KEY_INFOS,
  RETIRE_PGP_KEY_INFO,
} from '../../actions/actionTypes';
import {
  AdditionalActionButtonFactory,
  DeleteDialog,
  EnhancedTable,
  requestAction,
  Tooltip,
  useDialogState,
} from '@biomedit/next-widgets';
import { PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import {
  usePgpKeyInfoColumns,
  usePgpKeyInfoManageForm,
} from './PgpKeyInfoHooks';
import IconButton from '@mui/material/IconButton';
import KeyOff from '@mui/icons-material/KeyOff';
import { RootState } from '../../store';
import { PgpKeyInfoState } from '../../reducers/pgpKeyInfo';

export function isActive(status): boolean {
  return [PgpKeyInfoStatusEnum.PENDING, PgpKeyInfoStatusEnum.APPROVED].includes(
    status,
  );
}

export function hasActiveKey(itemList: PgpKeyInfo[]): boolean {
  return itemList.some(({ status }: PgpKeyInfo) => status && isActive(status));
}

export const PgpKeyInfoList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.PGP_KEY_INFO_LIST,
  ]);
  const columns = usePgpKeyInfoColumns();
  const username = useSelector<RootState, string | undefined>(
    (state) => state.auth.user?.username,
  );
  const { isFetching, isSubmitting, itemList } = useSelector<
    RootState,
    PgpKeyInfoState
  >((state) => state.pgpKeyInfo);
  const canAdd: boolean = useMemo(() => !hasActiveKey(itemList), [itemList]);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS, { username }));
  }, [dispatch, username]);

  const { openFormDialog, pgpKeyInfoManageForm } = usePgpKeyInfoManageForm();

  const { item, setItem, onClose, open } =
    useDialogState<Required<PgpKeyInfo>>();

  const retireKeyButtonLabel = 'retireKeyButton';
  const retireKeyTitle = 'Retire Key';
  const retireKeyButtonFactory: AdditionalActionButtonFactory<
    Required<PgpKeyInfo>
  > = useCallback(
    (item?: Required<PgpKeyInfo>) =>
      item && isActive(item.status) ? (
        <Tooltip key={retireKeyButtonLabel + item.id} title={retireKeyTitle}>
          <IconButton
            size={'large'}
            onClick={() => setItem(item)}
            aria-label={retireKeyButtonLabel}
          >
            <KeyOff />
          </IconButton>
        </Tooltip>
      ) : null,
    [retireKeyButtonLabel, setItem],
  );

  const onRetireButtonClick = (item: Required<PgpKeyInfo>) => {
    dispatch(
      requestAction(
        RETIRE_PGP_KEY_INFO,
        {
          id: String(item.id),
        },
        { type: CALL, callback: onClose },
      ),
    );
  };

  return (
    <>
      {item && (
        <DeleteDialog
          title={retireKeyTitle}
          open={open}
          onDelete={(isConfirmed) =>
            isConfirmed ? onRetireButtonClick(item) : onClose()
          }
          isSubmitting={isSubmitting}
          text={t(`${I18nNamespace.PGP_KEY_INFO_LIST}:dialogs.retireKey.text`)}
        />
      )}
      <EnhancedTable<Required<PgpKeyInfo>>
        itemList={itemList}
        columns={columns}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.key_plural`),
          }) as string
        }
        canAdd={canAdd}
        onAdd={openFormDialog}
        addButtonLabel={t(`${I18nNamespace.COMMON}:models.key`) as string}
        form={pgpKeyInfoManageForm}
        canDelete={false}
        inline
        additionalActionButtons={[retireKeyButtonFactory]}
      />
    </>
  );
};
