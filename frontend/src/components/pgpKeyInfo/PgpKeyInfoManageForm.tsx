import React, { ReactElement, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  FabButton,
  FormDialog,
  LabelledField,
  mustBeOneOf,
  pgpFingerprint,
  requestAction,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import {
  PgpKeyInfo,
  PgpKeyMetadata,
  PgpKeyMetadataStatusEnum,
} from '../../api/generated';
import { Trans, useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import { FormProvider } from 'react-hook-form';
import { ADD_PGP_KEY_INFO, CALL } from '../../actions/actionTypes';
import { generatedBackendApi } from '../../api/api';
import { Alert, AlertColor, Box, Link } from '@mui/material';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { styled } from '@mui/material/styles';
import { keyServer, settDoc } from '../../config';
import { RootState } from '../../store';
import { AuthState } from '../../reducers/auth';

type KeyMetadata = {
  keyUserId?: string;
  keyEmail?: string;
  keyStatus?: PgpKeyMetadataStatusEnum;
  error?: string;
};

export type PgpKeyInfoManageFormProps = {
  pgpKeyInfo: Partial<PgpKeyInfo>;
  onClose: () => void;
};

enum HelperTextEnum {
  CONDEMNED,
  INITIAL,
  NOT_FOUND,
  NOT_VERIFIED,
  REVOKED,
  VERIFIED,
}

type HelperText = {
  text: string | ReactElement;
  severity?: AlertColor;
};

function useHelperTexts(): Record<keyof typeof HelperTextEnum, HelperText> {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);

  return {
    INITIAL: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.initial`),
      severity: 'info',
    },
    VERIFIED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyVerified`),
      severity: 'warning',
    },
    CONDEMNED: {
      text: (
        <Trans
          i18nKey={`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyCondemned`}
          components={{
            break: <br />,
            settDoc: (
              <Link
                href={settDoc}
                target={'_blank'}
                rel={'noopener noreferrer'}
              />
            ),
          }}
        />
      ),
      severity: 'warning',
    },
    NOT_VERIFIED: {
      text: (
        <Trans
          i18nKey={`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotVerified`}
          components={{
            keyServer: (
              <Link
                href={keyServer}
                target={'_blank'}
                rel={'noopener noreferrer'}
              />
            ),
          }}
        />
      ),
      severity: 'error',
    },
    REVOKED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyRevoked`),
      severity: 'error',
    },
    NOT_FOUND: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotFound`),
      severity: 'error',
    },
  };
}

const StyledFabButton = styled(FabButton)(({ theme }) => ({
  marginLeft: theme.spacing(1),
}));

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);
  const form = useEnhancedForm<PgpKeyInfo>({ mode: 'onChange' });
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.pgpKeyInfo.isSubmitting,
  );
  const dispatch = useDispatch();
  const [keyMetadata, setKeyMetadata] = useState<KeyMetadata>();
  const { user } = useSelector<RootState, AuthState>((state) => state.auth);
  const userEmails = useMemo(
    () => user?.profile?.emails?.split(',') ?? [user?.email],
    [user],
  );
  const fingerprintFieldName = 'fingerprint';
  const helperTexts = useHelperTexts();
  const [helperText, setHelperText] = useState<HelperText>(helperTexts.INITIAL);
  const watchFingerprint = form.watch(fingerprintFieldName);
  const [isFetchingKeyMetadata, setIsFetchingKeyMetadata] =
    useState<boolean>(false);
  const uniqueCheck = async (value) =>
    generatedBackendApi.uniquePgpKeyInfo({
      uniquePgpKeyInfo: { fingerprint: value.fingerprint },
    });

  function submit(pgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        ADD_PGP_KEY_INFO,
        {
          pgpKeyInfo,
        },
        onSuccessAction,
      ),
    );
  }

  async function onSearchClick() {
    form.clearErrors('keyEmail');
    const { fingerprint: formFingerprint } = form.getValues();
    setIsFetchingKeyMetadata(true);
    generatedBackendApi
      .metadataPgpKeyMetadata({
        pgpKeyMetadata: { fingerprint: formFingerprint },
      })
      .then(({ fingerprint, email, userId, status, error }: PgpKeyMetadata) => {
        const keyMetadata: KeyMetadata = {
          keyEmail: email,
          keyUserId: userId,
          keyStatus: status,
        };
        setKeyMetadata(keyMetadata);
        if (error) {
          setHelperText({ text: error, severity: 'error' });
        } else if (!fingerprint) {
          setHelperText(helperTexts.NOT_FOUND);
        } else {
          switch (status) {
            case PgpKeyMetadataStatusEnum.VERIFIED:
              setHelperText(helperTexts.VERIFIED);
              break;
            case PgpKeyMetadataStatusEnum.CONDEMNED:
              setHelperText(helperTexts.CONDEMNED);
              break;
            case PgpKeyMetadataStatusEnum.NONVERIFIED:
              setHelperText(helperTexts.NOT_VERIFIED);
              break;
            default:
              setHelperText(helperTexts.REVOKED);
          }
        }
      })
      .finally(() => setIsFetchingKeyMetadata(false));
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        open={!!pgpKeyInfo}
        title={t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:title`)}
        onSubmit={submit}
        isSubmitting={isSubmitting || isFetchingKeyMetadata}
        onClose={onClose}
        confirmButtonProps={{
          disabled:
            !keyMetadata ||
            !!keyMetadata.error ||
            (keyMetadata.keyStatus !== PgpKeyMetadataStatusEnum.VERIFIED &&
              keyMetadata.keyStatus !== PgpKeyMetadataStatusEnum.CONDEMNED),
        }}
        fullWidth={true}
      >
        <FormFieldsContainer>
          <Alert
            severity={helperText.severity}
            sx={{ marginBottom: 3, marginRight: 0 }}
          >
            {helperText.text}
          </Alert>
          <Box sx={{ display: 'flex', marginRight: 0 }}>
            <LabelledField
              name={fingerprintFieldName}
              type="text"
              label={t(
                `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.${fingerprintFieldName}`,
              )}
              fullWidth
              validations={[required, pgpFingerprint]}
              unique={uniqueCheck}
              autofocus={true}
            />
            <StyledFabButton
              icon={'search'}
              onClick={onSearchClick}
              size={'small'}
              aria-label={
                t(
                  `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:buttons.search`,
                ) as string
              }
              disabled={
                !watchFingerprint ||
                watchFingerprint.length === 0 ||
                !!form.getFieldState(fingerprintFieldName).error ||
                isFetchingKeyMetadata
              }
            />
          </Box>
          <LabelledField
            name="keyUserId"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyUserId`,
            )}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyUserId ?? ''}
          />
          <LabelledField
            name="keyEmail"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyEmail`,
            )}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyEmail ?? ''}
            validations={mustBeOneOf(userEmails)}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
