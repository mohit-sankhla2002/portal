import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { Provider } from 'react-redux';
import { PgpKeyInfo, PgpKeyMetadataStatusEnum } from '../../api/generated';
import { makeStore } from '../../store';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';
import {
  mockConsoleError,
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { backend } from '../../api/api';
import { rest } from 'msw';

describe('PgpKeyInfoManageForm', function () {
  function getSearchButton() {
    return screen.findByLabelText('pgpKeyInfoManageForm:buttons.search');
  }

  function getFingerprintLabelledField() {
    return screen.findByLabelText('pgpKeyInfoManageForm:captions.fingerprint');
  }

  const testEmail = 'chuck.norris@roundhouse.kick';
  const testUserId = 'Chuck Norris';
  const testFingerprint = '0123456789ABCDEF0123456789ABCDEF01234567';
  const keyMetadata = {
    email: testEmail,
    user_id: testUserId,
    fingerprint: testFingerprint,
  };

  let consoleMock: jest.SpyInstance;

  beforeEach(() => {
    // Because we are using `value` (controlled) for `LabelledField`
    // (which should keep uncontrolled)
    consoleMock = mockConsoleError();
    const initialValues: Partial<PgpKeyInfo> = { fingerprint: undefined };
    render(
      <Provider store={makeStore()}>
        <PgpKeyInfoManageForm
          pgpKeyInfo={initialValues}
          onClose={() => {
            return;
          }}
        />
      </Provider>,
    );
  });

  afterEach(() => {
    consoleMock.mockRestore();
  });

  it('Should show the initialHelperText at first', async function () {
    expect(
      await screen.findByText('pgpKeyInfoManageForm:helpers.initial'),
    ).toBeInTheDocument();
  });

  describe('Search button', function () {
    const server: MockServer = setupMockApi();

    afterEach(() => {
      // Reset any runtime handlers tests may use.
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    it.each`
      fingerprint                                           | enabled
      ${''}                                                 | ${false}
      ${'0123456789ABCDEF'}                                 | ${false}
      ${'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'} | ${false}
      ${'malformedMalformedMalformedMalformed0000'}         | ${false}
      ${testFingerprint}                                    | ${true}
    `(
      'Should allow ($enabled) clicking the Search button if the input is $fingerprint',
      async function ({ fingerprint, enabled }) {
        server.use(
          rest.post(`${backend}pgpkey/unique/`, (req, res, ctx) => {
            return res(resJson(ctx), resBody(ctx, {}));
          }),
        );

        const user = userEvent.setup();
        const fingerprintLabelledField = await getFingerprintLabelledField();
        const searchButton = await getSearchButton();
        const otherLabelledField = await screen.findByLabelText(
          'pgpKeyInfoManageForm:captions.keyUserId',
        );

        // Search button should always be disabled at first
        expect(searchButton).toBeDisabled();

        await user.click(fingerprintLabelledField);
        if (fingerprint) {
          await user.keyboard(fingerprint);
        }

        // Trigger validations
        await user.click(otherLabelledField);

        enabled
          ? expect(searchButton).not.toBeDisabled()
          : expect(searchButton).toBeDisabled();
      },
    );

    it.each`
      response | helperText
      ${{
  ...keyMetadata,
  status: PgpKeyMetadataStatusEnum.VERIFIED,
}} | ${'pgpKeyInfoManageForm:helpers.keyVerified'}
      ${{
  fingerprint: keyMetadata.fingerprint,
  status: PgpKeyMetadataStatusEnum.NONVERIFIED,
}} | ${'pgpKeyInfoManageForm:helpers.keyNotVerified'}
      ${{
  fingerprint: keyMetadata.fingerprint,
  status: PgpKeyMetadataStatusEnum.REVOKED,
}} | ${'pgpKeyInfoManageForm:helpers.keyRevoked'}
      ${{}}    | ${'pgpKeyInfoManageForm:helpers.keyNotFound'}
    `(
      'Should show $helperText if response is $response',
      async ({ response, helperText }) => {
        server.use(
          rest.post(`${backend}pgpkey/unique/`, (req, res, ctx) => {
            return res(resJson(ctx), resBody(ctx, {}));
          }),
          rest.post(`${backend}pgpkey/metadata/`, (req, res, ctx) => {
            return res(resJson(ctx), resBody(ctx, response));
          }),
        );

        const user = userEvent.setup();
        const fingerprintLabelledField = await getFingerprintLabelledField();
        const searchButton = await getSearchButton();
        const otherLabelledField = await screen.findByLabelText(
          'pgpKeyInfoManageForm:captions.keyUserId',
        );

        await user.click(fingerprintLabelledField);
        await user.keyboard(testFingerprint);
        await user.click(otherLabelledField);

        expect(searchButton).not.toBeDisabled();
        await user.click(searchButton);

        expect(await screen.findByText(helperText)).toBeInTheDocument();
      },
    );
  });
});
