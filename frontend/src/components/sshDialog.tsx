import React, { MouseEventHandler, ReactElement } from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import Dialog from '@mui/material/Dialog';
import { CopyToClipboardTextField } from '@biomedit/next-widgets';

type SSHDialogProps = {
  command: string;
  onClose: MouseEventHandler;
  open: boolean;
};

export function SSHDialog({
  command,
  onClose,
  open,
}: SSHDialogProps): ReactElement {
  return (
    <Dialog onClose={onClose} aria-labelledby="ssh-dialog" open={open}>
      <DialogTitle>Connect To Your Instance</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To connect, execute the following command in your terminal.
        </DialogContentText>
        <CopyToClipboardTextField
          value={command}
          id="command"
          label="Command"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
