import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { getInitialState, makeStore, RootState } from '../../store';
import { AuthState } from '../../reducers/auth';
import React from 'react';
import {
  ProjectList,
  projectListItemName,
  ProjectListProps,
} from './ProjectList';
import {
  DataTransfer,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  Project,
  ProjectPermissionsEditUsersEnum,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import {
  addIconButtonLabelPrefix,
  DeepWriteable,
  deleteIconButtonLabelPrefix,
  editIconButtonLabelPrefix,
  expectToBeInTheDocument,
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import {
  resourceTableAddButtonLabel,
  resourceTableTitle,
} from './ResourceTable';
import {
  dtrTableAddButtonLabel,
  dtrTableTitle,
} from '../dataTransfer/DataTransferTable';
import { rest } from 'msw';
import { backend, generatedBackendApi } from '../../api/api';
import { allProjectsKey, archivedProjectsKey } from './ProjectListHooks';
import { act } from 'react-dom/test-utils';
import {
  createBatch,
  createDataProvider,
  createNodeAdminAuthState,
  createAuthState,
  createStaffAuthState,
  createUser,
  createProject,
  createProjectUser,
  createProjectPermissions,
  createProjectPermissionsEdit,
} from '../../../factories';

const dtrs: Array<DataTransfer> = [
  {
    id: 1,
    packages: [],
    requestorName: 'Chuck Norris (chuck.norris@roundhouse.kick)',
    requestorDisplayId: 'ID: 63457666153',
    requestorFirstName: 'Chuck',
    requestorLastName: 'Norris',
    requestorEmail: 'chuck.norris@roundhouse.kick',
    projectName: 'Test Project 1',
    project: 1,
    maxPackages: 1,
    status: DataTransferStatusEnum.INITIAL,
    dataProvider: 'area51',
    requestor: 2,
    purpose: DataTransferPurposeEnum.TEST,
  },
];

// A Node Admin can do anything except changing destination.
const editNodeAdmin = {
  name: true,
  code: true,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

// A Project Leader can NOT edit the project but can manage his people
const editPl = {
  name: false,
  code: false,
  destination: false,
  users: [
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: false,
  resources: false,
};

// Staff users are super-users and can do what they want with the project
const editStaff = {
  name: true,
  code: true,
  destination: true,
  users: [
    ProjectPermissionsEditUsersEnum.PL,
    ProjectPermissionsEditUsersEnum.PM,
    ProjectPermissionsEditUsersEnum.DM,
    ProjectPermissionsEditUsersEnum.USER,
  ],
  ipAddressRanges: true,
  resources: true,
};

type WriteableProject = DeepWriteable<Project>;

const itemList: Array<WriteableProject> = [
  createProject({
    dataTransferCount: 0,
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] })],
    archived: false,
  }),
  createProject({
    dataTransferCount: 1,
    resources: [],
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] })],
    archived: false,
  }),
  createProject({
    dataTransferCount: 0,
    resources: [],
    permissions: createProjectPermissions({
      edit: createProjectPermissionsEdit(editNodeAdmin),
    }),
    archived: false,
  }),
  createProject({
    dataTransferCount: 0,
    resources: [],
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.PL] })],
    permissions: createProjectPermissions({
      edit: createProjectPermissionsEdit(editPl),
    }),
    archived: false,
  }),
  createProject({
    dataTransferCount: 0,
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.PL] })],
    permissions: createProjectPermissions({
      edit: createProjectPermissionsEdit(editPl),
    }),
    archived: false,
  }),
  createProject({
    dataTransferCount: 0,
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] })],
    archived: true,
  }),
  createProject({
    dataTransferCount: 1,
    resources: [],
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] })],
    archived: true,
  }),
  createProject({
    dataTransferCount: 0,
    resources: [],
    archived: true,
  }),
  createProject({
    dataTransferCount: 0,
    resources: [],
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.PL] })],
    archived: true,
  }),
  createProject({
    dataTransferCount: 0,
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.PL] })],
    archived: true,
  }),
  createProject({
    dataTransferCount: 1,
    resources: [],
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] })],
    permissions: createProjectPermissions({
      edit: createProjectPermissionsEdit(editStaff),
    }),
    archived: false,
  }),
  createProject({
    dataTransferCount: 0,
    users: [createProjectUser({ roles: [ProjectUsersInnerRolesEnum.PL] })],
    permissions: createProjectPermissions({
      edit: createProjectPermissionsEdit(editStaff),
    }),
    archived: false,
  }),
];

const userStates = {
  staff: { auth: createStaffAuthState() }, // Staff
  nodeAdmin: { auth: createNodeAdminAuthState() }, // Node admin
  user: { auth: createAuthState() }, // Simple project user
};

describe('ProjectList', () => {
  describe('Component', () => {
    const server: MockServer = setupMockApi();

    afterEach(() => {
      // Reset any runtime handlers tests may use.
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    const renderProjectList = (
      authState: AuthState,
      projects?: Array<WriteableProject>,
      props?: ProjectListProps,
    ) => {
      // mocked since project is parametrized as an object, for `msw` the backend representation would be necessary
      jest
        .spyOn(generatedBackendApi, 'listProjectsRaw')
        .mockImplementation(() => {
          return new Promise((resolve) => {
            // @ts-expect-error not relevant for logic to be tested
            resolve({
              value: () =>
                new Promise((resolve2) => {
                  // @ts-expect-error not relevant for logic to be tested
                  resolve2([project]);
                }),
            });
          });
        });

      const dataProviders = [
        createDataProvider({
          coordinators: [
            createUser({ firstName: 'Chuck', lastName: 'Norris' }),
          ],
        }),
      ];

      server.use(
        rest.get(`${backend}users/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, createBatch(createUser, 2))),
        ),
        rest.get(`${backend}data-provider/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, dataProviders)),
        ),
        rest.get(`${backend}nodes/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
        rest.get(`${backend}data-transfer/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, dtrs)),
        ),
        rest.get(`${backend}identity/group/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
      );

      const state: DeepWriteable<RootState> = {
        ...getInitialState(),
        ...{
          project: {
            isFetching: false,
            isSubmitting: false,
            itemList: projects ? projects : [itemList[0]],
            ipAddress: '127.0.0.1',
          },
          ...{
            dataProvider: {
              isFetching: false,
              isSubmitting: false,
              itemList: dataProviders,
            },
          },
          ...{
            auth: authState,
          },
        },
      };
      render(
        <Provider
          // @ts-expect-error project role is expected to be an enum, which it can't be in a json file
          store={makeStore(undefined, state)}
        >
          <ProjectList {...props} />
        </Provider>,
      );
    };
    it('should allow adding a new project as node admin', async () => {
      renderProjectList(userStates['nodeAdmin'].auth, undefined);
      await screen.findByRole('button', {
        name: 'common:models.project',
      });
    });

    it.each`
      projectIndex | role           | showResources | canAddResource | showDtrs | canAddDtr | canEditProject | canDeleteProject
      ${0}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${0}         | ${'user'}      | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${1}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${2}         | ${'nodeAdmin'} | ${false}      | ${true}        | ${false} | ${false}  | ${true}        | ${false}
      ${3}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${4}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${true}        | ${false}
      ${5}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${6}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${6}         | ${'staff'}     | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${true}
      ${6}         | ${'user'}      | ${false}      | ${false}       | ${true}  | ${false}  | ${false}       | ${false}
      ${7}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${8}         | ${'nodeAdmin'} | ${false}      | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${'nodeAdmin'} | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${false}
      ${9}         | ${'staff'}     | ${true}       | ${false}       | ${false} | ${false}  | ${false}       | ${true}
      ${10}        | ${'staff'}     | ${false}      | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
      ${11}        | ${'staff'}     | ${true}       | ${true}        | ${true}  | ${true}   | ${true}        | ${true}
    `(
      'should show resources ($showResources), ' +
        'can add resource ($canAddResource), ' +
        'show DTRs ($showDtrs), ' +
        'can add DTR ($canAddDtr), ' +
        'can edit the project ($canEditProject), ' +
        'can delete the project ($canDeleteProject), ' +
        'on project with index $projectIndex, ' +
        'if user role is $role',
      async ({
        projectIndex,
        role,
        showResources,
        canAddResource,
        showDtrs,
        canAddDtr,
        canEditProject,
        canDeleteProject,
      }) => {
        const authState = userStates[role].auth;
        const project = itemList[projectIndex];
        const { hasProjects, staff, nodeAdmin, nodeViewer } =
          authState.user.permissions;
        const canSeeFilter = hasProjects && (staff || nodeAdmin || nodeViewer);
        renderProjectList(authState, [project]);

        expectToBeInTheDocument(
          screen.queryByText('projectList:archived'),
          (project.archived ?? false) && !canSeeFilter,
        );

        if (project.archived && canSeeFilter) {
          // for staff and node admins, archived projects are only visible under "Archived Projects"
          fireEvent.mouseDown(await screen.findByText(allProjectsKey));
          fireEvent.click(await screen.findByText(archivedProjectsKey));
          expect(
            screen.queryByText('projectList:archived'),
          ).toBeInTheDocument();
        }

        const accordion = await screen.findByText(project.name);

        await act(() => accordion.click());

        await screen.findByText('captions.code'); // wait for project to be expanded

        const resources = await screen.queryByText(resourceTableTitle);
        expectToBeInTheDocument(resources, showResources || canAddResource);

        if (showResources) {
          const resource = project?.resources?.[0].name;
          expect(resource).toBeDefined();
          await screen.getByText(resource as string); // safe cast as we verified it to be defined
        } else {
          expect(project.resources).toHaveLength(0);
        }

        const addResourceBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + resourceTableAddButtonLabel,
        });
        expectToBeInTheDocument(addResourceBtn, canAddResource);

        const dtrs = screen.queryByText(dtrTableTitle);
        expectToBeInTheDocument(dtrs, showDtrs);

        const addDtrBtn = screen.queryByRole('button', {
          name: addIconButtonLabelPrefix + dtrTableAddButtonLabel,
        });
        expectToBeInTheDocument(addDtrBtn, canAddDtr);

        const editProjectBtn = screen.queryByRole('button', {
          name: editIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(editProjectBtn, canEditProject);

        const deleteProjectBtn = screen.queryByRole('button', {
          name: deleteIconButtonLabelPrefix + projectListItemName,
        });
        expectToBeInTheDocument(deleteProjectBtn, canDeleteProject);
      },
    );

    it.each`
      ids       | selected
      ${[2, 3]} | ${'test_project_2'}
      ${[2, 3]} | ${undefined}
    `(
      'should display the selected project if any',
      async ({ ids, selected }) => {
        const projects = ids.map((id) =>
          createProject({
            id: id,
            code: `test_project_${id}`,
            dataTransferCount: 0,
            resources: [],
            users: [
              createProjectUser({ roles: [ProjectUsersInnerRolesEnum.USER] }),
            ],
            archived: false,
          }),
        );
        renderProjectList(userStates['staff'].auth, projects, { selected });
        // Add button is always there
        await screen.findByRole('button', {
          name: 'common:models.project',
        });
        expect(
          await screen.findAllByText('test_project', { exact: false }),
        ).toHaveLength(selected ? 1 : ids.length);
      },
    );
  });
});
