import React, { ReactElement, useMemo } from 'react';
import {
  ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { Description } from '@biomedit/next-widgets';
import { useUserRoleEntries } from '../../widgets/UserRoleEntries';
import { I18nNamespace } from '../../i18n';

export const ProjectUserTable = (
  projectUsers: Array<ProjectUsersInner>,
): ReactElement => {
  const usersByRole = useMemo(() => {
    const roleDict: Partial<
      Record<ProjectUsersInnerRolesEnum, ProjectUsersInner[]>
    > = {};
    // Preserve the order in which the users will be listed
    for (const role in ProjectUsersInnerRolesEnum) {
      roleDict[role] = [];
    }
    projectUsers.forEach((user) => {
      user.roles.forEach((role) => {
        roleDict[role] = (roleDict[role] ?? []).concat([user]);
      });
    });
    return roleDict;
  }, [projectUsers]);

  const userRoleEntries = useUserRoleEntries(
    usersByRole,
    I18nNamespace.PROJECT_LIST,
  );

  return <Description entries={userRoleEntries} labelWidth="20%" />;
};
