import { Required } from 'utility-types';
import { Project, ProjectResourcesInner } from '../../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../../i18n';
import React, { useMemo } from 'react';
import { EmailLink, useFormDialog } from '@biomedit/next-widgets';
import { ResourceForm } from './ResourceForm';
import { ResourceField } from './ResourceField';
import { createColumnHelper } from '@tanstack/react-table';

export const useResourceColumns = () => {
  const columnHelper = createColumnHelper<Required<ProjectResourcesInner>>();
  const { t } = useTranslation(I18nNamespace.PROJECT_RESOURCE_LIST);
  return useMemo(() => {
    return [
      columnHelper.accessor('name', {
        header: t('columns.name'),
        cell: ({ row: { original } }) => ResourceField(original),
      }),
      columnHelper.accessor('description', {
        header: t('columns.description'),
      }),
      columnHelper.accessor('contact', {
        header: t('columns.contact'),
        cell: (props) =>
          props.getValue() ? EmailLink({ email: props.getValue() }) : null,
      }),
    ];
  }, [t, columnHelper]);
};

export const useResourceForm = (project: Project) => {
  const newResource = {
    name: undefined,
    location: undefined,
    description: undefined,
    contact: undefined,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<ProjectResourcesInner>>(newResource);
  const resourceForm = data && (
    <ResourceForm project={project} resource={data} onClose={closeFormDialog} />
  );
  return {
    resourceForm,
    ...rest,
  };
};
