import {
  IconButton,
  InputAdornment,
  TextField,
  TextFieldProps,
} from '@mui/material';
import { PersonAdd } from '@mui/icons-material';
import React, { useState } from 'react';
import defaultTo from 'lodash/defaultTo';
import { useFormContext } from 'react-hook-form';
import { FieldValues } from 'react-hook-form/dist/types';
import { FieldPath } from 'react-hook-form/dist/types/path';
import isemail from 'isemail';
import { generatedBackendApi } from '../api/api';
import {
  ConvertedProjectUser,
  useUserEntries,
} from './projectForm/UsersListHooks';
import {
  BaseReducerState,
  Description,
  Dialog,
  useDialogState,
} from '@biomedit/next-widgets';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { IdRequired } from '../global';
import { CustomAffiliation } from '../api/generated';
import { useTranslation } from 'next-i18next';
import { I18nNamespace } from '../i18n';

type UserTextFieldProps<T extends FieldValues> = {
  name: FieldPath<T>;
  addUser: (user: ConvertedProjectUser) => void;
} & Omit<TextFieldProps, 'value' | 'type' | 'error' | 'helperText'>;

export function UserTextField<T extends FieldValues>({
  name,
  addUser,
  ...props
}: UserTextFieldProps<T>) {
  const { t } = useTranslation([I18nNamespace.USER_TEXT_FIELD]);
  const [inputValue, setInputValue] = useState<string>('');
  const {
    item: user,
    setItem: setUser,
    onClose,
    open,
  } = useDialogState<ConvertedProjectUser | null>();

  const {
    clearErrors,
    setError,
    formState: { errors },
  } = useFormContext<T>();

  const { itemList: customAffiliations } = useSelector<
    RootState,
    BaseReducerState<IdRequired<CustomAffiliation>>
  >((state) => state.customAffiliation);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    clearErrors(name);
    setInputValue(defaultTo(event.target.value, ''));
  };

  const userEntries = useUserEntries(user);

  const fetchUser = (email: string): void => {
    if (!isemail.validate(email, { minDomainAtoms: 2 })) {
      setError(name, {
        message: t('invalidEmail'),
      });
      return;
    }
    generatedBackendApi
      .listUsers({ email })
      .then((response) => {
        if (response.length === 0) {
          setError(name, {
            message: t('userNotFound'),
          });
          return;
        }
        const user = response[0];
        const projectUser: ConvertedProjectUser = {
          displayName: user?.profile?.displayName,
          email: user?.email,
          affiliation: user?.profile?.affiliation
            ? user?.profile?.affiliation
            : customAffiliations.find(
                (customAffiliation) =>
                  customAffiliation.id === user?.profile?.customAffiliation,
              )?.code,
          id: user.id,
          // initialize checkboxes to be empty
          roles: {
            DM: false,
            PL: false,
            PM: false,
            USER: false,
          },
          ...user,
        };
        setUser(projectUser);
      })
      .catch((error) => {
        setError(name, {
          message: error.message,
        });
      });
  };

  const AddUserButton = () => {
    return (
      <IconButton
        color="primary"
        aria-label="Add user"
        title="Add user"
        onClick={() => {
          fetchUser(inputValue);
        }}
        size="medium"
        disabled={inputValue.length === 0}
      >
        <PersonAdd />
      </IconButton>
    );
  };

  return (
    <>
      {user && (
        <Dialog
          title={'User'}
          isSubmitting={false}
          open={open}
          confirmLabel="Add"
          confirmButtonProps={{
            onClick: () => {
              addUser(user as ConvertedProjectUser);
              setUser(null);
              setInputValue('');
            },
            type: 'button',
          }}
          text={t('addUserConfirmationText')}
          onClose={onClose}
          maxWidth={false}
        >
          <Description
            entries={userEntries}
            labelWidth="30%"
            styleRules={{
              caption: {
                padding: 0,
                verticalAlign: 'middle',
              },
              value: {
                padding: 0,
              },
              row: {
                height: '2.5rem',
              },
              table: {
                marginTop: '1rem',
              },
            }}
          />
        </Dialog>
      )}

      <TextField
        sx={{ marginTop: 0 }}
        size="small"
        margin="normal"
        fullWidth
        placeholder="Enter email address"
        error={!!errors?.[name]}
        helperText={errors?.[name]?.message as string}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <AddUserButton />
            </InputAdornment>
          ),
        }}
        value={inputValue}
        type="email"
        onChange={onChange}
        {...props}
      />
    </>
  );
}
