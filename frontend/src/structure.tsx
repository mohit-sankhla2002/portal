import { PagePermissions } from './permissions';
import React from 'react';
import { $Keys } from 'utility-types';
import { Menu } from '@biomedit/next-widgets';
import {
  ChatBubble,
  Feedback,
  Home,
  Key,
  People,
  Person,
  GridOn,
  Work,
  MarkChatUnread,
  Sledding,
  Logout,
} from '@mui/icons-material';
import { logoutUrl } from './api/api';

export type StructureItem = {
  title: string;
  menu?: Menu;
  permission?: $Keys<PagePermissions>;
};

export const useStructureItems = (
  hasUnreadMessages = false,
  isChristmas = false,
) => {
  const structureItems: StructureItem[] = [
    {
      title: 'Home',
      menu: { link: '/', icon: <Home /> },
    },
    {
      title: 'Profile',
      menu: {
        link: '/profile',
        icon: isChristmas ? <Sledding /> : <Person />,
      },
    },
    {
      title: 'Administration',
      menu: { link: '/admin', icon: <People /> },
      permission: 'userManage',
    },
    {
      title: 'Projects',
      menu: { link: '/projects', icon: <GridOn /> },
    },
    {
      title: 'Data Transfers',
      menu: { link: '/data-transfers', icon: <Work /> },
      permission: 'dataTransfer',
    },
    {
      title: 'Messages',
      menu: {
        link: '/messages',
        icon: hasUnreadMessages ? <MarkChatUnread /> : <ChatBubble />,
      },
    },
    {
      title: 'My Keys',
      menu: { link: '/keys', icon: <Key /> },
    },
    {
      title: 'Contact Us',
      menu: { link: '/contact', icon: <Feedback /> },
    },
    {
      title: 'Logout',
      menu: { link: logoutUrl, icon: <Logout /> },
    },
  ];

  return structureItems;
};
