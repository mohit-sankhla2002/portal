// Used to uniquely identify HTML elements in component tests using react-testing-library with `screen.getByTestId(...)`
export enum TestId {
  FINGERPRINT = 'requestor-pgp-key-fingerprint',
  REQUESTOR = 'requestor',
}
