import { ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import { useStructureItems } from './structure';

describe('useStructureItems', () => {
  it.each`
    hasUnreadMessages | expectedIcon
    ${false}          | ${'ChatBubbleIcon'}
    ${true}           | ${'MarkChatUnreadIcon'}
    ${undefined}      | ${'ChatBubbleIcon'}
  `(
    'Should show $expectedIcon if hasUnreadMessages is $hasUnreadMessages',
    async ({ hasUnreadMessages, expectedIcon }) => {
      const messagesItem = useStructureItems(hasUnreadMessages).find(
        (item) => item.title === 'Messages',
      );

      render(messagesItem?.menu?.icon as ReactElement);
      await screen.findByTestId(expectedIcon);
    },
  );

  it.each`
    isChristmas  | expectedIcon
    ${false}     | ${'PersonIcon'}
    ${true}      | ${'SleddingIcon'}
    ${undefined} | ${'PersonIcon'}
  `(
    'Should show $expectedIcon if isChristmas is $isChristmas',
    async ({ isChristmas, expectedIcon }) => {
      const profileItem = useStructureItems(false, isChristmas).find(
        (item) => item.title === 'Profile',
      );

      render(profileItem?.menu?.icon as ReactElement);
      await screen.findByTestId(expectedIcon);
    },
  );
});
