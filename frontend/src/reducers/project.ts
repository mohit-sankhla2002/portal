import {
  ADD_PROJECT,
  ARCHIVE_PROJECT,
  DELETE_PROJECT,
  LOAD_PROJECTS,
  LOGIN,
  LoginAction,
  UNARCHIVE_PROJECT,
  UPDATE_APPROVAL,
  UPDATE_PROJECT,
} from '../actions/actionTypes';
import {
  AnyFunction,
  ApiAction,
  BaseReducerState,
  isIpInRange,
} from '@biomedit/next-widgets';
import { produce } from 'immer';
import { Project } from '../api/generated';
import {
  onAddSuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import { IdRequired } from '../global';

export type ProjectState = BaseReducerState<IdRequired<Project>> & {
  ipAddress?: string;
};

export const initialProjectState: ProjectState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
  ipAddress: undefined,
};

export function projectReducer(
  state: ProjectState = initialProjectState,
  action:
    | LoginAction
    | ApiAction<typeof LOAD_PROJECTS>
    | ApiAction<typeof UPDATE_PROJECT>
    | ApiAction<typeof ADD_PROJECT>
    | ApiAction<typeof DELETE_PROJECT>
    | ApiAction<typeof ARCHIVE_PROJECT>
    | ApiAction<typeof UNARCHIVE_PROJECT>
    | ApiAction<typeof UPDATE_APPROVAL>,
): ProjectState {
  return produce(state, (draft: ProjectState) => {
    switch (action.type) {
      case LOGIN:
        draft.ipAddress = action?.response?.ipAddress;
        break;
      case LOAD_PROJECTS.request:
        onFetchRequest(draft);
        break;
      case LOAD_PROJECTS.success:
        onLoadSuccess(
          draft,
          action.response instanceof Array
            ? action.response
            : [action.response],
          postProcessProject(draft.ipAddress),
        );
        break;
      case LOAD_PROJECTS.failure:
        onFetchFailure(draft);
        break;
      case ARCHIVE_PROJECT.success:
      case UPDATE_PROJECT.success:
      case UNARCHIVE_PROJECT.success:
        onUpdateSuccess(
          draft,
          action.response,
          postProcessProject(draft.ipAddress),
        );
        break;
      case UPDATE_PROJECT.failure:
      case ADD_PROJECT.failure:
      case DELETE_PROJECT.failure:
      case ARCHIVE_PROJECT.failure:
      case UNARCHIVE_PROJECT.failure:
        onSubmitFailure(draft);
        break;
      case UPDATE_PROJECT.request:
      case ADD_PROJECT.request:
      case DELETE_PROJECT.request:
      case ARCHIVE_PROJECT.request:
      case UNARCHIVE_PROJECT.request:
        onSubmitRequest(draft);
        break;
      case ADD_PROJECT.success:
        onAddSuccess(
          draft,
          action.response,
          postProcessProject(draft.ipAddress),
        );
        break;
      case DELETE_PROJECT.success:
        onDeleteSuccess(draft, action?.requestArgs?.id);
        break;
    }
  });
}

function postProcessProject(ip: string | undefined): AnyFunction {
  return (project) => ({
    ipAddressInRange: isIpInProjectRanges(ip, project.ipAddressRanges),
    ...project,
  });
}

function isIpInProjectRanges(ip: string | undefined, ipAddressRanges): boolean {
  return (
    !ip ||
    !ipAddressRanges ||
    ipAddressRanges.length === 0 ||
    ipAddressRanges.some(({ ipAddress, mask }) =>
      isIpInRange(ip, ipAddress, mask),
    )
  );
}
