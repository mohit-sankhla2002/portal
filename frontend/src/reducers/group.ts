import { produce } from 'immer';
import { Group, GroupPermissionsObjectInner } from '../api/generated';
import { ApiAction, BaseReducerState } from '@biomedit/next-widgets';
import {
  ADD_GROUP,
  DELETE_GROUP,
  LOAD_GROUPS,
  UPDATE_GROUP,
} from '../actions/actionTypes';
import {
  onAddSuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import { IdRequired } from '../global';

export type GroupState = BaseReducerState<IdRequired<Group>>;

export const initialGroupState: GroupState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
};

export function groupReducer(
  state: GroupState = initialGroupState,
  action:
    | ApiAction<typeof ADD_GROUP>
    | ApiAction<typeof LOAD_GROUPS>
    | ApiAction<typeof UPDATE_GROUP>
    | ApiAction<typeof DELETE_GROUP>,
): GroupState {
  return produce(state, (draft: GroupState) => {
    switch (action.type) {
      case LOAD_GROUPS.request:
        onFetchRequest(draft);
        break;
      case LOAD_GROUPS.failure:
        onFetchFailure(draft);
        break;
      case ADD_GROUP.request:
      case UPDATE_GROUP.request:
      case DELETE_GROUP.request:
        onSubmitRequest(draft);
        break;
      case ADD_GROUP.failure:
      case UPDATE_GROUP.failure:
      case DELETE_GROUP.failure:
        onSubmitFailure(draft);
        break;
      case LOAD_GROUPS.success: {
        onLoadSuccess(draft, action.response);
        break;
      }
      case ADD_GROUP.success:
        onAddSuccess(draft, action.response);
        break;
      case UPDATE_GROUP.success:
        onUpdateSuccess(draft, action.response);
        break;
      case DELETE_GROUP.success: {
        onDeleteSuccess(
          draft,
          action?.requestArgs?.id,
          withUpdatedPermissionsObject(action?.requestArgs?.id),
        );
        break;
      }
    }
  });
}

export function withUpdatedPermissionsObject(
  deletedItemId?: string,
): (item: IdRequired<Group>) => IdRequired<Group> {
  function withUpdatedObjects(
    permissionsObject: GroupPermissionsObjectInner,
    deletedItemId?: string,
  ): GroupPermissionsObjectInner {
    const updatedObjects = permissionsObject.objects.filter(
      (id) => String(id) !== String(deletedItemId),
    );

    return { ...permissionsObject, objects: updatedObjects };
  }

  return (item: IdRequired<Group>) => {
    const updatedPermissionsObject = item.permissionsObject
      ?.map((permissionsObject: GroupPermissionsObjectInner) =>
        withUpdatedObjects(permissionsObject, deletedItemId),
      )
      .filter(
        (permissionObject: GroupPermissionsObjectInner) =>
          permissionObject.objects.length !== 0,
      );
    return { ...item, permissionsObject: updatedPermissionsObject };
  };
}
