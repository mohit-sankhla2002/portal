import { initialProjectState, projectReducer } from './project';
import {
  makeMockStore,
  MockServer,
  requestAction,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { LOAD_PROJECTS } from '../actions/actionTypes';
import { backend } from '../api/api';
import { rest } from 'msw';
import { rootSaga } from '../actions/sagas';
import { createProject, toSnakeCased } from '../../factories';

describe('projectReducer', () => {
  let store;
  const server: MockServer = setupMockApi();

  afterEach(() => {
    // Reset any runtime handlers tests may use.
    server.resetHandlers();
  });

  afterAll(() => {
    server.close();
  });

  beforeEach(() => {
    store = makeMockStore(
      { project: initialProjectState },
      { project: projectReducer },
      rootSaga,
    );
  });

  it('should add projects to `itemList` when `LOAD_PROJECTS` is dispatched', async () => {
    // given
    const project = createProject({ users: [] });
    const snakeCasedProject = toSnakeCased(project);

    server.use(
      rest.get(`${backend}projects/`, (req, res, ctx) =>
        res(resJson(ctx), resBody(ctx, [snakeCasedProject, createProject()])),
      ),
    );

    // when
    store.dispatch(requestAction(LOAD_PROJECTS));
    await store.waitFor(LOAD_PROJECTS.success);

    // then
    const projectState = store.getState().project;
    const projectListItems = projectState.itemList;

    expect(projectListItems).toHaveLength(2);

    expect(projectListItems[0]).toMatchObject(project);
  });
});
