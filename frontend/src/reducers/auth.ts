import {
  ADD_PROJECT,
  CHECK_AUTH,
  LOGIN,
  LoginAction,
  UPDATE_PROJECT,
  UPDATE_USER_PROFILE,
} from '../actions/actionTypes';
import { produce } from 'immer';
import { Project, Userinfo, UserinfoPermissions } from '../api/generated';
import { noPermissions, PagePermissions } from '../permissions';
import { StatusCodes } from 'http-status-codes';
import {
  ApiAction,
  DeepWriteable,
  isSerializedResponse,
  SerializedResponse,
} from '@biomedit/next-widgets';

export type AuthState = {
  isAuthenticated: boolean;
  isFetching: boolean;
  isSubmitting: boolean;
  isExistingUsername: boolean;
  invalidMsg: string | null;
  user: Userinfo | null;
  pagePermissions: PagePermissions;
};

export const initialState: AuthState = {
  isAuthenticated: false,
  isFetching: false,
  isSubmitting: false,
  isExistingUsername: false,
  invalidMsg: null,
  user: null,
  pagePermissions: noPermissions,
};

export function auth(
  state: AuthState = initialState,
  action:
    | ApiAction<typeof UPDATE_USER_PROFILE>
    | ApiAction<typeof CHECK_AUTH>
    | ApiAction<typeof ADD_PROJECT>
    | ApiAction<typeof UPDATE_PROJECT>
    | LoginAction,
): AuthState {
  return produce(state, (draft: AuthState) => {
    switch (action.type) {
      case CHECK_AUTH.request:
        draft.isFetching = true;
        break;
      case LOGIN:
        draft.isFetching = false;
        draft.user = action.response;
        if (action.response.permissions) {
          draft.pagePermissions = pagePermissions(action.response.permissions);
        }
        draft.isAuthenticated = true;
        break;
      case CHECK_AUTH.failure:
        draft.isFetching = false;
        draft.isAuthenticated = false;
        break;
      case UPDATE_USER_PROFILE.request:
        draft.isSubmitting = true;
        draft.invalidMsg = null;
        draft.isExistingUsername = false;
        break;
      case UPDATE_USER_PROFILE.success:
        draft.isSubmitting = false;
        if (action.response && draft.user) {
          // action.response is of type User (NOT Userinfo), so we need to merge because User doesn't contain as much data as Userinfo
          draft.user = Object.assign({}, draft.user, action.response);
        }
        break;
      case UPDATE_USER_PROFILE.failure: {
        draft.isSubmitting = false;
        if (isSerializedResponse(action.error)) {
          const serializedResponse = action.error as SerializedResponse;
          const status = serializedResponse.status;
          if (status === StatusCodes.CONFLICT) {
            draft.isExistingUsername = true;
          } else if (status === StatusCodes.UNPROCESSABLE_ENTITY) {
            draft.invalidMsg = (
              serializedResponse.body as Record<string, string>
            ).detail;
          }
        }
        break;
      }
      case ADD_PROJECT.success:
      case UPDATE_PROJECT.success:
        if (action.response) {
          updateHasProjects(draft.user, action.response);
        }
        break;
    }
  });
}

function pagePermissions(roles: UserinfoPermissions): PagePermissions {
  return {
    project: {
      del: !!roles.staff,
      add: !!roles.staff || !!roles.nodeAdmin,
      edit: !!(roles.manager || roles.staff || roles.nodeAdmin),
    },
    dataTransfer: {
      dataManager: !!roles.dataManager,
      projectLeader: !!roles.projectLeader,
      staff: !!roles.staff,
      dataProviderAdmin: !!roles.dataProviderAdmin,
      dataProviderViewer: !!roles.dataProviderViewer,
      nodeViewer: !!roles.nodeViewer,
      nodeAdmin: !!roles.nodeAdmin,
      legalApprover: !!roles.legalApprover,
    },
    userManage: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
      dataProviderAdmin: !!roles.dataProviderAdmin,
      dataProviderViewer: !!roles.dataProviderViewer,
      groupManager: !!roles.groupManager,
    },
  };
}

/**
 * Sets {@code hasProjects} to {@code true}, if the currently logged in user
 * has {@code hasProjects = false} and is in the users of a created or modified project.
 *
 * {@code hasProjects} is only updated when the user logs in, which means when the user
 * doesn't have any projects after login, but does after adding themselves to the
 * users of a project, it would not be changed automatically, since no login is performed
 * again.
 */
function updateHasProjects(
  user: DeepWriteable<Userinfo> | null,
  project: Project,
) {
  if (
    user?.permissions &&
    !user?.permissions?.hasProjects &&
    project.users.some(
      (projectUser) =>
        projectUser?.username === user?.username && projectUser?.roles?.length,
    )
  ) {
    user.permissions.hasProjects = true;
  }
}
