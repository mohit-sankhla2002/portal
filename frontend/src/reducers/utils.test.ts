import { BaseReducerState, mockConsoleError } from '@biomedit/next-widgets';
import {
  noIdErrorMessage,
  noUpdatedItemErrorMessage,
  onAddSuccess,
  onClear,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';

type ItemType = {
  id: number;
  name: string;
};

let itemList: ItemType[];
let store: BaseReducerState<ItemType>;

beforeEach(() => {
  store = {
    isFetching: false,
    isSubmitting: false,
    itemList: [],
  };
  itemList = [
    { id: 1, name: 'foo' },
    { id: 2, name: 'bar' },
  ];
});

describe('utils', () => {
  describe('onFetchRequest', () => {
    it('Should set isFetching to true', () => {
      onFetchRequest(store);

      expect(store.isFetching).toBe(true);
    });
  });

  describe('onFetchFailure', () => {
    it('Should set isFetching to false', () => {
      onFetchRequest(store);
      onFetchFailure(store);

      expect(store.isFetching).toBe(false);
    });
  });

  describe('onLoadSuccess', () => {
    it('Should set isFetching to false and itemList to response', () => {
      onFetchRequest(store);
      onLoadSuccess(store, itemList);

      expect(store.isFetching).toBe(false);
      expect(store.itemList).toBe(itemList);
    });

    it('Should set itemList to [] if response is undefined', () => {
      onFetchRequest(store);
      onLoadSuccess(store, undefined);

      expect(store.isFetching).toBe(false);
      expect(store.itemList).toStrictEqual([]);
    });

    it('Should update itemList according to postProcessFunction', () => {
      const processedItem: ItemType = { id: 42, name: 'foobar' };
      const postProcessingFunction = () => processedItem;

      onFetchRequest(store);
      onLoadSuccess(store, itemList, postProcessingFunction);

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(2);
      expect(store.itemList).toContain(processedItem);
    });
  });

  describe('onSubmitRequest', () => {
    it('Should set isSubmitting to true', () => {
      onSubmitRequest(store);

      expect(store.isSubmitting).toBe(true);
    });
  });

  describe('onSubmitFailure', () => {
    it('Should set isSubmitting to false', () => {
      onSubmitRequest(store);
      onSubmitFailure(store);

      expect(store.isSubmitting).toBe(false);
    });
  });

  describe('onAddSuccess', () => {
    it('Should set isSubmitting to false and add the item to itemList', () => {
      const newItem: ItemType = { id: 3, name: 'baz' };

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onAddSuccess(store, newItem);

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(3);
      expect(store.itemList).toContain(newItem);
    });
  });

  describe('onUpdateSuccess', () => {
    it('Should set isSubmitting to false and update the item to itemList', () => {
      // @ts-expect-error possibly undefined warning not relevant for the test since
      // itemList is hardcoded
      const oldItem: ItemType = itemList.find((item) => item.id === 1);
      const updatedItem: ItemType = { ...oldItem, name: 'baz' };

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onUpdateSuccess(store, updatedItem);

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(2);
      expect(store.itemList).toContain(updatedItem);
      expect(store.itemList).not.toContain(oldItem);
    });

    it('Should trigger a console error if response is undefined', () => {
      const spy = mockConsoleError();

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onUpdateSuccess(store, undefined);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy.mock.calls[0][0]).toContain(noUpdatedItemErrorMessage);
      spy.mockRestore();
    });

    it('Should trigger a console error if response is missing the id property', () => {
      const spy = mockConsoleError();

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      // @ts-expect-error the typing is intentionally wrong to trigger the error
      onUpdateSuccess(store, { name: 'foo' });

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy.mock.calls[0][0]).toContain(noIdErrorMessage);
      spy.mockRestore();
    });

    it('Should update itemList according to postProcessFunction', () => {
      // @ts-expect-error possibly undefined warning not relevant for the test since
      // itemList is hardcoded
      const oldItem: ItemType = itemList.find((item) => item.id === 1);
      const updatedItem: ItemType = { ...oldItem, name: 'baz' };

      const processedItem: ItemType = { id: 42, name: 'foobar' };
      const postProcessingFunction = () => processedItem;

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onUpdateSuccess(store, updatedItem, postProcessingFunction);

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(2);
      expect(store.itemList).toContain(processedItem);
    });
  });

  describe('onDeleteSuccess', () => {
    it('Should set isSubmitting to false and remove the item from itemList', () => {
      const deletedItem: ItemType | undefined = itemList.find(
        (item) => item.id === 1,
      );

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onDeleteSuccess(store, '1');

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(1);
      expect(store.itemList).not.toContain(deletedItem);
    });

    it('Should update itemList according to postProcessFunction', () => {
      const processedItem: ItemType = { id: 42, name: 'foobar' };
      const postProcessingFunction = () => processedItem;

      onFetchRequest(store);
      onLoadSuccess(store, itemList);
      onSubmitRequest(store);
      onDeleteSuccess(store, '1', postProcessingFunction);

      expect(store.isSubmitting).toBe(false);
      expect(store.itemList).toHaveLength(1);
      expect(store.itemList).toContain(processedItem);
    });
  });

  describe('onClear', () => {
    it('Should set itemList to []', () => {
      onClear(store);

      expect(store.itemList).toHaveLength(0);
    });
  });
});
