import { produce } from 'immer';
import { DataTransfer } from '../api/generated';
import { ApiAction, BaseReducerState } from '@biomedit/next-widgets';
import {
  ADD_DATA_TRANSFER,
  DELETE_DATA_TRANSFER,
  LOAD_DATA_TRANSFERS,
  UPDATE_APPROVAL,
  UPDATE_DATA_TRANSFER,
} from '../actions/actionTypes';
import {
  onAddSuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import { IdRequired } from '../global';

export type DataTransferState = BaseReducerState<IdRequired<DataTransfer>>;

export const initialDataTransferState: DataTransferState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
};

export function dataTransfers(
  state: DataTransferState = initialDataTransferState,
  action:
    | ApiAction<typeof ADD_DATA_TRANSFER>
    | ApiAction<typeof LOAD_DATA_TRANSFERS>
    | ApiAction<typeof UPDATE_DATA_TRANSFER>
    | ApiAction<typeof DELETE_DATA_TRANSFER>
    | ApiAction<typeof UPDATE_APPROVAL>,
): DataTransferState {
  return produce(state, (draft: DataTransferState) => {
    switch (action.type) {
      case LOAD_DATA_TRANSFERS.request:
        onFetchRequest(draft);
        break;
      case LOAD_DATA_TRANSFERS.success:
        onLoadSuccess(draft, action.response);
        break;
      case LOAD_DATA_TRANSFERS.failure:
        onFetchFailure(draft);
        break;
      case ADD_DATA_TRANSFER.request:
      case UPDATE_DATA_TRANSFER.request:
      case DELETE_DATA_TRANSFER.request:
      case UPDATE_APPROVAL.request:
        onSubmitRequest(draft);
        break;
      case UPDATE_DATA_TRANSFER.success:
      case UPDATE_APPROVAL.success: {
        onUpdateSuccess(draft, action.response);
        break;
      }
      case ADD_DATA_TRANSFER.failure:
      case UPDATE_DATA_TRANSFER.failure:
      case DELETE_DATA_TRANSFER.failure:
      case UPDATE_APPROVAL.failure:
        onSubmitFailure(draft);
        break;
      case DELETE_DATA_TRANSFER.success: {
        onDeleteSuccess(draft, action?.requestArgs?.id);
        break;
      }
      case ADD_DATA_TRANSFER.success:
        onAddSuccess(draft, action.response);
        break;
    }
  });
}
