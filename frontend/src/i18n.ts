/**
 * Reflects the filenames in `public/locales/en`.
 * Make sure to add an entry here for every new file and change the value when changing the filenames.
 */
export enum I18nNamespace {
  ADMINISTRATION = 'administration',
  BOOLEAN = 'boolean',
  COMMON = 'common',
  CONTACT_US = 'contactUs',
  DATA_PROVIDER = 'dataProvider',
  DATA_TRANSFER = 'dataTransfer',
  DATA_TRANSFER_APPROVALS = 'dataTransferApprovals',
  FEED_LIST = 'feedList',
  FLAG_LIST = 'flagList',
  GROUP_LIST = 'groupList',
  GROUP_MANAGE_FORM = 'groupManageForm',
  HOME = 'home',
  IP_RANGE_LIST = 'ipRangeList',
  LIST = 'list',
  MESSAGE = 'message',
  NODE = 'node',
  PGP_KEY_INFO_LIST = 'pgpKeyInfoList',
  PGP_KEY_INFO_MANAGE_FORM = 'pgpKeyInfoManageForm',
  PROJECT_FORM = 'projectForm',
  PROJECT_LIST = 'projectList',
  PROJECT_RESOURCE_LIST = 'projectResourceList',
  PROJECT_USER_LIST = 'projectUserList',
  USER_INFO = 'userInfo',
  USER_LIST = 'userList',
  USER_MANAGE_FORM = 'userManageForm',
  USER_TEXT_FIELD = 'userTextField',
}
