import 'setimmediate';
import { generatedBackendApi } from './api/api';
import { createLogger, format, Logger, transports } from 'winston';
import TransportStream from 'winston-transport';
import { Environment } from '@biomedit/next-widgets';

/**
 * NOTE: When logging, specifying a `message` is mandatory!
 *       Otherwise, the log will not show up in Kibana.
 *
 * When using `log.error({...})`, the object may contain any key and value pairs,
 * however the values must be strings, except if the key is "json",
 * in this case the value can be any object.
 *
 * Example:
 * ```ts
 * const log = logger('Sagas');
 * const e = {some: "error"}
 * log.error({
 *   json: e,
 *   message: 'Something failed!',
 * });
 * ```
 *
 * @param namespace of the logger, usually the name of the file
 */
export const winstonLogger = (): Logger => {
  const log = createLogger({
    level: 'warn',
    format: format.json(),
    transports: [new transports.Console()],
  });

  switch (process.env.NODE_ENV) {
    case Environment.TEST:
      // expected log messages in tests should be mocked, unexpected ones will result in errors
      return createLogger({});
    case Environment.DEVELOPMENT:
      log.level = 'silly';
      break;
    case Environment.PRODUCTION:
      log.add(new RemoteLogger());
      break;
  }

  return log;
};

class RemoteLogger extends TransportStream {
  log(info, callback) {
    generatedBackendApi
      .createLog({ body: info })
      .then(() => {
        this.emit('logged', info);
      })
      .catch((error) => {
        this.emit('warn', error);
      });

    if (callback) {
      setImmediate(callback);
    }
  }
}
