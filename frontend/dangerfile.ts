import { danger, fail, schedule, warn } from 'danger';
import {
  checkBreakingChangeFooter,
  checkForbiddenWords,
  notifyTechnicalCoordinators,
  praiseDependencyReduction,
  warnNoTests,
} from '@biomedit/danger';

// Frontend: warn if there are changes in the application code but not in tests
warnNoTests(
  ['frontend/**/*.ts*'],
  ['frontend/**/*.test.*', 'frontend/test-data/**/*.*'],
  'frontend',
);

// Backend: warn if there are changes in the application code but not in tests
warnNoTests(['web/**/*.py*'], ['web/**/test_*.py'], 'backend');

// Praise the author
schedule(() => praiseDependencyReduction('frontend/package.json'));

// Notify technical coordinators about UX/UI changes
notifyTechnicalCoordinators();

// Warn if breaking change footer is missing
checkBreakingChangeFooter();

// Warn if forbidden words have been used
schedule(checkForbiddenWords);

// Warn if the `Action-Needed:` footer is present when certain files
// are edited.
async function checkActionNeededFooter() {
  const footer = 'Action-Needed:';
  const targets = [
    '.env.sample',
    'frontend/.env.sample',
    'web/*/tasks.py',
    'web/.config.json.sample',
    'web/scripts',
  ];
  const actionNeededCommits = danger.git.commits.filter((commit) =>
    commit.message.includes(footer),
  );
  const actionNeededLowerCaseCommits = danger.git.commits.filter((commit) =>
    commit.message.toLowerCase().includes(footer.toLowerCase()),
  );
  if (actionNeededLowerCaseCommits.length > actionNeededCommits.length) {
    fail(
      `This MR contains \`${footer}\` footer in the incorrect case. ` +
        `Kindly correct it to \`${footer}\`.`,
    );
  }
  if (
    !!danger.git.commits.length &&
    !actionNeededCommits.length &&
    danger.git.fileMatch(...targets).edited
  ) {
    warn(
      'This MR changes one of the files listed below, ' +
        `but none of its commits have the \`${footer}\` footer. ` +
        "That's OK as long as no manual action is needed when using this commit. " +
        `[${targets.join(', ')}]`,
    );
  }
}

schedule(checkActionNeededFooter);
