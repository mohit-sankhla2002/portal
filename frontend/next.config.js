const { i18n } = require('./next-i18next.config');

module.exports = {
  poweredByHeader: false,
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` module, like winston, see https://github.com/vercel/next.js/issues/7755
    if (!isServer) {
      config.resolve.fallback = { ...config.resolve.fallback, fs: false };
    }
    return config;
  },
  i18n,
  reactStrictMode: true,
  typescript: {
    ignoreBuildErrors: false,
  },
  eslint: {
    // Make sure that ESLint is run as part of the CI
    ignoreDuringBuilds: true,
  },
  images: {
    // Allow 'localhost' by default
    domains: (process.env.NEXT_PUBLIC_IMAGES_DOMAINS || 'localhost').split(','),
  },
};
