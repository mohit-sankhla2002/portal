import {
  expectToBeInTheDocument,
  isError,
  mockConsoleError,
  MockServer,
  resBody,
  resJson,
  setupMockApi,
} from '@biomedit/next-widgets';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import React from 'react';
import { makeStore } from '../src/store';
import { App, unsupportedBrowserLogMessage, useMenuItems } from '../pages/_app';
import { I18nNamespace } from '../src/i18n';
import { backend, generatedBackendApi } from '../src/api/api';
import { rest } from 'msw';

// Mock 'next-i18next' configuration file: it should become `null`/`undefined`
jest.mock('../next-i18next.config.js', () => {
  return {
    __esModule: true,
  };
});

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

jest.mock('../src/structure', () => {
  const originalModule = jest.requireActual('../src/structure');

  return {
    __esModule: true,
    ...originalModule,
    useStructureItems: () => [
      {
        title: 'No Permission',
        menu: { link: '/noPermission', icon: <></> },
      },
      {
        title: 'With Permission',
        menu: { link: '/withPermission', icon: <></> },
        permission: 'userManage',
      },
    ],
  };
});

describe('App', () => {
  describe('Component', () => {
    let consoleMock;
    let navigatorMock;

    const server: MockServer = setupMockApi();

    beforeAll(() => {
      navigatorMock = { userAgent: undefined };
      Object.defineProperty(window, 'navigator', {
        value: navigatorMock,
      });
    });

    beforeEach(() => {
      consoleMock = mockConsoleError();

      server.use(
        rest.post(`${backend}messages/`, (req, res, ctx) =>
          res(resJson(ctx), resBody(ctx, [])),
        ),
      );
    });

    afterEach(() => {
      consoleMock.mockRestore();
      navigatorMock.userAgent = undefined;
      server.resetHandlers();
    });

    afterAll(() => {
      jest.restoreAllMocks;
      server.close();
    });

    it.each`
      userAgent                                                                                                                                                                                              | isSupported
      ${'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.2924.77 Safari/537.36'}                                                                                             | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.3809.100 Safari/537.36'}                                                                               | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.3865.90 Safari/537.36'}                                                                                | ${true}
      ${'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0'}                                                                                                                        | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/77.0'}                                                                                                                    | ${false}
      ${'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/78.0'}                                                                                                                    | ${true}
      ${'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}                                                                                                                          | ${true}
      ${'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Chrome/W.X.Y.Z Safari/537.36'}                                                       | ${true}
      ${'Googlebot/2.1 (+http://www.google.com/bot.html)'}                                                                                                                                                   | ${true}
      ${'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/W.X.Y.Z Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'} | ${true}
      ${'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon'}                                                                           | ${true}
      ${'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) Chrome/38.0.1025.166 Mobile Safari/535.19'}                                  | ${true}
    `(
      'should show a browser as supported ($isSupported) if the user agent is $userAgent',
      async ({ userAgent, isSupported }) => {
        let attemptedLogin = false;
        jest
          .spyOn(generatedBackendApi, 'retrieveUserinfoRaw')
          .mockImplementation(() => {
            attemptedLogin = true;
            return new Promise((resolve) => {
              // @ts-expect-error not relevant for logic to be tested
              resolve({});
            });
          });
        navigatorMock.userAgent = userAgent;
        // verify mock is working properly
        expect(window.navigator.userAgent).toEqual(userAgent);

        try {
          render(
            <Provider store={makeStore()}>
              <App />
            </Provider>,
          );
        } catch (e) {
          if (isError(e)) {
            expect(e.message).toContain(
              'Element type is invalid: expected a string',
            );
          } else {
            throw e;
          }
        }

        await waitFor(() => {
          expectToBeInTheDocument(
            screen.queryByText(`${I18nNamespace.COMMON}:unsupportedBrowser`),
            !isSupported,
          );
        });

        if (isSupported) {
          const calls = consoleMock.mock.calls;
          calls.forEach((call) => {
            expect(call).not.toContain(unsupportedBrowserLogMessage);
          });
        } else {
          expect(attemptedLogin).toBe(false);
          expect(consoleMock.mock.calls.length).toBeGreaterThanOrEqual(1);
          expect(consoleMock.mock.calls[0][0]).toContain(
            unsupportedBrowserLogMessage,
          );
        }
      },
    );
  });
});

describe('useMenuItems', () => {
  const privilegedItems = [
    {
      title: 'With Permission',
      link: '/withPermission',
      icon: <></>,
      permission: 'userManage',
    },
  ];

  const unprivilegedItems = [
    {
      title: 'No Permission',
      link: '/noPermission',
      icon: <></>,
    },
  ];

  it.each`
    permissions                        | expectedMenuItems
    ${{}}                              | ${[...unprivilegedItems]}
    ${{ userManage: { staff: true } }} | ${[...unprivilegedItems, ...privilegedItems]}
  `(
    'Should return $expectedMenuItems if permissions are $permissions',
    ({ permissions, expectedMenuItems }) => {
      expect(useMenuItems(permissions)).toEqual(expectedMenuItems);
    },
  );
});
