module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
  plugins: [
    '@typescript-eslint',
    'react-hooks',
    'jest',
    'typescript-sort-keys',
  ],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jest/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@next/next/recommended',
    'plugin:prettier/recommended', // must be last to override conflicting rules!
    'prettier', // must be last to override conflicting rules!
  ],
  ignorePatterns: ['jest.config.js'],
  rules: {
    'no-nested-ternary': 'error',
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'react/prop-types': 'off',
    'react/no-array-index-key': 'error',
    'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
    'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
    'jest/no-identical-title': 'off', // too many false-positives when using `each`
    'jest/no-conditional-expect': 'off',
    'jest/expect-expect': 'off', // doesn't take implicit assertions into account, like when using `@testing-library/react` matchers for example
    'typescript-sort-keys/string-enum': 'error',
    quotes: [
      'warn',
      'single',
      { avoidEscape: true, allowTemplateLiterals: true },
    ], // enforce use of single quotes
    'linebreak-style': ['error', 'unix'],
    semi: ['warn', 'always'],
    // override default options for rules from base configurations
    'no-cond-assign': ['error', 'always'],
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    // see https://github.com/eslint/eslint/issues/13905
    // Turn off this rule as the rule already exists as the typescript-specific variant
    // @typescript-eslint/no-unused-vars, so it would be redundant to use both the
    // typescript and the javascript one
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'no-restricted-imports': [
      'error',
      {
        patterns: ['@mui/*/*/*', '!@mui/material/test-utils/*'],
      },
    ],
  },
  overrides: [
    {
      files: ['*.js', '*.jsx', '*.ts', '*.tsx'],
    },
  ],
  settings: { react: { version: 'detect' } },
};
