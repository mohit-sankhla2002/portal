const nextJest = require('next/jest');

const createJestConfig = nextJest();

// Any custom config you want to pass to Jest
const customJestConfig = {
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/widgets/effects/.*',
    '<rootDir>/src/api/generated/.*',
    '<rootDir>/src/api/config.ts',
    '<rootDir>/src/testUtils.ts',
    '<rootDir>/src/config.ts',
    '<rootDir>/src/i18n.ts',
    '<rootDir>/.next/.*',
    '<rootDir>/next-i18next.config.js',
    '<rootDir>/next.config.js',
    '<rootDir>/ts-check.js',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/coverage/.*',
  ],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transformIgnorePatterns: [
    '/next[/\\\\]dist/',
    '/node_modules/',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
  moduleDirectories: ['node_modules', 'src', 'pages', 'pagesTests'],
  setupFiles: ['./src/testSetup.ts'],
  setupFilesAfterEnv: ['./src/testSetupAfterEnv.ts'],
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    // Jest 28 specific fix/hack.
    // See https://github.com/microsoft/accessibility-insights-web/pull/5421#issuecomment-1109168149
    '^uuid$': '<rootDir>/node_modules/uuid/dist/index.js',
  },
};

// createJestConfig is exported in this way to ensure that next/jest can load the Next.js config which is async
module.exports = createJestConfig(customJestConfig);
