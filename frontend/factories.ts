import { fakerEN } from '@faker-js/faker';
import {
  CustomAffiliation,
  DataProvider,
  DataTransfer,
  DataTransferDataProviderApprovalsInner,
  DataTransferDataProviderApprovalsInnerStatusEnum,
  DataTransferGroupApprovalsInner,
  DataTransferGroupApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInner,
  DataTransferNodeApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInnerTypeEnum,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  Flag,
  Group,
  Node,
  NodeNodeStatusEnum,
  Permission,
  PgpKeyInfo,
  PgpKeyInfoStatusEnum,
  Project,
  User,
  UserProfile,
  Userinfo,
  UserinfoManages,
  UserinfoPermissions,
  ProjectPermissions,
  ProjectResourcesInner,
  ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
  ProjectPermissionsEdit,
} from './src/api/generated';
import { AuthState } from './src/reducers/auth';
import {
  DataTransferPermissions,
  PagePermissions,
  ProjectPermissions as ProjectPagePermission,
  UserManagePermissions,
} from './src/permissions';
import { snakeCase } from 'lodash';

type Factory<T> = (partialEntity?: Partial<T>) => T;
type SnakeCasedFactory<T> = (partialEntity?: Partial<T>) => Partial<T>;

export function createBatch<T>(factory: Factory<T>, times: number): T[] {
  return Array.from({ length: times }, () => factory({}));
}

export function createSnakeCasedBatch<T>(
  factory: Factory<T>,
  times: number,
): Partial<T>[] {
  return Array.from({ length: times }, () => createSnakeCased(factory)({}));
}

export function toSnakeCased<T>(entity: T): Partial<T> {
  const snakeCased = {};
  for (const key in entity) {
    snakeCased[snakeCase(key)] = entity[key];
  }
  return snakeCased;
}

export function createSnakeCased<T>(factory: Factory<T>): SnakeCasedFactory<T> {
  return (partialEntity: Partial<T> = {}) =>
    toSnakeCased(factory(partialEntity));
}

export function createNode(node: Partial<Node> = {}): Required<Node> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    nodeStatus: fakerEN.helpers.enumValue(NodeNodeStatusEnum),
    ticketingSystemEmail: fakerEN.internet.email(),
    objectStorageUrl: fakerEN.internet.url({ protocol: 'https' }),
    oauth2Client: fakerEN.number.int(),
    ...node,
  };
}

export function createDataProvider(
  dataProvider: Partial<DataProvider> = {},
): Required<DataProvider> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    enabled: fakerEN.datatype.boolean(),
    node: fakerEN.word.noun(),
    coordinators: [],
    ...dataProvider,
  };
}

export function createUser(user: Partial<User> = {}): Required<User> {
  return {
    email: fakerEN.internet.email(),
    firstName: fakerEN.person.firstName(),
    flags: [],
    groups: [],
    id: fakerEN.number.int(),
    isActive: fakerEN.datatype.boolean(),
    lastLogin: fakerEN.date.recent(),
    lastName: fakerEN.person.lastName(),
    profile: createUserProfile(),
    username: fakerEN.internet.userName(),
    ...user,
  };
}

function createUserProfile(): Required<UserProfile> {
  const institution = fakerEN.word.noun();
  const id = fakerEN.string.numeric();

  return {
    affiliation: institution,
    affiliationId: fakerEN.internet.email({ provider: institution }),
    uid: fakerEN.number.int(),
    gid: fakerEN.number.int(),
    emails: `${fakerEN.internet.email()},${fakerEN.internet.email()}`,
    localUsername: fakerEN.internet.userName(),
    displayName: `${fakerEN.person.firstName()} ${fakerEN.person.lastName()} (ID: ${id}) (${fakerEN.internet.email()})`,
    displayId: `ID: ${id}`,
    namespace: fakerEN.location.countryCode(),
    displayLocalUsername: `ID: ${id}`,
    affiliationConsent: fakerEN.datatype.boolean(),
    customAffiliation: fakerEN.number.int(),
  };
}

function createDataTransferApproval() {
  return {
    id: fakerEN.number.int(),
    canApprove: fakerEN.datatype.boolean(),
    rejectionReason: fakerEN.word.words(),
    created: fakerEN.date.recent(),
    changeDate: fakerEN.date.recent(),
  };
}

export function createDataTransferNodeApproval(
  approval: Partial<DataTransferNodeApprovalsInner> = {},
): Required<DataTransferNodeApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(DataTransferNodeApprovalsInnerStatusEnum),
    type: fakerEN.helpers.enumValue(DataTransferNodeApprovalsInnerTypeEnum),
    node: createNode(),
    ...approval,
  };
}

export function createDataTransferDataProviderApproval(
  approval: Partial<DataTransferDataProviderApprovalsInner> = {},
): Required<DataTransferDataProviderApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(
      DataTransferDataProviderApprovalsInnerStatusEnum,
    ),
    dataProvider: createDataProvider(),
    ...approval,
  };
}

export function createDataTransferGroupApproval(
  approval: Partial<DataTransferGroupApprovalsInner> = {},
): Required<DataTransferGroupApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(
      DataTransferGroupApprovalsInnerStatusEnum,
    ),
    group: createGroup(),
    ...approval,
  };
}

export function createDataTransfer(
  dataTransfer: Partial<DataTransfer> = {},
): Required<DataTransfer> {
  const requestor = createUser();
  const project = createProject();
  const dataProvider = createDataProvider();

  return {
    maxPackages: [-1, 1][fakerEN.number.int(2)],
    project: project.id || fakerEN.number.int(),
    purpose: fakerEN.helpers.enumValue(DataTransferPurposeEnum),
    id: fakerEN.number.int(),
    packages: [],
    nodeApprovals: [createDataTransferNodeApproval()],
    dataProviderApprovals: [
      createDataTransferDataProviderApproval({ dataProvider: dataProvider }),
    ],
    groupApprovals: [createDataTransferGroupApproval()],
    requestorName: `${requestor.firstName} Norris (ID: 63457666153) (${requestor.email})`,
    requestorDisplayId: `ID: ${fakerEN.string.numeric()}`,
    requestorFirstName: requestor.firstName,
    requestorLastName: requestor.lastName,
    requestorEmail: requestor.email || fakerEN.internet.email(),
    projectName: project.name,
    projectArchived: project.archived,
    transferPath: [],
    canSeeCredentials: fakerEN.datatype.boolean(),
    status: fakerEN.helpers.enumValue(DataTransferStatusEnum),
    dataProvider: dataProvider.code,
    requestor: requestor.id,
    dataSelectionConfirmation: fakerEN.datatype.boolean(),
    legalBasis: fakerEN.word.words(),
    creationDate: fakerEN.date.recent(),
    changeDate: fakerEN.date.recent(),
    ...dataTransfer,
  };
}

function createProjectResource(): Required<ProjectResourcesInner> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    location: fakerEN.internet.url(),
    description: fakerEN.word.words(),
    contact: fakerEN.internet.email(),
  };
}

export function createProjectUser(
  projectUser: Partial<ProjectUsersInner> = {},
): Required<ProjectUsersInner> {
  const userProfile = createUserProfile();
  const user = createUser({ profile: userProfile });

  return {
    ...user,
    ...userProfile,
    uid: userProfile.uid.toString(),
    gid: userProfile.gid.toString(),
    customAffiliation: fakerEN.string.numeric(),
    authorized: fakerEN.datatype.boolean(),
    roles: [fakerEN.helpers.enumValue(ProjectUsersInnerRolesEnum)],
    ...projectUser,
  };
}

export function createProjectPermissionsEdit(
  edit: Partial<ProjectPermissionsEdit> = {},
): Required<ProjectPermissionsEdit> {
  return {
    name: false,
    code: false,
    destination: false,
    users: [],
    ipAddressRanges: false,
    resources: false,
    legalSupportContact: false,
    legalApprovalGroup: false,
    expirationDate: false,
    ...edit,
  };
}

export function createProjectPermissions(
  permissions: Partial<ProjectPermissions> = {},
): Required<ProjectPermissions> {
  return {
    edit: createProjectPermissionsEdit(),
    archive: false,
    ...permissions,
  };
}

export function createProject(
  project: Partial<Project> = {},
): Required<Project> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    created: fakerEN.date.recent(),
    gid: fakerEN.number.int(),
    permissions: {},
    dataTransferCount: fakerEN.number.int(),
    archived: fakerEN.datatype.boolean(),
    name: fakeCode.toUpperCase(),
    code: fakeCode,
    legalApprovalGroup: fakerEN.number.int(),
    destination: fakerEN.word.noun(),
    legalSupportContact: fakerEN.internet.email(),
    expirationDate: fakerEN.date.soon(),
    ipAddressRanges: [],
    resources: [createProjectResource()],
    users: [createProjectUser()],
    ...project,
  };
}

export function createFlag(flag: Partial<Flag>): Required<Flag> {
  return {
    id: fakerEN.number.int(),
    code: fakerEN.word.noun(),
    description: fakerEN.word.words(),
    users: [],
    ...flag,
  };
}

export function createCustomAffiliation(): Required<CustomAffiliation> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
  };
}

export function createGroup(group: Partial<Group> = {}): Required<Group> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    profile: {},
    users: [],
    permissions: [],
    permissionsObject: [],
    ...group,
  };
}

export function createPgpKeyInfo(
  pgpKeyInfo: Partial<PgpKeyInfo> = {},
): Required<PgpKeyInfo> {
  return {
    fingerprint: fakerEN.string.hexadecimal({ length: 40 }),
    status: fakerEN.helpers.enumValue(PgpKeyInfoStatusEnum),
    id: fakerEN.number.int(),
    keyUserId: `${fakerEN.person.firstName} ${fakerEN.person.lastName}`,
    keyEmail: fakerEN.internet.email(),
    ...pgpKeyInfo,
  };
}

export function createAuthState(
  authState: Partial<AuthState> = {},
): Required<AuthState> {
  return {
    isAuthenticated: true,
    isFetching: false,
    isSubmitting: false,
    isExistingUsername: false,
    invalidMsg: fakerEN.word.words(),
    user: createUserinfo(),
    pagePermissions: createPagePermissions(),
    ...authState,
  };
}
export function createUserinfo(
  userinfo: Partial<Userinfo> = {},
): Required<Userinfo> {
  const user = createUser();

  return {
    ipAddress: fakerEN.internet.ip(),
    permissions: createUserinfoPermissions(),
    manages: createUserinfoManages(),
    ...user,
    username: user.username || fakerEN.internet.userName(),
    id: user.id || fakerEN.number.int(),
    ...userinfo,
  };
}

function createUserinfoPermissions(
  userinfoPermissions: Partial<UserinfoPermissions> = {},
): Required<UserinfoPermissions> {
  return {
    manager: false,
    staff: false,
    dataManager: false,
    projectLeader: false,
    dataProviderAdmin: false,
    dataProviderViewer: false,
    nodeAdmin: false,
    nodeViewer: false,
    groupManager: false,
    hasProjects: false,
    legalApprover: false,
    ...userinfoPermissions,
  };
}

function createUserinfoManages(
  userinfoManages: Partial<UserinfoManages> = {},
): Required<UserinfoManages> {
  return {
    dataProviderAdmin: [],
    nodeAdmin: [],
    ...userinfoManages,
  };
}

function createPagePermissions(
  pagePermissions: Partial<PagePermissions> = {},
): Required<PagePermissions> {
  return {
    project: createProjectPagePermissions(),
    dataTransfer: createDataTransferPagePermissions(),
    userManage: createUserManagePagePermissions(),
    ...pagePermissions,
  };
}

function createProjectPagePermissions(
  projectPermissions: Partial<ProjectPagePermission> = {},
): Required<ProjectPagePermission> {
  return {
    del: false,
    add: false,
    edit: false,
    ...projectPermissions,
  };
}

function createDataTransferPagePermissions(
  dataTransferPermissions: Partial<DataTransferPermissions> = {},
): Required<DataTransferPermissions> {
  return {
    dataManager: false,
    projectLeader: false,
    staff: false,
    dataProviderAdmin: false,
    dataProviderViewer: false,
    nodeViewer: false,
    nodeAdmin: false,
    legalApprover: false,
    ...dataTransferPermissions,
  };
}

function createUserManagePagePermissions(
  userManagePermissions: Partial<UserManagePermissions> = {},
): Required<UserManagePermissions> {
  return {
    staff: false,
    nodeAdmin: false,
    nodeViewer: false,
    dataProviderAdmin: false,
    dataProviderViewer: false,
    groupManager: false,
    ...userManagePermissions,
  };
}

export function createNodeAdminAuthState(nodes: Node[] = [createNode()]) {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        manager: true,
        dataProviderAdmin: true,
        nodeAdmin: true,
        hasProjects: true,
      }),
      manages: createUserinfoManages({ nodeAdmin: nodes }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({ add: true, edit: true }),
      dataTransfer: createDataTransferPagePermissions({ nodeAdmin: true }),
      userManage: createUserManagePagePermissions({ nodeAdmin: true }),
    }),
  });
}

export function createDataProviderAdminAuthState(
  dataProviders: DataProvider[] = [createDataProvider()],
) {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        dataProviderAdmin: true,
      }),
      manages: createUserinfoManages({
        dataProviderAdmin: dataProviders,
      }),
    }),
    pagePermissions: createPagePermissions({
      dataTransfer: createDataTransferPagePermissions({
        dataProviderAdmin: true,
      }),
    }),
  });
}

export function createStaffAuthState() {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        staff: true,
        dataManager: true,
        hasProjects: true,
      }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({
        add: true,
        edit: true,
        del: true,
      }),
      dataTransfer: createDataTransferPagePermissions({
        dataManager: true,
        staff: true,
      }),
      userManage: createUserManagePagePermissions({ staff: true }),
    }),
  });
}

export function createPermission(
  permission: Partial<Permission> = {},
): Required<Permission> {
  const verb = fakerEN.word.verb();
  const noun = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    name: `Can ${verb} ${noun}`,
    codename: `${verb}_${noun}`,
    contentType: fakerEN.number.int(),
    ...permission,
  };
}

type PermissionObject = { id: number; name: string };

export function createPermissionObject(
  permissionObject: Partial<PermissionObject>,
): Required<PermissionObject> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    ...permissionObject,
  };
}

export function createObjectByPermission(
  perm_id: number = fakerEN.number.int(),
  objects: PermissionObject[] = [],
) {
  const response = {};
  response[perm_id] = objects;
  return response;
}
