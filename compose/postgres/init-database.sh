#!/bin/bash

set -e
set -x

psql --echo-all -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER $DB_USER WITH PASSWORD '$DB_PASS';
    ALTER USER $DB_USER SUPERUSER;
    CREATE DATABASE $DB_NAME OWNER $DB_USER;
EOSQL

# If you would like to start with a non-empty portal, put a db-dump named
# `portal.sql` in this directory, it will be mounted into
# `/docker-entrypoint-initdb-d/` inside the docker container.
if test -f /docker-entrypoint-initdb.d/portal.sql;
then
    psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" < /docker-entrypoint-initdb.d/portal.sql
fi
