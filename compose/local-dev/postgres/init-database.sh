#!/bin/bash

set -e
set -x

psql --echo-all -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    DROP DATABASE IF EXISTS dcc;
    CREATE DATABASE dcc;
EOSQL

if test -e /portal.sql;
then
    psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" < /portal.sql
fi