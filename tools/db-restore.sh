#!/usr/bin/env bash

set -o pipefail
container="${1:-portal-db-1}"

[[ -z "$container" ]] && echo "💥 No container specified" >&2 && exit 1

exec gunzip -c - | docker exec -i "$container" bash -c 'dropdb -U $DB_USER $DB_USER ; createdb -U $DB_USER $DB_NAME -O $DB_USER && psql -U $DB_USER'
