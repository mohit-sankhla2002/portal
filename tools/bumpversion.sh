#!/usr/bin/env bash

set -e

# Set default values.
dry_run=""
CLIFF_CONFIG=".cliff.toml"
CHANGELOG_FILE="CHANGELOG.md"
FILES_TO_COMMIT=(frontend/package.json frontend/package-lock.json "$CHANGELOG_FILE")
FILES_TO_BUMP=(docker-compose.yml)
NODE_PKG_VERSION_SCRIPT="console.log(JSON.parse(require('fs').readFileSync(process.argv[1]))['version'])"
DOCSTRING="BIWG's version bump script for new portal releases.

DESCRIPTION:
  Creates a new commit in the repo that contains changes for the upcoming
  new release. The updates to $CHANGELOG_FILE are made using git cliff.
  Specifically:
  * Updates $CHANGELOG_FILE for the new release.
  * Adds a new release commit with the updated version of $CHANGELOG_FILE.
  * Tags the new release commit with the upcoming version to be released.

USAGE:
  $(basename $0) dev|prod [OPTIONS]

  The type of release 'dev' or 'prod' must always be specified.

  OPTIONS:
   -d / --dry-run: run script without making changes to files and commits.
   -h / --help: display this help message.

EXAMPLES:
  bumpversion.sh dev -d
  bumpversion.sh prod -d
"


# The `sed` in-place command behaves differently between the BSD (MacOS) and GNU (Linux)
# implementations. This makes the command portable.
# Default case for Linux sed, just use "-i"
sedi=(-i)
case "$(uname)" in
  # For MacOS, use two parameters
  Darwin*) sedi=(-i "") ;;
esac

# Get command line arguments passed by the user.
release_type=""
while [ "$1" ]; do
  case $1 in
    dev | prod)
      release_type=$1
      ;;
    -d | --dry-run)
      dry_run=TRUE
      ;;
    -h | h | [-]*help)
      echo "$DOCSTRING"
      exit 1
      ;;
    *)
      echo "💥 Bad argument. Please see --help for details." >&2
      exit 1
      ;;
  esac
  shift
done
if [[ -z $release_type ]]; then
  echo "💥 No release type value specified. Please pass 'dev' or 'prod'." >&2
  echo "   Pass the -h/--help option for details."
  exit 1
fi


# Get the current version.
dryrun_label="🔍 dry-run mode"
version_current=$(node -e "${NODE_PKG_VERSION_SCRIPT}" -- frontend/package.json)
echo "📌 Bumping version for portal ${release_type}$(if [ $dry_run ]; then echo " [$dryrun_label]"; fi)" >&2
echo "📌 Current version: $version_current" >&2

# Update CHANGELOG 
if [[ $release_type == "dev" ]]; then
  cd frontend
  npm version --preid dev prerelease >/dev/null
  version_new=$(node -e "${NODE_PKG_VERSION_SCRIPT}" -- package.json)
  cd ..
  echo "⚙️  Bumping to the next 'dev' version: $version_new" >&2
else
  # Generate a temporary CHANGELOG to detect whether the upcomming release
  # is a new major, minor or patch version.
  changelog_unreleased=$(git cliff -c "$CLIFF_CONFIG" --unreleased)

  # Get the label associated with the "parser group" for new "features"
  # as defined in the Git cliff config file. Because the label starts
  # with a value like "<!-- 0 -->" that gets removed during the generation
  # of the CHANGELOG, this pattern must here be removed from the label.
  feature_group_label="### $(grep -o 'message = "\^feat".*' "$CLIFF_CONFIG" | sed 's/.*group = "//; s/" .*//; s/<!--.*-->//')"

  # Determine the type of version update by parsing the CHANGELOG.
  case $changelog_unreleased in
    *"BREAKING CHANGES"*)
      version_bump_recommended="major"
      ;;
    *"$feature_group_label"*)
      version_bump_recommended="minor"
      ;;
    *)
      version_bump_recommended="patch"
      ;;
  esac
  cd frontend
  npm version "$version_bump_recommended" >/dev/null
  version_new=$(node -e "${NODE_PKG_VERSION_SCRIPT}" -- package.json)
  cd ..
  echo "⚙️  Bumping to the next $version_bump_recommended version: $version_new" >&2

  # Append content for the next release to the CHANGELOG file.
  echo "📜 Generating changelog..." >&2
  echo "----------------------------------------------------------------------------------------------------" >&2
  git cliff -c "$CLIFF_CONFIG" -u -p "$CHANGELOG_FILE" --tag "$version_new" >&2
  echo "----------------------------------------------------------------------------------------------------" >&2
fi

# Add new commit and tag to the repo.
commit_msg="chore(release): ${version_new}"
if [ ! $dry_run ]; then
  sed "${sedi[@]}" -E "s/(frontend|web):$version_current/\1:$version_new/g" "${FILES_TO_BUMP[@]}"
  git add "${FILES_TO_COMMIT[@]}" "${FILES_TO_BUMP[@]}"
  git commit -q -m "$commit_msg"
  git tag -a "$version_new" -m "$commit_msg"
else
  git restore "${FILES_TO_COMMIT[@]}"
fi
echo "🖊️  New commit: $commit_msg" >&2
echo "🏷️  New tag: $version_new" >&2
echo "🚀 Version bumped to: $version_new" >&2

# Print end-message to user.
if [ ! $dry_run ]; then
  echo "👷 You can now push the new version using: git push --follow-tags origin $(git branch --show-current)" >&2
else
  echo "$dryrun_label: completed test run." >&2
fi

# Print the new version on stdout, in case the user wants to retrieve this
# value for further usage by the shell.
echo "$version_new"

