#!/usr/bin/env bash

find /app/public -mindepth 1 -maxdepth 1 -exec rm -rf -- {} +
cp -r /app/public /app

if [[ $OTEL_PYTHON_DJANGO_INSTRUMENT = True ]]; then
	/usr/local/bin/opentelemetry-instrument /usr/local/bin/gunicorn \
		portal.wsgi:application --config portal/otel_gunicorn.conf.py \
		-w 5 -b :8000
else
	/usr/local/bin/gunicorn portal.wsgi:application -w 5 -b :8000
fi
