from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from ..models.ssh_public_key import SSHPublicKey
from ..permissions.ssh_public_key import SSHPublicKeyPermission
from ..serializers.ssh_public_key import SSHPublicKeySerializer


# pylint: disable=too-many-ancestors
class SSHPublicKeyViewSet(viewsets.ModelViewSet):
    """API endpoint for the users' SSH public keys."""

    serializer_class = SSHPublicKeySerializer
    permission_classes = (IsAuthenticated, SSHPublicKeyPermission)

    def get_queryset(self):
        """Allow normal users to only see their own SSH public keys.
        This method is applied to GET, PUT, PATCH and DELETE requests
        *before* the actual create/update method in the SSHPublicKeySerializer
        is called."""
        if self.request.user.is_staff:
            return SSHPublicKey.objects.all()
        return SSHPublicKey.objects.filter(user=self.request.user)
