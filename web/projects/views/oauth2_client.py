from rest_framework import viewsets
from django_drf_utils.permissions import IsRead

from identities.models import OAuth2Client
from projects.permissions import IsStaff
from projects.permissions.node import IsNodeAdmin, IsNodeViewer
from projects.serializers.oauth2_client import OAuth2ClientSerializer


class OAuth2ClientViewSet(
    viewsets.ReadOnlyModelViewSet
):  # pylint: disable=too-many-ancestors
    permission_classes = (IsRead & (IsNodeAdmin | IsNodeViewer | IsStaff),)
    serializer_class = OAuth2ClientSerializer
    queryset = OAuth2Client.objects.all()
