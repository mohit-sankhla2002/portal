from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import viewsets

from ..models.flag import Flag
from ..models.tile import QuickAccessTile
from ..permissions import IsStaffOrReadOnly
from ..serializers.quick_access_tile import QuickAccessTileSerializer

User = get_user_model()


def get_flags(requester: User):
    return Flag.objects.filter(users=requester)


class QuickAccessTileViewSet(
    viewsets.ModelViewSet
):  # pylint: disable=too-many-ancestors
    serializer_class = QuickAccessTileSerializer
    permission_classes = (IsStaffOrReadOnly,)

    def get_queryset(self):
        requester: User = self.request.user
        if requester.is_staff:
            return QuickAccessTile.objects.all()
        # Return only tiles without any flag or, if one is specified,
        # then the requester should have it.
        return QuickAccessTile.objects.filter(
            Q(flag__isnull=True) | Q(flag__in=get_flags(requester))
        ).distinct()
