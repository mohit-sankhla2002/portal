from django.db.models import Q
from rest_framework import serializers
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response

from ..models.data_provider import (
    get_data_provider_permission_data_providers,
)
from ..models.data_transfer import DataTransfer
from ..models.node import get_node_permission_nodes
from ..models.user import get_legal_approval_role_groups
from ..permissions.data_transfer import DataTransferPermission
from ..serializers import DataTransferSerializer


class S3OptsSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    endpoint = serializers.URLField(
        read_only=True,
        help_text="S3 server endpoint",
    )
    bucket = serializers.CharField(
        read_only=True,
        help_text="Bucket name on S3 server",
    )


class DataTransferViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    permission_classes = (DataTransferPermission,)
    serializer_class = DataTransferSerializer

    def get_queryset(self):
        requestor = self.request.user
        user_nodes = get_node_permission_nodes(requestor)
        data_providers = get_data_provider_permission_data_providers(requestor)
        user_approval_groups = get_legal_approval_role_groups(requestor)
        if requestor.is_staff:
            query = DataTransfer.objects
        else:
            query = DataTransfer.objects.filter(
                Q(project__users__user=requestor)
                | Q(data_provider__in=data_providers)
                | (
                    Q(project__destination__in=user_nodes)
                    | Q(data_provider__node__in=user_nodes)
                )
                | (
                    Q(group_approvals__group__in=user_approval_groups)
                    & Q(purpose=DataTransfer.PRODUCTION)
                )
            ).distinct()
        return self.serializer_class.prefetch(query)

    @action(
        detail=True,
        permission_classes=(permissions.AllowAny,),
        get_queryset=DataTransfer.objects.all,
        serializer_class=S3OptsSerializer,
        methods=["get"],
    )
    def s3opts(self, _: Request, pk=None):  # pylint: disable=unused-argument
        dtr: DataTransfer = self.get_object()
        return Response(
            {
                "endpoint": dtr.project.destination.object_storage_url,
                "bucket": dtr.project.code.replace("_", "-"),
            }
        )
