import logging

import requests
import xmltodict
from django_drf_utils.views.utils import DetailedResponse
from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from identities.models import OAuth2Client
from portal.settings import CONFIG
from projects.models.data_transfer import DataTransfer
from projects.oauth import server

logger = logging.getLogger(__name__)


class CredentialsSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    access_key_id = serializers.CharField(
        label="Access key ID",
        help_text="Access key ID",
        read_only=True,
    )
    secret_access_key = serializers.CharField(
        label="Secret access key",
        help_text="Secret access key",
        read_only=True,
    )
    session_token = serializers.CharField(
        label="Session token",
        help_text="Session token",
        read_only=True,
    )
    expiration = serializers.DateTimeField(
        label="Expiration date time",
        help_text="Expiration date time",
        read_only=True,
    )
    dtr = serializers.IntegerField(label="DTR ID", help_text="Data Transfer ID")

    def to_internal_value(self, data):
        return {
            "access_key_id": data["AccessKeyId"],
            "secret_access_key": data["SecretAccessKey"],
            "session_token": data["SessionToken"],
            "expiration": data["Expiration"],
            "dtr": self.context["request"].data["dtr"],
        }


class LoggedErrorResponse(DetailedResponse):
    def __init__(self, detail, status_code):
        super().__init__(detail, status_code)
        logger.error(detail)


class SecurityTokenService(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CredentialsSerializer

    def get_serializer_context(self):
        return {"request": self.request, "format": self.format_kwarg, "view": self}

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    # pylint: disable=unused-argument, too-many-return-statements
    def post(self, request, *args, **kwargs):
        request.grant_type = OAuth2Client.GrantType.AUTHORIZATION_CODE
        request.scope = "openid"
        request.credential = None
        request.authorization_code = None
        dtr_key = "dtr"
        try:
            dtr = DataTransfer.objects.get(id=request.data[dtr_key])
        except DataTransfer.DoesNotExist:
            return LoggedErrorResponse(
                f"Given '{dtr_key}' ('{request.data[dtr_key]}') NOT found",
                status.HTTP_404_NOT_FOUND,
            )
        except KeyError:
            return LoggedErrorResponse(
                f"Missing '{dtr_key}' parameter",
                status.HTTP_404_NOT_FOUND,
            )
        if dtr.status != DataTransfer.AUTHORIZED:
            return LoggedErrorResponse(
                f"Data transfer '{dtr}' is NOT authorized", status.HTTP_403_FORBIDDEN
            )
        if request.user not in dtr.data_provider.data_engineers:
            return LoggedErrorResponse(
                f"User '{request.user}' is NOT a data engineer for '{dtr.data_provider}'",
                status.HTTP_403_FORBIDDEN,
            )
        node = dtr.project.destination
        if not node.oauth2_client:
            return LoggedErrorResponse(
                f"No OAuth2 client found for node '{node}'",
                status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        request.client = node.oauth2_client
        object_storage_url = node.object_storage_url
        if not object_storage_url:
            return LoggedErrorResponse(
                f"No object storage URL for node '{node}' has been specified",
                status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        grant = server.get_token_grant(request)
        token = grant.generate_token(user=request.user, scope=request.scope)
        grant.execute_hook("process_token", token=token, dtr=dtr)
        payload = {
            "Action": "AssumeRoleWithWebIdentity",
            "WebIdentityToken": token["id_token"],
            "Version": "2011-06-15",
            "DurationSeconds": CONFIG.oauth.sts.duration_seconds,
        }
        try:
            answer = requests.post(object_storage_url, data=payload, timeout=3)
        except requests.exceptions.RequestException as e:
            return LoggedErrorResponse(
                f"A problem occurred while reaching '{object_storage_url}': {e}",
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        # Because the answer is always XML, independent of the `Accept` header
        data = xmltodict.parse(answer.text)
        if answer.status_code != 200:
            return LoggedErrorResponse(
                f"STS '{object_storage_url}' returned HTTP {answer.status_code}: {data['ErrorResponse']['Error']['Message']}",
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        credentials = data["AssumeRoleWithWebIdentityResponse"][
            "AssumeRoleWithWebIdentityResult"
        ]["Credentials"]
        logger.info(
            "Credentials for user '%s' successfully obtained from STS '%s'",
            request.user.username,
            object_storage_url,
        )
        serializer = self.get_serializer(data=credentials)
        serializer.is_valid(raise_exception=True)
        return Response(
            serializer.data,
            status=status.HTTP_200_OK,
        )
