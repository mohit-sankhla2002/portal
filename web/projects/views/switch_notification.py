import logging

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.views.utils import DetailedResponse
from requests.auth import HTTPBasicAuth
from rest_framework import status, response
from rest_framework.decorators import parser_classes, renderer_classes
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView

from portal.user_utils import (
    send_affiliation_change_email,
    send_no_affiliation_consent_email,
)
from .. import SCIM_MEDIA_TYPE
from ..parsers import ScimParser
from ..permissions.switch_notification import IsSwitchNotificationUser
from ..renderers import ScimRenderer

logger = logging.getLogger(__name__)

User = get_user_model()

EDUID_AFFILIATION_KEY = "eduPersonScopedAffiliation"


class SwitchNotificationResponse(response.Response):
    def __init__(self, request, user: User):
        super().__init__(
            data={
                "schemas": ["urn:ietf:params:scim:schemas:core:2.0:User"],
                "id": user.username,
            },
            status=status.HTTP_200_OK,
            content_type=SCIM_MEDIA_TYPE,
            headers={"Location": request.build_absolute_uri()},
        )


@parser_classes([ScimParser])
@renderer_classes([ScimRenderer])
class SwitchNotificationView(APIView):
    """
    Allows switch notification API to push changes to user's affiliation to portal.

    See https://www.switch.ch/edu-id/docs/services/sp-notification/ for the implementation details.
    """

    permission_classes = (IsSwitchNotificationUser,)

    # pylint: disable=no-member
    def put(self, request, *args, **kwargs):
        try:
            # username of the user which has updated affiliations
            username = request.data["id"]

            user_queryset = User.objects.filter(username=username)
            if user_queryset.count() == 0:
                return DetailedResponse(
                    "User not found", status_code=status.HTTP_404_NOT_FOUND
                )
            user = user_queryset.get()

            old_affiliations_str = user.profile.affiliation
            old_affiliation_ids_str = user.profile.affiliation_id

            if not user.profile.affiliation_consent:
                logger.info(
                    "User %s did NOT give consent to retrieve "
                    "affiliation data from the SWITCH SCIM API. "
                    "No request to the SWITCH SCIM API was made and "
                    "affiliations are left unchanged. "
                    "Current affiliation(s): '%s' and affiliation_id(s): '%s'",
                    user.username,
                    old_affiliations_str,
                    old_affiliation_ids_str,
                )
                send_no_affiliation_consent_email(user, old_affiliations_str)
            elif old_affiliation_ids_str:
                self._update_existing_affiliations(user)
            else:
                logger.info(
                    "User %s did NOT previously have any affiliations, "
                    "they most likely added a new affiliation_id. "
                    "Current affiliation(s): '%s' and affiliation_id(s): '%s'",
                    user.username,
                    old_affiliations_str,
                    old_affiliation_ids_str,
                )

            return SwitchNotificationResponse(request, user)
        except BaseException as e:
            # in the case of any error other than not found, SWITCH expects a 400 error
            raise ValidationError from e

    def _update_existing_affiliations(self, user):
        old_affiliations_str = user.profile.affiliation
        old_affiliations = set(old_affiliations_str.split(","))
        old_affiliation_ids_str = user.profile.affiliation_id
        old_affiliation_ids = set(old_affiliation_ids_str.split(","))
        logger.debug(
            "User %s currently has affiliation(s): %s and affiliation_id(s): %s",
            user.username,
            old_affiliations,
            old_affiliation_ids,
        )

        new_affiliations = set()
        new_affiliation_ids = set()
        for affiliation_id in old_affiliation_ids:
            if affiliations_for_id := self._get_switch_affiliations(
                affiliation_id, user
            ):
                new_affiliations |= affiliations_for_id
                new_affiliation_ids.add(affiliation_id)
        logger.debug(
            "New affiliations: %s and new affiliation_ids: %s for user %s",
            new_affiliations,
            new_affiliation_ids,
            user.username,
        )

        if affiliation_changed := old_affiliations != new_affiliations:
            new_affiliations_str = ",".join(sorted(new_affiliations))
            user.profile.affiliation = new_affiliations_str
            logger.info(
                "Changed affiliation of user %s from '%s' to '%s'",
                user.username,
                old_affiliations_str,
                new_affiliations_str,
            )
            send_affiliation_change_email(
                user,
                old_affiliations_str,
                new_affiliations_str,
            )
        else:
            logger.info(
                "affiliation of user %s is unchanged.",
                user.username,
            )

        if affiliation_id_changed := old_affiliation_ids != new_affiliation_ids:
            new_affiliation_ids_str = ",".join(sorted(new_affiliation_ids))
            user.profile.affiliation_id = new_affiliation_ids_str
            logger.info(
                "Changed affiliation_id of user %s from '%s' to '%s'",
                user.username,
                old_affiliation_ids_str,
                new_affiliation_ids_str,
            )
        else:
            logger.info("affiliation_id of user %s is unchanged.", user.username)

        if affiliation_changed or affiliation_id_changed:
            user.profile.save()

    @staticmethod
    def _get_switch_affiliations(affiliation_id: str, user: User) -> set[str]:
        logger.debug(
            "Checking affiliation_id %s of user %s using SWITCH SCIM API...",
            affiliation_id,
            user.username,
        )
        url = settings.CONFIG.switch.scim_api_affiliations_url + affiliation_id
        resp = requests.get(
            url=url,
            headers={"Accept": SCIM_MEDIA_TYPE},
            auth=HTTPBasicAuth(
                settings.CONFIG.switch.scim_api_username,
                settings.CONFIG.switch.scim_api_password,
            ),
            timeout=1,
        )
        if resp.status_code == status.HTTP_200_OK:
            # affiliations for affiliation_id have either changed or stayed the same
            affiliations_for_id = set(resp.json()[EDUID_AFFILIATION_KEY])
            logger.debug(
                "affiliation_id %s has affiliations: %s for user: %s",
                affiliation_id,
                affiliations_for_id,
                user.username,
            )
            return affiliations_for_id
        if resp.status_code != status.HTTP_404_NOT_FOUND:
            # if response status code is 404, no affiliation will be appended to
            # new_affiliations, all other status codes are unexpected
            logger.error(
                "Unexpected status code %s when making a request to %s "
                "for user %s with response: %s",
                resp.status_code,
                url,
                user.username,
                resp.content,
            )
            raise ValidationError(
                "Unexpected status code when making a request to SWITCH SCIM API."
            )
        # if affiliation_id was removed
        return set()
