import functools

from django.conf import settings
from django_drf_utils.views.utils import unique_check
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.schemas.openapi import AutoSchema

from ..filters.pgp_key_info import PgpKeyInfoLookupFilter
from ..models.pgp import PgpKeyInfo
from ..permissions import IsAuthenticatedAndUniqueCheck
from ..serializers import (
    PgpKeyInfoSerializer,
    PgpKeyMetadataSerializer,
    PgpKeyStatusSerializer,
)
from ..utils.pgp import (
    KeyDownloadError,
    KeyNotFoundError,
    assert_key_algorithm_valid,
    download_key_metadata,
)


def rgetattr(obj, path, default=None):
    try:
        return functools.reduce(getattr, path.split("."), obj)
    except AttributeError:
        return default


class PgpKeyStatusSchema(AutoSchema):
    def get_operation_id(self, path, method):
        return f"status{self.get_operation_id_base(path, method, 'status')}"


@unique_check((IsAuthenticatedAndUniqueCheck,))
class PgpKeyInfoViewSet(  # pylint: disable=too-many-ancestors
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    """API endpoint that displays PGP key metadata info.

    Notes:
     * This view does not accept DELETE requests, as PGP key info records
       should never be deleted.
     * This view does not allow PUT requests, as key metadata (outside of the
       "status" value that can be modified via PATCH) should never be modified.
    """

    serializer_class = PgpKeyInfoSerializer
    filter_backends = (PgpKeyInfoLookupFilter,)

    def get_permissions(self):
        match self.action:
            case "partial_update":
                permission = IsAdminUser
            case "update":
                raise MethodNotAllowed(self.request.method)
            case "status":
                permission = permissions.AllowAny
            case _:
                permission = IsAuthenticated

        # Django expects the function to return an iterable with instances of
        # permission classes.
        return [permission()]

    def get_queryset(self):
        """Controls the objects returned by a GET request. Here we enforce the
        following rules:
         - Users can only view their own key(s).
         - Portal admins can view all keys.
        """
        if self.request and self.request.user.is_staff:
            return PgpKeyInfo.objects.all()
        return PgpKeyInfo.objects.filter(user=self.request.user)

    @action(detail=False, serializer_class=PgpKeyMetadataSerializer, methods=["post"])
    def metadata(self, request: Request):
        error = None
        data = {}
        try:
            keyserver = rgetattr(settings, "CONFIG.pgp.keyserver")
            end_relax = rgetattr(settings, "CONFIG.pgp.end_relax")
            k = download_key_metadata(
                fingerprint=request.data["fingerprint"],
                keyserver_url=keyserver,
                end_relax=end_relax,
            )
            assert_key_algorithm_valid(k, keyserver)
            data = {
                "fingerprint": k.fingerprint,
                "user_id": k.user_id,
                "email": k.email,
                "status": k.status.name,
            }
        except KeyNotFoundError:
            pass
        except KeyDownloadError as e:
            error = str(e)
        return Response(data | {"error": error})

    @action(detail=True, methods=["post"])
    def retire(self, request: Request, pk=None):  # pylint: disable=unused-argument
        pgpKeyInfo: PgpKeyInfo = self.get_object()

        if pgpKeyInfo.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        pgpKeyInfo.status = (
            PgpKeyInfo.Status.APPROVAL_REVOKED
            if pgpKeyInfo.status == PgpKeyInfo.Status.APPROVED
            else PgpKeyInfo.Status.REJECTED
        )
        pgpKeyInfo.save()

        return Response(
            PgpKeyInfoSerializer(pgpKeyInfo, context={"request": request}).data
        )

    @action(
        detail=False,
        methods=("post",),
        serializer_class=PgpKeyStatusSerializer,
        queryset=PgpKeyInfo.objects.none(),
        schema=PgpKeyStatusSchema(),
    )
    def status(self, request: Request):
        """Return the status of one or more PGP keys based on their fingerprints.

        The return value is a list of object with `fingerprint` and `status` fields.
        """
        response = super().create(request)
        # Override the response to return a list directly, instead of an object
        # whose value is a list.
        response.data = response.data["status_by_fingerprint"]
        # The default status code returned by the serializer for a POST
        # request is 201_CREATED, so we override it here.
        response.status_code = status.HTTP_200_OK
        return response
