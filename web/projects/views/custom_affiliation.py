from rest_framework import permissions, viewsets

from ..filters.custom_affiliation import CustomAffiliationLookupFilter
from ..models.user import CustomAffiliation
from ..serializers import CustomAffiliationSerializer


# pylint: disable=too-many-ancestors
class CustomAffiliationViewSet(viewsets.ReadOnlyModelViewSet):
    """Organisations that are not (yet) part of SWITCH edu-ID"""

    serializer_class = CustomAffiliationSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (CustomAffiliationLookupFilter,)

    queryset = CustomAffiliation.objects.all()
