from django.contrib.auth import get_user_model
from rest_framework import viewsets, mixins

from ..filters.service import ServiceLookupFilter
from ..models.project import Service
from ..permissions.project import ProjectPermission
from ..serializers import ServiceSerializer

User = get_user_model()


# pylint: disable=too-many-ancestors
class ServiceViewSet(
    viewsets.ReadOnlyModelViewSet,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):
    """Service endpoint.

    Note that creation does NOT happen via this endpoint.
    """

    serializer_class = ServiceSerializer
    permission_classes = (ProjectPermission,)
    filter_backends = (ServiceLookupFilter,)

    queryset = Service.objects.all()

    def update(self, request, *args, **kwargs):
        partial = kwargs.get("partial", False)
        # Only PATCH
        if partial:
            return super().update(request, *args, **kwargs)
        raise NotImplementedError("`PUT` NOT supported")
