from rest_framework.mixins import (
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
)
from rest_framework.viewsets import GenericViewSet

from ..models.message import Message
from ..permissions import IsOwner
from ..serializers import MessageSerializer


class MessageViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):  # pylint: disable=too-many-ancestors
    serializer_class = MessageSerializer
    permission_classes = (IsOwner,)

    def get_queryset(self):
        return Message.objects.filter(user=self.request.user).order_by("-created")
