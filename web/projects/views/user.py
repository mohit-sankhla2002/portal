import csv
from datetime import datetime

from django.http import HttpResponse
from django_drf_utils.exceptions import ConflictError
from django_drf_utils.permissions import IsRead, IsPatch, IsPost
from django_drf_utils.views.utils import unique_check
from rest_framework import (
    mixins,
    permissions,
    response,
    viewsets,
    generics,
    serializers,
)
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.schemas.openapi import AutoSchema
from rest_framework.views import APIView

from identities.models import User, Group
from identities.permissions import has_group_manager_groups, has_change_user_permission
from ..filters.user import UserActiveFilter, UserProjectRoleFilter, UserLookupFilter
from ..models.data_provider import (
    has_any_data_provider_permissions,
    DataProvider,
    get_data_provider_role_group,
)
from ..models.node import has_any_node_permissions, Node, get_node_role_group
from ..models.project import Project
from ..models.user import UserNamespace
from ..permissions import IsStaff, IsAuthenticated
from ..permissions.node import IsNodeAdmin
from ..permissions.project import is_manager
from ..permissions.user import (
    UserModelPermission,
    UserUniquePermission,
    IsAllowedPatch,
)
from ..serializers import (
    UserNamespaceSerializer,
    UserSerializer,
    UserinfoSerializer,
    ServiceSerializer,
)


class UserinfoSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "permissions": {
                    "type": "object",
                    "readOnly": True,
                    "properties": {
                        "manager": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": "Whether current user is either a "
                            "project manager (PM) or a project leader (PL)",
                        },
                        "staff": {"readOnly": True, "type": "boolean"},
                        "data_manager": {"readOnly": True, "type": "boolean"},
                        "project_leader": {"readOnly": True, "type": "boolean"},
                        "data_provider_admin": {"readOnly": True, "type": "boolean"},
                        "data_provider_viewer": {"readOnly": True, "type": "boolean"},
                        "node_admin": {"readOnly": True, "type": "boolean"},
                        "node_viewer": {"readOnly": True, "type": "boolean"},
                        "group_manager": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": "Whether current user is allowed to manage groups "
                            "(a node/data provider manager for instance)",
                        },
                        "has_projects": {"readOnly": True, "type": "boolean"},
                        "legal_approver": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": "Whether current user is allowed to approve DTR group approvals",
                        },
                    },
                },
                "manages": {
                    "readOnly": True,
                    "type": "object",
                    "properties": {
                        "data_provider_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/DataProvider"},
                        },
                        "node_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/Node"},
                        },
                    },
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field


# pylint: disable=too-many-ancestors
@unique_check((UserUniquePermission,))
class UserViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    """Show details of registered users."""

    serializer_class = UserSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (
        UserModelPermission
        | (
            IsAuthenticated & (IsRead | (IsPost & IsStaff) | (IsPatch & IsAllowedPatch))
        ),
    )
    filter_backends = (
        UserActiveFilter,
        UserProjectRoleFilter,
        UserLookupFilter,
    )
    schema = UserinfoSchema()

    def get_queryset(self):
        user = self.request.user

        # Note: this list will decrease with more issues around GDPR compliance implemented
        def needs_user():
            return (
                user.is_staff
                or has_change_user_permission(user)
                or has_any_node_permissions(user)
                or has_any_data_provider_permissions(user)
            )

        def needs_lookup():
            return is_manager(user) or has_group_manager_groups(user)

        if needs_user() or (
            any(param in self.request.query_params for param in ("email",))
            and needs_lookup()
        ):
            return User.objects.all()
        # Restrict unnecessary access to other people data
        return User.objects.filter(username=user.username)

    def create(self, request, *args, **kwargs):
        # False positive, exists on mixins.CreateModelMixin
        # pylint: disable=no-member
        try:
            resp = super().create(request, *args, **kwargs)
        except ValidationError as exc:
            raise ConflictError(exc) from exc

        created_user = User.objects.get(id=resp.data["id"])
        created_user.set_unusable_password()
        created_user.save()
        return resp

    @action(
        detail=True,
        permission_classes=(IsStaff,),
        methods=["GET"],
    )
    def services(self, request, pk=None):  # pylint: disable=unused-argument
        user = self.get_object()
        services = user.services.all()
        serialized = ServiceSerializer(services, many=True)
        return response.Response(serialized.data)


class UserinfoView(generics.RetrieveAPIView):
    serializer_class = UserinfoSerializer
    permission_classes = (permissions.IsAuthenticated,)
    schema = UserinfoSchema()

    def get_queryset(self):
        return User.objects.filter(username=self.request.user.username)

    def get_object(self):
        return self.get_queryset()[0]


class UserNamespaceViewSet(viewsets.ReadOnlyModelViewSet):
    """View available namespaces."""

    queryset = UserNamespace.objects.all()
    serializer_class = UserNamespaceSerializer
    permission_classes = (permissions.IsAuthenticated,)


class RowObject:
    def __init__(
        self,
        user_id,
        first_name,
        last_name,
        email,
        username,
        last_login,
        is_active,
        affiliation,
        affiliation_id,
        custom_affiliation,
        uid,
        gid,
        local_username,
        display_name,
        display_id,
        namespace,
        display_local_username,
        affiliation_consent,
        user_flag,
        user_project,
        user_project_node,
        user_data_provider,
        user_data_provider_role,
        user_data_provider_node,
        user_node,
        user_node_role,
    ):
        self.user_id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.username = username
        self.last_login = last_login
        self.is_active = is_active
        self.affiliation = affiliation
        self.affiliation_id = affiliation_id
        self.custom_affiliation = custom_affiliation
        self.uid = uid
        self.gid = gid
        self.local_username = local_username
        self.display_name = display_name
        self.display_id = display_id
        self.namespace = namespace
        self.display_local_username = display_local_username
        self.affiliation_consent = affiliation_consent
        self.user_flag = user_flag
        self.user_project = user_project
        self.user_project_node = user_project_node
        self.user_data_provider = user_data_provider
        self.user_data_provider_role = user_data_provider_role
        self.user_data_provider_node = user_data_provider_node
        self.user_node = user_node
        self.user_node_role = user_node_role

    def get_row_data(self):
        return [
            self.user_id,
            self.first_name,
            self.last_name,
            self.email,
            self.username,
            self.last_login,
            self.is_active,
            self.affiliation,
            self.affiliation_id,
            self.custom_affiliation,
            self.uid,
            self.gid,
            self.local_username,
            self.display_name,
            self.display_id,
            self.namespace,
            self.display_local_username,
            self.affiliation_consent,
            self.user_flag,
            self.user_project,
            self.user_project_node,
            self.user_data_provider,
            self.user_data_provider_role,
            self.user_data_provider_node,
            self.user_node,
            self.user_node_role,
        ]


class ExportUsersCsvSchema(AutoSchema):
    def get_responses(self, path, method):
        return {
            "200": {
                "content": {"test/csv": {"schema": {"type": "string"}}},
                "description": "Export users as CSV",
            }
        }


class ExportUsersCSV(APIView):
    permission_classes = (IsStaff | IsNodeAdmin,)
    schema = ExportUsersCsvSchema()

    # pylint: disable=too-many-statements
    def get(self, request, *args, **kwargs):
        delimiter_format = request.query_params.get("delimiter", "comma")
        map_delimiter_format = {"tab": "\t", "comma": ",", "semicolon": ";"}
        delimiter = map_delimiter_format.get(delimiter_format, ",")
        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        filename = f"users_export_{timestamp}.csv"
        csv_response = HttpResponse(content_type="text/csv")
        csv_response["Content-Disposition"] = f'attachment; filename="{filename}"'
        csv_response["Content-Type"] = "text/csv"
        writer = csv.writer(csv_response, delimiter=delimiter)

        # Write the column headers to csv file
        self.write_header(writer)

        users = User.objects.all()
        nodes = Node.objects.all()
        dataproviders = DataProvider.objects.all()

        # For each user, write rows for flags, projects, nodes, data providers
        for user in users:
            # Init variables
            affiliation = ""
            affiliation_id = ""
            uid = ""
            gid = ""
            local_username = ""
            display_name = ""
            display_id = ""
            namespace = ""
            display_local_username = ""
            affiliation_consent = ""
            custom_affiliation = ""
            if hasattr(user, "profile"):
                profile = user.profile
                affiliation = profile.affiliation
                affiliation_id = profile.affiliation_id
                custom_affiliation = profile.custom_affiliation
                uid = profile.uid
                gid = profile.gid
                local_username = profile.local_username
                display_name = profile.display_name
                display_id = profile.display_id
                namespace = profile.namespace
                display_local_username = profile.display_local_username
                affiliation_consent = profile.affiliation_consent
            user_flag = ""
            user_project = ""
            user_project_node = ""
            user_data_provider = ""
            user_data_provider_role = ""
            user_data_provider_node = ""
            user_node = ""
            user_node_role = ""

            rowobj = RowObject(
                user.id,
                user.first_name,
                user.last_name,
                user.email,
                user.username,
                user.last_login,
                user.is_active,
                affiliation,
                affiliation_id,
                custom_affiliation,
                uid,
                gid,
                local_username,
                display_name,
                display_id,
                namespace,
                display_local_username,
                affiliation_consent,
                user_flag,
                user_project,
                user_project_node,
                user_data_provider,
                user_data_provider_role,
                user_data_provider_node,
                user_node,
                user_node_role,
            )

            # Outputs the user flags
            has_flags = self.write_user_flags(user, rowobj, writer)
            # Reset value set by `write_user_flags`
            rowobj.user_flag = ""

            # Write the projects the user is associated with
            has_projects = self.write_user_projects(user, rowobj, writer)
            # Reset values set by `write_user_projects`
            rowobj.user_project = ""
            rowobj.user_project_node = ""

            # Helper method to provide arrays for different user groups
            node_groups_of_user, data_provider_groups_of_user = self.get_groups_of_user(
                user
            )

            # Write the DPs and Nodes the user is associated with
            if len(data_provider_groups_of_user) > 0:
                self.write_user_data_providers(
                    dataproviders, data_provider_groups_of_user, rowobj, writer
                )
                # Reset values set by `write_user_data_providers`
                rowobj.user_data_provider = ""
                rowobj.user_data_provider_role = ""
                rowobj.user_data_provider_node = ""

            if len(node_groups_of_user) > 0:
                self.write_user_nodes(nodes, node_groups_of_user, rowobj, writer)

            # Output users even though they have no flags, projects, nodes, or data providers associated with them
            if (
                not has_flags
                and not has_projects
                and not node_groups_of_user
                and not data_provider_groups_of_user
            ):
                writer.writerow(rowobj.get_row_data())

        return csv_response

    @staticmethod
    def write_header(writer):
        writer.writerow(
            [
                "ID",
                "First Name",
                "Last Name",
                "Email",
                "Username",
                "Last Login",
                "Is Active",
                "Affiliation",
                "Affiliation ID",
                "Custom Affiliation",
                "Uid",
                "Gid",
                "Local username",
                "Display name",
                "Display ID",
                "Namespace",
                "Display local username",
                "Affiliation consent",
                "Flags",
                "Projects",
                "Project Node",
                "Data Provider",
                "Data Provider Role",
                "Data Provider Node",
                "Node",
                "Node Role",
            ]
        )

    @staticmethod
    def write_user_flags(user, row: RowObject, writer) -> bool:
        flags = user.flags.all()
        for flag in flags:
            row.user_flag = f"{flag.code} ({flag.description})"
            writer.writerow(row.get_row_data())
        return bool(flags)

    @staticmethod
    def write_user_projects(user, row: RowObject, writer) -> bool:
        projects = Project.objects.filter(users__user=user).distinct()
        for project in projects:
            row.user_project = f"{project.code} ({project.name})"
            row.user_project_node = project.destination
            writer.writerow(row.get_row_data())
        return bool(projects)

    @staticmethod
    def get_groups_of_user(user):
        node_groups_of_user = []
        data_provider_groups_of_user = []
        groups = Group.objects.filter(user=user).select_related(
            "profile__role_entity_type"
        )
        for group in groups:
            try:
                role_entity_type = group.profile.role_entity_type
            except Group.profile.RelatedObjectDoesNotExist:  # pylint: disable=no-member
                continue
            if role_entity_type is None:
                continue
            user_group_type = group.profile.role_entity_type.model
            if user_group_type == "node":
                node_groups_of_user.append(group)
            elif user_group_type == "dataprovider":
                data_provider_groups_of_user.append(group)
        return node_groups_of_user, data_provider_groups_of_user

    @staticmethod
    def write_user_data_providers(
        data_providers, data_provider_groups_of_user, rowobj, writer
    ):
        for data_provider in data_providers:
            for dp_group_of_user in data_provider_groups_of_user:
                try:
                    group = get_data_provider_role_group(
                        data_provider, dp_group_of_user.profile.role
                    )
                except (
                    Group.DoesNotExist,
                    Group.profile.RelatedObjectDoesNotExist,  # pylint: disable=no-member
                ):
                    continue
                if group.id == dp_group_of_user.id:
                    rowobj.user_data_provider = data_provider.name
                    rowobj.user_data_provider_role = group.name
                    rowobj.user_data_provider_node = data_provider.node
                    writer.writerow(rowobj.get_row_data())

    @staticmethod
    def write_user_nodes(nodes, node_groups_of_user, rowobj, writer):
        for node in nodes:
            for node_group_of_user in node_groups_of_user:
                try:
                    group = get_node_role_group(node, node_group_of_user.profile.role)
                except (
                    Group.DoesNotExist,
                    Group.profile.RelatedObjectDoesNotExist,  # pylint: disable=no-member
                ):
                    continue
                if group.id == node_group_of_user.id:
                    rowobj.user_node = node.name
                    rowobj.user_node_role = group.name
                    writer.writerow(rowobj.get_row_data())
