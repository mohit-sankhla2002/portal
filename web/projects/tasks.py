import logging
import time
from datetime import date, timedelta
from datetime import datetime, timezone

from celery import shared_task
from django.conf import settings
from django_drf_utils.email import sendmail

from .models.pgp import PgpKeyInfo
from .models.project import Project, ProjectRole, ProjectUserRole
from .models.task import PeriodicTaskRun
from .notifications.pgp_key_info import (
    pgp_key_info_status_updated,
    pgp_key_info_status_all_updated,
)
from .notifications.project_user_notification import (
    review_user_roles_notification,
    project_expiration_notification,
)
from .utils.pgp import KeyDownloadError, KeyStatus, download_key_metadata
from .views.project import archive_project

logger = logging.getLogger(__name__)


def review_user_roles_recipients(project_user_roles):
    return [
        role.user.email
        for role in project_user_roles.filter(
            role__in=[ProjectRole.PL.value, ProjectRole.PM.value]
        )
    ]


def user_roles_recipients_exp_warning(project_user_roles):
    return [
        role.user.email
        for role in project_user_roles.filter(
            role__in=[ProjectRole.PL.value, ProjectRole.PM.value, ProjectRole.DM.value]
        )
    ]


def send_review_user_roles_notification():
    for project in Project.objects.filter(archived=False):
        project_user_roles = ProjectUserRole.objects.filter(project=project)
        subject, body = review_user_roles_notification(
            project.name,
            {(role.user, ProjectRole(role.role)) for role in project_user_roles},
        )

        sendmail(
            subject=subject,
            body=body,
            recipients=review_user_roles_recipients(project_user_roles),
            email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
        )


# note: projects that expire "today" are still valid until midnight. therefore, check yesterday
@shared_task(ignore_result=True)
def archive_expired_project():
    current_date = date.today() - timedelta(days=1)
    for project in Project.objects.filter(archived=False):
        if (
            project.expiration_date is not None
            and current_date >= project.expiration_date
        ):
            archive_project(project, True)


# this job should only run once per day, otherwise it will send too many emails
# sends an email 3 months in advance but could possibly send emails closer to expiration date
@shared_task(ignore_result=True)
def send_notification_email_expiring_projects():
    date_in_3_months = date.today() + timedelta(days=90)
    for project in Project.objects.filter(archived=False):
        expiration_date = project.expiration_date
        recipients = ProjectUserRole.objects.filter(project=project)
        if date_in_3_months == project.expiration_date:
            subject, body = project_expiration_notification(
                project.name, expiration_date
            )
            sendmail(
                subject=subject,
                body=body,
                recipients=user_roles_recipients_exp_warning(recipients),
                email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
            )


# Remember to migrate `PeriodicTask` and `PeriodicTaskRun` when you move / rename this method!
@shared_task(
    bind=True,
    # Retry policy. See https://docs.celeryproject.org/en/stable/userguide/tasks.html#retrying for details.
    acks_late=True,
    autoretry_for=(Exception,),
    max_retries=5,
    ignore_result=True,
)
def review_user_roles(self):
    now = datetime.now(timezone.utc)
    send_review_user_roles_notification()
    PeriodicTaskRun.objects.create(task=self.name, created_at=now)
    logger.info("Successfully sent notification emails for user roles review")


@shared_task(ignore_result=True)
def update_pgp_keys(delay: float = 0.25):
    """Update PGP key info from a keyserver.

    Update the PGP key status based on key information received from a
    keyserver, e.g. in the event of removing user ID from a key.

    :param delay: delay in seconds between subsequent queries to a keyserver.
        The current rate limits are available at
        https://keys.openpgp.org/about/api#rate-limiting
    """

    keys = PgpKeyInfo.objects.exclude(
        status__in=(
            PgpKeyInfo.Status.DELETED,
            PgpKeyInfo.Status.KEY_REVOKED,
            PgpKeyInfo.Status.APPROVAL_REVOKED,
            PgpKeyInfo.Status.REJECTED,
        )
    )
    errors = []
    changed: list[PgpKeyInfo] = []
    key: PgpKeyInfo
    for key in keys:
        try:
            metadata = download_key_metadata(
                fingerprint=key.fingerprint,
                keyserver_url=settings.CONFIG.pgp.keyserver,  # pylint: disable=no-member
                end_relax=settings.CONFIG.pgp.end_relax,  # pylint: disable=no-member
            )
            match metadata.status:
                case KeyStatus.NONVERIFIED:
                    key.status = PgpKeyInfo.Status.DELETED
                    changed.append(key)
                case KeyStatus.REVOKED:
                    key.status = PgpKeyInfo.Status.KEY_REVOKED
                    changed.append(key)
        except KeyDownloadError as e:
            errors.append(f"{key.fingerprint} {e}")
        time.sleep(delay)

    if changed:
        updated_count = PgpKeyInfo.objects.bulk_update(
            changed, ("status",), batch_size=50
        )
        logger.info(
            "Changed status of %s PGP keys: %s",
            updated_count,
            ", ".join(key.fingerprint for key in changed),
        )
        # Create a ticket by email
        subject, body = pgp_key_info_status_all_updated(changed)
        sendmail(
            subject,
            body,
            # pylint: disable=no-member
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
        )
        # Email each UserID
        for key in changed:
            subject, body = pgp_key_info_status_updated(key)
            sendmail(
                subject,
                body,
                recipients=(key.key_email,),
                email_cfg=settings.CONFIG.email,  # pylint: disable=no-member
            )
    if errors:
        logger.error(
            "PGP key update failed with the following errors: %s", "; ".join(errors)
        )
    not_changed_count = len(keys) - len(changed) - len(errors)
    if not_changed_count > 0:
        logger.debug("No changes detected in %s keys", not_changed_count)
