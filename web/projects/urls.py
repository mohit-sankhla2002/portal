from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter

from . import views
from .apps import APP_NAME

router = DefaultRouter()
router.register(r"nodes", views.NodeViewSet, "node")
router.register(r"projects", views.ProjectViewSet, "project")
router.register(r"messages", views.MessageViewSet, "message")
router.register(r"users", views.UserViewSet, "user")
router.register(r"services", views.ServiceViewSet, "service")
router.register(r"user-namespaces", views.UserNamespaceViewSet, "user-namespace")
router.register(r"pgpkey", views.PgpKeyInfoViewSet, "pgpkey")
router.register(r"ssh-public-keys", views.SSHPublicKeyViewSet, "ssh_public_keys")
router.register(
    r"audit/project-permission",
    views.ProjectUserRoleHistoryViewSet,
    "audit-project-permission",
)
router.register(r"feed", views.FeedViewSet, "feed")
router.register(r"data-provider", views.DataProviderViewSet, "dataprovider")
router.register(r"data-package/log", views.DataPackageLogViewSet, "datapackagelog")
router.register(
    r"data-package/check", views.DataPackageCheckViewSet, "datapackagecheck"
)
router.register(r"data-package", views.DataPackageViewSet, "datapackage")
router.register(r"data-transfer", views.DataTransferViewSet, "datatransfer")
router.register(r"approval", views.ApprovalViewSet, "approval")
router.register(r"log", views.LogView, "log")
router.register(r"contact", views.ContactView, "contact")
router.register(r"flags", views.FlagViewSet, "flag")
router.register(
    r"custom-affiliations", views.CustomAffiliationViewSet, "customaffiliation"
)
router.register(r"quick-accesses", views.QuickAccessTileViewSet, "quickaccess")
router.register(r"oauth2-clients", views.OAuth2ClientViewSet, "oauth2client")

# The API URLs are now determined automatically by the router.
app_name = APP_NAME
urlpatterns = [
    re_path(r"^", include(router.urls)),
    re_path(r"^userinfo/$", views.UserinfoView.as_view(), name="userinfo"),
    re_path(
        r"^switch-notification/",
        views.SwitchNotificationView.as_view(),
        name="switchnotification",
    ),
    re_path(r"^sts/", views.SecurityTokenService.as_view(), name="sts"),
    re_path(r"^userscsv", views.ExportUsersCSV.as_view(), name="usersexport"),
]
