import threading
from abc import ABC, abstractmethod
from typing import Type

from authlib.integrations.django_oauth2 import (
    AuthorizationServer,
)
from authlib.jose import JsonWebKey, KeySet
from authlib.oauth2.rfc6749 import grants as oauth2_grants
from authlib.oidc.core import grants as core_grants, UserInfo
from django.contrib.auth import get_user_model

from identities.models import OAuth2Client, OAuth2Token, AuthorizationCode
from portal.settings import CONFIG
from projects.models.data_transfer import DataTransfer

User = get_user_model()


class AuthorizationCodeGrant(oauth2_grants.AuthorizationCodeGrant):
    def save_authorization_code(self, code, request):
        # OpenID request MAY have "nonce" parameter
        nonce = request.data.get("nonce")
        client = request.client
        auth_code = AuthorizationCode(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            response_type=request.response_type,
            scope=request.scope,
            user=request.user,
            nonce=nonce,
        )
        auth_code.save()
        return auth_code

    def query_authorization_code(self, code, client):
        try:
            item = AuthorizationCode.objects.get(code=code, client_id=client.client_id)
        except AuthorizationCode.DoesNotExist:
            return None
        return item if not item.is_expired() else None

    def delete_authorization_code(self, authorization_code):
        authorization_code.delete()

    def authenticate_user(self, authorization_code):
        return authorization_code.user


class PortalAuthorizationServer(AuthorizationServer):
    def save_token(self, token, request):
        """We do NOT store the `token` in the database at all."""


class IClaimProvider(ABC):
    @staticmethod
    @abstractmethod
    def name() -> str:
        pass

    @abstractmethod
    def get_claim(self, user: User, dtr: DataTransfer | None) -> str:
        pass


class OpenIDCode(core_grants.OpenIDCode):
    def __init__(self, *claim_providers: [Type[IClaimProvider]]):
        # Keep client in per-thread local storage
        self._thread_local = threading.local()
        self._claim_providers = {cp.name(): cp() for cp in claim_providers}
        super().__init__(False)

    JWT_CONFIG = {
        # Gets overwritten below
        "key": "secret_key",
        "alg": CONFIG.oauth.alg,
        # Issuer claim
        "iss": CONFIG.oauth.issuer,
        # Expiration time claim
        "exp": CONFIG.oauth.expiration_time_seconds,
    }

    def exists_nonce(self, nonce, request):
        try:
            AuthorizationCode.objects.get(client_id=request.client_id, nonce=nonce)
            return True
        except AuthorizationCode.DoesNotExist:
            return False

    def get_jwt_config(self, grant):
        with open(CONFIG.oauth.jwt_key.private_path) as fp:
            data = fp.read()
            private_key = JsonWebKey.import_key(data)
            self.JWT_CONFIG["key"] = KeySet([private_key]).as_dict(is_private=True)
        return self.JWT_CONFIG

    def process_token(
        self, grant, token, dtr=None
    ):  # ignore pylint: disable=arguments-differ
        # Instead of overwriting complete `process_token` method, use
        # the following hack. Client claims and DTR are needed in `generate_user_info` below.
        self._thread_local = {"client": grant.request.client, "dtr": dtr}
        return super().process_token(grant, token)

    def generate_user_info(self, user, scope):
        user_info = UserInfo(sub=str(user.pk), name=user.username)
        for claim in self._thread_local["client"].claims.split(","):
            # If a claim provider is registered for the claim, use it, even though it returns `None`.
            if claim_provider := self._claim_providers.get(claim.strip().lower()):
                claim_value = claim_provider.get_claim(
                    user, dtr=self._thread_local["dtr"]
                )
            else:
                claim_value = getattr(user, claim, None)
            if isinstance(claim_value, str):  # Ignore None and other types
                user_info[claim] = claim_value
        return user_info


class PolicyClaim(IClaimProvider):
    @staticmethod
    def name() -> str:
        return "policy"

    def get_claim(self, user: User, dtr: DataTransfer | None) -> str | None:
        """Returns the name of the MinIO policy which should be taken.

        `consoleAdmin` is a predefined one but `dataProvider` has to be setup.
        """
        if user.is_staff:
            return "consoleAdmin"
        if user in dtr.data_provider.data_engineers:
            return "dataProvider"
        return None


class NameClaim(IClaimProvider):
    """We (ab)use the `${jwt:name}` to store bucket's name, based on project code."""

    @staticmethod
    def name() -> str:
        return "name"

    def get_claim(self, user: User, dtr: DataTransfer | None) -> str | None:
        return dtr and dtr.project.code


server = PortalAuthorizationServer(OAuth2Client, OAuth2Token)
server.register_grant(
    AuthorizationCodeGrant,
    [OpenIDCode(PolicyClaim, NameClaim)],
)
