from django.conf import settings
from django_drf_utils.email import sendmail
from django_drf_utils.serializers.utils import (
    ValidationError,
    DetailedValidationError,
    get_request_username,
)
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from ..models.pgp import PgpKeyInfo, fingerprint_validator
from ..notifications.pgp_key_info import (
    create_pgp_key_info_approved_or_rejected_notification,
    create_pgp_key_info_new_approval_request_notification,
)
from ..utils.pgp import (
    KeyStatus,
    KeyDownloadError,
    KeyMetadata,
    download_key_metadata_with_validation,
)

# Definition of allowed transitions between PGP key status values. In this
# dict, keys are the initial status, and the values the allowed transitions.
ALLOWED_KEY_STATUS_TRANSITIONS = {
    PgpKeyInfo.Status.PENDING: (
        PgpKeyInfo.Status.APPROVED,
        PgpKeyInfo.Status.DELETED,
        PgpKeyInfo.Status.KEY_REVOKED,
        PgpKeyInfo.Status.REJECTED,
    ),
    PgpKeyInfo.Status.APPROVED: (
        PgpKeyInfo.Status.APPROVAL_REVOKED,
        PgpKeyInfo.Status.DELETED,
        PgpKeyInfo.Status.KEY_REVOKED,
    ),
    PgpKeyInfo.Status.APPROVAL_REVOKED: (PgpKeyInfo.Status.KEY_REVOKED,),
    PgpKeyInfo.Status.DELETED: (PgpKeyInfo.Status.KEY_REVOKED,),
    PgpKeyInfo.Status.REJECTED: (),
    PgpKeyInfo.Status.KEY_REVOKED: (),
}


class PgpKeyInfoSerializer(serializers.ModelSerializer):
    """Serializer to convert PgpKeyInfo objects."""

    class Meta:
        model = PgpKeyInfo
        read_only_fields = ("id", "key_user_id", "key_email")
        fields = ("fingerprint", "status") + read_only_fields

    def create(self, validated_data):
        """Method called by POST requests to the endpoint. "validated_data"
        is a dictionary that contains the input entered by the user in the
        backend API form.

        Note: while "validated_data" can contain more elements than just
        "fingerprint", these are here disregarded because, when adding a new
        key, users are not allowed to set their own "status" for their key -
        only the key authority is allowed to do this.
        """

        # Verify that the user does not already have an active (i.e. approved
        # or pending) key in the database. Note: get_request_username(self)
        # returns the currently authenticated user of the application.
        user = get_request_username(self)
        if [
            instance
            for instance in PgpKeyInfo.objects.filter(user=user)
            if instance.status
            in (PgpKeyInfo.Status.APPROVED.value, PgpKeyInfo.Status.PENDING.value)
        ]:
            raise ValidationError(
                f"An '{PgpKeyInfo.Status.APPROVED.name}' or "
                f"'{PgpKeyInfo.Status.PENDING.name}' key already exists for this user."
            )

        # Retrieve key metadata from the keyserver and add it to database.
        key_metadata = retrieve_key_metadata(validated_data["fingerprint"])
        user_emails = user.profile.emails.split(",")

        if not key_metadata.email in user_emails:
            raise ValidationError(f"Key email must be one of {', '.join(user_emails)}")

        instance = super().create(
            {
                "fingerprint": validated_data["fingerprint"],
                "user": user,
                "key_user_id": key_metadata.user_id,
                "key_email": key_metadata.email,
            }
        )

        # pylint: disable=no-member
        sendmail(
            *create_pgp_key_info_new_approval_request_notification(instance),
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,
        )

        return instance

    def update(self, instance: PgpKeyInfo, validated_data):
        """Method called when PUT and PATCH requests are made to the endpoint.

        Note: in this particular case, PUT requests are not allowed and thus
        only PATCH requests will be processed here.
        """

        # Note: at this point the values in "validated_data" are already
        # validated.
        if new_status := validated_data.get("status"):
            # Verify that the requested change in key status is allowed.
            if new_status not in ALLOWED_KEY_STATUS_TRANSITIONS[instance.status]:
                raise DetailedValidationError(
                    "This change is not allowed for this field", field="status"
                )

            # If the new status is APPROVED, verify that the key is still
            # present on the keyserver and has not been deleted/revoked in the
            # mean time.
            if new_status == PgpKeyInfo.Status.APPROVED:
                _ = retrieve_key_metadata(instance.fingerprint)

            old_status = instance.status
            instance.status = new_status
            instance.save()

            if old_status == PgpKeyInfo.Status.PENDING and new_status in [
                PgpKeyInfo.Status.APPROVED,
                PgpKeyInfo.Status.REJECTED,
            ]:
                subject, body = create_pgp_key_info_approved_or_rejected_notification(
                    instance
                )
                # pylint: disable=no-member
                sendmail(
                    subject=subject,
                    body=body,
                    recipients=(instance.user.email,),
                    email_cfg=settings.CONFIG.email,
                )

            return instance

        # Return an error if the request is missing the "status" data.
        raise DetailedValidationError("This field is mandatory", field="status")


class PgpKeyStatusSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """Serializer that accepts a list of one or more fingerprints as input and
    returns a list of {"fingerprint": <value>, "status": <value>} objects as
    output.
    """

    class StatusByFingerprintSerializer(
        serializers.Serializer
    ):  # pylint: disable=abstract-method
        """Serializer for inputs/outputs of type: {"fingerprint": ..., "status": ...}"""

        fingerprint = serializers.CharField(
            max_length=40,
            validators=(fingerprint_validator,),
            read_only=True,
        )
        status = serializers.ChoiceField(
            choices=PgpKeyInfo.Status.choices,
            read_only=True,
        )

    # The "fingerprints" field is used by the serializer for input only, i.e.
    # serializing data received in a POST request.
    # Note: allow_empty is set to "True" so that the endpoint accepts empty
    #       requests (it returns an empty list for such requests).
    fingerprints = serializers.ListField(
        child=serializers.CharField(max_length=40, validators=(fingerprint_validator,)),
        allow_empty=True,
        write_only=True,
    )
    # The "status_by_fingerprint" field is a list of objects that look like
    # {"fingerprint": <value>, "status": <value>}. It's only used by the
    # serializer for output, i.e. the value returned to a POST request.
    status_by_fingerprint = StatusByFingerprintSerializer(many=True, read_only=True)

    def create(self, validated_data) -> PgpKeyInfo:
        """De-serialization function: turns validated data passed to the API
        into a PgpKeyInfo instance.
        """

        def get_status(fpr: str) -> dict[str, str | KeyStatus]:
            # Each fingerprint (fpr) is unique so there can be only one key
            # with a given fingerprint in the database table. If the filter
            # returns 0 keys, it means the key is not present in the database.
            try:
                key_info: PgpKeyInfo = PgpKeyInfo.objects.filter(fingerprint=fpr)[0]
                status = key_info.status
            except IndexError:
                status = "UNKNOWN_KEY"
            return {"fingerprint": fpr, "status": status}

        # The serializer must return an object of type
        # Dict["status_by_fingerprint": List[status_by_fingerprint objects]]
        return {
            "status_by_fingerprint": [
                get_status(fpr) for fpr in validated_data["fingerprints"]
            ]
        }


def retrieve_key_metadata(fingerprint: str) -> KeyMetadata:
    """Download key from keyserver and extract its metadata."""
    try:
        # pylint: disable=no-member
        return download_key_metadata_with_validation(
            fingerprint=fingerprint,
            keyserver_url=settings.CONFIG.pgp.keyserver,
            end_relax=settings.CONFIG.pgp.end_relax,
        )
    except KeyDownloadError as e:
        raise ValidationError(
            "No verified key matching the specified fingerprint was "
            f"found on the keyserver. Reason for error: {str(e)}"
        ) from e


class PgpKeyMetadataSerializer(serializers.Serializer):
    fingerprint = serializers.CharField()
    user_id = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)
    status = serializers.ChoiceField(
        choices=[(s.name, s.value) for s in KeyStatus], read_only=True
    )
    error = serializers.CharField(read_only=True)

    def create(self, validated_data):
        raise MethodNotAllowed(method="POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")
