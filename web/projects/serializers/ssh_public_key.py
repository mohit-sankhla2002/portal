from django.db import transaction
from rest_framework import serializers

from ..models.ssh_public_key import SSHPublicKey


# pylint: disable=too-many-ancestors
class SSHPublicKeySerializer(serializers.ModelSerializer):
    """Serializer for SSHPublicKey objects."""

    class Meta:
        model = SSHPublicKey
        read_only_fields = ("id",)
        fields = ("user", "project", "created", "changed", "key") + read_only_fields

    @transaction.atomic
    def create(self, validated_data):
        """Method called by POST requests to the endpoint.
        If an SSH key already exists for that user and project,
        it will be updated instead.
        """
        existing_ssh = SSHPublicKey.objects.filter(
            user=validated_data["user"],
            project=validated_data["project"],
        ).first()
        if existing_ssh:
            return super().update(instance=existing_ssh, validated_data=validated_data)
        return super().create(validated_data=validated_data)
