from typing import List

from django.db import transaction
from django.db.models import QuerySet
from django_drf_utils.serializers.utils import (
    DetailedValidationError,
    get_request_username,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .approval import (
    NodeApprovalSerializer,
    DataProviderApprovalSerializer,
    block_approvals,
    GroupApprovalSerializer,
)
from .data_package import DataPackageSerializer
from ..models.approval import DataProviderApproval, NodeApproval, GroupApproval
from ..models.data_provider import DataProvider
from ..models.data_transfer import DataTransfer
from ..models.project import Project, ProjectRole, ProjectUserRole
from ..notifications.dtr_notification import DtrCreatedNotification

CREATE_MESSAGE_ARCHIVED_PROJECT = "Cannot create data transfer for an archived project."
UPDATE_MESSAGE_ARCHIVED_PROJECT = "Cannot update data transfer of an archived project."


class DataTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataTransfer
        read_only_fields = (
            "id",
            "packages",
            "node_approvals",
            "data_provider_approvals",
            "group_approvals",
            "requestor_name",
            "requestor_display_id",
            "requestor_first_name",
            "requestor_last_name",
            "requestor_email",
            "project_name",
            "project_archived",
            "transfer_path",
            "can_see_credentials",
        )
        fields = read_only_fields + (
            "project",
            "max_packages",
            "status",
            "data_provider",
            "requestor",
            "purpose",
            "data_selection_confirmation",
            "legal_basis",
            "creation_date",
            "change_date",
        )
        extra_kwargs = {"requestor": {"required": False}}

    # Sets `required: true` in the OpenAPI schema
    max_packages = serializers.IntegerField(required=True)
    data_provider = serializers.SlugRelatedField(
        queryset=DataProvider.objects.all(),
        read_only=False,
        slug_field="code",
        required=True,
    )

    packages = DataPackageSerializer(
        many=True,
        read_only=True,
    )

    node_approvals = NodeApprovalSerializer(
        many=True,
        read_only=True,
    )

    data_provider_approvals = DataProviderApprovalSerializer(
        many=True,
        read_only=True,
    )

    group_approvals = GroupApprovalSerializer(
        many=True,
        read_only=True,
    )

    requestor_display_id = serializers.CharField(
        source="requestor.profile.display_id", read_only=True
    )
    requestor_first_name = serializers.CharField(
        source="requestor.first_name", read_only=True
    )
    requestor_last_name = serializers.CharField(
        source="requestor.last_name", read_only=True
    )
    requestor_email = serializers.CharField(source="requestor.email", read_only=True)

    project_name = serializers.CharField(source="project.name", read_only=True)

    project_archived = serializers.BooleanField(
        source="project.archived", read_only=True
    )
    data_selection_confirmation = serializers.BooleanField(
        write_only=True,
        required=False,
        help_text=(
            "Confirmation that there is an agreement of what data should "
            "be sent for this data transfer"
        ),
    )
    transfer_path = serializers.SerializerMethodField()
    can_see_credentials = serializers.SerializerMethodField(
        help_text="Whether to show credentials panel or not in the UI."
    )

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related(
            "node_approvals__node",
            "data_provider_approvals__data_provider",
            "packages__trace",
        ).select_related(
            "project__destination", "requestor__profile", "data_provider__node"
        )

    def validate(self, attrs):
        purpose = attrs.get("purpose")
        if (
            purpose is not None  # PATCH request may not contain `purpose`
            and purpose == DataTransfer.PRODUCTION
            and not attrs.get("legal_basis", "")
        ):
            raise ValidationError("Production data transfers require legal basis.")
        return super().validate(attrs)

    @transaction.atomic
    def create(self, validated_data):
        if not validated_data.pop("data_selection_confirmation", False):
            raise ValidationError(
                "Data transfer confirmation is required for creating new DTRs."
            )
        if validated_data["project"].archived:
            raise ValidationError(CREATE_MESSAGE_ARCHIVED_PROJECT)
        if validated_data.get("status", DataTransfer.INITIAL) != DataTransfer.INITIAL:
            raise DetailedValidationError(
                "Cannot create new transfer in state different than 'initial'",
                field="status",
            )
        request_user = get_request_username(self)
        requestor = validated_data.setdefault("requestor", request_user)
        if requestor != request_user and not request_user.is_staff:
            raise DetailedValidationError(
                "Not allowed to specify requestor", field="requestor"
            )
        if not ProjectUserRole.objects.filter(
            project=validated_data["project"], user=requestor, role=ProjectRole.DM.value
        ).exists():
            raise DetailedValidationError(
                "Requestor is not a data manager of the project", field="requestor"
            )
        instance = super().create(validated_data)
        self.create_approvals(instance)
        self.notify_on_creation(
            instance, self.context["request"].build_absolute_uri("/")
        )
        return instance

    @transaction.atomic
    def update(self, instance: DataTransfer, validated_data):
        # data_selection_confirmation is only relevant for new DTR creation
        validated_data.pop("data_selection_confirmation", None)
        if instance.project.archived:
            raise ValidationError(UPDATE_MESSAGE_ARCHIVED_PROJECT)
        if validated_data.get("purpose", instance.purpose) != instance.purpose:
            raise DetailedValidationError(
                "Cannot update data transfer's purpose",
                field="purpose",
            )
        new_status: str = validated_data.get("status")
        # If we try to change the status to `UNAUTHORIZED`
        if (
            instance.status is not new_status
            and new_status == DataTransfer.UNAUTHORIZED
        ):
            block_approvals(instance)
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["packages"] = sorted(
            response["packages"], key=lambda x: x["file_name"]
        )
        return response

    @staticmethod
    def create_approvals(data_transfer: DataTransfer):
        approvals = [
            DataProviderApproval(
                data_transfer=data_transfer, data_provider=data_transfer.data_provider
            )
        ]
        for node in tuple(
            {
                data_transfer.project.destination,
                data_transfer.data_provider.node,
            }
        ):
            approval = NodeApproval(
                node=node,
                data_transfer=data_transfer,
                type=NodeApproval.Type.HOSTING
                if node == data_transfer.project.destination
                else NodeApproval.Type.TRANSFER,
            )
            approvals.append(approval)
        # Legal approval group's approval is needed for `PRODUCTION` DTR only!
        if (
            data_transfer.purpose == DataTransfer.PRODUCTION
            and data_transfer.project.legal_approval_group
        ):
            approvals.append(
                GroupApproval(
                    data_transfer=data_transfer,
                    group=data_transfer.project.legal_approval_group,
                )
            )
        for approval in approvals:
            approval.save()

    @staticmethod
    def notify_on_creation(dtr: DataTransfer, base_url: str):
        dtr_notification = DtrCreatedNotification(dtr, base_url)
        dtr_notification.send_messages()
        dtr_notification.sendmail()

    @staticmethod
    def get_transfer_path(dtr: DataTransfer) -> List[str]:
        data_provider: DataProvider = dtr.data_provider
        project: Project = dtr.project
        steps: list[str] = [data_provider.name, data_provider.node.name]
        if project.destination != data_provider.node:
            steps.append(project.destination.name)
        steps.append(project.name)
        return steps

    def get_can_see_credentials(self, dtr: DataTransfer) -> bool:
        """Whether current user can see the credentials tab on the DTR panel (frontend) or not.

        Will be displayed to data provider data engineers and DMs of the project only, if the DTR is authorized.
        """
        request_user = get_request_username(self)
        return (
            request_user in dtr.data_provider.data_engineers
            or request_user in dtr.project.users_by_role(ProjectRole.DM)
        ) and dtr.status == DataTransfer.AUTHORIZED
