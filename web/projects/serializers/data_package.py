import hashlib
import json

from django.db import transaction
from django.db.models import QuerySet
from django.utils import dateparse
from django_drf_utils.serializers.utils import DetailedValidationError
from rest_framework import serializers

from ..models.data_transfer import DataPackage, DataPackageTrace, DataTransfer
from ..models.node import Node
from ..models.pgp import PgpKeyInfo
from ..models.project import Project, ProjectRole


class NodeField(serializers.Field):
    """Field for deserializing a node code into a Node object"""

    def to_representation(self, value):
        return value.code

    def to_internal_value(self, data):
        try:
            return Node.objects.get(code=data)
        except Node.DoesNotExist as e:
            raise DetailedValidationError(
                f"Invalid node: {data}", field=self.field_name
            ) from e


class DataPackageCheckSerializer(serializers.ModelSerializer):
    """Check PKG integrity (read-only POST)"""

    project_code = serializers.CharField(
        source="data_transfer.project.code", read_only=True
    )

    class Meta:
        model = DataPackage
        read_only_fields = (
            "metadata_hash",
            "data_transfer",
            "purpose",
            "project_code",
        )
        fields = read_only_fields + ("file_name", "metadata")

    def create(self, validated_data):
        _, data = check_dpkg(**validated_data)
        return DataPackage(**data)


def check_dpkg(
    metadata, file_name, file_size: int | None = None, read_only: bool = True
):
    """Check if DataPackage is valid.

    validated_data: request data
    read_only: If True, the check will always fail for status != AUTHORIZED.
        If False, the check also succeeds if status is EXPIRED and
        DataPackage with the given metadata_hash already exists.
        This flag is necessary to differentiate between the following two cases:
          1. `read_only` is `True`: this function is called from sett through the `check` endpoint
              when attempting to transfer a data package. sett only checks if the package is valid
              and doesn't perform any modifying operations (is `read_only`).
          2. `read_only` is `False`: this function is called implicitly in the
             `data-package/log` endpoint to check if the data package data is valid before creating
             a data package, which is a modifying operation (is NOT `read_only`).
    """
    metadata_str = metadata
    try:
        metadata = json.loads(metadata)
    except json.decoder.JSONDecodeError as e:
        raise DetailedValidationError(f"Invalid metadata: {e}") from e
    try:
        metadata_hash = hash_metadata(metadata)
        transfer_id = metadata["transfer_id"]
        purpose = metadata["purpose"]
    except KeyError as e:
        raise DetailedValidationError(f"metadata is missing field '{e}'") from e
    try:
        transfer = DataTransfer.objects.get(id=transfer_id)
    except (ValueError, DataTransfer.DoesNotExist):
        raise DetailedValidationError(f"Invalid transfer: {transfer_id}") from None
    dpkg_exists = DataPackage.objects.filter(metadata_hash=metadata_hash).exists()
    if transfer.status != DataTransfer.AUTHORIZED:
        if read_only or not (transfer.status == DataTransfer.EXPIRED and dpkg_exists):
            raise DetailedValidationError(
                "Cannot create data package for transfer with "
                f"ID '{transfer_id}' in state '{transfer.status}'"
            )
    if read_only and dpkg_exists:
        raise DetailedValidationError(
            "Data package already exists for transfer with "
            f"ID '{transfer_id}' with the same metadata '{metadata_str}'"
        )
    if not transfer.data_provider.enabled:
        raise DetailedValidationError(
            f"Data Provider: {transfer.data_provider.code} is currently "
            f"not able to send data for transfer with ID '{transfer_id}'"
        )
    if transfer.project.destination.node_status != Node.STATUS_ONLINE:
        raise DetailedValidationError(
            f"Node: {transfer.project.destination.name} is not online"
        )
    check_user_permissions(transfer.project, metadata["recipients"], metadata["sender"])
    if purpose != transfer.purpose:
        raise DetailedValidationError(
            f"Got a '{purpose}' package for a '{transfer.purpose}' "
            f"data transfer with ID '{transfer_id}'"
        )

    return (
        transfer,
        {
            "metadata_hash": metadata_hash,
            "purpose": purpose,
            "data_transfer": transfer,
            "file_name": file_name,
            "file_size": file_size,
            "metadata": metadata_str,
        },
    )


def check_user_permissions(project: Project, recipients, sender) -> None:
    """Check if all users involved in the dtr are allowed to be involved"""
    recipients_keys: QuerySet[PgpKeyInfo] = PgpKeyInfo.objects.filter(
        fingerprint__in=recipients
    )
    keys = recipients_keys | PgpKeyInfo.objects.filter(fingerprint=sender)
    unapproved_fingerprints = {
        (key.fingerprint, "unapproved")
        for key in keys
        if key.status != PgpKeyInfo.Status.APPROVED
    }
    unauthorized_fingerprints = {
        (key.fingerprint, f"not a data manager of project {project}")
        for key in recipients_keys
        if key.user not in project.users_by_role(ProjectRole.DM)
    }
    missing_fingerprints = {
        (fpr, "not found on Portal")
        for fpr in {*recipients, sender} - {key.fingerprint for key in keys}
    }
    if violations := (
        *unapproved_fingerprints,
        *unauthorized_fingerprints,
        *missing_fingerprints,
    ):
        raise DetailedValidationError(
            "The following keys are invalid: "
            + ", ".join(f"{fpr} ({reason})" for fpr, reason in violations)
        )


class DataPackageTraceSerializer(serializers.ModelSerializer):
    node = NodeField()

    class Meta:
        model = DataPackageTrace
        fields = ("node", "timestamp", "status")


class DataPackageSerializer(DataPackageCheckSerializer):
    """Serializer to list data packages"""

    destination_node = serializers.CharField(
        source="data_transfer.project.destination.code", read_only=True
    )
    trace = DataPackageTraceSerializer(read_only=True, many=True)
    data_provider = serializers.CharField(
        source="data_transfer.data_provider.code", read_only=True
    )
    sender_pgp_key_info = serializers.SerializerMethodField()

    class Meta(DataPackageCheckSerializer.Meta):
        more_fields = (
            "id",
            "trace",
            "destination_node",
            "data_provider",
            "file_size",
            "sender_pgp_key_info",
        )
        read_only_fields = (
            DataPackageCheckSerializer.Meta.read_only_fields + more_fields
        )
        fields = DataPackageCheckSerializer.Meta.fields + more_fields

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related(
            "data_transfer__project__destination",
            "data_transfer__data_provider",
            "trace",
        )

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["trace"] = sorted(
            response["trace"], key=lambda x: dateparse.parse_datetime(x["timestamp"])
        )
        return response

    def validate(self, attrs):
        _, validated_data = check_dpkg(**attrs)
        return validated_data

    def get_sender_pgp_key_info(self, obj: DataPackage) -> str:
        """Return data package's sender's PGP key info"""

        sender_info = "Sender not found"
        try:
            if sender := json.loads(obj.metadata).get("sender"):
                pgp_key_info = PgpKeyInfo.objects.get(fingerprint=sender)
                sender_info = f"{pgp_key_info.key_user_id} {pgp_key_info.fingerprint}"
        except (PgpKeyInfo.DoesNotExist, json.decoder.JSONDecodeError):
            pass
        return sender_info


class DataPackageLogSerializer(DataPackageTraceSerializer):
    """Serializer for logging data packages reported by a landing zone (involves metadata checks)"""

    file_name = serializers.CharField()
    metadata = serializers.CharField()
    file_size = serializers.IntegerField(
        required=False,
        min_value=1,
        help_text="File size in bytes (min. 1b)",
    )

    class Meta:
        model = DataPackageTrace
        fields = ("node", "status", "file_name", "file_size", "metadata")

    @transaction.atomic
    def create(self, validated_data):
        transfer, data = check_dpkg(
            validated_data["metadata"],
            validated_data["file_name"],
            validated_data.get("file_size"),
            read_only=False,
        )
        matadata_hash = data.pop("metadata_hash")
        dpkg, created = DataPackage.objects.get_or_create(
            metadata_hash=matadata_hash, defaults=data
        )
        if created and 0 < transfer.max_packages <= transfer.packages.count():
            transfer.status = DataTransfer.EXPIRED
            transfer.save()
        return dpkg


def hash_metadata(metadata: dict) -> str:
    return hashlib.sha3_256(
        repr(
            (
                metadata["sender"],
                metadata["recipients"],
                metadata["transfer_id"],
                metadata["timestamp"],
                metadata["checksum"],
            )
        ).encode()
    ).hexdigest()
