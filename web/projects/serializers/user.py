import logging
from functools import wraps

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db import utils
from django.db.utils import DatabaseError
from django_drf_utils.exceptions import UnprocessableEntityError, ConflictError
from django_drf_utils.serializers.utils import get_request_username
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.validators import UniqueValidator

from identities.permissions import has_group_manager_groups
from ..models.data_provider import (
    has_data_provider_admin_data_providers,
    has_data_provider_viewer_data_providers,
    get_data_provider_admin_data_providers,
)
from ..models.flag import Flag
from ..models.node import (
    get_node_admin_nodes,
    has_node_viewer_nodes,
    has_node_admin_nodes,
)
from ..models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    has_projects,
    is_data_manager,
    is_manager,
    is_project_leader,
)
from ..models.user import (
    PROFILE_LOCAL_USERNAME_REGEX,
    PROFILE_LOCAL_USERNAME_REGEX_MESSAGE,
    Profile,
    UserNamespace,
    is_legal_approver,
)
from ..signals.project import (
    update_permissions_signal,
    DictUserRole,
)

User = get_user_model()
logger = logging.getLogger(__name__)


class FlagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flag
        read_only_fields = ("id",)
        fields = read_only_fields + ("code", "description", "users")


def apiexception(detail: str | None = None):
    def decorator(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except ValidationError as e:
                errorMsg = detail or "".join(e.messages)
                raise UnprocessableEntityError(detail=errorMsg) from e

        return decorated

    return decorator


class LocalUsernameMinLengthValidator(validators.MinLengthValidator):
    @apiexception()
    def __call__(self, value):
        super().__call__(value)


class LocalUsernameMaxLengthValidator(validators.MaxLengthValidator):
    @apiexception()
    def __call__(self, value):
        super().__call__(value)


class LocalUsernameRegexValidator(validators.RegexValidator):
    @apiexception(detail=PROFILE_LOCAL_USERNAME_REGEX_MESSAGE)
    def __call__(self, value):
        super().__call__(value)


class ProfileSerializer(serializers.ModelSerializer):
    namespace = serializers.SlugRelatedField(slug_field="name", read_only=True)
    # pylint: disable=no-member
    local_username = serializers.CharField(
        required=True,
        allow_blank=False,
        validators=(
            LocalUsernameRegexValidator(PROFILE_LOCAL_USERNAME_REGEX),
            LocalUsernameMinLengthValidator(
                settings.CONFIG.user.local_username.min_length,
            ),
            LocalUsernameMaxLengthValidator(
                settings.CONFIG.user.local_username.max_length,
            ),
        ),
    )
    # pylint: disable=no-member
    uid = serializers.IntegerField(
        required=False,
        validators=(
            validators.MinValueValidator(settings.CONFIG.user.uid.min),
            validators.MaxValueValidator(settings.CONFIG.user.uid.max),
        ),
    )
    # pylint: disable=no-member
    gid = serializers.IntegerField(
        required=False,
        validators=(
            validators.MinValueValidator(settings.CONFIG.user.gid.min),
            validators.MaxValueValidator(settings.CONFIG.user.gid.max),
        ),
    )

    class Meta:
        model = Profile
        read_only_fields = (
            "affiliation",
            "affiliation_id",
            "uid",
            "gid",
            "emails",
        )
        fields = read_only_fields + (
            "local_username",
            "display_name",
            "display_id",
            "namespace",
            "display_local_username",
            "affiliation_consent",
            "custom_affiliation",
        )


class ProjectUserServiceListSerializer(serializers.ListSerializer):
    def update(self, *args, **kwargs):
        raise NotImplementedError(
            "Update currently not supported (suppresses .update from ListSerializer"
        )

    def to_representation(self, data):
        return [
            self.child.to_representation(item)
            for item in data.filter(project=self.context.get("project"))
        ]


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(required=False)
    username = serializers.CharField(
        required=False, validators=[UniqueValidator(queryset=User.objects.all())]
    )
    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=False,
        many=True,
        required=False,
        queryset=Flag.objects.all(),
    )
    groups = serializers.SlugRelatedField(
        slug_field="name",
        read_only=False,
        many=True,
        required=False,
        queryset=Group.objects.all(),
    )

    class Meta:
        model = User
        read_only_fields = (
            "id",
            "last_login",
        )
        fields = read_only_fields + (
            "profile",
            "username",
            "email",
            "first_name",
            "last_name",
            "groups",
            "flags",
            "is_active",
        )

    @staticmethod
    def _set_local_username(profile: Profile, new_local_username: str):
        profile.local_username = new_local_username
        # set default user namespace
        # pylint: disable=no-member
        namespace, _ = UserNamespace.objects.get_or_create(
            name=settings.CONFIG.user.default_namespace
        )
        profile.namespace = namespace

    @staticmethod
    def _next_uid(profile: Profile):
        uid_max = Profile.objects.aggregate(models.Max("uid"))["uid__max"]
        if uid_max:
            profile.uid = uid_max + 1
        else:
            profile.uid = settings.CONFIG.user.uid.min  # pylint: disable=no-member

    @staticmethod
    def _next_gid(profile: Profile):
        gid_max = Profile.objects.aggregate(models.Max("gid"))["gid__max"]
        if gid_max:
            profile.gid = gid_max + 1
        else:
            profile.gid = settings.CONFIG.user.gid.min  # pylint: disable=no-member

    def _deactivate_user(self, instance: User):
        # remove all associations to user
        project_user_roles = ProjectUserRole.objects.all()
        user_roles_by_project: dict[Project, list[DictUserRole]] = {}
        for pur in project_user_roles:
            if pur.project not in user_roles_by_project:
                user_roles_by_project[pur.project] = []
            if pur.user.id != instance.id:
                user_roles_by_project[pur.project].append(
                    {"user": pur.user, "role": ProjectRole(pur.role)}
                )
        for project, user_roles in user_roles_by_project.items():
            update_permissions_signal.send(
                sender=project,
                user=get_request_username(self),
                user_roles=user_roles,
            )

        # remove all permissions
        instance.groups.clear()
        instance.user_permissions.clear()

        instance.flags.set([])

    @transaction.atomic
    def update(self, instance, validated_data):  # pylint: disable=too-many-branches
        if "profile" in validated_data:
            if not getattr(instance, "profile", False):
                Profile(user=instance)
            request_profile = validated_data["profile"]
            if "local_username" in request_profile:
                self._set_local_username(
                    instance.profile, request_profile["local_username"]
                )
            if not instance.profile.uid:
                self._next_uid(instance.profile)
            if not instance.profile.gid:
                self._next_gid(instance.profile)
            for attr in (
                "affiliation",
                "affiliation_id",
                "uid",
                "gid",
                "affiliation_consent",
                "custom_affiliation",
            ):
                if attr in request_profile:
                    setattr(instance.profile, attr, request_profile[attr])
            try:
                instance.profile.save()
            except utils.IntegrityError as exc:
                raise ConflictError(exc) from exc
            except DatabaseError as exc:
                raise ConflictError(exc) from exc
        for field in ("flags", "groups"):
            if field in validated_data:
                getattr(instance, field).set(validated_data[field])
        for field in ("first_name", "last_name", "email"):
            if field in validated_data:
                setattr(instance, field, validated_data[field])
        if (
            "is_active" in validated_data
            and validated_data["is_active"] != instance.is_active
        ):
            request_user = get_request_username(self)
            logger.info(
                "User (ID: %s) changed `is_active` of user (ID: %s) from %s to %s",
                request_user.id,
                instance.id,
                instance.is_active,
                validated_data["is_active"],
            )
            instance.is_active = validated_data["is_active"]
            if not instance.is_active:
                self._deactivate_user(instance)

        instance.save()
        return instance

    def create(self, validated_data):
        """Create a new user and its associated user Profile."""
        profile = validated_data.pop("profile", {})
        if not validated_data.get("username"):
            validated_data["username"] = validated_data["email"]
        try:
            user = super().create(validated_data)
        except DatabaseError as e:
            raise ConflictError(e) from e

        # Create a user Profile database entry associated to the user.
        try:
            Profile.objects.create(user=user, **profile)
        except DatabaseError as e:
            # If the Profile creation fails, the entire user creation is
            # aborted and the database entry that was just created must
            # also be removed.
            user.delete()
            raise ConflictError(e) from e

        return user


class UserShortSerializer(serializers.ModelSerializer):
    """A read-only user serializer."""

    display_local_username = serializers.CharField(
        source="profile.display_local_username", read_only=True
    )
    local_username = serializers.CharField(
        source="profile.local_username", read_only=True
    )
    affiliation = serializers.CharField(source="profile.affiliation", read_only=True)
    affiliation_id = serializers.CharField(
        source="profile.affiliation_id", read_only=True
    )
    uid = serializers.IntegerField(source="profile.uid", read_only=True)
    gid = serializers.IntegerField(source="profile.gid", read_only=True)
    roles = serializers.SerializerMethodField()

    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=True,
        many=True,
        required=False,
    )

    class Meta:
        model = User
        read_only_fields = (
            "username",
            "email",
            "last_name",
            "first_name",
            "local_username",
            "affiliation",
            "affiliation_id",
            "display_local_username",
            "uid",
            "gid",
            "flags",
            "roles",
        )
        fields = read_only_fields

    # Make sure that this serializer is NOT used for POST resp. PUT requests

    def get_roles(self, user: User):
        user_roles = self.context.get("user_roles", {})
        return user_roles.get(user.username, [])

    def create(self, validated_data):
        raise MethodNotAllowed(method="POST")

    def update(self, instance, validated_data):
        raise MethodNotAllowed(method="PUT")


class UserinfoSerializer(serializers.ModelSerializer):
    """Serializer that extracts data used to display the user infobox in the
    portal.
    """

    # Sets `required: true` in the OpenAPI schema.
    id = serializers.IntegerField(required=True)
    profile = ProfileSerializer(read_only=True)
    ip_address = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField(
        help_text="List of permissions currently held by the user. "
        "Note that the permissions are project/data provider/node agnostic: "
        "one association is sufficient to get `true` here."
    )
    manages = serializers.SerializerMethodField(
        help_text="List of entities (data provider/node) the user is being `admin` of."
    )

    groups = serializers.SlugRelatedField(
        slug_field="name",
        read_only=True,
        many=True,
        required=False,
    )

    flags = serializers.SlugRelatedField(
        slug_field="code",
        read_only=True,
        many=True,
        required=False,
    )

    class Meta:
        """List of all fields returned by the serializer"""

        model = User
        read_only_fields = ("ip_address", "permissions", "manages", "flags", "groups")
        fields = read_only_fields + (
            "username",
            "email",
            "first_name",
            "last_name",
            "profile",
            "id",
        )

    def get_ip_address(self, _: User):
        request = self.context["request"]
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            return x_forwarded_for.split(",")[0]
        return request.META.get("REMOTE_ADDR")

    def get_permissions(self, _: User):
        request = self.context["request"]
        return {
            "manager": is_manager(request.user),
            "staff": request.user.is_staff,
            "data_manager": is_data_manager(request.user),
            "project_leader": is_project_leader(request.user),
            "data_provider_admin": has_data_provider_admin_data_providers(request.user),
            "data_provider_viewer": has_data_provider_viewer_data_providers(
                request.user
            ),
            "node_admin": has_node_admin_nodes(request.user),
            "node_viewer": has_node_viewer_nodes(request.user),
            "group_manager": has_group_manager_groups(request.user),
            "has_projects": has_projects(request.user),
            "legal_approver": is_legal_approver(request.user),
        }

    def get_manages(self, _: User):
        request = self.context["request"]
        return {
            "data_provider_admin": get_data_provider_admin_data_providers(
                request.user
            ).values(),
            "node_admin": get_node_admin_nodes(request.user).values(),
        }
