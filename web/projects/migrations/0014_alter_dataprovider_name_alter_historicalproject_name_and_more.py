# Generated by Django 4.1.5 on 2023-01-12 09:46

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0013_historicalproject_legal_approval_group_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="dataprovider",
            name="name",
            field=models.CharField(
                max_length=512,
                unique=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Allowed characters: "a-z", "A-Z", "0-9", "-", "_", "(", ")", "\'", spaces and diacritics.',
                        regex="^[a-zA-Z0-9\\-_ ()ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ']*$",
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="historicalproject",
            name="name",
            field=models.CharField(
                db_index=True,
                max_length=512,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Allowed characters: "a-z", "A-Z", "0-9", "-", "_", "(", ")", "\'", spaces and diacritics.',
                        regex="^[a-zA-Z0-9\\-_ ()ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ']*$",
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="node",
            name="name",
            field=models.CharField(
                max_length=512,
                unique=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Allowed characters: "a-z", "A-Z", "0-9", "-", "_", "(", ")", "\'", spaces and diacritics.',
                        regex="^[a-zA-Z0-9\\-_ ()ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ']*$",
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="project",
            name="name",
            field=models.CharField(
                max_length=512,
                unique=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Allowed characters: "a-z", "A-Z", "0-9", "-", "_", "(", ")", "\'", spaces and diacritics.',
                        regex="^[a-zA-Z0-9\\-_ ()ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ']*$",
                    )
                ],
            ),
        ),
    ]
