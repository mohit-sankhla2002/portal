# Generated by Django 3.2.12 on 2022-04-25 13:49

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0001_squashed_0001_to_0030"),
    ]

    operations = [
        migrations.AlterField(
            model_name="historicalprofile",
            name="local_username",
            field=models.CharField(
                blank=True,
                max_length=64,
                null=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Enter a valid value. Allowed characters: "a-z", "0-9", "_". Must start with a lowercase letter.',
                        regex="^[a-z][a-z0-9_]*$",
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="profile",
            name="local_username",
            field=models.CharField(
                blank=True,
                max_length=64,
                null=True,
                validators=[
                    django.core.validators.RegexValidator(
                        message='Enter a valid value. Allowed characters: "a-z", "0-9", "_". Must start with a lowercase letter.',
                        regex="^[a-z][a-z0-9_]*$",
                    )
                ],
            ),
        ),
    ]
