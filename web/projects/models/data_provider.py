from typing import Iterator, Iterable, Any

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models import QuerySet
from django.dispatch import receiver
from django.utils.functional import cached_property
from django_drf_utils.models import CodeField, NameField
from guardian.shortcuts import (
    assign_perm,
    get_objects_for_user,
)

from identities.models import GroupProfile
from .const import (
    APP_PERM_CHANGE_GROUP,
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE,
    APP_PERM_DATA_PROVIDER_VIEWER,
    APP_PERM_VIEW_GROUP,
    PERM_DATA_PROVIDER_ADMIN,
)
from .node import Node, get_node_permission_nodes

User = get_user_model()


class DataProvider(models.Model):
    code = CodeField()
    name = NameField()
    node = models.ForeignKey(Node, on_delete=models.SET_NULL, null=True, blank=False)
    enabled = models.BooleanField(default=True)

    class Meta:
        permissions = [
            (
                PERM_DATA_PROVIDER_ADMIN,
                "Can edit a data provider (except for Code and Node)",
            ),
        ]

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    @cached_property
    def coordinators(self) -> Iterator[User]:
        return get_data_provider_coordinators(self)

    @cached_property
    def data_engineers(self) -> Iterator[User]:
        return get_data_provider_role_users(
            self, GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER
        )


# DP Viewer


def get_data_provider_viewer_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, APP_PERM_DATA_PROVIDER_VIEWER)


def has_data_provider_viewer_data_providers(user: User) -> bool:
    return get_data_provider_viewer_data_providers(user).exists()


def has_dp_viewer_role(user: User, data_provider: DataProvider | None = None) -> bool:
    return user.has_perm(APP_PERM_DATA_PROVIDER_VIEWER, data_provider)


# DP Coordinator


def has_dp_coordinator_role(
    user: User, data_provider: DataProvider | None = None
) -> bool:
    """Whether given user has DP Coordinator permission on given Data Provider.

    Being added to a group means that the user inherits the permissions of this group.
    Note that this method returns `True` for superusers.
    """
    return user.has_perm(
        APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE
    ) and has_dp_viewer_role(user, data_provider)


def get_data_provider_coordinators(data_provider: DataProvider) -> Iterator[User]:
    """Return list of DP coordinators.

    A DP coordinator has been effectively assigned to the
    DP coordinators group (no shortcut for superusers).
    """

    return get_data_provider_role_users(
        data_provider, GroupProfile.Role.DATA_PROVIDER_COORDINATOR
    )


# DP entry administration rights (NOT to be confused with 'DP Technical Admin')


def get_data_provider_admin_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    return get_objects_for_user(user, APP_PERM_DATA_PROVIDER_ADMIN)


def has_data_provider_admin_data_providers(user: User) -> bool:
    return get_data_provider_admin_data_providers(user).exists()


def has_data_provider_admin_permission(
    user: User, data_provider: DataProvider | None = None
) -> bool:
    return user.has_perm(APP_PERM_DATA_PROVIDER_ADMIN, data_provider)


def has_any_data_provider_permissions(user: User) -> bool:
    return get_data_provider_permission_data_providers(user).exists()


def get_data_provider_permission_data_providers(
    user: User,
) -> QuerySet[DataProvider]:
    """Returns all the data providers associated with given user either by a
    `admin_dataprovider` (DP Manager) or a `view_dataprovider`
    (DP Viewer, DP Technical Admin, DP Coordinator) permission.
    """
    return get_data_provider_admin_data_providers(
        user
    ) | get_data_provider_viewer_data_providers(user)


def assign_permissions_on_objects(
    user_or_group: Group | User,
    permissions: Iterable[str],
    objects: Iterable[Any],
):
    """Assign the cartesian product of permissions × objects to
    the given group or user
    """
    for obj in objects:
        for permission in permissions:
            assign_perm(perm=permission, user_or_group=user_or_group, obj=obj)


def get_data_provider_role_group(data_provider: DataProvider, role: str) -> Group:
    return Group.objects.get(
        profile__role=role,
        profile__role_entity_type__model=GroupProfile.RoleEntityType.DATA_PROVIDER.value,
        profile__role_entity_id=data_provider.id,
    )


def get_data_provider_role_users(
    data_provider: DataProvider, role: str
) -> QuerySet[User]:
    try:
        return get_data_provider_role_group(data_provider, role).user_set.all()
    except Group.DoesNotExist:
        return User.objects.none()


def get_data_provider_personnel_groups(user: User) -> QuerySet[Group]:
    nodes = get_node_permission_nodes(user)
    data_providers = get_data_provider_permission_data_providers(
        user
    ) | DataProvider.objects.filter(node__in=nodes)
    return Group.objects.filter(
        profile__role_entity_id__in=[dp.id for dp in data_providers],
        profile__role_entity_type__model=GroupProfile.RoleEntityType.DATA_PROVIDER.value,
    )


def assign_dp_viewer_role(dp_viewer: Group, dp: DataProvider):
    """The DP viewer role obviously only contains view permission."""
    assign_perm(perm=APP_PERM_DATA_PROVIDER_VIEWER, user_or_group=dp_viewer, obj=dp)


def assign_dp_technical_admin_role(dp_technical_admin: Group, dp: DataProvider):
    """The DP Technical Admin role might get more permissions in the future."""
    assign_perm(
        perm=APP_PERM_DATA_PROVIDER_VIEWER, user_or_group=dp_technical_admin, obj=dp
    )


def assign_dp_coordinator_role(dp_coordinator: Group, dp: DataProvider):
    """The DP Coordinator role might get more permissions in the future."""
    assign_dp_viewer_role(dp_coordinator, dp)
    assign_perm(
        perm=APP_PERM_DATA_PROVIDER_APPROVAL_STATUS_CHANGE, user_or_group=dp_coordinator
    )


# pylint: disable=unused-argument
@receiver(models.signals.post_save, sender=DataProvider)
def create_dp_groups(sender, instance, created, *args, **kwargs):
    if created:
        dp_ta = Group.objects.create(
            name=f"{instance.code} DP Technical Admin",
        )
        assign_dp_technical_admin_role(dp_ta, instance)

        dp_viewer = Group.objects.create(name=f"{instance.code} DP Viewer")
        assign_dp_viewer_role(dp_viewer, instance)

        dp_coordinator = Group.objects.create(
            name=f"{instance.code} DP Coordinator",
        )
        assign_dp_coordinator_role(dp_coordinator, instance)

        dp_de = Group.objects.create(
            name=f"{instance.code} DP Data Engineer",
        )
        assign_dp_viewer_role(dp_de, instance)

        dp_so = Group.objects.create(
            name=f"{instance.code} DP Security Officer",
        )
        assign_dp_viewer_role(dp_so, instance)

        dp_manager = Group.objects.create(
            name=f"{instance.code} DP Manager",
        )
        # DP managers can admin the groups created above
        assign_permissions_on_objects(
            user_or_group=dp_manager,
            permissions=(APP_PERM_VIEW_GROUP, APP_PERM_CHANGE_GROUP),
            objects=(dp_viewer, dp_coordinator, dp_de, dp_ta, dp_so),
        )
        # DP managers are only allowed to edit their own DP entry
        assign_permissions_on_objects(
            user_or_group=dp_manager,
            permissions=(APP_PERM_DATA_PROVIDER_ADMIN,),
            objects=(instance,),
        )

        for group, role, description in [
            (
                dp_ta,
                GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN,
                "Responsible for all technical tasks required for the setup and "
                "maintenance of a secure connection from the DP organization to the BioMedIT Node.",
            ),
            (
                dp_viewer,
                GroupProfile.Role.DATA_PROVIDER_VIEWER,
                "A DP Viewer can monitor the status of data transfers from their DP institution.",
            ),
            (
                dp_coordinator,
                GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
                "Responsible for internally coordinating the DP's readiness to send data, "
                "ensuring the legal compliance, i.e., DTUA, DTPA, etc. and all technical measures "
                "required from the DP side are in place for projects in which Data Provider "
                "institution participates.",
            ),
            (
                dp_de,
                GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER,
                "Responsible to prepare, encrypt and sign the data packages, "
                "and transfer them from the DP organization to the assigned BioMedIT Node.",
            ),
            (
                dp_so,
                GroupProfile.Role.DATA_PROVIDER_SECURITY_OFFICER,
                "The designated point of contact from the DP organization to receive incident and security "
                "notifications from BioMedIT such as (but not limited to) scheduled and non-scheduled downtimes "
                "in response to security incidents, emergency changes and systems upgrades.",
            ),
            (
                dp_manager,
                GroupProfile.Role.DATA_PROVIDER_MANAGER,
                "Represents the Data Provider institution within the scope of BioMedIT. "
                "Responsible for the institution's internal processes related to BioMedIT, "
                "delegates responsibilities to other users in their organization by assigning/removing DP roles.",
            ),
        ]:
            GroupProfile.objects.create(
                group=group, role=role, role_entity=instance, description=description
            )
