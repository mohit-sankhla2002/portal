from datetime import datetime

from django.contrib.auth.models import Group
from django.db import models
from django_drf_utils.models import CreatedMixin
from simple_history.models import HistoricalRecords

from .const import (
    PERM_DATA_PROVIDER_APPROVAL_STATUS,
    PERM_NODE_APPROVAL_STATUS,
    PERM_GROUP_APPROVAL_STATUS,
)
from .data_provider import DataProvider
from .data_transfer import DataTransfer
from .node import Node


class BaseApproval(CreatedMixin):
    class ApprovalStatus(models.TextChoices):
        APPROVED = "A"
        REJECTED = "R"
        WAITING = "W"
        BLOCKED = "B"

    data_transfer = models.ForeignKey(
        DataTransfer,
        on_delete=models.CASCADE,
        help_text="Data Transfer associated with this Approval",
    )

    status = models.CharField(
        help_text="Current status of this Approval",
        max_length=1,
        choices=ApprovalStatus.choices,
        default=ApprovalStatus.WAITING,
    )

    rejection_reason = models.CharField(
        help_text="Reason for rejecting this Approval",
        max_length=512,
        blank=True,
    )

    # User who performed the entry will be store here
    history = HistoricalRecords(inherit=True)
    change_date = models.DateTimeField(auto_now=True, null=True)

    def save(self, *args, **kwargs):
        self.data_transfer.change_date = datetime.now()
        self.data_transfer.save()
        super().save(*args, **kwargs)

    class Meta:
        abstract = True

    def declarations(self, purpose) -> tuple:
        """Returns the list of required declarations for given purpose.

        The declarations are typically implemented via mandatory checkboxes
        on frontend side, and must be ticked for approval.
        """
        if purpose == DataTransfer.PRODUCTION:
            return ("existing_legal_basis",)
        return ()

    def __str__(self) -> str:
        return f"Approval (DTR {self.data_transfer.id}, {self.status})"


class DataProviderApproval(BaseApproval):
    data_provider = models.ForeignKey(
        DataProvider,
        on_delete=models.CASCADE,
        help_text="Data Provider associated with this Approval",
    )

    class Meta:
        default_related_name = "data_provider_approvals"
        permissions = [
            (
                PERM_DATA_PROVIDER_APPROVAL_STATUS,
                "Can change data provider approval (only status)",
            ),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["data_transfer", "data_provider"],
                name="unique_data_transfer_data_provider",
            )
        ]

    def declarations(self, purpose) -> tuple:
        return ("technical_measures_in_place",) + super().declarations(purpose)


class NodeApproval(BaseApproval):
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        help_text="Node associated with this Approval",
    )

    class Type(models.TextChoices):
        HOSTING = "H"
        TRANSFER = "T"

    type = models.CharField(
        help_text="Either hosting or transfer node",
        max_length=1,
        default=Type.HOSTING,
        choices=Type.choices,
    )

    def declarations(self, purpose) -> tuple:
        declarations = ("technical_measures_in_place",) + super().declarations(purpose)
        if self.type == NodeApproval.Type.HOSTING:
            declarations += ("project_space_ready",)
        return declarations

    class Meta:
        default_related_name = "node_approvals"
        permissions = [
            (
                PERM_NODE_APPROVAL_STATUS,
                "Can change node approval (only status)",
            ),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["data_transfer", "node"],
                name="unique_data_transfer_node",
            )
        ]


class GroupApproval(BaseApproval):
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        help_text="Group associated with this Approval",
    )

    class Meta:
        default_related_name = "group_approvals"
        permissions = [
            (
                PERM_GROUP_APPROVAL_STATUS,
                "Can change group approval (only status)",
            )
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["data_transfer", "group"],
                name="unique_data_transfer_group",
            )
        ]
