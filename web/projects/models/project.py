from enum import Enum
from typing import Iterator

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import validators
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_drf_utils.models import CodeField, CreatedMixin, NameField
from simple_history.models import HistoricalRecords

from identities.models import GroupProfile
from .node import Node

User = get_user_model()


class ProjectRole(Enum):
    USER: "User" = 1
    DM: "Data Manager" = 2
    PM: "Permission Manager" = 3
    PL: "Project Leader" = 4

    @property
    def label(self) -> str:
        # pylint: disable=no-member
        return type(self).__annotations__[self.name]

    @classmethod
    def choices(cls) -> list[tuple[int, str]]:
        # pylint: disable=no-member
        return [(member.value, cls.__annotations__[member.name]) for member in cls]


class Project(CreatedMixin):
    gid = models.PositiveIntegerField(null=True, unique=True)
    code = CodeField()
    name = NameField()
    destination = models.ForeignKey(
        Node, on_delete=models.SET_NULL, null=True, related_name="projects"
    )
    archived = models.BooleanField(default=False)
    history = HistoricalRecords()
    legal_support_contact = models.EmailField(max_length=254, blank=True, null=True)
    legal_approval_group = models.ForeignKey(
        Group, on_delete=models.SET_NULL, related_name="+", blank=True, null=True
    )
    expiration_date = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ("-created",)

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    def users_by_role(self, role: ProjectRole) -> Iterator[User]:
        return (
            project_user_role.user
            for project_user_role in self.users.filter(role=role.value)
        )


class ProjectUserRole(CreatedMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="users")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")
    role = models.SmallIntegerField(choices=ProjectRole.choices())

    class Meta:
        unique_together = [["project", "user", "role"]]


class ProjectUserRoleHistory(CreatedMixin):
    project = models.ForeignKey(
        Project,
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
        related_query_name="project_user_role_project",
    )
    project_str = models.CharField(max_length=32)
    changed_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="+",
        related_query_name="project_user_role_changed_by",
    )
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="+",
        related_query_name="project_user_role_user",
    )
    role = models.SmallIntegerField(choices=ProjectRole.choices())
    enabled = models.BooleanField()

    @property
    def display_changed_by(self):
        return " ".join(
            filter(
                None,
                (
                    self.changed_by.first_name,
                    self.changed_by.last_name,
                    f"({self.changed_by.username})",
                ),
            )
        )

    @property
    def display_user(self):
        return " ".join(
            filter(
                None,
                (self.user.first_name, self.user.last_name, f"({self.user.username})"),
            )
        )

    @property
    def display_permission(self):
        return " ".join(
            map(
                str.capitalize,
                ProjectRole(self.role).name.split("_"),
            )
        )


class ProjectIPAddresses(models.Model):
    project = models.ForeignKey(
        Project, related_name="ip_address_ranges", on_delete=models.CASCADE
    )
    ip_address = models.GenericIPAddressField()
    mask = models.PositiveSmallIntegerField(
        validators=(validators.MaxValueValidator(32),)
    )
    history = HistoricalRecords()

    class Meta:
        unique_together = ("project", "ip_address", "mask")
        ordering = ("project", "ip_address", "mask")

    def __str__(self):
        return f"{self.ip_address}/{self.mask}"


RESOURCE_SCHEMES = ("http", "https", "ftp", "ftps", "sftp", "ssh")


class CustomUrlFormField(forms.fields.URLField):
    default_validators = [validators.URLValidator(schemes=RESOURCE_SCHEMES)]


class CustomUrlModelField(models.URLField):
    default_validators = [validators.URLValidator(schemes=RESOURCE_SCHEMES)]

    def formfield(self, **kwargs):
        return super().formfield(form_class=CustomUrlFormField, **kwargs)


class Resource(models.Model):
    project = models.ForeignKey(
        Project, related_name="resources", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=512)
    location = CustomUrlModelField()
    description = models.TextField(blank=True)
    contact = models.EmailField(null=False, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.location)


class Service(CreatedMixin):
    class RequestedAction(models.TextChoices):
        NONE = "NONE", _("No_action")
        CREATE = "CREATE", _("Create service")
        LOCK = "LOCK", _("Lock service")
        ARCHIVE = "ARCHIVE", _("Archive service")

    class State(models.TextChoices):
        INITIAL = "INITIAL", _("Initial state")
        PROGRESS = "PROGRESS", _("Action in progress")
        CREATED = "CREATED", _("Service created")
        LOCKED = "LOCKED", _("Service locked")
        ARCHIVED = "ARCHIVED", _("Service archived")

    project = models.ForeignKey(
        Project, related_name="services", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        related_name="services",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=2048, blank=True)
    data = models.TextField(blank=True, default="")
    action = models.CharField(
        max_length=16,
        choices=RequestedAction.choices,
        default=RequestedAction.NONE,
    )
    action_timestamp = models.DateTimeField(null=True, blank=True)
    state = models.CharField(
        max_length=16,
        choices=State.choices,
        default=State.INITIAL,
    )
    changed = models.DateTimeField(auto_now=True)
    locked = models.DateTimeField(null=True, blank=True)
    archived = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = [["project", "user", "name"]]


def is_manager(user: User) -> bool:
    return has_any_project_role(
        user,
        (
            ProjectRole.PM,
            ProjectRole.PL,
        ),
    )


def is_project_leader(user: User) -> bool:
    return has_any_project_role(user, (ProjectRole.PL,))


def is_data_manager(user: User) -> bool:
    return has_any_project_role(user, (ProjectRole.DM,))


def is_project_manager(user: User) -> bool:
    return has_any_project_role(user, (ProjectRole.PM,))


def has_any_project_role(user: User, roles: tuple[ProjectRole, ...]):
    return ProjectUserRole.objects.filter(
        user=user, role__in=(role.value for role in roles)
    ).exists()


def has_project_role(user: User, project: Project, role: ProjectRole) -> bool:
    return project.users.filter(user=user, role=role.value).exists()


def has_projects(user: User) -> bool:
    return ProjectUserRole.objects.filter(user=user).exists()


def get_project_user_groups() -> models.QuerySet[Group]:
    """Return ELSI groups needed on any project to display the legal approval
    group name for a given group ID."""
    return Group.objects.filter(profile__role=GroupProfile.Role.ELSI)
