from django.db import models
from django.utils import timezone

from django_drf_utils.models import CreatedMixin


class Feed(CreatedMixin):
    class FeedLabel(models.TextChoices):
        INFO = "INFO"
        WARNING = "WARN"

    label = models.CharField(max_length=4, choices=FeedLabel.choices)
    title = models.TextField(blank=True)
    message = models.TextField(blank=True)
    from_date = models.DateTimeField(default=timezone.now, blank=False)
    until_date = models.DateTimeField(blank=True, null=True)

    def __str__(self) -> str:
        return f"{self.label}: {self.title}"
