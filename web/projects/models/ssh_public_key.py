from django.contrib.auth import get_user_model
from django.db import models
from simple_history.models import HistoricalRecords
import sshpubkeys

from .project import Project

User = get_user_model()


def validate_ssh_key(value: str):
    """Alternative way to check ssh key by using a bit outdated library"""
    ssh = sshpubkeys.SSHKey(value, strict=True)
    try:
        ssh.parse()
    except sshpubkeys.InvalidKeyError as exc:
        raise ValueError("Invalid key: ", exc) from exc
    except NotImplementedError as exc:
        raise ValueError("Invalid key type", exc) from exc


class KeyType(models.TextChoices):
    """Key types used in the public ssh key."""

    RSA = "ssh-rsa"
    ED25519 = "ssh-ed25519"


class SSHPublicKey(models.Model):
    """Django data model for ssh public keys that belong to users.
    For every project, the user can upload an individual ssh public key.
    """

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="ssh_public_keys",
    )
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="ssh_public_keys",
    )
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)
    key = models.CharField(max_length=2048, validators=[validate_ssh_key])

    history = HistoricalRecords()

    def __str__(self):
        return str(self.key)
