from django.contrib.auth import get_user_model
from django.db import models
from django_drf_utils.models import CodeField
from simple_history.models import HistoricalRecords

User = get_user_model()


class Flag(models.Model):
    code = CodeField()
    # Description could be empty
    description = models.TextField(blank=True)
    users = models.ManyToManyField(User, related_name="flags", blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.code)
