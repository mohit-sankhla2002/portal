from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.db import models
from django.db.models import QuerySet
from django.dispatch import receiver
from django_drf_utils.models import CodeField, CreatedMixin, NameField
from guardian.shortcuts import assign_perm, get_objects_for_user

from identities.models import GroupProfile, OAuth2Client

from .const import (
    APP_PERM_CHANGE_GROUP,
    APP_PERM_NODE_ADMIN,
    APP_PERM_NODE_VIEWER,
    APP_PERM_VIEW_GROUP,
    PERM_NODE_ADMIN,
)

User = get_user_model()


class Node(CreatedMixin):
    STATUS_ONLINE = "ON"
    STATUS_OFFLINE = "OFF"
    STATUS_CHOICES = ((STATUS_ONLINE, "Online"), (STATUS_OFFLINE, "Offline"))
    code = CodeField()
    name = NameField()
    ticketing_system_email = models.EmailField(null=False, blank=False)
    node_status = models.CharField(max_length=4, choices=STATUS_CHOICES)
    object_storage_url = models.URLField(
        blank=True, verbose_name="Object Storage URL", help_text="S3 repository URL"
    )
    oauth2_client = models.ForeignKey(
        OAuth2Client,
        on_delete=models.SET_NULL,
        null=True,
        help_text="OAuth2 client used by the S3 repository",
        verbose_name="OAuth2 client",
    )

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"

    class Meta:
        permissions = [
            (
                PERM_NODE_ADMIN,
                "Can edit a node (except for Code)",
            ),
        ]


def get_node_viewer_nodes(user: User) -> QuerySet[Node]:
    """Return nodes for which the specified 'user' has 'view_node' permission."""

    return get_objects_for_user(user, APP_PERM_NODE_VIEWER)


def get_node_role_group(node: Node, role: str) -> Group:
    return Group.objects.get(
        profile__role=role,
        profile__role_entity_type__model=GroupProfile.RoleEntityType.NODE.value,
        profile__role_entity_id=node.id,
    )


def get_node_role_users(node: Node, role: str) -> QuerySet[User]:
    try:
        return get_node_role_group(node, role).user_set.all()
    except Group.DoesNotExist:
        return User.objects.none()


def get_node_personnel_groups(user: User) -> QuerySet[Group]:
    return Group.objects.filter(
        profile__role_entity_id__in=[
            node.id for node in get_node_permission_nodes(user)
        ],
        profile__role_entity_type__model=GroupProfile.RoleEntityType.NODE.value,
    )


def get_node_admins(node: Node) -> QuerySet[User]:
    return get_node_role_users(node, GroupProfile.Role.NODE_ADMIN)


def has_node_viewer_nodes(user: User) -> bool:
    return get_node_viewer_nodes(user).exists()


def has_node_viewer_permission(user: User, node: Node | None = None) -> bool:
    return user.has_perm(APP_PERM_NODE_VIEWER, node)


def get_node_admin_nodes(user: User) -> QuerySet[Node]:
    return get_objects_for_user(user, APP_PERM_NODE_ADMIN)


def has_node_admin_nodes(user: User) -> bool:
    return get_node_admin_nodes(user).exists()


def has_node_admin_permission(user: User, node: Node | None = None) -> bool:
    return user.has_perm(APP_PERM_NODE_ADMIN, node)


def has_any_node_permissions(user: User) -> bool:
    return get_node_permission_nodes(user).exists()


def get_node_permission_nodes(user: User) -> QuerySet[Node]:
    return get_node_admin_nodes(user) | get_node_viewer_nodes(user)


def assign_node_viewer(node_viewer: Group, node: Node) -> Permission:
    return assign_perm(APP_PERM_NODE_VIEWER, node_viewer, node)


def assign_node_admin(node_admin: Group, node: Node) -> Permission:
    return assign_perm(APP_PERM_NODE_ADMIN, node_admin, node)


def assign_node_manager(node_manager: Group, node_viewer: Group, node_admin: Group):
    for group in (node_viewer, node_admin):
        assign_perm(APP_PERM_CHANGE_GROUP, node_manager, group)
        assign_perm(APP_PERM_VIEW_GROUP, node_manager, group)


# pylint: disable=unused-argument
@receiver(models.signals.post_save, sender=Node)
def create_node_groups(sender, instance, created, *args, **kwargs):
    if created:
        node_viewer = Group.objects.create(name=f"{instance.name} Node Viewer")
        assign_node_viewer(node_viewer, instance)

        node_admin = Group.objects.create(name=f"{instance.name} Node Admin")
        assign_node_admin(node_admin, instance)

        node_manager = Group.objects.create(name=f"{instance.name} Node Manager")
        assign_node_manager(node_manager, node_viewer, node_admin)

        for group, role in [
            (node_viewer, GroupProfile.Role.NODE_VIEWER),
            (node_admin, GroupProfile.Role.NODE_ADMIN),
            (node_manager, GroupProfile.Role.NODE_MANAGER),
        ]:
            GroupProfile.objects.create(
                group=group,
                role=role,
                role_entity=instance,
            )
