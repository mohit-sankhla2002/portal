from enum import Enum

from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models
from simple_history.models import HistoricalRecords

User = get_user_model()

fingerprint_validator = validators.RegexValidator(
    regex=r"^[a-fA-F0-9]{40}$",
    message="Invalid PGP fingerprint: "
    "fingerprints must be exactly 40 hexadecimal chars long.",
)


class PgpKeyInfo(models.Model):
    """Django data model for metadata of PGP/GPG keys that belong to to users.
    The model does not store the key itself, but only the metadata associated
    to it (user ID, user email, fingerprint, key approval status).

    Notes:
     * Not all users need a PGP key and therefore not all users in the User
       model will have a matching entry in this model.
     * While users can only have 1 active (non-revoked) key at any time, they
       can have one or more revoked keys associated with them. These revoked
       keys are kept for auditing purposes, or to show them to the user if
       needed. Therefore the User <-> PgpKeyInfo relationship is one-to-many.
    """

    class Status(models.TextChoices):
        """Trust model for PGP keys. APPROVED keys are keys that are
        approved/validated by the central key validation authority.
        A key that was revoked by the user (KEY_REVOKED) is also considered as
        no longer being approved/trusted by the key validation authority. In
        that sense, the KEY_REVOKED status encompasses the APPROVAL_REVOKED
        status.
        """

        APPROVED = "APPROVED"  # Key approved by key authority (analogous to signature).
        APPROVAL_REVOKED = "APPROVAL_REVOKED"  # Approval was revoked by key authority.
        DELETED = "DELETED"  # Key was deleted from keyserver (user ID is removed).
        KEY_REVOKED = "KEY_REVOKED"  # Key was revoked by user.
        PENDING = "PENDING"  # Key info was added to database but key not yet approved.
        REJECTED = "REJECTED"  # Key was never approved and is not trusted.

    # NOTE: By default, all fields are set to "null=False" and "blank=False",
    # meaning that they cannot be left empty.
    # NOTE: using models.CASCADE removes the PGP key from the database when
    # the user to which it is linked is removed.
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="pgp_keys",
    )

    # PGP key fingerprint.
    # Fingerprints are strings of 40 hexadecimal characters. Each fingerprint
    # is unique and can only appear once in the data model/table.
    fingerprint = models.CharField(
        max_length=40, validators=(fingerprint_validator,), unique=True
    )

    # PGP key user ID and email.
    # Example: "Alice Smith (Home Institution) <alice.smith@example.com>"
    key_user_id = models.CharField(max_length=256)
    key_email = models.EmailField()

    # Key validation authority approval.
    status = models.CharField(
        max_length=16,
        choices=Status.choices,
        default=Status.PENDING,
    )

    # Special field that stores the history of changes that were done to an
    # object. Note: this creates an additional table named HistoricalPgpKeyInfo
    # to the database.
    history = HistoricalRecords()

    # String representation of a PGP key object.
    def __str__(self):
        return str(f"{self.key_user_id} <{self.key_email}>")

    @property
    def key_id(self) -> str:
        """The Key ID of a PGP key is its last 16 characters."""
        return self.fingerprint[-16:]


class KeyAlgorithm(Enum):
    """Encryption algorithm used in a public PGP key.
    The numeric values associated to algorithms follow the GnuPG settings.
    """

    UNKNOWN = 0
    RSA = 1
    DSA = 17
    ECC = 22
