import logging
from datetime import date, timedelta
from unittest import mock

import pytest
from django.contrib.auth import get_user_model

from projects import tasks
from projects.models.data_transfer import DataTransfer
from projects.models.pgp import PgpKeyInfo
from projects.models.project import Project
from projects.models.project import ProjectRole, ProjectUserRole
from projects.tasks import (
    review_user_roles_recipients,
    send_review_user_roles_notification,
    send_notification_email_expiring_projects,
    archive_expired_project,
    update_pgp_keys,
)
from projects.utils.pgp import KeyDownloadError, KeyMetadata, KeyStatus
from .factories import (
    DataTransferFactory,
    ProjectFactory,
    NodeFactory,
)

User = get_user_model()
PROJECT_NAME = "Doghouse"


def test_review_user_roles_recipients(
    project_factory, user_factory, project_user_role_factory
):
    project = project_factory(name=PROJECT_NAME)
    project_user_role_factory.create(
        project=project,
        user=user_factory.create(email="walter.white@doghouse.com"),
        role=ProjectRole.PL.value,
    )
    project_user_role_factory.create(
        project=project,
        user=user_factory.create(email="jesse.pinkman@doghouse.com"),
        role=ProjectRole.PM.value,
    )
    project_user_role_factory.create_batch(5, role=ProjectRole.USER.value)
    project_factory.create()

    assert set(
        review_user_roles_recipients(ProjectUserRole.objects.filter(project=project))
    ) == {
        "walter.white@doghouse.com",
        "jesse.pinkman@doghouse.com",
    }


def test_review_user_roles_notification(project_factory):
    with mock.patch.object(tasks, "sendmail") as mock_sendmail:
        send_review_user_roles_notification()
        mock_sendmail.assert_not_called()
        active_projects = project_factory.create_batch(3, archived=False)
        project_factory.create(archived=True)
        send_review_user_roles_notification()
        assert mock_sendmail.call_count == len(active_projects)


@pytest.mark.parametrize(
    "expiration_date1, count_email_sent",
    [
        (date.today() + timedelta(days=90), 1),
        (date.today() + timedelta(days=50), 0),
        (date.today() + timedelta(days=100), 0),
        (date.today() - timedelta(days=10), 0),
    ],
)
def test_send_notification_email_expiring_projects(
    project_user_role_factory, expiration_date1, count_email_sent
):
    with mock.patch.object(tasks, "sendmail") as mock_sendmail:
        destination1 = NodeFactory()
        project1 = ProjectFactory(
            destination=destination1, expiration_date=expiration_date1
        )
        # create one user and add it to the project and make it a PL
        project_user_role_factory(project=project1, role=ProjectRole.PL.value)
        data_transfer = DataTransferFactory(project=project1)
        assert data_transfer.status == DataTransfer.INITIAL
        send_notification_email_expiring_projects()
        assert mock_sendmail.call_count == count_email_sent


def test_archive_expired_project(node_factory, project_factory, data_transfer_factory):
    destination = node_factory()
    project = project_factory(
        destination=destination, expiration_date=date.today() - timedelta(days=1)
    )
    data_transfer = data_transfer_factory(project=project)
    assert data_transfer.status == DataTransfer.INITIAL
    assert project.archived is False
    archive_expired_project()
    assert DataTransfer.objects.get(pk=data_transfer.pk).status == DataTransfer.EXPIRED
    assert Project.objects.get(pk=project.pk).archived is True


@pytest.mark.parametrize(
    "keyserver_result",
    (
        (KeyStatus.VERIFIED, KeyStatus.VERIFIED),
        (
            KeyStatus.VERIFIED,
            KeyDownloadError("https://keys.example.org", "Something went wrong"),
        ),
        (KeyDownloadError("https://keys.example.org", "Something went wrong"),),
        (KeyStatus.VERIFIED, KeyStatus.NONVERIFIED, KeyStatus.VERIFIED),
        (KeyStatus.NONVERIFIED, KeyStatus.VERIFIED),
        (KeyStatus.NONVERIFIED, KeyStatus.NONVERIFIED),
        (KeyStatus.REVOKED, KeyStatus.NONVERIFIED),
        (KeyStatus.REVOKED, KeyStatus.REVOKED),
    ),
)
def test_update_pgp_keys(keyserver_result, pgp_key_info_factory, caplog):
    keys = pgp_key_info_factory.create_batch(
        len(keyserver_result), status=PgpKeyInfo.Status.APPROVED
    )
    caplog.set_level(logging.DEBUG, logger="projects.tasks")
    with mock.patch(
        "projects.tasks.download_key_metadata",
        mock.Mock(
            side_effect=[
                KeyMetadata(status=s) if isinstance(s, KeyStatus) else s
                for s in keyserver_result
            ]
        ),
    ), mock.patch.object(tasks, "sendmail") as mock_sendmail:
        update_pgp_keys(delay=0)

    # Check for the info log and verify that keys have been updated in the DB
    if set(keyserver_result) & {KeyStatus.NONVERIFIED, KeyStatus.REVOKED}:
        deleted = [
            k
            for k, r in zip(keys, keyserver_result)
            if isinstance(r, KeyStatus) and r is KeyStatus.NONVERIFIED
        ]
        revoked = [
            k
            for k, r in zip(keys, keyserver_result)
            if isinstance(r, KeyStatus) and r is KeyStatus.REVOKED
        ]
        assert caplog.records[0].levelname == "INFO"
        msg = caplog.records[0].message
        assert msg.startswith(f"Changed status of {len(deleted + revoked)} PGP keys: ")
        assert set(msg.split(":")[1].strip().split(", ")) == {
            k.fingerprint for k in deleted
        } | {k.fingerprint for k in revoked}
        # Verify that deleted keys have DELETED status in the DB
        for k in deleted:
            k.refresh_from_db()
            assert k.status == PgpKeyInfo.Status.DELETED
        # Verify that revoked keys have KEY_REVOKED status in the DB
        for k in revoked:
            k.refresh_from_db()
            assert k.status == PgpKeyInfo.Status.KEY_REVOKED
        # Verify that other keys have their original status in the DB
        for k in keys:
            if k not in deleted and k not in revoked:
                k.refresh_from_db()
                assert k.status == PgpKeyInfo.Status.APPROVED
        if concatenated := [*revoked, *deleted]:
            mail_count = len(concatenated) + 1
            assert mock_sendmail.call_count == mail_count, f"Sent {mail_count} emails"
        else:
            mock_sendmail.assert_not_called()

    # Check for the error log
    errors = [
        f"{k.fingerprint} {r}"
        for k, r in zip(keys, keyserver_result)
        if isinstance(r, KeyDownloadError)
    ]
    if errors:
        err_log = next(rec for rec in caplog.records if rec.levelname == "ERROR")
        assert err_log.message.endswith("; ".join(errors))

    # Check for the debug log
    verified_count = keyserver_result.count(KeyStatus.VERIFIED)
    if verified_count:
        assert caplog.records[-1].levelname == "DEBUG"
        assert (
            caplog.records[-1].message
            == f"No changes detected in {verified_count} keys"
        )
