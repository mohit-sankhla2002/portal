from datetime import timezone
from typing import Tuple, Callable

import factory
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django_drf_utils.tests.factories import DjangoModelFactoryNoSignals
from guardian.shortcuts import assign_perm

from identities.models import GroupProfile, INTERNAL_LEGAL_APPROVAL_GROUP_NAME
from identities.permissions import perm_group_manager
from projects.models.approval import DataProviderApproval, NodeApproval, GroupApproval
from projects.models.const import (
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_NODE_ADMIN,
    APP_PERM_NODE_VIEWER,
    APP_PERM_GROUP_APPROVAL_STATUS,
)
from projects.models.data_provider import (
    DataProvider,
    assign_dp_coordinator_role,
    assign_dp_viewer_role,
)
from projects.models.data_transfer import DataPackage, DataPackageTrace, DataTransfer
from projects.models.feed import Feed
from projects.models.flag import Flag
from projects.models.message import Message
from projects.models.node import Node
from projects.models.pgp import PgpKeyInfo
from projects.models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
)
from projects.models.task import PeriodicTaskRun
from projects.models.tile import QuickAccessTile
from projects.models.user import Profile, UserNamespace, CustomAffiliation

User = get_user_model()
USER_PASSWORD = "pass"  # nosec


class FeedFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Feed

    label = factory.Faker("random_element", elements=Feed.FeedLabel)
    title = factory.Faker("sentence")
    message = factory.Faker("paragraph")


class FlagFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Flag
        django_get_or_create = ("code",)

    code = "flag"
    description = "This is a flag description"


class QuickAccessTileFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = QuickAccessTile

    title = "GitLab"
    url = "https://gitlab.com/"
    image = factory.django.ImageField()
    flag = factory.SubFactory(FlagFactory)


class ProfileFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Profile

    affiliation = factory.Faker("company")
    emails = factory.Faker("email")

    @factory.post_generation
    def post_emails(
        self, create, extracted, **kwargs
    ):  # pylint: disable=unused-argument
        if email := self.user.email:
            self.emails = email


class UserFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: f"user_{n}")
    password = factory.PostGenerationMethodCall("set_password", USER_PASSWORD)
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    profile = factory.RelatedFactory(ProfileFactory, "user")

    class Params:
        basic = factory.Trait(
            username="user-basic",
            email="foo@bar.org",
        )
        staff = factory.Trait(
            username="user-staff",
            is_staff=True,
        )

    @factory.post_generation
    def post_flags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for flag in extracted:
                self.flags.add(flag)


class UserNamespaceFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = UserNamespace

    name = factory.Faker("word")


class GroupFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Group
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: f"Group {n}")


class GroupProfileFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = GroupProfile

    group = factory.SubFactory(GroupFactory)


class NodeFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Node

    code = factory.Sequence(lambda n: f"node_{n}")
    name = factory.Sequence(lambda n: f"HPC center {n}")
    node_status = Node.STATUS_ONLINE
    ticketing_system_email = factory.Faker("email")


class CustomAffiliationFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = CustomAffiliation

    code = factory.Sequence(lambda n: f"custom_affiliation_{n}")
    name = factory.Sequence(lambda n: f"Custom Affiliation {n}")


class ProjectFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Project

    gid = factory.Sequence(lambda n: n)
    code = factory.Sequence(lambda n: f"project_{n}")
    name = factory.Sequence(lambda n: f"Project {n}")
    destination = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"project_node_{n}"),
        name=factory.Sequence(lambda n: f"Project Node {n}"),
    )


class ProjectUserRoleFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = ProjectUserRole

    project = factory.SubFactory(ProjectFactory)
    user = factory.SubFactory(UserFactory)
    role = ProjectRole.USER.value


class ProjectUserRoleHistoryFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = ProjectUserRoleHistory

    changed_by = factory.SubFactory(UserFactory)
    user = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ProjectFactory)
    project_str = factory.LazyAttribute(lambda o: o.project.code)
    role = ProjectRole.USER.value
    enabled = True


class DataProviderFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = DataProvider

    name = factory.Faker("sentence")
    code = factory.Faker("word")
    node = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"data_provider_node_{n}"),
        name=factory.Sequence(lambda n: f"Data Provider Node {n}"),
    )


class DataTransferFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = DataTransfer

    project = factory.SubFactory(ProjectFactory)
    data_provider = factory.SubFactory(DataProviderFactory)
    requestor = factory.SubFactory(UserFactory)
    purpose = DataTransfer.TEST


class DataPackageFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = DataPackage

    metadata = "{}"
    data_transfer = factory.SubFactory(DataTransferFactory)


class DataPackageTraceFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = DataPackageTrace

    data_package = factory.SubFactory(DataPackageFactory)
    node = factory.SubFactory(NodeFactory)


class PeriodicTaskRunFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = PeriodicTaskRun

    created_at = factory.Faker("date_time", tzinfo=timezone.utc)
    task = factory.Faker("word")


class MessageFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = Message

    title = factory.Faker("sentence")
    body = factory.Faker("paragraph", nb_sentences=5)
    user = factory.SubFactory(UserFactory)


class PgpKeyInfoFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = PgpKeyInfo

    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def fingerprint(x):  # pylint: disable=no-self-argument
        return f"{'A' * (40 - len(str(x)))}{x}"

    key_user_id = factory.Faker("name")
    key_email = factory.Faker("email")
    status = PgpKeyInfo.Status.PENDING


class NodeApprovalFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = NodeApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    node = factory.SubFactory(NodeFactory)
    type = NodeApproval.Type.HOSTING


class DataProviderApprovalFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = DataProviderApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    data_provider = factory.SelfAttribute("data_transfer.data_provider")


class GroupApprovalFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = GroupApproval

    data_transfer = factory.SubFactory(
        DataTransferFactory, purpose=DataTransfer.PRODUCTION
    )
    group = factory.SubFactory(GroupFactory)

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            self.group.user_set.add(*extracted)


def make_node_admin(
    user: User | None = None,
    node: Node | None = None,
    group: Group | None = None,
) -> User:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    if group is None:
        group = GroupFactory(name=f"Node Admin {node.code}")
    try:
        group.profile
    except Group.profile.RelatedObjectDoesNotExist:  # pylint: disable=no-member
        GroupProfileFactory(
            group=group,
            role_entity=node,
            role=GroupProfile.Role.NODE_ADMIN,
        )
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_ADMIN, group, node)
    return user


def make_node_viewer(
    user: User | None = None,
    node: Node | None = None,
    group: Group | None = None,
) -> User:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    if group is None:
        group = GroupFactory(name=f"Node Viewer {node.code}")
    try:
        group.profile
    except Group.profile.RelatedObjectDoesNotExist:  # pylint: disable=no-member
        GroupProfileFactory(
            group=group,
            role_entity=node,
            role=GroupProfile.Role.NODE_VIEWER,
        )
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_VIEWER, group, node)
    return user


def make_group_manager(
    managed_group: Group, user: User | None = None, manager_group: Group | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if manager_group is None:
        manager_group = Group.objects.create(name=f"Group Manager {managed_group.name}")
    manager_group.user_set.add(user)
    assign_perm(perm_group_manager, manager_group, managed_group)
    return user


def _make_data_provider_user(
    group_profile_role: GroupProfile.Role,
    assign_fn: Callable[[Group, DataProvider], None],
    group_name_prepend: str,
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> User:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    if group is None:
        group = GroupFactory(name=f"{group_name_prepend} {data_provider.code}")
    try:
        group.profile
    except Group.profile.RelatedObjectDoesNotExist:  # pylint: disable=no-member
        GroupProfileFactory(
            group=group,
            role_entity=data_provider,
            role=group_profile_role,
        )
    group.user_set.add(user)
    assign_fn(group, data_provider)
    return user


def make_data_provider_admin(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> User:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    if group is None:
        group = GroupFactory(name=f"Data Provider Admin {data_provider.code}")
    try:
        group.profile
    except Group.profile.RelatedObjectDoesNotExist:  # pylint: disable=no-member
        GroupProfileFactory(
            group=group,
            role_entity=data_provider,
            role=GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN,
        )
    group.user_set.add(user)
    assign_perm(APP_PERM_DATA_PROVIDER_ADMIN, group, data_provider)
    return user


def make_data_provider_coordinator(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> User:
    return _make_data_provider_user(
        GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
        assign_dp_coordinator_role,
        "Data Provider Coordinator",
        user,
        data_provider,
        group,
    )


def make_data_provider_viewer(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> User:
    return _make_data_provider_user(
        GroupProfile.Role.DATA_PROVIDER_VIEWER,
        assign_dp_viewer_role,
        "Data Provider Viewer",
        user,
        data_provider,
        group,
    )


def make_data_provider_data_engineer(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> User:
    return _make_data_provider_user(
        GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER,
        assign_dp_viewer_role,
        "Data Provider Data Engineer",
        user,
        data_provider,
        group,
    )


def make_data_provider_users(dp: DataProvider) -> Tuple[User, User, User, User]:
    viewer, technical_admin, coordinator, data_engineer = UserFactory.create_batch(4)
    make_data_provider_viewer(viewer, dp)
    make_data_provider_coordinator(coordinator, dp)
    make_data_provider_admin(technical_admin, dp)
    make_data_provider_data_engineer(data_engineer, dp)
    return viewer, coordinator, technical_admin, data_engineer


def make_project_user(
    project: Project | None = None, role=ProjectRole, user: User | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if project is None:
        project = ProjectFactory()
    ProjectUserRoleFactory(project=project, user=user, role=role.value)
    return user


def make_legal_approver(user: User | None = None, group: Group | None = None) -> User:
    if user is None:
        user = UserFactory()
    if group is None:
        group = make_legal_approval_group()
    group.user_set.add(user)
    # We need to refetch the user from the database as the permission caching
    # prevents us to get the updated version. See following link for more details:
    # https://docs.djangoproject.com/en/4.2/topics/auth/default/#permission-caching
    return User.objects.get(pk=user.pk)


def make_legal_approval_group(
    name: str = INTERNAL_LEGAL_APPROVAL_GROUP_NAME,
    entity=None,
    group: Group | None = None,
) -> Group:
    if group is None:
        group = GroupFactory(name=name)
    GroupProfileFactory(group=group, role=GroupProfile.Role.ELSI, role_entity=entity)
    assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, group)
    return group
