from projects.models.project import ProjectRole
from projects.signals.project import send_user_emails


def test_send_user_emails_with_empty_lists(project_factory, mailoutbox):
    send_user_emails(project_factory(), [], [])
    assert len(mailoutbox) == 0


def test_send_user_emails(project_factory, user_factory, mailoutbox):
    added, removed = user_factory.create_batch(2)
    send_user_emails(
        project_factory(),
        [(added, ProjectRole.PL)],
        [(removed, ProjectRole.PL)],
    )
    assert len(mailoutbox) == 1
