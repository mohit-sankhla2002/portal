import pytest
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from .. import APPLICATION_JSON


def test_list_anonymous(client):
    url = reverse(f"{APP_NAME}:flag-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_list_authenticated(client):
    url = reverse(f"{APP_NAME}:flag-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.usefixtures("basic_user_auth")
def test_flag_as_non_staff(client, user_factory, flag_factory):
    new_user = user_factory()
    flag = flag_factory()
    assert flag.code == "flag"
    assert flag.description.startswith("This is a flag description")
    url = reverse(f"{APP_NAME}:flag-detail", kwargs={"pk": flag.id})
    r = client.patch(
        url,
        {"code": "new", "description": "lorem ipsum", "users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "You do not have permission to perform this action."}


@pytest.mark.usefixtures("staff_user_auth")
def test_flag_as_staff(client, flag_factory, user_factory):
    new_user = user_factory()
    flag = flag_factory()
    # Edit a flag
    url = reverse(f"{APP_NAME}:flag-detail", kwargs={"pk": flag.id})
    r = client.patch(
        url,
        {"users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    # Create a flag
    url = reverse(f"{APP_NAME}:flag-list")
    r = client.post(
        url,
        {"code": "new", "description": "lorem ipsum", "users": [new_user.id]},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_201_CREATED
