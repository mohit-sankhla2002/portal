import pytest
from django.urls import reverse
from rest_framework import status

from identities.models import User
from projects.apps import APP_NAME
from projects.models.approval import BaseApproval, NodeApproval
from projects.models.data_transfer import DataTransfer
from projects.views.approval import INSTITUTION_URL_PARAM
from .. import APPLICATION_JSON
from ..factories import make_data_provider_coordinator, USER_PASSWORD
from ...serializers import DataTransferSerializer


@pytest.mark.usefixtures("staff_user_auth")
def test_wrong_institution(client):
    url = approval_url(0)
    r = client.patch(
        url,
        {
            "status": BaseApproval.ApprovalStatus.APPROVED,
            INSTITUTION_URL_PARAM: "does_not_exist",
        },
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    assert "MUST be one of" in r.json()["detail"]


@pytest.mark.parametrize(
    "project_space_ready,http_status,response_json_subset,node_approval_type",
    (
        (
            False,
            status.HTTP_400_BAD_REQUEST,
            {
                "project_space_ready": "NOT all the declarations are fulfilled for approval"
            },
            NodeApproval.Type.HOSTING,
        ),
        (
            True,
            status.HTTP_200_OK,
            {
                "id": 1,
                "packages": [],
                "data_provider_approvals": [],
                "requestor_display_id": None,
                "project_archived": False,
                "project": 1,
                "max_packages": 1,
                "status": DataTransfer.AUTHORIZED,
                "requestor": 3,
                "purpose": DataTransfer.TEST,
                "legal_basis": "",
            },
            NodeApproval.Type.HOSTING,
        ),
        (
            False,
            status.HTTP_200_OK,
            {
                "id": 1,
                "packages": [],
                "data_provider_approvals": [],
                "requestor_display_id": None,
                "project_archived": False,
                "project": 1,
                "max_packages": 1,
                "status": DataTransfer.AUTHORIZED,
                "requestor": 3,
                "purpose": DataTransfer.TEST,
                "legal_basis": "",
            },
            NodeApproval.Type.TRANSFER,
        ),
    ),
)
@pytest.mark.usefixtures("staff_user_auth")
def test_node_approval(
    client,
    node_factory,
    data_provider_factory,
    data_transfer_factory,
    node_approval_factory,
    project_factory,
    project_space_ready,
    http_status,
    response_json_subset,
    node_approval_type,
):
    hosting, transfer = node_factory.create_batch(2)
    data_provider = data_provider_factory(node=hosting)
    destination = (
        hosting if node_approval_type == NodeApproval.Type.HOSTING else transfer
    )
    project = project_factory(destination=destination)
    approval_node = (
        hosting if node_approval_type == NodeApproval.Type.HOSTING else transfer
    )
    node_approval = node_approval_factory(
        node=approval_node,
        type=node_approval_type,
        data_transfer=data_transfer_factory(
            data_provider=data_provider, project=project
        ),
    )
    url = approval_url(node_approval.id)
    r = client.patch(
        url,
        {
            "status": BaseApproval.ApprovalStatus.APPROVED,
            INSTITUTION_URL_PARAM: "node",
            "existing_legal_basis": True,
            "technical_measures_in_place": True,
            "project_space_ready": project_space_ready,
        },
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == http_status
    r_json = r.json()
    assert response_json_subset.items() <= r_json.items()
    # test rejection reason is empty for approval
    node_approval = node_approval_factory()
    assert len(node_approval.rejection_reason) == 0


@pytest.mark.parametrize(
    "approval_status,http_status,response_json_subset",
    (
        (
            BaseApproval.ApprovalStatus.APPROVED,
            status.HTTP_200_OK,
            {
                "id": 1,
                "packages": [],
                "node_approvals": [],
                "status": DataTransfer.AUTHORIZED,
                "purpose": DataTransfer.TEST,
            },
        ),
        (
            "DOES_NOT_EXIST",
            status.HTTP_400_BAD_REQUEST,
            {"status": ['"DOES_NOT_EXIST" is not a valid choice.']},
        ),
    ),
)
@pytest.mark.usefixtures("staff_user_auth")
def test_data_provider_approval(
    client,
    data_provider_approval_factory,
    approval_status,
    http_status,
    response_json_subset,
):
    url = approval_url(data_provider_approval_factory().id)
    body = {
        "status": approval_status,
        INSTITUTION_URL_PARAM: "data_provider",
        "existing_legal_basis": True,
        "technical_measures_in_place": True,
    }
    r = client.patch(
        url,
        body,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == http_status
    r_json = r.json()
    assert response_json_subset.items() <= r_json.items()
    # test rejection reason is empty for approval
    data_provider_approval = data_provider_approval_factory()
    assert len(data_provider_approval.rejection_reason) == 0


@pytest.mark.parametrize(
    "http_status,checkboxes",
    (
        (
            status.HTTP_200_OK,
            {
                "existing_legal_basis": True,
                "technical_measures_in_place": True,
            },
        ),
        (
            status.HTTP_400_BAD_REQUEST,
            {
                "existing_legal_basis": True,
                "technical_measures_in_place": False,
            },
        ),
        (
            status.HTTP_400_BAD_REQUEST,
            {
                "existing_legal_basis": True,
            },
        ),
    ),
)
@pytest.mark.usefixtures("staff_user_auth")
def test_data_provider_approval_with_checkboxes(
    client,
    data_provider_approval_factory,
    http_status,
    checkboxes,
):
    url = approval_url(data_provider_approval_factory().id)
    body = {
        "status": BaseApproval.ApprovalStatus.APPROVED,
        INSTITUTION_URL_PARAM: "data_provider",
    } | checkboxes
    r = client.patch(
        url,
        body,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == http_status


@pytest.mark.usefixtures("staff_user_auth")
def test_node_approval_rejection(
    client,
    node_approval_factory,
    data_provider_approval_factory,
    node_factory,
    data_provider_factory,
    project_factory,
    data_transfer_factory,
):
    node = node_factory()
    node_approval = node_approval_factory(
        node=node,
        data_transfer=data_transfer_factory(
            data_provider=data_provider_factory(node=node),
            project=project_factory(destination=node),
        ),
    )
    url = approval_url(node_approval.id)
    dtr = node_approval.data_transfer
    data_provider_approval = data_provider_approval_factory(data_transfer=dtr)
    assert len(dtr.approvals) == 2
    r = client.patch(
        url,
        {
            "status": BaseApproval.ApprovalStatus.REJECTED,
            INSTITUTION_URL_PARAM: "node",
            "rejection_reason": "a reason",
        },
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    response_json = r.json()
    # We return a full DTR
    assert DataTransferSerializer.Meta.read_only_fields <= tuple(response_json.keys())
    # Node approval has been rejected
    assert (
        response_json["node_approvals"][0]["status"]
        == BaseApproval.ApprovalStatus.REJECTED
    )
    node_approval.refresh_from_db()
    assert node_approval.status == BaseApproval.ApprovalStatus.REJECTED
    # rejection reason is not empty
    assert len(node_approval.rejection_reason) > 0
    # The whole DTR is now `UNAUTHORIZED`
    assert response_json["status"] == DataTransfer.UNAUTHORIZED
    # `WAITING` approvals are set to `BLOCKED`
    data_provider_approval.refresh_from_db()
    assert data_provider_approval.status == BaseApproval.ApprovalStatus.BLOCKED


@pytest.mark.usefixtures("basic_user_auth")
def test_data_provider_approval_with_basic_user(client, data_provider_approval_factory):
    url = approval_url(data_provider_approval_factory().id)
    body = {
        "status": BaseApproval.ApprovalStatus.APPROVED,
        INSTITUTION_URL_PARAM: "data_provider",
        "existing_legal_basis": True,
        "technical_measures_in_place": True,
    }
    r = client.patch(
        url,
        body,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    assert r.json() == {
        "status": "Only staff or an user with approval permissions can update the status"
    }


@pytest.mark.parametrize(
    "http_status,existing_legal_basis",
    (
        (status.HTTP_200_OK, True),
        (status.HTTP_400_BAD_REQUEST, False),
    ),
)
def test_group_approval_with_group_user(
    basic_user_auth: User,
    client,
    group_approval_factory,
    http_status,
    existing_legal_basis,
):
    group_approval = group_approval_factory(users=(basic_user_auth,))
    url = approval_url(group_approval.id)
    client.login(username=basic_user_auth.username, password=USER_PASSWORD)
    body = {
        "status": BaseApproval.ApprovalStatus.APPROVED,
        "existing_legal_basis": existing_legal_basis,
        INSTITUTION_URL_PARAM: "group",
    }
    r = client.patch(
        url,
        body,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == http_status
    client.logout()


def test_group_approval_with_non_group_user(
    user_factory,
    client,
    group_approval_factory,
):
    for user, http_status in zip(
        (user_factory(basic=True), user_factory(staff=True)),
        (status.HTTP_400_BAD_REQUEST, status.HTTP_200_OK),
    ):
        # User is NOT added to the group
        url = approval_url(group_approval_factory().id)
        client.login(username=user.username, password=USER_PASSWORD)
        body = {
            "status": BaseApproval.ApprovalStatus.APPROVED,
            "existing_legal_basis": True,
            INSTITUTION_URL_PARAM: "group",
        }
        r = client.patch(
            url,
            body,
            content_type=APPLICATION_JSON,
        )
        assert r.status_code == http_status
        client.logout()


def test_data_provider_approval_with_dp_coordinator(
    client, data_provider_approval_factory, user_factory
):
    data_provider_approval = data_provider_approval_factory()
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, data_provider_approval.data_provider)
    url = approval_url(data_provider_approval.id)
    body = {
        "status": BaseApproval.ApprovalStatus.APPROVED,
        INSTITUTION_URL_PARAM: "data_provider",
        "existing_legal_basis": True,
        "technical_measures_in_place": True,
    }
    client.login(username=coordinator.username, password=USER_PASSWORD)
    r = client.patch(
        url,
        body,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    client.logout()


@pytest.mark.usefixtures("staff_user_auth")
def test_block_approvals(
    node_approval_factory,
    data_provider_approval_factory,
    group_approval_factory,
    client,
):
    node_approval = node_approval_factory()
    dtr = node_approval.data_transfer
    data_provider_approval_factory(data_transfer=dtr)
    group_approval_factory(data_transfer=dtr)
    assert len(dtr.approvals) == 3
    assert {approval.status for approval in dtr.approvals} == {
        BaseApproval.ApprovalStatus.WAITING
    }
    url = reverse(f"{APP_NAME}:datatransfer-detail", kwargs={"pk": dtr.id})
    r = client.patch(
        url,
        {"status": DataTransfer.UNAUTHORIZED},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    # `WAITING` approvals are set to `BLOCKED`
    dtr.refresh_from_db()
    assert {approval.status for approval in dtr.approvals} == {
        BaseApproval.ApprovalStatus.BLOCKED
    }


def approval_url(approval_id: int):
    return reverse(
        f"{APP_NAME}:approval-detail",
        kwargs={"pk": approval_id},
    )
