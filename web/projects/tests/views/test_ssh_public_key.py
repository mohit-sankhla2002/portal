import pytest
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from projects.apps import APP_NAME
from projects.models.project import ProjectRole
from projects.models.ssh_public_key import validate_ssh_key

from .. import APPLICATION_JSON
from ..factories import USER_PASSWORD, ProjectFactory, UserFactory, make_project_user


@pytest.mark.parametrize(
    "test_input,expected_exception,expected_errmsg",
    [
        (
            "xxxxxx AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXzAHCIgPNaJr6cIQU1JYxDjFgp anna.goeldi@email.ch",
            ValueError,
            r"unrecognized option name",
        ),
        (
            "ssh-garbage xxxxxx anna.goeldi@email.ch",
            ValueError,
            r"Unable to decode",
        ),
        (
            "ssh-rsa AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXz anna.goeldi@email.ch",
            ValueError,
            r"Keytype mismatch",
        ),
        (
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXz anna.goeldi@email.ch",
            ValueError,
            r"only \d+ bytes available",
        ),
    ],
)
def test_check_ssh_validation(test_input, expected_exception, expected_errmsg):
    with pytest.raises(expected_exception=expected_exception, match=expected_errmsg):
        validate_ssh_key(test_input)


class TestUserView(TestCase):
    def setUp(self):
        self.project1, self.project2 = ProjectFactory.create_batch(2)
        self.basic_user = make_project_user(self.project1, ProjectRole.USER)
        self.pm_user = make_project_user(self.project1, ProjectRole.PM)
        self.dm_user = make_project_user(self.project1, ProjectRole.DM)
        self.staff = UserFactory(staff=True)
        self.url = reverse(f"{APP_NAME}:ssh_public_keys-list")

    def test_create_ssh_public_key(self):
        data = {
            "project": self.project1.id,
            "user": self.basic_user.id,
            "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXzAHCIgPNaJr6cIQU1JYxDjFgp meret.oppenheim@email.ch",
        }
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.post(self.url, data, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        r = self.client.get(self.url, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertEqual(r.json()[0]["key"], data["key"])

        # a second post should simply update the existing public ssh key
        # for that user and project, not create a new one
        data2 = {
            "project": self.project1.id,
            "user": self.basic_user.id,
            "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDR+PU1hRBIPfCfz761he5GrVP9b4eJb2262zGRG9LyU meret.oppenheim@email.ch",
        }
        r = self.client.post(self.url, data2, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        r = self.client.get(self.url, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertEqual(r.json()[0]["key"], data2["key"])

    def test_permissions(self):
        data = {
            "project": self.project1.id,
            "user": self.basic_user.id,
            "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXzAHCIgPNaJr6cIQU1JYxDjFgp meret.oppenheim@email.ch",
        }
        self.client.login(username=self.pm_user.username, password=USER_PASSWORD)
        r = self.client.post(self.url, data, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.post(self.url, data, content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        obj_id = r.json()["id"]

        # Change own SSH key
        data_up = {
            "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJeDgCiEoG1CvsxGAXXzAHCIgPNaJr6cIQU1JYxDjFgp iris.von.rothen@email.ch",
        }
        r = self.client.patch(
            self.url + f"{obj_id}/", data_up, content_type=APPLICATION_JSON
        )
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        # SSH public keys are not public to everyone
        self.client.login(username=self.pm_user.username, password=USER_PASSWORD)
        r = self.client.get(self.url + f"{obj_id}/", content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

        # ... therefore patching should not work either. We expect a 404, not a 403,
        # because of the get_queryset method in SSHPUblicKeyViewSet is applied *before*
        # the update.
        r = self.client.patch(
            self.url + f"{obj_id}/", data_up, content_type=APPLICATION_JSON
        )
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

        # Staff should be allowed to read, but not to update or delete
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        r = self.client.get(self.url + f"{obj_id}/", content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        r = self.client.patch(
            self.url + f"{obj_id}/", data_up, content_type=APPLICATION_JSON
        )
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        r = self.client.delete(self.url + f"{obj_id}/", content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        # The owner of the SSH public key is allowed to delete it
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.delete(self.url + f"{obj_id}/", content_type=APPLICATION_JSON)
        self.assertEqual(r.status_code, status.HTTP_204_NO_CONTENT)
