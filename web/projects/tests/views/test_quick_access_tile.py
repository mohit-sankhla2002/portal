import io
import tempfile

import pytest
from PIL import Image
from django.test import override_settings
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.tile import QuickAccessTile
from .. import APPLICATION_JSON
from ..factories import UserFactory, USER_PASSWORD


def get_media_root():
    # Should be preferably used over 'tempfile.mkdtemp()'
    # as it will be automatically deleted by the OS. We do NOT know though.
    with tempfile.TemporaryDirectory(prefix="mediatest") as tmpdir:
        return tmpdir


@pytest.fixture
def basic_user_with_flag_auth(
    db, client, flag_factory
):  # pylint: disable=unused-argument
    user = UserFactory(basic=True, post_flags=(flag_factory(),))
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


def generate_photo_file():
    file = io.BytesIO()
    image = Image.new("RGBA", size=(100, 100), color=(155, 0, 0))
    image.save(file, "png")
    file.name = "test.png"
    file.seek(0)
    return file


def test_list_anonymous(client):
    url = reverse(f"{APP_NAME}:quickaccess-list")
    assert client.get(url).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
@override_settings(MEDIA_ROOT=get_media_root())
def test_list_authenticated(client, quick_access_tile_factory):
    def assert_accessible_but_empty():
        url = reverse(f"{APP_NAME}:quickaccess-list")
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == []

    # Read-only access for authenticated users
    assert_accessible_but_empty()
    # Create a tile
    quick_access_tile = quick_access_tile_factory()
    assert quick_access_tile.flag.code == "flag"
    # Still empty as user does not match the flag
    assert_accessible_but_empty()


@pytest.mark.usefixtures("basic_user_with_flag_auth")
@override_settings(MEDIA_ROOT=get_media_root())
def test_list_authenticated_with_flag(client, quick_access_tile_factory):
    quick_access_tile = quick_access_tile_factory()
    assert quick_access_tile.flag.code == "flag"
    url = reverse(f"{APP_NAME}:quickaccess-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    # Not empty as user's flags contain tile's one
    assert not response.json() == {}


@pytest.mark.usefixtures("basic_user_auth")
@override_settings(MEDIA_ROOT=get_media_root())
def test_quick_access_tile_as_non_staff(client, quick_access_tile_factory):
    quick_access_tile = quick_access_tile_factory()
    url = reverse(f"{APP_NAME}:quickaccess-detail", kwargs={"pk": quick_access_tile.id})
    r = client.patch(
        url,
        {},
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "You do not have permission to perform this action."}


@pytest.mark.usefixtures("staff_user_auth")
@override_settings(MEDIA_ROOT=get_media_root())
def test_quick_access_tile_as_staff(client, quick_access_tile_factory):
    quick_access_tile = quick_access_tile_factory()
    assert quick_access_tile.flag.code == "flag"
    # Edit a quick_access_tile
    url = reverse(f"{APP_NAME}:quickaccess-detail", kwargs={"pk": quick_access_tile.id})
    r = client.patch(
        url,
        {
            "title": "Google",
            "description": "This is Google!!!",
            "url": "http://www.google.com",
        },
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    # Ensure that entity has been correctly updated
    assert QuickAccessTile.objects.filter(title="Google").exists()
    # Create a quick_access_tile
    url = reverse(f"{APP_NAME}:quickaccess-list")
    r = client.post(
        url,
        {
            "title": "GitHub",
            "url": "https://github.com",
            "image": generate_photo_file(),
        },
        format="multipart",
    )
    assert r.status_code == status.HTTP_201_CREATED
    # Make sure that we have now two entries
    assert QuickAccessTile.objects.count() == 2
