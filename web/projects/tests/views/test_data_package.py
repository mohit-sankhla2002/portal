import json

import factory
from django.db import models
from django.urls import reverse
from django.utils.dateparse import parse_datetime
from guardian.shortcuts import assign_perm
from rest_framework import status

from .test_data_transfer import TransferViewTestBase, assert_access_forbidden
from .. import APPLICATION_JSON
from ..factories import (
    DataPackageFactory,
    DataPackageTraceFactory,
    DataProviderFactory,
    DataTransferFactory,
    NodeFactory,
    PgpKeyInfoFactory,
    ProjectFactory,
    UserFactory,
    USER_PASSWORD,
    make_node_admin,
    make_data_provider_users,
    make_project_user,
)
from ...apps import APP_NAME
from ...models.data_transfer import DataPackage, DataTransfer, DataPackageTrace
from ...models.node import Node
from ...models.pgp import PgpKeyInfo
from ...models.project import ProjectRole
from ...serializers.data_package import hash_metadata, DataPackageSerializer


class TestDataPackageView(
    TransferViewTestBase
):  # pylint: disable=too-many-public-methods
    reverse_url = f"{APP_NAME}:datapackage"
    url = reverse(reverse_url + "-list")
    url_check = reverse(reverse_url + "check-list")
    url_log = reverse(reverse_url + "log-list")

    def setUp(self):
        super().setUp()
        self.dm1 = UserFactory(email="dm1@users.org")
        self.dm2 = UserFactory(email="dm2@users.org")
        for dm in (self.dm1, self.dm2):
            make_project_user(self.project, ProjectRole.DM, dm)
        # User with "Can add data package trace" permission
        self.user_data_package_reporter = UserFactory()
        assign_perm(f"{APP_NAME}.add_datapackagetrace", self.user_data_package_reporter)
        self.key1 = PgpKeyInfoFactory(
            user=self.dm1, key_email="dm1@users.org", status=PgpKeyInfo.Status.APPROVED
        )
        self.key2 = PgpKeyInfoFactory(status=PgpKeyInfo.Status.APPROVED)
        _file_name = "package.tar"
        _purpose = DataPackage.PRODUCTION
        _metadata = {
            "transfer_id": self.transfer.id,
            "purpose": _purpose,
            "sender": self.key2.fingerprint,
            "recipients": [
                self.key1.fingerprint,
            ],
            "checksum": "ABCDEF",
        }

        def metadata(created="YYYY-mm-dd", **kwargs):
            return {**_metadata, "timestamp": created, **kwargs}

        self.metadata = metadata

        self.pkg_json = {
            "metadata_hash": hash_metadata(self.metadata()),
            "data_transfer": self.transfer.id,
            "file_name": _file_name,
            "file_size": 123456789,
            "metadata": json.dumps(metadata()),
            "purpose": _purpose,
            "id": 1,
            "trace": [],
            "destination_node": self.project.destination.code,
            "project_code": self.project.code,
            "data_provider": self.data_provider.code,
            "sender_pgp_key_info": f"{self.key2.key_user_id} {self.key2.fingerprint}",
        }

        def pkg_json_read_only(**kwargs):
            return {
                "metadata": json.dumps(metadata(**kwargs)),
                "file_name": _file_name,
            }

        self.pkg_json_read_only = pkg_json_read_only

        def pkg_json_post(**kwargs):
            data = pkg_json_read_only(**kwargs)
            data["node"] = self.node.code
            data["status"] = DataPackageTrace.ENTER
            return data

        self.pkg_json_post = pkg_json_post

    @staticmethod
    def to_json(pkg):
        return DataPackageSerializer(pkg).data

    def _setUpPkg(self, transfer=None, file_name=None, file_size=None, metadata=None):
        if transfer is None:
            transfer = self.transfer
        if file_name is None:
            file_name = self.pkg_json["file_name"]
        if metadata is None:
            metadata = self.metadata(transfer_id=transfer.id)
        return DataPackageFactory(
            metadata_hash=hash_metadata(metadata),
            data_transfer=transfer,
            file_name=file_name,
            file_size=file_size,
            metadata=json.dumps(metadata),
            purpose=self.pkg_json["purpose"],
        )

    def _setUpPkgTrace(self, trace=()):
        pkg = self._setUpPkg()
        node, trace_status = zip(*trace)
        return DataPackageTraceFactory.create_batch(
            len(trace),
            data_package=pkg,
            node=factory.Iterator(node),
            status=factory.Iterator(trace_status),
        )

    def test_view_list(self):
        other_node = NodeFactory(node_status=Node.STATUS_ONLINE)
        other_transfer = DataTransferFactory(
            project=ProjectFactory(destination=other_node),
            data_provider=DataProviderFactory(node=other_node),
            requestor=self.data_manager,
            status=DataTransfer.AUTHORIZED,
            purpose=DataTransfer.PRODUCTION,
        )
        self._setUpPkg(file_size=123456789)
        other_pkg = self._setUpPkg(transfer=other_transfer, file_size=987654321)
        other_pkg_json = self.to_json(other_pkg)
        na_user, dm_user = UserFactory.create_batch(2)
        make_node_admin(na_user, self.dest_node)
        make_project_user(self.project, ProjectRole.DM, dm_user)
        (dp_viewer, dp_coordinator, dp_technical_admin, _) = make_data_provider_users(
            self.data_provider
        )

        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        for username, expected_pkgs in (
            (self.staff.username, [self.pkg_json, other_pkg_json]),
            (self.node_viewer.username, [self.pkg_json]),
            (self.user_data_package_reporter.username, []),
            (self.data_manager.username, [self.pkg_json]),
            (self.basic_user.username, []),
            (na_user.username, [self.pkg_json]),
            (dm_user.username, [self.pkg_json]),
            (dp_viewer.username, [self.pkg_json]),
            (dp_technical_admin.username, [self.pkg_json]),
            (dp_coordinator.username, [self.pkg_json]),
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                sorted(r.json(), key=lambda dct: dct["data_transfer"]),
                expected_pkgs,
            )
            self.client.logout()

    def test_view_sorted_list(self):
        file_a = "a.tar"
        file_z = "z.tar"
        self._setUpPkg(file_name=file_z)
        self._setUpPkg(file_name=file_a, metadata=self.metadata(checksum="GHIJKL"))
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        for ascending, expected in (
            (True, [file_a, file_z]),
            (False, [file_z, file_a]),
        ):
            url = self.get_data_package_url("file_name", ascending)
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual([pkg["file_name"] for pkg in r.json()], expected)
        self.client.logout()

    def get_data_package_url(self, ordering: str, ascending: bool = True):
        return self.url + f"?{'' if ascending else '-'}ordering={ordering}"

    def test_view_list_trace(self):
        [trace_obj] = self._setUpPkgTrace(
            trace=((self.node, DataPackageTrace.PROCESSED),)
        )

        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        pkg_json = {
            **self.pkg_json,
            "trace": [
                {
                    "node": self.node.code,
                    "timestamp": trace_obj.timestamp,
                    "status": DataPackageTrace.PROCESSED,
                }
            ],
        }
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        (j,) = r.json()
        (trace,) = j["trace"]
        timestamp = parse_datetime(trace["timestamp"])
        self.assertDictEqual({**trace, "timestamp": timestamp}, pkg_json["trace"][0])
        self.client.logout()

    def test_create(self):
        self.transfer.max_packages = -1
        self.transfer.save()
        test_cases = (
            (self.basic_user, self.pkg_json_post(), status.HTTP_403_FORBIDDEN),
            (self.staff, self.pkg_json_post(), status.HTTP_201_CREATED),
            (
                self.user_data_package_reporter,
                self.pkg_json_post(checksum="BBBBB"),
                status.HTTP_201_CREATED,
            ),
        )
        for user, data, expected_return_code in test_cases:
            self.client.login(username=user.username, password=USER_PASSWORD)
            self.assertEqual(
                self.client.post(
                    self.url_log,
                    data=data,
                    content_type=APPLICATION_JSON,
                ).status_code,
                expected_return_code,
            )
            self.client.logout()

        for trace in DataPackageTrace.objects.all():
            self.assertEqual(
                (trace.node, trace.status), (self.node, DataPackageTrace.ENTER)
            )

        def trim_data(dpkg: dict) -> dict:
            """Also makes sure, that all keys are present in the dict"""
            return {key: dpkg[key] for key in ("metadata", "file_name")}

        expected_pkg_data = [
            trim_data(data)
            for _user, data, expected_return_code in test_cases
            if expected_return_code == status.HTTP_201_CREATED
        ]

        actual_pkg_data = [trim_data(vars(dpkg)) for dpkg in DataPackage.objects.all()]
        self.assertEqual(actual_pkg_data, expected_pkg_data)

    def test_create_with_file_size(self):
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        for file_size, http_status in (
            (-1, status.HTTP_400_BAD_REQUEST),
            (0, status.HTTP_400_BAD_REQUEST),
            (5 << 40, status.HTTP_201_CREATED),
        ):
            data = self.pkg_json_post() | {"file_size": file_size}
            self.assertEqual(
                self.client.post(
                    self.url_log,
                    data=data,
                    content_type=APPLICATION_JSON,
                ).status_code,
                http_status,
            )
            if file_size > 0:
                data_package = DataPackage.objects.get(file_name=data["file_name"])
                self.assertEqual(data_package.file_name, data["file_name"])
                self.assertEqual(data_package.file_size, file_size)
        self.client.logout()

    def test_check_duplicate_metadata(self):
        self.transfer.max_packages = -1
        self.transfer.save()
        # create data package
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(
                self.url_log,
                data=self.pkg_json_post(),
                content_type=APPLICATION_JSON,
            ).status_code,
            status.HTTP_201_CREATED,
        )
        self.client.logout()

        # make sure 'check' doesn't allow to create the same package again (read_only)
        self.assertContains(
            self.client.post(
                self.url_check,
                data=self.pkg_json_read_only(),
                content_type=APPLICATION_JSON,
            ),
            "Data package already exists",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    def test_create_wrong_status(self):
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(
                self.url_log,
                data={**self.pkg_json_post(), "status": "BULLSHIT"},
                content_type=APPLICATION_JSON,
            ).status_code,
            status.HTTP_400_BAD_REQUEST,
        )
        self.client.logout()

    def test_create_unauthorized(self):
        data = self.pkg_json_post()
        self.assertEqual(
            self.client.post(
                self.url_log, data, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        del data["node"]
        del data["status"]
        response = self.client.post(self.url_check, data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("project_code" in response.data)
        self.assertEqual(DataPackageTrace.objects.all().count(), 0)
        self.assertEqual(DataPackage.objects.all().count(), 0)

    def test_invalid_metadata(self):
        for user in (self.user_data_package_reporter,):
            self.client.login(username=user.username, password=USER_PASSWORD)
            data = self.pkg_json_post()
            data["metadata"] = "{"
            self.assertEqual(
                self.client.post(
                    self.url_log, data, content_type=APPLICATION_JSON
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
            data = self.pkg_json_post()
            data["metadata"] = self.metadata(transfer_id="nonexistent")
            self.assertEqual(
                self.client.post(
                    self.url_log, data, content_type=APPLICATION_JSON
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_max_pkg(self):
        for user in (self.user_data_package_reporter,):
            self.client.login(username=user.username, password=USER_PASSWORD)
            current_id = (
                DataTransfer.objects.aggregate(models.Max("id"))["id__max"] or 0
            )
            for id_, purpose in enumerate(
                (DataTransfer.PRODUCTION, DataTransfer.TEST), current_id + 1
            ):
                transfer = self.create_transfer(id_, purpose)
                with self.subTest(purpose=purpose):
                    forbidden_purpose = (
                        DataTransfer.TEST
                        if purpose == DataTransfer.PRODUCTION
                        else DataTransfer.PRODUCTION
                    )
                    # Non matching purposes are forbidden:
                    self.assertEqual(
                        self.client.post(
                            self.url_log,
                            self.pkg_json_post(
                                transfer_id=transfer.id,
                                purpose=forbidden_purpose,
                                checksum="11111",
                            ),
                            content_type=APPLICATION_JSON,
                        ).status_code,
                        status.HTTP_400_BAD_REQUEST,
                    )
                    # OK:
                    self.assertEqual(
                        self.client.post(
                            self.url_log,
                            self.pkg_json_post(
                                transfer_id=transfer.id, purpose=purpose
                            ),
                            content_type=APPLICATION_JSON,
                        ).status_code,
                        status.HTTP_201_CREATED,
                    )
                    # Limit reached:
                    self.assertEqual(
                        self.client.post(
                            self.url_log,
                            self.pkg_json_post(
                                transfer_id=transfer.id,
                                purpose=purpose,
                                created="now",
                            ),
                            content_type=APPLICATION_JSON,
                        ).status_code,
                        status.HTTP_400_BAD_REQUEST,
                    )
            self.client.logout()

    def test_node_down(self):
        for user in (self.user_data_package_reporter,):
            self.client.login(username=user.username, password=USER_PASSWORD)
            self.dest_node.node_status = Node.STATUS_OFFLINE
            self.dest_node.save()
            self.assertEqual(
                self.client.post(
                    self.url_log,
                    self.pkg_json_post(created="now"),
                    content_type=APPLICATION_JSON,
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_data_provider_disabled(self):
        for user in (self.user_data_package_reporter,):
            self.client.login(username=user.username, password=USER_PASSWORD)
            self.data_provider.enabled = False
            self.data_provider.save()
            self.assertEqual(
                self.client.post(
                    self.url_log,
                    self.pkg_json_post(created="now"),
                    content_type=APPLICATION_JSON,
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_wrong_user_permissions(self):
        for user in (self.user_data_package_reporter,):
            self.client.login(username=user.username, password=USER_PASSWORD)
            # key missing in database
            PgpKeyInfo.objects.get(id=self.key1.id).delete()
            self.assertEqual(
                self.client.post(
                    self.url_log,
                    self.pkg_json_post(created="now"),
                    content_type=APPLICATION_JSON,
                ).status_code,
                status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_write_methods_not_allowed(self):
        # PUT, DELETE are not allowed
        self.assertEqual(
            self.client.put(self.url).status_code, status.HTTP_403_FORBIDDEN
        )
        assert_access_forbidden(
            self.client,
            self.url,
            (
                (self.basic_user.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.staff.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.node_viewer.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (
                    self.user_data_package_reporter.username,
                    status.HTTP_405_METHOD_NOT_ALLOWED,
                ),
            ),
            methods=("put", "delete"),
            test_unauthorized=False,
        )
        self.assertEqual(
            self.client.put(self.url_check).status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED,
        )
        assert_access_forbidden(
            self.client,
            self.url_check,
            (
                (self.basic_user.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.staff.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.node_viewer.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (
                    self.user_data_package_reporter.username,
                    status.HTTP_405_METHOD_NOT_ALLOWED,
                ),
            ),
            methods=("put", "delete"),
            test_unauthorized=False,
        )
        self.assertEqual(
            self.client.put(self.url_log).status_code, status.HTTP_403_FORBIDDEN
        )
        assert_access_forbidden(
            self.client,
            self.url_log,
            (
                (self.basic_user.username, status.HTTP_403_FORBIDDEN),
                (self.staff.username, status.HTTP_405_METHOD_NOT_ALLOWED),
                (self.node_viewer.username, status.HTTP_403_FORBIDDEN),
                (
                    self.user_data_package_reporter.username,
                    status.HTTP_403_FORBIDDEN,
                ),
            ),
            methods=("put", "delete"),
            test_unauthorized=False,
        )

    def test_multiple_enter(self):
        self._setUpPkgTrace(
            trace=(
                (self.node, DataPackageTrace.ENTER),
                (self.node, DataPackageTrace.ERROR),
            )
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        response = self.client.post(
            self.url_log, self.pkg_json_post(), content_type=APPLICATION_JSON
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            DataPackageTrace.objects.filter(status=DataPackageTrace.ENTER).count(), 1
        )
        self.client.logout()

    def test_block_logging(self):
        other_node = NodeFactory()
        self._setUpPkgTrace(trace=((self.node, DataPackageTrace.ENTER),))
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        response = self.client.post(
            self.url_log,
            {
                **self.pkg_json_post(),
                "status": DataPackageTrace.PROCESSED,
                "node": other_node.code,
            },
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        self.client.logout()

    def test_deny_reprocessing(self):
        other_node = NodeFactory()
        self._setUpPkgTrace(
            trace=(
                (self.node, DataPackageTrace.ENTER),
                (self.node, DataPackageTrace.PROCESSED),
                (other_node, DataPackageTrace.ENTER),
                (other_node, DataPackageTrace.PROCESSED),
            )
        )
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        response = self.client.post(
            self.url_log, self.pkg_json_post(), content_type=APPLICATION_JSON
        )
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.client.logout()

    def test_node_does_not_exist(self):
        self._setUpPkgTrace(trace=((self.node, DataPackageTrace.ENTER),))
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        response = self.client.post(
            self.url_log,
            {
                **self.pkg_json_post(),
                "status": DataPackageTrace.PROCESSED,
                "node": "DOES_NOT_EXIST",
            },
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(), {"node": {"node": "Invalid node: DOES_NOT_EXIST"}}
        )
        self.client.logout()
