from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import assign_perm
from rest_framework import status

from identities.models import GroupProfile
from identities.permissions import perm_change_user, has_change_user_permission
from projects.apps import APP_NAME
from projects.models.data_provider import has_any_data_provider_permissions
from projects.models.flag import Flag
from projects.models.node import has_node_admin_nodes
from projects.models.project import ProjectUserRole, ProjectRole
from .. import APPLICATION_JSON
from ..factories import (
    DataProviderFactory,
    FlagFactory,
    GroupFactory,
    GroupProfileFactory,
    NodeFactory,
    CustomAffiliationFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    UserFactory,
    USER_PASSWORD,
    UserNamespaceFactory,
    make_data_provider_admin,
    make_data_provider_viewer,
    make_node_admin,
    make_node_viewer,
    make_project_user,
)

FEDERATED_QUERY_FLAG_NAME = "fqs"
User = get_user_model()


class TestUserView(TestCase):
    def setUp(self):
        self.project1, self.project2 = ProjectFactory.create_batch(2)
        self.basic_user = make_project_user(self.project1, ProjectRole.USER)
        self.staff = UserFactory(staff=True)
        self.pm_user = make_project_user(self.project1, ProjectRole.PM)
        self.dm_user = make_project_user(self.project1, ProjectRole.DM)
        self.user_with_perm = UserFactory()
        assign_perm(perm_change_user, self.user_with_perm)
        self.flag = FlagFactory(code=FEDERATED_QUERY_FLAG_NAME)
        self.node = NodeFactory()
        self.dp = DataProviderFactory()
        self.node_admin = make_node_admin(node=self.node)
        self.node_viewer = make_node_viewer(node=self.node)
        self.dp_admin = make_data_provider_admin(data_provider=self.dp)
        self.dp_viewer = make_data_provider_viewer(data_provider=self.dp)
        self.custom_affiliation = CustomAffiliationFactory()

    def test_view_list(self):
        url = reverse(f"{APP_NAME}:user-list")
        # Basic user
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        self.assertEqual(r.json()[0]["username"], self.basic_user.username)
        self.client.logout()
        # Powerusers
        for username in (
            self.staff.username,
            self.user_with_perm.username,
            self.node_admin.username,
            self.node_viewer.username,
            self.dp_admin.username,
            self.dp_viewer.username,
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            # All
            r = self.client.get(url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), User.objects.count())
            # Filter by email
            r = self.client.get(url, {"email": self.basic_user.email})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1)
            self.client.logout()

    def test_view_list_hide_not_active_users(self):
        url = reverse(f"{APP_NAME}:user-list")
        user_count = User.objects.count()
        self.client.login(username=self.staff.username, password=USER_PASSWORD)

        self.assertEqual(len(self.client.get(url).json()), user_count)

        # inactive users are hidden by default
        self.basic_user.is_active = False
        self.basic_user.save()
        self.assertEqual(len(self.client.get(url).json()), user_count - 1)

        # adding request parameter includes inactive users as well
        self.assertEqual(
            len(self.client.get(url, {"include_inactive": True}).json()), user_count
        )

        self.client.logout()

    def test_view_list_user_project_role_filter(self):
        url = reverse(f"{APP_NAME}:user-list")
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        ProjectUserRoleFactory(
            project=self.project1, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectUserRoleFactory(
            project=self.project2, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectFactory()

        user = ProjectRole.USER.name
        dm = ProjectRole.DM.name
        pl = ProjectRole.PL.name

        for parameters, expected_user_ids in (
            ({"project_id": 1}, [2, 3, 4, 5]),
            ({"project_id": 2}, [3]),
            ({"project_id": 3}, []),
            ({"role": user}, [2, 3]),
            ({"role": dm}, [5]),
            ({"role": pl}, []),
            ({"project_id": 1, "role": user}, [2, 3]),
            ({"project_id": 2, "role": user}, [3]),
            ({"project_id": 2, "role": dm}, []),
        ):
            response = self.client.get(url, parameters).json()
            assert [usr["id"] for usr in response] == expected_user_ids
        self.client.logout()

    def test_update_as_user_with_perm(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})

        self.client.login(username=self.user_with_perm.username, password=USER_PASSWORD)
        self.assertTrue(has_change_user_permission(self.user_with_perm))

        with self.subTest(
            "Changing 'local_username' and/or 'uid' possible if "
            "the user has the permission 'identities.change_user'"
        ):
            for expected_profile in (
                {"uid": 1000020},
                {"local_username": "new_user_local"},
                {"local_username": "cookie_monster", "uid": 1000050},
            ):
                user_change = {"profile": expected_profile}
                resp = self.client.patch(
                    url,
                    user_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                # Verify that user has been updated
                profile = User.objects.get(pk=self.basic_user.pk).profile
                assert expected_profile.items() <= profile.__dict__.items()

        with self.subTest(
            "Updating 'flags' possible if the user has the permission 'identities.change_user'"
        ):
            # Ensure flag `CLINERION_FLAG_NAME` does NOT have any user
            flag = Flag.objects.get(code=FEDERATED_QUERY_FLAG_NAME)
            assert flag.users.count() == 0
            user_change = {"flags": [FEDERATED_QUERY_FLAG_NAME]}
            resp = self.client.patch(
                url,
                user_change,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            # Verify that user has been updated
            flag = Flag.objects.get(code=FEDERATED_QUERY_FLAG_NAME)
            assert tuple(flag.users.all()) == (self.basic_user,)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "update some specific user properties. Everything else will be ignored."
        ):
            for user_change in (
                {"first_name": "Kirkia"},
                {"last_name": "Douglasie"},
                {"email": "kirkia.douglasie@localhost"},
            ):
                resp = self.client.patch(
                    url,
                    user_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key, value in user_change.items():
                    self.assertEqual(resp.json()[key], value)

            for profile_change in (
                # Non-updatable fields
                {"profile": {"affiliation_id": "21501@institution.ch"}},
            ):
                resp = self.client.patch(
                    url,
                    profile_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key, value in profile_change["profile"].items():
                    self.assertIsNot(resp.json()["profile"][key], value)
                user = User.objects.get(pk=self.basic_user.pk)
                assert user == self.basic_user

            for no_change in (
                # Non-existing fields will be ignored
                {"not_a_user_field": 12345},
                {"this_is_really_not_there": "void"},
            ):
                resp = self.client.patch(
                    url,
                    no_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key in no_change:
                    self.assertTrue(key not in resp.json())

            new_local_username = "new_user_local"
            resp = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_id": "21501@institution.ch",
                        "local_username": new_local_username,
                    },
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            profile = User.objects.get(pk=self.basic_user.pk).profile
            assert profile.local_username == new_local_username
            assert profile.affiliation_id == self.basic_user.profile.affiliation_id

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "use method PATCH"
        ):
            resp = self.client.put(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        self.client.logout()

    def test_update_as_user(self):
        # pylint: disable=too-many-statements
        default_namespace = "ch"
        user = User.objects.get(username=self.pm_user.username)
        user.profile.local_username = "user_local"
        user.profile.namespace = UserNamespaceFactory(name=default_namespace)
        user.profile.save()
        self.basic_user.profile.affiliation_consent = True
        self.basic_user.profile.save()
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})
        dm_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.dm_user.pk})
        na_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.node_admin.pk})

        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        with self.subTest("local_username is missing"):
            for value in ("", None):
                r = self.client.patch(
                    url,
                    {"profile": {"local_username": value}},
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            r = self.client.patch(url, {"profile": {}}, content_type=APPLICATION_JSON)
            self.assert_no_permission(r)

        with self.subTest(
            "should not be allowed to use modifying methods other than PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("`local_username` has a max length"):
            too_long_local_username = "a" * 100
            r = self.client.patch(
                url,
                {"profile": {"local_username": too_long_local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)

        with self.subTest("`local_username` has a min length"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "short"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)
            response = r.json()
            self.assertEqual(
                response["detail"],
                "Ensure this value has at least 6 characters (it has 5).",
            )

        with self.subTest("local_username and namespace must be unique together"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": user.profile.local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            response = r.json()
            self.assertEqual(
                response["detail"],
                "UNIQUE constraint failed: projects_profile.namespace_id, projects_profile.local_username",
            )

        with self.subTest(
            "normal user should not be able to change first_name, last_name or email"
        ):
            r = self.client.patch(
                url,
                {
                    "first_name": "Chuck",
                    "last_name": "Norris",
                    "email": "chuck.norris@localhost",
                },
                content_type=APPLICATION_JSON,
            )
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest("Changing local_username is forbidden"):
            self.basic_user.profile.local_username = "user_local"
            self.basic_user.profile.save()
            r = self.client.patch(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Revoking affiliation_consent is forbidden"):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": False}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Changing username and revoking affiliation_consent is forbidden"
        ):
            r = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_consent": False,
                        "local_username": "new_user_local",
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Cannot change username when giving affiliation consent"):
            self.client.login(username=self.dm_user.username, password=USER_PASSWORD)
            self.dm_user.profile.local_username = "dm_username"
            self.dm_user.profile.save()
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "local_username": "local_us",
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Can give affiliation consent independently of username already being set"
        ):
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["local_username"], "dm_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        with self.subTest(
            "Can set both local_username and affiliation_consent at once"
        ):
            self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
            r = self.client.patch(
                na_url,
                {
                    "profile": {
                        "local_username": "na_username",
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["local_username"], "na_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        self.client.logout()

    # pylint: disable=too-many-statements
    def test_update_as_staff(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.user_with_perm.pk})
        self.client.login(username=self.staff.username, password=USER_PASSWORD)

        with self.subTest("should not be allowed to set username of someone else"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("should be allowed to set a custom affiliation for a user"):
            r = self.client.patch(
                url,
                {"profile": {"custom_affiliation": self.custom_affiliation.id}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                r.json()["profile"]["custom_affiliation"], self.custom_affiliation.id
            )

        with self.subTest(
            "should not be allowed to give affiliation consent for someone else"
        ):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": True}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "should remove objects associated to the user when setting `is_active` to `False`"
        ):
            p3_node = NodeFactory()
            active_user = make_node_admin(node=p3_node)

            url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": active_user.pk})

            make_node_admin(self.staff, p3_node)
            users = (active_user, self.staff)

            p3 = ProjectFactory(destination=p3_node)
            ProjectUserRoleFactory(
                project=p3,
                user=active_user,
                role=ProjectRole.USER.value,
            )
            make_project_user(p3, ProjectRole.DM, active_user)
            p3_staff_pl = ProjectUserRoleFactory(
                project=p3,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p4 = ProjectFactory(destination=NodeFactory())
            p4_staff_pl = ProjectUserRoleFactory(
                project=p4,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p5 = ProjectFactory(destination=NodeFactory())
            ProjectUserRoleFactory(
                project=p5,
                user=active_user,
                role=ProjectRole.USER.value,
            )

            # project without ProjectUserRoles to check for raised exceptions
            ProjectFactory(destination=NodeFactory())

            data_provider = DataProviderFactory(node=p3_node)
            coordinator_group = GroupFactory(
                name=f"Data Provider Coordinator {data_provider.code}"
            )
            GroupProfileFactory(
                group=coordinator_group,
                role_entity=data_provider,
                role=GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
            )
            for u in users:
                make_data_provider_viewer(u, data_provider, coordinator_group)
            data_provider.save()

            flag = FlagFactory()
            flag.users.set(users)
            flag.save()

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertTrue(has_node_admin_nodes(active_user))

            r = self.client.patch(
                url,
                {"is_active": False},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

            email = mail.outbox
            assert len(email) == 3
            assert [obj.subject for obj in email] == [
                f"Permissions changed in '{p3.name}'",
                f"User(s) added to/removed from project '{p3.name}'",
                f"User(s) added to/removed from project '{p5.name}'",
            ]
            self.assertEqual(
                ProjectUserRole.objects.filter(project=p3).get(), p3_staff_pl
            )
            self.assertEqual(
                ProjectUserRole.objects.filter(project=p4).get(), p4_staff_pl
            )
            self.assertEqual(ProjectUserRole.objects.filter(project=p5).count(), 0)

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertFalse(has_node_admin_nodes(active_user))
            self.assertFalse(has_any_data_provider_permissions(active_user))
            self.assertEqual(flag.users.get(), self.staff)

        with self.subTest("should be able to change `is_active` to `True` again"):
            r = self.client.patch(
                url,
                {"is_active": True},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

        self.client.logout()

    def test_create_local_user(self):
        url = reverse(f"{APP_NAME}:user-list")
        data = {
            "first_name": "Chuck",
            "last_name": "Norris",
            "username": "0815@portal.ch",
            "email": "chuck@norris.gov",
            "profile": {"local_username": "chuck_norris"},
            "flags": [],
        }

        with self.subTest("Creating local user as non-staff user is forbidden"):
            self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest(
            "Creating local user as user with 'identities.change_user' is forbidden"
        ):
            self.client.login(
                username=self.user_with_perm.username,
                password=USER_PASSWORD,
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest("Create local user as staff"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response["first_name"], data["first_name"])
            self.assertEqual(response["last_name"], data["last_name"])
            self.assertEqual(response["email"], data["email"])
            self.assertEqual(response["username"], data["username"])
            self.assertEqual(
                response["profile"]["local_username"], data["profile"]["local_username"]
            )
            created_local_user = User.objects.get(id=response["id"])
            # make sure user cannot be used to log in
            self.assertFalse(created_local_user.has_usable_password())
            self.client.logout()

        with self.subTest("Cannot create user with non-unique email address"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            new_data = {
                **data,
                "username": "brandnew_username",
                "email": "chuck@norris.gov",
            }
            r = self.client.post(
                url,
                new_data,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(
                r.json(),
                {
                    "detail": "{'email': [ErrorDetail(string='user with this email address already exists.', code='unique')]}"
                },
            )
            self.client.logout()

        with self.subTest("Cannot create user with non-unique username"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            new_data = {
                **data,
                "username": "0815@portal.ch",
                "email": "brandnew_email@norris.gov",
            }

            r = self.client.post(
                url,
                new_data,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(
                r.json(),
                {
                    "detail": "{'username': [ErrorDetail(string='This field must be unique.', code='unique')]}"
                },
            )
            self.client.logout()

    def assert_no_permission(self, r):
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        self.assertDictEqual(
            r.json(),
            {"detail": "You do not have permission to perform this action."},
        )
