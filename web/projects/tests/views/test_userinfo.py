from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

URL = reverse(f"{APP_NAME}:userinfo")


def test_get_unauthenticated(client):
    assert client.get(URL).status_code == status.HTTP_403_FORBIDDEN


def test_get(client, basic_user_auth, flag_factory):
    flag = flag_factory.create()
    flag.users.add(basic_user_auth)
    response = client.get(URL)
    data = response.json()
    display_name = (
        f"{basic_user_auth.first_name} {basic_user_auth.last_name} "
        f"({basic_user_auth.email})"
    )
    data_ref = {
        "username": basic_user_auth.username,
        "email": basic_user_auth.email,
        "first_name": basic_user_auth.first_name,
        "last_name": basic_user_auth.last_name,
        "permissions": {
            "manager": False,
            "staff": False,
            "data_manager": False,
            "project_leader": False,
            "node_admin": False,
            "node_viewer": False,
            "data_provider_admin": False,
            "data_provider_viewer": False,
            "group_manager": False,
            "has_projects": False,
            "legal_approver": False,
        },
        "profile": {
            "affiliation": basic_user_auth.profile.affiliation,
            "affiliation_id": basic_user_auth.profile.affiliation_id,
            "custom_affiliation": None,
            "local_username": basic_user_auth.profile.local_username,
            "display_name": display_name,
            "display_id": None,
            "display_local_username": "",
            "uid": None,
            "gid": None,
            "namespace": None,
            "affiliation_consent": False,
            "emails": basic_user_auth.profile.emails,
        },
        "manages": {"node_admin": [], "data_provider_admin": []},
        "groups": [],
        "flags": ["flag"],
    }
    assert response.status_code == status.HTTP_200_OK
    assert data_ref == {
        key: value for key, value in data.items() if key not in ("id", "ip_address")
    }
    assert isinstance(data["id"], int)
    assert isinstance(data["ip_address"], str)
