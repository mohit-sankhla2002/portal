import logging
from copy import deepcopy
from typing import List
from unittest.mock import Mock, patch

import pytest
from django.contrib.auth import get_user_model
from django.core import mail
from django.urls import reverse
from rest_framework import status

from portal import user_utils
from portal.config import SwitchNotification
from projects import SCIM_MEDIA_TYPE
from projects.apps import APP_NAME
from projects.views.switch_notification import EDUID_AFFILIATION_KEY
from .. import TEST_SERVER_URL
from ..conftest import SWITCH_USERNAME
from ..factories import UserFactory

User = get_user_model()

SWITCH_SCIM_API_USERNAME = "switchuser"
SWITCH_SCIM_API_PASSWORD = "switchpass"
SWITCH_SCIM_API_AFFILIATIONS_URL = "switchurl/"

EDUID_USER = "709429474319@eduid.ch"
URL = reverse(f"{APP_NAME}:switchnotification") + EDUID_USER
REQUEST_DATA = {
    "schemas": ["urn:ietf:params:scim:schemas:core:2.0:User"],
    "id": EDUID_USER,
}
kwargs = {
    "path": URL,
    "data": REQUEST_DATA,
    "content_type": SCIM_MEDIA_TYPE,
    "HTTP_ACCEPT": SCIM_MEDIA_TYPE,
}

AFFILIATION_1_SINGLE = "staff@aff1.edu"
AFFILIATION_1_MULTIPLE = f"{AFFILIATION_1_SINGLE},member@aff1.edu"
AFFILIATION_2_SINGLE = "staff@aff2.edu"
AFFILIATION_2_MULTIPLE = f"{AFFILIATION_2_SINGLE},member@aff2.edu"
AFFILIATION_1_2_SINGLE = f"{AFFILIATION_1_SINGLE},{AFFILIATION_2_SINGLE}"
AFFILIATION_1_2_MULTIPLE = f"{AFFILIATION_1_MULTIPLE},{AFFILIATION_2_MULTIPLE}"
AFFILIATION_ID_1 = "aff_id1"
AFFILIATION_ID_2 = "aff_id2"
AFFILIATION_ID_1_2 = f"{AFFILIATION_ID_1},{AFFILIATION_ID_2}"


@pytest.fixture(name="apply_switch_settings")
def apply_switch_config(
    apply_notification_settings, settings
):  # pylint: disable=unused-argument
    settings.CONFIG.switch = SwitchNotification(
        auth_username=SWITCH_USERNAME,
        scim_api_username=SWITCH_SCIM_API_USERNAME,
        scim_api_password=SWITCH_SCIM_API_PASSWORD,
        scim_api_affiliations_url=SWITCH_SCIM_API_AFFILIATIONS_URL,
    )


@pytest.mark.parametrize("method", ("get", "post", "put"))
@pytest.mark.usefixtures("apply_switch_settings")
def test_unauthenticated(client, method):
    assert getattr(client, method)(**kwargs).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.parametrize("method", ("get", "post", "put"))
@pytest.mark.usefixtures("staff_user_auth")
@pytest.mark.usefixtures("apply_switch_settings")
def test_staff_unauthorized(client, method):
    assert getattr(client, method)(**kwargs).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.parametrize("method", ("get", "post", "put"))
@pytest.mark.usefixtures("switch_user_auth")
def test_without_switch_config_unauthorized(client, method, settings):
    settings.CONFIG.switch = None
    assert getattr(client, method)(**kwargs).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
@pytest.mark.usefixtures("switch_user_auth")
@pytest.mark.usefixtures("apply_switch_settings")
class TestWithAuth:
    @pytest.fixture(autouse=True)
    def before_each(self):
        # pylint: disable=attribute-defined-outside-init
        self.user = UserFactory(
            basic=True,
            username=EDUID_USER,
            email=EDUID_USER,
            profile__affiliation_consent=True,
        )
        # pylint: enable=attribute-defined-outside-init

    @pytest.mark.parametrize("method", ("get", "post"))
    def test_methods_not_allowed(self, client, method):
        assert (
            getattr(client, method)(**kwargs).status_code == status.HTTP_403_FORBIDDEN
        )

    def test_user_not_found(self, client):
        kwargs_copy = deepcopy(kwargs)
        kwargs_copy["data"]["id"] = "non@existing.user"
        response = client.put(**kwargs_copy)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_no_id(self, client):
        kwargs_copy = deepcopy(kwargs)
        del kwargs_copy["data"]["id"]
        response = client.put(**kwargs_copy)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_no_previous_affiliation_id(self, client, caplog, patch_request):
        caplog.set_level(logging.INFO)
        mock_request = patch_request(Mock())
        response = client.put(**kwargs)
        self.verify_response(response)
        assert len(caplog.record_tuples) == 2
        assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
        assert "did NOT previously have any affiliations" in caplog.record_tuples[1][2]
        mock_request.assert_not_called()

    def test_error_from_scim_api(self, client, caplog, patch_request):
        caplog.set_level(logging.INFO)
        self.setup_user(AFFILIATION_1_SINGLE, AFFILIATION_ID_1)

        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        mock_request = patch_request(Mock(status_code=status_code))
        response = client.put(**kwargs)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert len(caplog.record_tuples) == 3
        assert caplog.record_tuples[0][0] == "django_guid"
        assert caplog.record_tuples[1][1] == logging.ERROR
        assert f"Unexpected status code {status_code}" in caplog.record_tuples[1][2]
        assert caplog.record_tuples[2][0] == "django.request"
        self.verify_user(AFFILIATION_1_SINGLE, AFFILIATION_ID_1)
        self.verify_request(mock_request, [AFFILIATION_ID_1])

    def test_remove_affiliation_one_affiliation_id(self, client, caplog, patch_request):
        caplog.set_level(logging.INFO)
        old_affiliation = AFFILIATION_1_SINGLE
        old_affiliation_id = AFFILIATION_ID_1
        self.setup_user(old_affiliation, old_affiliation_id)

        mock_request = patch_request(Mock(status_code=status.HTTP_404_NOT_FOUND))
        response = client.put(**kwargs)
        self.verify_response(response)
        self.verify_logs(
            caplog,
            f"Changed affiliation of user {EDUID_USER} "
            f"from '{old_affiliation}' to ''",
            f"Changed affiliation_id of user {EDUID_USER} from '{old_affiliation_id}' to ''",
        )
        self.verify_user("", "")
        self.verify_request(mock_request, [old_affiliation_id])
        self.verify_email(old_affiliation=old_affiliation)

    def test_remove_affiliation_two_affiliation_ids(
        self, client, caplog, patch_request_side_effect
    ):
        caplog.set_level(logging.INFO)
        old_affiliation = AFFILIATION_1_2_SINGLE
        new_affiliation = AFFILIATION_2_SINGLE
        old_affiliation_id = AFFILIATION_ID_1_2
        new_affiliation_id = AFFILIATION_ID_2
        self.setup_user(old_affiliation, old_affiliation_id)

        mock_request = patch_request_side_effect(
            self.request_mock_multiple(
                Mock(status_code=status.HTTP_404_NOT_FOUND),
                self.request_mock(status.HTTP_200_OK, [new_affiliation]),
            )
        )
        response = client.put(**kwargs)
        self.verify_response(response)

        self.verify_logs(
            caplog,
            f"Changed affiliation of user {EDUID_USER} "
            f"from '{old_affiliation}' to '{new_affiliation}'",
            f"Changed affiliation_id of user {EDUID_USER} "
            f"from '{old_affiliation_id}' to '{new_affiliation_id}'",
        )
        self.verify_user(new_affiliation, new_affiliation_id)
        self.verify_request(mock_request, [AFFILIATION_ID_1, new_affiliation_id])
        self.verify_email(
            old_affiliation=old_affiliation, new_affiliation=new_affiliation
        )

    def test_change_one_affiliation_id_one_affiliation(
        self, client, caplog, patch_request
    ):
        caplog.set_level(logging.INFO)
        old_affiliation = AFFILIATION_1_SINGLE
        new_affiliation = AFFILIATION_2_SINGLE
        self.setup_user(old_affiliation, AFFILIATION_ID_1)

        mock_request = patch_request(
            self.request_mock(status.HTTP_200_OK, [new_affiliation])
        )
        response = client.put(**kwargs)
        self.verify_response(response)
        self.verify_logs(
            caplog,
            f"Changed affiliation of user {EDUID_USER} "
            f"from '{old_affiliation}' to '{new_affiliation}'",
            f"affiliation_id of user {EDUID_USER} is unchanged.",
        )
        self.verify_user(new_affiliation, AFFILIATION_ID_1)
        self.verify_request(mock_request, [AFFILIATION_ID_1])
        self.verify_email(
            old_affiliation=old_affiliation, new_affiliation=new_affiliation
        )

    def test_change_two_affiliation_ids_two_affiliations(
        self, client, caplog, patch_request_side_effect
    ):
        caplog.set_level(logging.INFO)
        old_affiliation = AFFILIATION_1_2_MULTIPLE
        new_affiliation = AFFILIATION_1_2_SINGLE
        self.setup_user(old_affiliation, AFFILIATION_ID_1_2)

        mock_request = patch_request_side_effect(
            self.request_mock_multiple(
                self.request_mock(status.HTTP_200_OK, [AFFILIATION_1_SINGLE]),
                self.request_mock(status.HTTP_200_OK, [AFFILIATION_2_SINGLE]),
            )
        )
        response = client.put(**kwargs)
        self.verify_response(response)
        self.verify_logs(
            caplog,
            f"Changed affiliation of user {EDUID_USER} "
            f"from '{old_affiliation}' to '{new_affiliation}'",
            f"affiliation_id of user {EDUID_USER} is unchanged.",
        )
        self.verify_user(new_affiliation, AFFILIATION_ID_1_2)
        self.verify_request(mock_request, [AFFILIATION_ID_1, AFFILIATION_ID_2])
        self.verify_email(
            old_affiliation=old_affiliation, new_affiliation=new_affiliation
        )

    @pytest.mark.parametrize("send_no_affiliation_consent_email", [True, False])
    def test_no_affiliation_consent(
        self, client, caplog, patch_request, settings, send_no_affiliation_consent_email
    ):
        self.user.profile.affiliation_consent = False
        self.user.profile.save()
        caplog.set_level(logging.INFO)
        mock_request = patch_request(Mock())
        settings.CONFIG.notification.send_no_affiliation_consent_email = (
            send_no_affiliation_consent_email
        )
        with patch.object(user_utils, "sendmail") as mock_sendmail:
            response = client.put(**kwargs)
            self.verify_response(response)
            assert len(caplog.record_tuples) == 2
            assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
            assert "did NOT give consent" in caplog.record_tuples[1][2]
            mock_request.assert_not_called()
            if send_no_affiliation_consent_email:
                mock_sendmail.assert_called_once()
            else:
                mock_sendmail.assert_not_called()

    def setup_user(self, affiliation: str, affiliation_id: str):
        self.user.profile.affiliation = affiliation
        self.user.profile.affiliation_id = affiliation_id
        self.user.profile.save()

    @staticmethod
    def request_mock(status_code: status, affiliations: List[str]):
        def json():
            return {EDUID_AFFILIATION_KEY: affiliations}

        return Mock(status_code=status_code, json=json)

    @staticmethod
    def request_mock_multiple(affiliation_id_1: Mock, affiliation_id_2: Mock):
        def side_effect(*_, **kwargs):
            url = kwargs["url"]
            if AFFILIATION_ID_1 in url:
                return affiliation_id_1
            if AFFILIATION_ID_2 in url:
                return affiliation_id_2
            return None

        return side_effect

    @staticmethod
    def verify_response(response):
        assert response.status_code == status.HTTP_200_OK
        assert response.headers.get("Location") == TEST_SERVER_URL + URL
        assert response.headers.get("Content-Type") == SCIM_MEDIA_TYPE
        assert response.json() == REQUEST_DATA

    @staticmethod
    def verify_logs(caplog, affiliation_message, affiliation_id_message):
        assert len(caplog.record_tuples) == 3
        assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
        assert caplog.record_tuples[1][2] == affiliation_message
        assert caplog.record_tuples[2][2] == affiliation_id_message

    def verify_user(self, affiliation, affiliation_id):
        user_profile = User.objects.get(pk=self.user.pk).profile
        assert user_profile.affiliation == affiliation
        assert user_profile.affiliation_id == affiliation_id

    @staticmethod
    def verify_request(mock_request, affiliation_ids: List[str]):
        assert mock_request.call_count == len(affiliation_ids)
        calls = mock_request.call_args_list

        expected_urls = {
            SWITCH_SCIM_API_AFFILIATIONS_URL + id for id in affiliation_ids
        }
        actual_urls = {call.kwargs["url"] for call in calls}
        assert expected_urls == actual_urls

        for call in calls:
            assert call.kwargs["headers"] == {"Accept": SCIM_MEDIA_TYPE}

    @staticmethod
    def verify_email(old_affiliation: str = "", new_affiliation: str = ""):
        def format_affiliations(affiliation: str):
            return ", ".join(affiliation.split(","))

        emails = mail.outbox
        assert len(emails) == 1
        email = emails[0]
        assert f"Affiliation change of user '{EDUID_USER}'" in email.subject
        assert f"Old affiliation: {format_affiliations(old_affiliation)}" in email.body
        assert f"New affiliation: {format_affiliations(new_affiliation)}" in email.body
