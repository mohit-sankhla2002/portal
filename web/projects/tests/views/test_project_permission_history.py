from datetime import timedelta
from unittest import mock

from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

from .test_data_transfer import assert_access_forbidden
from ..factories import ProjectUserRoleHistoryFactory, UserFactory, USER_PASSWORD


class TestProjectUserRoleHistory(TestCase):
    def setUp(self):
        self.user_basic = UserFactory()
        self.staff = UserFactory(staff=True)
        self.history_record = ProjectUserRoleHistoryFactory()

    def test_view_list(self):
        url = reverse(f"{APP_NAME}:audit-project-permission-list")
        # Anonymous
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        # Basic user
        self.client.login(username=self.user_basic.username, password=USER_PASSWORD)
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        self.client.logout()
        # Create a second project with a different node
        ProjectUserRoleHistoryFactory()
        # Staff
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.json()), 2)
        self.client.logout()
        # Node user
        # Only allowed to see records for the projects associated with their node
        node_user = UserFactory()
        with mock.patch(
            "projects.views.project.get_node_permission_nodes"
        ) as mock_get_node_permission_nodes, mock.patch(
            "projects.permissions.audit.has_any_node_permissions"
        ) as mock_has_any_nodes_permission:
            mock_get_node_permission_nodes.return_value = (
                self.history_record.project.destination,
            )
            mock_has_any_nodes_permission.return_value = True
            self.client.login(username=node_user.username, password=USER_PASSWORD)
            resp = self.client.get(url)
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            self.assertEqual(len(resp.json()), 1)
            self.client.logout()

    def test_filter(self):
        url = reverse(f"{APP_NAME}:audit-project-permission-list")
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        date_from = (self.history_record.created - timedelta(seconds=1)).isoformat()
        date_to = (self.history_record.created + timedelta(seconds=1)).isoformat()
        self.assertEqual(len(self.client.get(url, {"from": date_from}).json()), 1)
        self.assertEqual(len(self.client.get(url, {"to": date_from}).json()), 0)
        self.assertEqual(len(self.client.get(url, {"to": date_to}).json()), 1)
        self.assertEqual(len(self.client.get(url, {"from": date_to}).json()), 0)
        self.assertEqual(
            len(self.client.get(url, {"from": date_from, "to": date_to}).json()), 1
        )
        self.client.logout()

    def test_write_methods_not_allowed(self):
        # Modifying methods are not allowed
        assert_access_forbidden(
            self.client,
            url=reverse(f"{APP_NAME}:audit-project-permission-list"),
            users=[
                (u, status.HTTP_403_FORBIDDEN)
                for u in (
                    self.user_basic.username,
                    self.staff.username,
                )
            ],
            methods=("post", "put", "delete"),
        )
