from django.contrib.auth.models import Group
from django.urls import reverse
from guardian.shortcuts import get_perms
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.const import (
    APP_PERM_CHANGE_GROUP,
    APP_PERM_DATA_PROVIDER_ADMIN,
    APP_PERM_DATA_PROVIDER_VIEWER,
    APP_PERM_VIEW_GROUP,
)
from projects.models.data_provider import (
    DataProvider,
    get_data_provider_coordinators,
)
from projects.models.project import ProjectRole
from projects.serializers.user import UserShortSerializer
from .test_data_transfer import TransferViewTestBase, assert_access_forbidden
from .. import APPLICATION_JSON
from ..factories import (
    DataProviderFactory,
    USER_PASSWORD,
    DataTransferFactory,
    make_data_provider_coordinator,
    make_data_provider_users,
    make_node_viewer,
    make_project_user,
)


class TestDataProviderView(TransferViewTestBase):
    url = reverse(f"{APP_NAME}:dataprovider-list")

    def setUp(self):
        super().setUp()
        self.data_provider_2 = DataProviderFactory(node=self.dest_node)

    def test_view_list(self):
        # Unauthenticated cannot see data providers
        assert_access_forbidden(self.client, self.url)
        # Regular user cannot see data providers
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.get(self.url).status_code, status.HTTP_403_FORBIDDEN
        )
        self.client.logout()

        data_transfer = DataTransferFactory()
        make_data_provider_coordinator(self.staff, self.data_provider_2)
        (
            dp_viewer,
            dp_coordinator,
            dp_technical_admin,
            dp_data_engineer,
        ) = make_data_provider_users(data_transfer.data_provider)
        node_viewer = make_node_viewer(node=data_transfer.project.destination)
        # Staff and authenticated users can see data providers
        for username in (
            self.staff.username,
            make_project_user(data_transfer.project, ProjectRole.DM).username,
            node_viewer.username,
            dp_viewer.username,
            dp_technical_admin.username,
            dp_coordinator.username,
            dp_data_engineer.username,
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            data_provider_list = r.json()
            data_provider_list_expected = [
                {
                    "id": dp.id,
                    "name": dp.name,
                    "code": dp.code,
                    "enabled": dp.enabled,
                    "node": (dp.node.code if dp.node is not None else None),
                    "coordinators": UserShortSerializer(
                        get_data_provider_coordinators(dp),
                        many=True,
                    ).data,
                }
                for dp in DataProvider.objects.all()
            ]
            assert data_provider_list == data_provider_list_expected
            self.client.logout()

        self.data_provider.enabled = False
        self.data_provider.save()
        self.client.login(username=node_viewer.username, password=USER_PASSWORD)
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        assert r.json() == [
            dp
            for dp in data_provider_list_expected
            if dp["code"] != self.data_provider.code
        ]
        self.client.logout()

    def test_dp_create(self):
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        url = reverse(f"{APP_NAME}:dataprovider-list")
        dp_code = "chuck"
        data = {"name": "Chuck", "code": dp_code, "node": self.dest_node.code}
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )
        dp = DataProvider.objects.get(code=dp_code)
        dp_ta = Group.objects.get(name=f"{dp_code} DP Technical Admin")
        dp_viewer = Group.objects.get(name=f"{dp_code} DP Viewer")
        dp_manager = Group.objects.get(name=f"{dp_code} DP Manager")
        dp_coordinator = Group.objects.get(name=f"{dp_code} DP Coordinator")
        # DP Viewer, Technical Admin, Coordinator
        for group in (dp_ta, dp_viewer, dp_coordinator):
            assert APP_PERM_DATA_PROVIDER_VIEWER.endswith(get_perms(group, dp)[0])
        # DP Manager
        for group in (dp_ta, dp_viewer, dp_coordinator):
            assert APP_PERM_CHANGE_GROUP.endswith(
                sorted(get_perms(dp_manager, group))[0]
            )
            assert APP_PERM_VIEW_GROUP.endswith(sorted(get_perms(dp_manager, group))[1])
        assert APP_PERM_DATA_PROVIDER_ADMIN.endswith(get_perms(dp_manager, dp)[0])
        self.client.logout()
