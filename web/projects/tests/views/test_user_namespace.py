import pytest
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME

URL = reverse(f"{APP_NAME}:user-namespace-list")


def test_view_list_unauthenticated(client):
    assert client.get(URL).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("staff_user_auth")
def test_view_list(client, user_namespace_factory):
    namespace, *_ = user_namespace_factory.create_batch(5)
    r = client.get(URL)
    assert r.status_code == status.HTTP_200_OK
    assert len(r.json()) == 5
    assert r.json()[0] == {"id": namespace.id, "name": namespace.name}


@pytest.mark.parametrize("method", ("post", "put", "delete"))
@pytest.mark.usefixtures("staff_user_auth")
def test_write_methods_not_allowed(client, method):
    assert (
        getattr(client, method)(URL).status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    )
