from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.message import Message
from projects.serializers import MessageSerializer
from .. import APPLICATION_JSON
from ..factories import USER_PASSWORD

url_list = reverse(f"{APP_NAME}:message-list")


def get_url_detail(pk: int):
    return reverse(f"{APP_NAME}:message-detail", kwargs={"pk": pk})


def test_anonymous(client, message_factory):
    msg: Message = message_factory.create()
    for url in (url_list, get_url_detail(msg.id)):
        assert client.get(url).status_code == status.HTTP_403_FORBIDDEN


def test_create_new_message_not_allowed(admin_client):
    assert admin_client.post(url_list).status_code == status.HTTP_405_METHOD_NOT_ALLOWED


def test_list_messages(client, user_factory, message_factory):
    user1, user2 = user_factory.create_batch(2)
    user1_messages = message_factory.create_batch(5, user=user1)
    user2_messages = message_factory.create_batch(5, user=user2)
    client.login(username=user1.username, password=USER_PASSWORD)
    r = client.get(url_list)
    assert r.status_code == status.HTTP_200_OK
    r_json = r.json()
    assert len(r_json) == 5
    ids = sorted([obj["id"] for obj in r_json])
    assert ids == sorted([x.id for x in user1_messages])
    # Does NOT contain
    assert len(set(ids) - {x.id for x in user2_messages}) == 5
    client.logout()


def test_change_message_status(client, user_factory, message_factory):
    user1, user2 = user_factory.create_batch(2)
    message = message_factory.create(user=user1, status=Message.MessageStatus.UNREAD)
    message_data = MessageSerializer(message).data
    # Non-user
    client.login(username=user2.username, password=USER_PASSWORD)
    for method in ("get", "put", "delete"):
        assert (
            getattr(client, method)(get_url_detail(message.id)).status_code
            == status.HTTP_404_NOT_FOUND
        )
    client.logout()
    url = get_url_detail(message.id)
    # Owner
    client.login(username=user1.username, password=USER_PASSWORD)
    # get
    r = client.get(url)
    assert r.status_code == status.HTTP_200_OK
    assert r.json() == message_data
    # update (only status can be updated, other fields are ignored)
    for method in ("patch", "put"):
        r = getattr(client, method)(
            url,
            {"status": Message.MessageStatus.READ, "title": "foo", "body": "bar"},
            content_type=APPLICATION_JSON,
        )
        assert r.status_code == status.HTTP_200_OK
        assert r.json() == {**message_data, "status": Message.MessageStatus.READ.value}
    # delete
    assert client.delete(url).status_code == status.HTTP_204_NO_CONTENT
    assert len(Message.objects.all()) == 0
    client.logout()
