from unittest import mock
from projects.utils import rt


def test_create_ticket():
    response = """RT/4.0.6 200 Ok

# Ticket 775 created."""
    mock_post = mock.Mock(return_value=response)
    with mock.patch("projects.utils.rt.post", mock_post):
        created_id = rt.create_ticket(
            host="rt",
            session_cookie={},
            subject="",
            requestor_mail="",
            queue="",
            body="Hi there\nLorem Ipsum",
            parent=1,
        )
    expected_from_id = 775
    assert created_id == expected_from_id
    expected_to_id = 1
    expected_post_body = f"""DependsOn: {expected_to_id}
HasMember: {expected_from_id}
"""
    assert mock_post.has_call(
        mock.call(
            url="rt/REST/1.0/ticket/1/links",
            data=expected_post_body.encode(),
            cookies={},
        )
    )


def create_mock_rt_session_class() -> tuple[type, list]:
    mock_tickets: list[dict[str, str]] = []

    class MockRtSession(rt.RTSession):
        # Here, we dont want to call super.__init__ to prevent login.
        # On the other side, we want pylint check, that the signatures match
        def __init__(
            self, host: str, username: str, password: str, **kwargs
        ):  # pylint: disable=super-init-not-called
            self.post_args = kwargs
            self.n = 0

        def create_ticket(self, **kwargs):
            mock_tickets.append({**self.post_args, **kwargs})
            n = self.n
            self.n += 1
            return n

    return (MockRtSession, mock_tickets)
