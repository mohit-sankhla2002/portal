import pytest

from guardian.shortcuts import assign_perm

from projects.permissions.user import IsAllowedPatch
from identities.models import User
from identities.permissions import perm_change_user

OWNER = "owner"
STAFF = "staff"
PRIVILEGED = "privileged"

PROFILE = {"local_username": "chuck", "affiliation_consent": True}
REQUEST_WITH_PROFILE = {
    "email": "chuck_norris@example.org",
    "profile": {
        "local_username": "chuck_norris",
        "affiliation_consent": True,
    },
}


@pytest.mark.parametrize(
    "requestor, request_data, existing_profile, expected",
    (
        # Unprivileged owner can update only profile information.
        (OWNER, {"not_allowed": "not_allowed"}, None, False),
        (OWNER, {"profile": {"not_allowed": "not_allowed"}}, None, False),
        (
            OWNER,
            {"email": "chuck_norris@example.org", "not_allowed": "not_allowed"},
            None,
            False,
        ),
        (OWNER, REQUEST_WITH_PROFILE, None, True),
        (OWNER, {"email": "chuck_norris@example.org"}, None, True),
        # Staff and privileged users can only update other users non-profile
        # information
        (STAFF, {"first_name": "chucky"}, None, True),
        (PRIVILEGED, {"first_name": "chucky"}, None, True),
        (STAFF, REQUEST_WITH_PROFILE, None, False),
        (PRIVILEGED, REQUEST_WITH_PROFILE, None, False),
        # Owner cannot overwrite profile information
        (OWNER, REQUEST_WITH_PROFILE, PROFILE, False),
    ),
)
def test_is_allowed_patch(
    requestor,
    request_data,
    existing_profile,
    expected,
    rf,
    user_factory,
    profile_factory,
):
    user = user_factory(username=OWNER, profile=None)
    user_factory(username=STAFF, staff=True)
    assign_perm(perm_change_user, user_factory(username=PRIVILEGED))

    if existing_profile:
        profile_factory(**existing_profile, user=user)

    request = rf.request()
    request.user = User.objects.get(username=requestor)
    request.data = request_data

    assert IsAllowedPatch().has_object_permission(request, None, user) is expected
