from unittest import mock

import pytest
from rest_framework.test import APIRequestFactory

from projects.permissions.audit import AuditPermission


@pytest.mark.parametrize(
    "method,permitted",
    (
        ("get", True),
        ("options", True),
        ("head", True),
        ("post", False),
        ("put", False),
        ("patch", False),
        ("delete", False),
    ),
)
def test_audit_permission(user_factory, method, permitted):
    request = getattr(APIRequestFactory(), method)("/")
    # Anonymous and basic user
    for user in (None, user_factory()):
        request.user = user
        assert AuditPermission().has_permission(request, None) is False
    # Staff user
    request.user = user_factory(staff=True)
    assert AuditPermission().has_permission(request, None) is permitted
    # Node user
    with mock.patch(
        "projects.permissions.audit.has_any_node_permissions"
    ) as mock_has_any_nodes_permission:
        mock_has_any_nodes_permission.return_value = True
        request.user = user_factory()
        assert AuditPermission().has_permission(request, None) is permitted
