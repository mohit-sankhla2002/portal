import pytest

from projects.filters.project import (
    ProjectsLookupFilter,
    ProjectUserRoleHistoryLookupFilter,
)
from projects.models.node import Node
from projects.models.project import Project, ProjectUserRoleHistory


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 3],
        [{"name": "Dark Arts"}, 1],
        [{"name": "dark arts"}, 1],
        [{"code": "da"}, 1],
        [{"destination": "hogwarts"}, 2],
        [{"destination": "durmstrang"}, 1],
        [{"name": "mathematics"}, 0],
        [{"code": "mt"}, 0],
        [{"destination": "node"}, 0],
    ],
)
def test_project_lookup_filter(project_factory, query_params, expected_length, rf):
    for name, code, dest in (
        ("Defence Against the Dark Arts", "dada", "hogwarts"),
        ("Dark Arts", "da", "durmstrang"),
        ("Transfiguration", "tr", "hogwarts"),
    ):
        project_factory.create(
            name=name,
            code=code,
            destination=Node.objects.get_or_create(code=dest, name=dest)[0],
        )

    request = rf.get("/")
    request.query_params = query_params
    queryset = Project.objects.all()
    q = ProjectsLookupFilter().filter_queryset(request, queryset, None)
    assert len(q) == expected_length


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 3],
        [{"project": "prefix_project_1"}, 1],
        [{"project": "prefix_.*"}, 2],
        [{"project": "prefix_project_3"}, 0],
    ],
)
def test_project_user_role_history_lookup_filter(
    project_user_role_history_factory,
    query_params,
    expected_length,
    rf,
):
    for code in (("prefix_project_1"), ("prefix_project_2"), ("project_3")):
        project_user_role_history_factory(project_str=code)

    request = rf.get("/")
    request.query_params = query_params
    filtered_queryset = ProjectUserRoleHistoryLookupFilter().filter_queryset(
        request, ProjectUserRoleHistory.objects.all(), None
    )
    assert len(filtered_queryset) == expected_length
