import pytest

from projects.filters.pgp_key_info import PgpKeyInfoLookupFilter
from projects.models.pgp import PgpKeyInfo
from ..views.test_pgpkey import (
    CN_EMAIL,
    CN_FINGERPRINT,
    CN_USER_ID,
    SGT_HARTMANN_EMAIL,
    SGT_HARTMANN_FINGERPRINT,
    SGT_HARTMANN_USER_ID,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 2],
        [{"fingerprint": CN_FINGERPRINT}, 1],
        [{"key_email": CN_EMAIL}, 1],
        [{"key_user_id": CN_USER_ID}, 1],
        [{"username": "chuck_norris"}, 1],
        [{"status": PgpKeyInfo.Status.APPROVED}, 1],
    ],
)
def test_user_lookup_filter(
    user_factory, pgp_key_info_factory, query_params, expected_length, rf
):
    for fingerprint, key_email, key_user_id, username, status in (
        (
            CN_FINGERPRINT,
            CN_EMAIL,
            CN_USER_ID,
            "chuck_norris",
            PgpKeyInfo.Status.APPROVED,
        ),
        (
            SGT_HARTMANN_FINGERPRINT,
            SGT_HARTMANN_EMAIL,
            SGT_HARTMANN_USER_ID,
            "sgt_hartmann",
            PgpKeyInfo.Status.REJECTED,
        ),
    ):
        user = user_factory(username=username)
        pgp_key_info_factory(
            fingerprint=fingerprint,
            key_email=key_email,
            key_user_id=key_user_id,
            user=user,
            status=status,
        )

    request = rf.get("/")
    request.query_params = query_params
    queryset = PgpKeyInfo.objects.all()
    q = PgpKeyInfoLookupFilter().filter_queryset(request, queryset, None)
    assert len(q) == expected_length
