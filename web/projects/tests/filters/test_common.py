from unittest import mock
import factory
import pytest

from rest_framework import generics, permissions, serializers

from projects.filters.common import (
    BaseLookupFilter,
    CaseInsensitiveOrderingFilter,
    QueryParam,
)
from projects.models.project import Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = "__all__"


class OrderingListView(generics.ListAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.AllowAny,)
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("name",)


class TestCaseInsensitiveOrderingFilter:
    names = (
        "aBc",
        "Bcd",
        "cde",
        "AaC",
        "AbC",
        "AbD",
        "-AB",
        "-AC",
        "AA",
        "BB",
        "ÇA",
        "CB",
        "çc",
        "DD",
        "A̧A",
        "B̧B",
        "ḐD",
        "łó",
        "Ęt",
        "abc",
        "Abc",
        "abC",
        "ABC",
        "Test 1",
        "Test 2",
        "Test 3",
    )
    test_input_orderby_expected = (
        (names, "name", sorted(names, key=str.lower)),
        (names, "-name", sorted(names, key=str.lower, reverse=True)),
    )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "test_input, order_by, expected", test_input_orderby_expected
    )
    def test_ordering(self, test_input, order_by, expected, rf, project_factory):
        project_factory.create_batch(len(test_input), name=factory.Iterator(test_input))
        view = OrderingListView.as_view()
        request = rf.get("/", {"ordering": order_by})
        response = view(request)
        assert [x["name"] for x in response.data] == expected


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_lookups",
    [
        [{}, {}],
        [{"first_name": "Harry"}, {"first_name__iexact": "Harry"}],
        [
            {"first_name": "Harry", "last_name": "Potter"},
            {"first_name__iexact": "Harry", "last_name__iexact": "Potter"},
        ],
        [
            {"last_name": "", "age": ""},
            {"last_name__iexact": "", "age__isnull": True},
        ],
        [{"animagus": ""}, {"animagus__species__iexact": ""}],
    ],
)
def test_base_lookup_filter(query_params, expected_lookups, rf):
    request = rf.get("/")
    request.query_params = query_params
    mock_queryset = mock.Mock()
    base_lookup_filter = BaseLookupFilter()
    base_lookup_filter.query_params = [
        QueryParam("first_name", "first_name__iexact", "first_name"),
        QueryParam("last_name", "last_name__iexact", "last_name"),
        QueryParam("age", "age__iexact", "age", "integer"),
        QueryParam("animagus", "animagus__species__iexact", "animagus"),
    ]
    base_lookup_filter.filter_queryset(request, mock_queryset, None)

    # pylint: disable=protected-access
    assert base_lookup_filter._get_lookups(request) == expected_lookups

    if query_params:
        mock_queryset.filter.assert_called()
        mock_queryset.all.assert_not_called()
    else:
        mock_queryset.filter.assert_not_called()
        mock_queryset.all.assert_called()
