from projects.models.data_transfer import DataTransfer
from projects.oauth import PolicyClaim, NameClaim
from projects.tests.factories import make_data_provider_data_engineer


def test_policy_claim(user_factory, data_transfer_factory):
    user = user_factory()
    policy_claim = PolicyClaim()

    dt = data_transfer_factory(status=DataTransfer.AUTHORIZED)
    make_data_provider_data_engineer(user, dt.data_provider)

    assert policy_claim.get_claim(user, dt) == "dataProvider"


def test_name_claim(user_factory, data_transfer_factory):
    user = user_factory()
    name_claim = NameClaim()

    dt = data_transfer_factory(status=DataTransfer.AUTHORIZED)
    make_data_provider_data_engineer(user, dt.data_provider)

    assert name_claim.get_claim(user, dt) == dt.project.code
