from unittest.mock import DEFAULT, patch
import pytest

from django.contrib.auth import get_user_model
from django_drf_utils.serializers.utils import ValidationError

from projects.serializers import pgp
from projects.models.pgp import PgpKeyInfo
from projects.serializers.pgp import PgpKeyInfoSerializer
from projects.utils.pgp import KeyMetadata, KeyStatus


User = get_user_model()


def test_notify_new_pgp_key_approval_request(profile_factory) -> None:
    """Test that an email is sent when a new PgpKeyInfo is created"""
    anonymous_user = User.objects.get(id=1)
    profile_factory(emails=anonymous_user.email, user=anonymous_user)

    with patch.multiple(
        pgp,
        sendmail=DEFAULT,
        get_request_username=lambda _: anonymous_user,
        retrieve_key_metadata=lambda _: KeyMetadata(
            status=KeyStatus.VERIFIED,
            user_id="Chuck Norris",
            email=anonymous_user.email,
        ),
    ) as mocks:
        PgpKeyInfoSerializer().create({"fingerprint": "0123456789ABCDEF"})

        assert mocks["sendmail"].called


@pytest.mark.parametrize(
    ("old_status", "new_status", "should_send_email"),
    (
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.APPROVED, True),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.REJECTED, True),
        (PgpKeyInfo.Status.PENDING, PgpKeyInfo.Status.DELETED, False),
    ),
)
def test_notify_user_for_approval_or_rejection(
    old_status: PgpKeyInfo.Status,
    new_status: PgpKeyInfo.Status,
    should_send_email: bool,
    pgp_key_info_factory,
) -> None:
    """Test that an email is sent when a PgpKeyInfo is approved or rejected"""

    pgp_key_info: PgpKeyInfo = pgp_key_info_factory(status=old_status)

    with patch.multiple(pgp, sendmail=DEFAULT, retrieve_key_metadata=DEFAULT) as mocks:
        PgpKeyInfoSerializer().update(pgp_key_info, {"status": new_status})

        # Email should be sent upon update only if the request has been approved
        # or rejected
        assert should_send_email == mocks["sendmail"].called


@pytest.mark.parametrize(
    ("user_emails", "should_raise_exception"),
    (
        ("correct@email.com,wrong@email.com", False),
        ("wrong@email.com", True),
    ),
)
def test_email_validation(user_emails, should_raise_exception, user_factory) -> None:
    user = user_factory()
    user.profile.emails = user_emails

    with patch.multiple(
        pgp,
        get_request_username=lambda _: user,
        retrieve_key_metadata=lambda _: KeyMetadata(
            status=KeyStatus.VERIFIED,
            user_id="Chuck Norris",
            email="correct@email.com",
        ),
    ):
        try:
            PgpKeyInfoSerializer().create({"fingerprint": "0123456789ABCDEF"})
            assert not should_raise_exception
        except ValidationError:
            assert should_raise_exception
