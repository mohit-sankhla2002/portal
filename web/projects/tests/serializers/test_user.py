import pytest
from projects.serializers.user import UserSerializer

from ..factories import UserFactory


@pytest.mark.django_db
def test_user_local_username():
    user = UserFactory()
    profile = user.profile
    new_local_username = "some_new_local_username"
    # pylint: disable=protected-access
    UserSerializer._set_local_username(
        profile=profile, new_local_username=new_local_username
    )
    assert profile.local_username == new_local_username
    assert profile.namespace is not None


@pytest.mark.django_db
def test_user_uid():
    user = UserFactory()
    profile = user.profile
    prev_uid = profile.uid
    # pylint: disable=protected-access
    UserSerializer._next_uid(profile=profile)
    assert prev_uid is not profile.uid
    assert isinstance(profile.uid, int)

    # once set, uid should not increase
    prev_uid = profile.uid
    # pylint: disable=protected-access
    UserSerializer._next_uid(profile=profile)
    assert profile.uid == prev_uid


@pytest.mark.django_db
def test_user_gid():
    user = UserFactory()
    profile = user.profile
    prev_gid = profile.gid
    # pylint: disable=protected-access
    UserSerializer._next_gid(profile=profile)
    assert prev_gid is not profile.gid
    assert isinstance(profile.gid, int)

    # once set, gid should not increase
    prev_gid = profile.gid
    # pylint: disable=protected-access
    UserSerializer._next_gid(profile=profile)
    assert profile.gid == prev_gid
