import pytest
from django_drf_utils.config import Email

from projects.models.approval import DataProviderApproval, GroupApproval, NodeApproval
from projects.models.data_transfer import DataTransfer
from projects.models.message import Message
from projects.models.project import ProjectRole
from projects.notifications.data_transfer import (
    create_data_transfer_creation_notification,
)
from projects.serializers.data_transfer import DataTransferSerializer
from ..factories import (
    make_data_provider_coordinator,
    make_legal_approval_group,
    make_node_admin,
)


@pytest.mark.parametrize(
    "purpose",
    (DataTransfer.TEST, DataTransfer.PRODUCTION),
)
def test_notify_on_creation(
    data_transfer_factory,
    project_user_role_factory,
    user_factory,
    settings,
    mailoutbox,
    purpose,
    group_factory,
    project_factory,
):
    ticketing_system_email = "chuck@example.org"
    settings.CONFIG.notification.ticket_mail = ticketing_system_email
    settings.CONFIG.email = Email.empty()
    dtr: DataTransfer = data_transfer_factory(
        purpose=purpose,
        project=project_factory(legal_approval_group=make_legal_approval_group()),
    )
    node_destination = dtr.project.destination
    node_transit = dtr.data_provider.node
    project_user_role_factory(project=dtr.project, role=ProjectRole.PL.value)
    (
        coordinator,
        na1_destination,
        na2_destination,
        na_transfer,
        legal_approval_user,
    ) = user_factory.create_batch(5)
    make_data_provider_coordinator(coordinator, dtr.data_provider)
    group = group_factory()
    make_node_admin(na1_destination, node_destination, group)
    make_node_admin(na2_destination, node_destination, group)
    make_node_admin(na_transfer, node_transit, group)
    dtr.project.legal_approval_group.user_set.add(legal_approval_user)

    url = "https://portal.test"
    DataTransferSerializer.create_approvals(dtr)
    DataTransferSerializer.notify_on_creation(dtr, url)

    subject, body = create_data_transfer_creation_notification(dtr)
    # Check if messages are generated
    messages = Message.objects.all()
    assert len(messages) == 5 if purpose == DataTransfer.TEST else 6
    expected = {
        coordinator,
        na1_destination,
        na2_destination,
        na_transfer,
        dtr.requestor,
    } | ({legal_approval_user} if purpose == DataTransfer.PRODUCTION else set())
    assert {m.user for m in messages} == expected
    # Check if all messages are identical
    assert {m.title for m in messages} == {subject}
    assert len({m.body for m in messages}) == 1
    assert messages[0].body.startswith(body.replace("\n", "\r\n"))
    assert messages[0].body.endswith(f"[Go to the DTR](data-transfers/{dtr.id})")
    # Check if emails are being sent
    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    assert m.subject == subject
    assert m.body.startswith(body)
    assert f"{url}/data-transfers/{dtr.id}" in m.body
    expected_recipients = (
        {
            u.email
            for u in (
                coordinator,
                na1_destination,
                na2_destination,
                na_transfer,
                dtr.requestor,
            )
        }
        | {
            node_destination.ticketing_system_email,
            node_transit.ticketing_system_email,
            ticketing_system_email,
        }
        | ({legal_approval_user.email} if purpose == DataTransfer.PRODUCTION else set())
    )
    assert set(m.bcc) == expected_recipients


@pytest.mark.parametrize(
    "purpose, has_legal_approval_group",
    (
        (DataTransfer.TEST, False),
        (DataTransfer.PRODUCTION, False),
        (DataTransfer.PRODUCTION, True),
    ),
)
def test_create_approvals(
    purpose,
    has_legal_approval_group,
    node_factory,
    project_factory,
    data_transfer_factory,
    data_provider_factory,
):
    local, remote = node_factory.create_batch(2)
    legal_approval_group = make_legal_approval_group()
    project = project_factory(
        legal_approval_group=legal_approval_group if has_legal_approval_group else None,
        destination=local,
    )
    data_provider = data_provider_factory(node=remote)
    DataTransferSerializer.create_approvals(
        data_transfer_factory(
            project=project,
            purpose=purpose,
            data_provider=data_provider,
        )
    )

    data_provider_approvals = DataProviderApproval.objects.all()
    assert len(data_provider_approvals) == 1
    assert data_provider_approvals[0].data_provider == data_provider

    assert len(NodeApproval.objects.all()) == 2
    assert NodeApproval.objects.filter(
        node=local, type=NodeApproval.Type.HOSTING
    ).exists()
    assert NodeApproval.objects.filter(
        node=remote, type=NodeApproval.Type.TRANSFER
    ).exists()

    assert len(GroupApproval.objects.all()) == int(
        has_legal_approval_group and purpose == DataTransfer.PRODUCTION
    )
    if has_legal_approval_group:
        assert GroupApproval.objects.filter(group=legal_approval_group).exists()
