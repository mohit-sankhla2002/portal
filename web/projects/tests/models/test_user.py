import pytest
from django.conf import settings
from django_drf_utils.tests.models.utils import validate_field

from projects.models.user import (
    Profile,
    UserNamespace,
    is_legal_approver,
    get_legal_approval_role_groups,
)
from projects.tests.factories import make_legal_approver


def test_user_namespace_str():
    assert str(UserNamespace(name="ch")) == "Namespace (ch)"


@pytest.mark.parametrize(
    "test_input, expected",
    (
        (
            ("cnorris", "Chuck", "Norris", "cnorris@roundhouse.gov", None, None),
            ("Chuck Norris (cnorris@roundhouse.gov)", ""),
        ),
        (
            (
                "CNORRIS@roundhouse.gov",
                None,
                None,
                "cnorris@roundhouse.gov",
                "chuck",
                "ch",
            ),
            ("chuck (cnorris@roundhouse.gov)", "ch_chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", None),
            ("chuck", "chuck"),
        ),
        (
            ("CNORRIS@roundhouse.gov", None, None, None, "chuck", "ch"),
            ("chuck", "ch_chuck"),
        ),
    ),
)
def test_profile_display(profile_factory, user_factory, test_input, expected):
    namespace = None if test_input[5] is None else UserNamespace(name=test_input[5])
    profile = profile_factory.build(
        user=user_factory.build(
            username=test_input[0],
            first_name=test_input[1],
            last_name=test_input[2],
            email=test_input[3],
        ),
        local_username=test_input[4],
        namespace=namespace,
    )
    assert profile.display_name == expected[0]
    assert profile.display_local_username == expected[1]


@pytest.mark.parametrize(
    "test_input, expected",
    (
        # "local_user" cannot start with a number, not with a capital.
        ("1username", False),
        ("Code", False),
        # Valid "local_user" values.
        ("code", True),
        ("cod3", True),
        # Only "_" is allowed as special character.
        ("bad_u$er_name", False),
        ("bad-user-name", False),
        ("bad user name", False),
        ("bad\tuser\tname", False),
        # "local_username" of less <= 64 characters are allowed.
        ("user_name_with_a_length_of_exactly_than_64_characters_true_story", True),
        # "local_username" with > 64 characters are allowed.
        (
            "this_really_is_a_user_name_that_is_longer_than_64_characters_isnt_it",
            False,
        ),
    ),
)
def test_profile_local_username(test_input, expected):
    validate_field(Profile, "local_username", test_input, expected)


def test_profile_str(profile_factory, user_factory):
    assert (
        str(profile_factory.build(user=user_factory.build(username="bob")))
        == "Profile (bob)"
    )


@pytest.mark.usefixtures("apply_flags_settings")
@pytest.mark.parametrize(
    "flag_exists, has_flag, expected",
    ((False, False, False), (True, False, False), (True, True, True)),
)
def test_authorized(flag_exists, has_flag, expected, flag_factory, user_factory):
    user = user_factory()
    wrong_flag = flag_factory(code="wrong")
    wrong_flag.users.add(user)

    if flag_exists:
        # pylint: disable=no-member
        flag = flag_factory(code=settings.CONFIG.flags.authorized_user)
        if has_flag:
            flag.users.add(user)

    assert user.profile.authorized == expected


def test_is_legal_approver(user_factory):
    user = user_factory()
    assert not is_legal_approver(user)
    user = make_legal_approver(user)
    assert is_legal_approver(user)


def test_get_legal_approval_role_groups(user_factory, group_factory):
    legal_approver, group_user = user_factory.create_batch(2)
    make_legal_approver(legal_approver)

    group = group_factory()
    group.user_set.add(legal_approver, group_user)

    assert len(get_legal_approval_role_groups(legal_approver)) == 1
    assert len(get_legal_approval_role_groups(group_user)) == 0
