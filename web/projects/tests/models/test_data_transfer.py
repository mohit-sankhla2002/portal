def test_approvals(node_approval_factory):
    node_approval = node_approval_factory()
    data_transfer = node_approval.data_transfer
    assert len(data_transfer.approvals) == 1
    assert data_transfer.approvals[0] == node_approval
