from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import TestCase

from identities.models import GroupProfile
from projects.models.node import (
    Node,
    create_node_groups,
    get_node_admin_nodes,
    get_node_role_users,
    get_node_viewer_nodes,
    has_node_admin_nodes,
    has_node_admin_permission,
    has_node_viewer_nodes,
    has_node_viewer_permission,
)
from ..factories import NodeFactory, UserFactory, make_node_admin, make_node_viewer

User = get_user_model()


def test_node_str():
    node = Node(code="node_1", name="HPC center 1", node_status=Node.STATUS_ONLINE)
    assert str(node) == f"{node.name} ({node.code})"


class TestNodeViewer(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_viewer = make_node_viewer(node=self.node)
        self.staff = UserFactory(staff=True)

    def test_get_node_viewer_nodes(self):
        assert tuple(get_node_viewer_nodes(self.node_viewer)) == (self.node,)
        node2 = NodeFactory.create()
        make_node_viewer(self.node_viewer, node2)
        assert tuple(get_node_viewer_nodes(self.node_viewer)) == (self.node, node2)
        assert not get_node_viewer_nodes(self.staff).exists()

    def test_has_node_viewer_nodes(self):
        self.assertTrue(has_node_viewer_nodes(self.node_viewer))
        self.assertFalse(has_node_viewer_nodes(self.staff))
        make_node_viewer(self.staff, self.node)
        self.assertTrue(has_node_viewer_nodes(self.staff))

    def test_has_node_viewer_permission(self):
        # only `node_viewer` has node viewer permission for `node`
        self.assertTrue(has_node_viewer_permission(self.node_viewer, self.node))
        self.assertFalse(has_node_viewer_permission(self.staff, self.node))
        # `node_viewer` does NOT have node viewer permission for all nodes
        self.assertFalse(has_node_viewer_permission(self.node_viewer))


class TestNodeAdmin(TestCase):
    def setUp(self):
        self.node = NodeFactory.create()
        self.node_admin = make_node_admin(node=self.node)
        self.staff = UserFactory(staff=True)

    def test_get_node_admin_nodes(self):
        assert tuple(get_node_admin_nodes(self.node_admin)) == (self.node,)
        node2 = NodeFactory.create()
        make_node_admin(self.node_admin, node2)
        assert tuple(get_node_admin_nodes(self.node_admin)) == (self.node, node2)
        assert not get_node_admin_nodes(self.staff).exists()

    def test_has_node_admin_nodes(self):
        self.assertTrue(has_node_admin_nodes(self.node_admin))
        self.assertFalse(has_node_admin_nodes(self.staff))
        make_node_admin(self.staff, self.node)
        self.assertTrue(has_node_admin_nodes(self.staff))

    def test_has_node_admin_permission(self):
        # only `node_admin` has node admin permission for `node`
        self.assertTrue(has_node_admin_permission(self.node_admin, self.node))
        self.assertFalse(has_node_admin_permission(self.staff, self.node))
        # `node_admin` does NOT have node admin permission for all nodes
        self.assertFalse(has_node_admin_permission(self.node_admin))


def test_get_node_role_users(node_factory, user_factory):
    node = node_factory()

    assert not get_node_role_users(node, GroupProfile.Role.NODE_ADMIN)

    user = user_factory()
    node_admin = make_node_admin(node=node)
    node_viewer = make_node_viewer(node=node)
    node_admins = get_node_role_users(node, GroupProfile.Role.NODE_ADMIN)

    assert node_admin in node_admins
    assert not node_viewer in node_admins
    assert not user in node_admins


def test_create_node_groups(node_factory):
    node_roles = {
        GroupProfile.Role.NODE_VIEWER,
        GroupProfile.Role.NODE_ADMIN,
        GroupProfile.Role.NODE_MANAGER,
    }

    node = node_factory()
    create_node_groups(None, node, True)

    groups = Group.objects.all()
    group_profiles = GroupProfile.objects.all()

    assert len(groups) == len(group_profiles) == len(node_roles)
    assert set(groups) == {profile.group for profile in group_profiles}
    assert node_roles == {profile.role for profile in group_profiles}

    for profile in group_profiles:
        assert profile.role_entity == node
        assert profile.role_entity_type.model == GroupProfile.RoleEntityType.NODE.value
