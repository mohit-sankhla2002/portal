from django.contrib.auth.models import Group
from django.test import TestCase

from identities.models import GroupProfile
from projects.models.data_provider import (
    get_data_provider_admin_data_providers,
    get_data_provider_coordinators,
    get_data_provider_viewer_data_providers,
    has_data_provider_admin_data_providers,
    has_data_provider_admin_permission,
    has_data_provider_viewer_data_providers,
    has_dp_viewer_role,
    has_dp_coordinator_role,
    assign_dp_viewer_role,
    get_data_provider_role_group,
    get_data_provider_role_users,
    create_dp_groups,
)
from ..factories import (
    UserFactory,
    DataProviderFactory,
    make_data_provider_admin,
    make_data_provider_coordinator,
    make_data_provider_viewer,
)


class TestDataProviderCoordinator(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.coordinator = UserFactory()
        make_data_provider_coordinator(self.coordinator, self.dp)

    def test_get_data_provider_coordinator_data_providers(self):
        assert tuple(get_data_provider_viewer_data_providers(self.coordinator)) == (
            self.dp,
        )

    def test_has_dp_coordinator_role(self):
        assert has_dp_coordinator_role(self.coordinator, self.dp)


def test_get_data_provider_coordinators(data_provider_factory, user_factory):
    dp = data_provider_factory()
    user1, user2, *_ = user_factory.create_batch(4)
    make_data_provider_coordinator(user1, dp)
    coordinators = get_data_provider_coordinators(dp)
    assert len(coordinators) == 1
    assert coordinators[0] == user1
    get_data_provider_role_group(
        dp, GroupProfile.Role.DATA_PROVIDER_COORDINATOR
    ).user_set.add(user2)
    assert all(u in get_data_provider_coordinators(dp) for u in (user1, user2))


def test_get_data_provider_coordinators_different_groups(data_provider_factory):
    dp1, dp2 = data_provider_factory.create_batch(2)
    make_data_provider_coordinator(data_provider=dp1)
    make_data_provider_viewer(data_provider=dp2)
    assert len(get_data_provider_coordinators(dp1)) == 1
    assert len(get_data_provider_coordinators(dp2)) == 0


def test_superuser_dp_viewer_not_coordinator(
    data_provider_factory, group_factory, user_factory
):
    dp = data_provider_factory()
    superuser = user_factory(staff=True, is_superuser=True)
    group = group_factory()
    group.user_set.add(superuser)
    assign_dp_viewer_role(group, dp)
    assert len(get_data_provider_coordinators(dp)) == 0


class TestDataProviderViewer(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.viewer = UserFactory()
        make_data_provider_viewer(self.viewer, self.dp)
        self.dp_viewer_group = get_data_provider_role_group(
            self.dp, GroupProfile.Role.DATA_PROVIDER_VIEWER
        )
        self.staff = UserFactory(staff=True)

    def test_get_dp_viewer_dps(self):
        assert tuple(get_data_provider_viewer_data_providers(self.viewer)) == (self.dp,)

        dp2 = DataProviderFactory()
        make_data_provider_viewer(self.viewer, dp2)

        assert tuple(get_data_provider_viewer_data_providers(self.viewer)) == (
            self.dp,
            dp2,
        )

        assert not get_data_provider_viewer_data_providers(self.staff).exists()

    def test_has_dp_viewer_dps(self):
        assert has_data_provider_viewer_data_providers(self.viewer)
        assert not has_data_provider_viewer_data_providers(self.staff)
        self.dp_viewer_group.user_set.add(self.staff)
        assert has_data_provider_viewer_data_providers(self.staff)

    def test_has_dp_viewer_permission(self):
        # only `dataprovider_viewer` has data provider viewer permission for `dp`
        assert has_dp_viewer_role(self.viewer, self.dp)
        assert not has_dp_viewer_role(self.staff, self.dp)
        # `dataprovider_viewer` does NOT have data provider viewer permission
        # for all data providers
        assert not has_dp_viewer_role(self.viewer)


class TestDataProviderAdmin(TestCase):
    def setUp(self):
        self.dp = DataProviderFactory()
        self.dp_admin = UserFactory()
        make_data_provider_admin(self.dp_admin, self.dp)
        self.dp_admin_group = get_data_provider_role_group(
            self.dp, GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN
        )
        self.staff = UserFactory(staff=True)

    def test_get_dp_admin_dps(self):
        assert tuple(get_data_provider_admin_data_providers(self.dp_admin)) == (
            self.dp,
        )

        dp2 = DataProviderFactory()
        make_data_provider_admin(self.dp_admin, dp2)
        assert tuple(get_data_provider_admin_data_providers(self.dp_admin)) == (
            self.dp,
            dp2,
        )

        assert not get_data_provider_admin_data_providers(self.staff).exists()

    def test_has_dp_admin_dps(self):
        assert has_data_provider_admin_data_providers(self.dp_admin)
        assert not has_data_provider_admin_data_providers(self.staff)
        self.dp_admin_group.user_set.add(self.staff)
        assert has_data_provider_admin_data_providers(self.staff)

    def test_has_dp_admin_permission(self):
        # only `dataprovider_admin` has data provider admin permission for `dp`
        assert has_data_provider_admin_permission(self.dp_admin, self.dp)
        assert not has_data_provider_admin_permission(self.staff, self.dp)
        # `dataprovider_admin` does NOT have data provider admin permission
        # for all data providers
        assert not has_data_provider_admin_permission(self.dp_admin)


def test_get_data_provider_role_users(data_provider_factory):
    data_provider = data_provider_factory()

    assert not get_data_provider_role_users(
        data_provider, GroupProfile.Role.DATA_PROVIDER_COORDINATOR
    ).exists()

    make_data_provider_coordinator(data_provider=data_provider)

    assert (
        len(
            get_data_provider_role_users(
                data_provider, GroupProfile.Role.DATA_PROVIDER_COORDINATOR
            )
        )
        == 1
    )


def test_create_dp_groups(data_provider_factory):
    dp_roles = {
        GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
        GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER,
        GroupProfile.Role.DATA_PROVIDER_MANAGER,
        GroupProfile.Role.DATA_PROVIDER_SECURITY_OFFICER,
        GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN,
        GroupProfile.Role.DATA_PROVIDER_VIEWER,
    }

    data_provider = data_provider_factory()
    create_dp_groups(None, data_provider, True)

    groups = Group.objects.all()
    group_profiles = GroupProfile.objects.all()

    assert len(groups) == len(group_profiles) == len(dp_roles)
    assert set(groups) == {profile.group for profile in group_profiles}
    assert dp_roles == {profile.role for profile in group_profiles}

    for profile in group_profiles:
        assert profile.role_entity == data_provider
        assert (
            profile.role_entity_type.model
            == GroupProfile.RoleEntityType.DATA_PROVIDER.value
        )
