import pytest
from django.conf import settings

from projects.models.approval import BaseApproval
from projects.models.data_transfer import DataTransfer
from projects.notifications.dtr_notification import (
    DtrApprovedNotification,
    DtrAuthorizedNotification,
    DtrCreatedNotification,
    DtrRejectedNotification,
)
from projects.tests.factories import (
    make_data_provider_coordinator,
    make_legal_approval_group,
    make_node_admin,
)


@pytest.mark.parametrize(
    "approval_status, dtr_status, DtrNotificationClass, purpose, has_legal_approval_group",
    [
        (
            BaseApproval.ApprovalStatus.WAITING,
            DataTransfer.INITIAL,
            DtrCreatedNotification,
            DataTransfer.TEST,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.WAITING,
            DataTransfer.INITIAL,
            DtrCreatedNotification,
            DataTransfer.PRODUCTION,
            True,
        ),
        (
            BaseApproval.ApprovalStatus.WAITING,
            DataTransfer.INITIAL,
            DtrCreatedNotification,
            DataTransfer.PRODUCTION,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.INITIAL,
            DtrApprovedNotification,
            DataTransfer.TEST,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.INITIAL,
            DtrApprovedNotification,
            DataTransfer.PRODUCTION,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.INITIAL,
            DtrApprovedNotification,
            DataTransfer.PRODUCTION,
            True,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.AUTHORIZED,
            DtrAuthorizedNotification,
            DataTransfer.TEST,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.AUTHORIZED,
            DtrAuthorizedNotification,
            DataTransfer.PRODUCTION,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.APPROVED,
            DataTransfer.AUTHORIZED,
            DtrAuthorizedNotification,
            DataTransfer.PRODUCTION,
            True,
        ),
        (
            BaseApproval.ApprovalStatus.REJECTED,
            DataTransfer.UNAUTHORIZED,
            DtrRejectedNotification,
            DataTransfer.TEST,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.REJECTED,
            DataTransfer.UNAUTHORIZED,
            DtrRejectedNotification,
            DataTransfer.PRODUCTION,
            False,
        ),
        (
            BaseApproval.ApprovalStatus.REJECTED,
            DataTransfer.UNAUTHORIZED,
            DtrRejectedNotification,
            DataTransfer.PRODUCTION,
            True,
        ),
    ],
)
def test_email_recipients(
    approval_status,
    dtr_status,
    DtrNotificationClass,
    purpose,
    has_legal_approval_group,
    user_factory,
    node_factory,
    data_transfer_factory,
    project_factory,
    data_provider_factory,
    node_approval_factory,
    group_factory,
):
    """Test that the list of email recipients is correct"""
    local, remote = nodes = node_factory.create_batch(2)
    approver, *_ = node_admins = user_factory.create_batch(2)
    for node, node_admin in zip(nodes, node_admins):
        group = group_factory()
        make_node_admin(node_admin, node, group)
        group.user_set.add(node_admin)

    data_provider = data_provider_factory(node=local)
    dp_coordinator = user_factory()
    make_data_provider_coordinator(dp_coordinator, data_provider)
    legal_approval_group = make_legal_approval_group()
    project = project_factory(
        destination=remote,
        legal_approval_group=legal_approval_group if has_legal_approval_group else None,
    )
    if has_legal_approval_group:
        legal_approval_user = user_factory()
        legal_approval_group.user_set.add(legal_approval_user)
    requestor = user_factory()

    dtr = data_transfer_factory(
        project=project,
        data_provider=data_provider,
        requestor=requestor,
        status=dtr_status,
        purpose=purpose,
    )

    approval, *_ = (
        node_approval_factory(node=node, data_transfer=dtr, status=approval_status)
        for node in nodes
    )
    default_recipients = set(node_admins).union({dp_coordinator, requestor})
    if (
        purpose == DataTransfer.PRODUCTION
        and DtrNotificationClass == DtrCreatedNotification
        and has_legal_approval_group
    ):
        default_recipients.add(legal_approval_user)
    approved = approval_status == BaseApproval.ApprovalStatus.APPROVED
    authorized = dtr_status == DataTransfer.AUTHORIZED
    partially_approved = approved and not authorized

    dtr_notification = (
        DtrNotificationClass(
            "http://example.ch",
            approval,
            approver,
        )
        if DtrNotificationClass is DtrApprovedNotification
        or DtrNotificationClass is DtrRejectedNotification
        else DtrNotificationClass(dtr, "http://example.ch")
    )
    recipients = dtr_notification.email_recipients()

    # Test that all default recipients are included. Note that for all
    # approval type that correspond to the start or end of the approval
    # process (so all except DtrApprovedNotification, which corresponds to
    # just a part of the process), an email is also sent to the ticketing
    # systems.
    assert all(list(user.email in recipients for user in default_recipients))
    default_recipients_length = len(default_recipients)
    if DtrNotificationClass is not DtrApprovedNotification:
        # 3 additional emails are sent for the 3 ticketing systems.
        default_recipients_length += 3
    assert len(recipients) == default_recipients_length

    # Test that DCC ticketing system is included except for partial approvals
    # pylint: disable=no-member
    assert (
        settings.CONFIG.notification.ticket_mail in recipients
    ) != partially_approved

    # Test that nodes ticketing systems are NOT included if the DTR is approved but not yet authorized
    assert all(
        (node.ticketing_system_email in recipients) != partially_approved
        for node in nodes
    )
