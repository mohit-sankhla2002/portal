import pytest
from django.contrib.auth import get_user_model

from projects.models.pgp import PgpKeyInfo
from projects.notifications.pgp_key_info import (
    create_pgp_key_info_new_approval_request_notification,
    create_pgp_key_info_approved_or_rejected_notification,
    pgp_key_info_status_all_updated,
    pgp_key_info_status_updated,
)

User = get_user_model()


def test_create_pgp_key_info_new_approval_request_notification(
    pgp_key_info_factory,
) -> None:
    """Test that subject and body are generated correctly"""

    pgp_key_info = pgp_key_info_factory()
    subject, body = create_pgp_key_info_new_approval_request_notification(pgp_key_info)
    assert pgp_key_info.user.profile.display_name in subject
    for s in (
        pgp_key_info.user.profile.display_name,
        pgp_key_info.key_user_id,
        pgp_key_info.key_email,
        pgp_key_info.fingerprint,
    ):
        assert s in body


@pytest.mark.parametrize(
    "status, expected, should_raise_exception",
    (
        (PgpKeyInfo.Status.APPROVED, "approved", False),
        (PgpKeyInfo.Status.REJECTED, "rejected", False),
        (PgpKeyInfo.Status.PENDING, "rejected", True),
    ),
)
def test_create_pgp_key_info_approved_or_rejected_notification(
    status: PgpKeyInfo.Status,
    expected: str,
    should_raise_exception: bool,
    pgp_key_info_factory,
) -> None:
    """Test that subject and body are generated correctly"""

    pgp_key_info = pgp_key_info_factory(status=status)

    try:
        subject, body = create_pgp_key_info_approved_or_rejected_notification(
            pgp_key_info
        )
        assert expected in subject
        for s in (
            expected,
            pgp_key_info.key_user_id,
            pgp_key_info.key_email,
            pgp_key_info.fingerprint,
        ):
            assert s in body
    except ValueError:
        assert should_raise_exception


def test_pgp_key_info_status_all_updated(
    pgp_key_info_factory,
) -> None:
    all_keys = pgp_key_info_factory.create_batch(3)
    subject, body = pgp_key_info_status_all_updated(all_keys)
    assert "3" in subject
    key = all_keys[0]
    assert f"- {key.fingerprint} ({key.key_user_id} <{key.key_email}>)" in body


def test_pgp_key_info_status_updated(
    pgp_key_info_factory,
) -> None:
    pgp_key_info = pgp_key_info_factory()
    subject, body = pgp_key_info_status_updated(pgp_key_info)
    assert pgp_key_info.fingerprint in subject
    assert pgp_key_info.key_user_id in body
    assert pgp_key_info.status in body
