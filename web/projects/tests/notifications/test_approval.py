from unittest import mock

import pytest
from django.conf import settings

from projects.models.approval import DataProviderApproval, NodeApproval, GroupApproval
from projects.models.data_transfer import DataTransfer
from projects.models.project import ProjectRole
from projects.notifications import approval
from projects.notifications.approval import (
    create_reject_notification,
    create_approve_notification,
    create_final_approval_notification,
)
from ..factories import (
    make_legal_approval_group,
    make_project_user,
    make_data_provider_coordinator,
)


@pytest.mark.parametrize(
    "node_approval_type, dtr_purpose, project_space_ready, legal_basis",
    (
        (
            NodeApproval.Type.HOSTING,
            DataTransfer.PRODUCTION,
            True,
            True,
        ),
        (
            NodeApproval.Type.TRANSFER,
            DataTransfer.TEST,
            False,
            False,
        ),
    ),
)
def test_create_approve_notification(
    node_approval_type,
    dtr_purpose,
    project_space_ready,
    legal_basis,
    data_transfer_factory,
    data_provider_approval_factory,
    node_approval_factory,
    user_factory,
):
    dtr = data_transfer_factory(purpose=dtr_purpose)
    data_provider_approval = data_provider_approval_factory(data_transfer=dtr)
    node_approval = node_approval_factory(data_transfer=dtr, type=node_approval_type)
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, data_provider_approval.data_provider)
    legal_basis_text = "There is a legal basis for the Data Transfer:"
    project_space_ready_text = (
        "A project space in the BioMedIT environment has been setup: Yes"
    )

    with mock.patch.object(approval, "project_metadata") as mock_method:
        dp_subject, dp_body = create_approve_notification(
            data_provider_approval, coordinator
        )
        node_subject, node_body = create_approve_notification(
            node_approval, coordinator
        )

        # project_metadata is always called
        assert mock_method.call_count == 2

    for subject in (dp_subject, node_subject):
        # Subject is always the same
        assert subject.startswith(
            f"[DTR-{data_provider_approval.data_transfer.id}] - Data Transfer Request "
            f"from '{data_provider_approval.data_provider.name}'"
        )

    for body, institution in zip(
        (dp_body, node_body), (data_provider_approval.data_provider, node_approval.node)
    ):
        # Body always contains the institution
        assert str(institution) in body
        # Body always contains the user
        assert str(coordinator) in body
        # Legal basis is Yes only if purpose is PRODUCTION
        assert f"{legal_basis_text} {'Yes' if legal_basis else 'No'}" in body

        # Project space ready is present only for the hosting node
    assert not project_space_ready_text in dp_body
    if project_space_ready:
        assert project_space_ready_text in node_body
    else:
        assert not project_space_ready_text in node_body


def test_create_reject_notification(node_approval_factory, user_factory):
    node_approval = node_approval_factory()
    user = user_factory()
    subject, body = create_reject_notification(node_approval, user)
    assert subject.startswith(
        f"[DTR-{node_approval.data_transfer.id}] - Data Transfer Request "
        f"from '{node_approval.data_transfer.data_provider.name}'"
    )
    assert f"Please be informed that the DTR 1 has been rejected by {user}." in body


def test_create_final_approval_notification(
    data_transfer_factory,
    data_provider_factory,
    project_factory,
    node_factory,
    user_factory,
):
    data_provider = data_provider_factory()
    project = project_factory()
    data_manager = make_project_user(
        project,
        ProjectRole.DM,
    )
    dtr = data_transfer_factory(data_provider=data_provider, project=project)

    # NOTE: we can't use approval factories here, they don't produce
    # historical records
    node = node_factory()
    user1, user2, legal_approval_user = user_factory.create_batch(3)
    na = NodeApproval.objects.create(
        data_transfer=dtr,
        node=node,
        type=NodeApproval.Type.HOSTING,
        status=NodeApproval.ApprovalStatus.APPROVED,
    )
    na_history_latest = na.history.latest()
    na_history_latest.history_user = user1
    na_history_latest.save()
    dpa = DataProviderApproval.objects.create(
        data_transfer=dtr,
        data_provider=data_provider,
        status=DataProviderApproval.ApprovalStatus.APPROVED,
    )
    dpa_history_latest = dpa.history.latest()
    dpa_history_latest.history_user = user2
    dpa_history_latest.save()

    legal_approval_group = make_legal_approval_group()
    legal_approval_group.user_set.add(legal_approval_user)
    group_approval = GroupApproval.objects.create(
        data_transfer=dtr,
        group=legal_approval_group,
        status=DataProviderApproval.ApprovalStatus.APPROVED,
    )
    group_approval_history_latest = group_approval.history.latest()
    group_approval_history_latest.history_user = legal_approval_user
    group_approval_history_latest.save()

    subject, body = create_final_approval_notification(dtr)

    assert (
        subject
        == f"[DTR-{dtr.id}] - Approval of Data Transfer Request from {data_provider.name} to {project.name} Project"
    )
    for s in (
        f"DTR {dtr.id}",
        f"project {project.name} ({project.code})",
        f"{data_provider.name}",
        f"{data_provider.node.name}",
        f"{data_manager.profile.display_name}",
        f"{settings.CONFIG.notification.ticket_mail}",  # pylint: disable=no-member
        f"{node.name} by {user1.profile.display_name} at {na.change_date:%Y-%m-%d %H:%M:%S}",
        f"{data_provider.name} by {user2.profile.display_name} at {dpa.change_date:%Y-%m-%d %H:%M:%S}",
        f"{legal_approval_group.name} by {legal_approval_user.profile.display_name} at {group_approval.change_date:%Y-%m-%d %H:%M:%S}",
    ):
        assert s in body, f"{s} not in {body}."
