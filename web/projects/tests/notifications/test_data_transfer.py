from django.conf import settings

from projects.models.data_transfer import DataTransfer
from projects.models.project import ProjectRole
from projects.notifications.data_transfer import (
    create_data_transfer_creation_notification,
)
from projects.serializers.data_transfer import DataTransferSerializer
from ..factories import make_data_provider_coordinator


def test_data_transfer_creation_notification(
    data_transfer_factory, project_user_role_factory, user_factory
):
    dtr: DataTransfer = data_transfer_factory(purpose=DataTransfer.PRODUCTION)
    pl = project_user_role_factory(project=dtr.project, role=ProjectRole.PL.value).user
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, dtr.data_provider)
    DataTransferSerializer.create_approvals(dtr)
    subject, body = create_data_transfer_creation_notification(dtr)
    assert subject == (
        f"[DTR-{dtr.id}] - Data Transfer Request from "
        f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
    )
    assert not "Recurring" in body
    for s in (
        f"Project Lead: {pl.first_name} {pl.last_name}",
        (
            f"Data Provider: {dtr.data_provider.name}, "
            f"{coordinator.first_name} {coordinator.last_name} ({coordinator.email})"
        ),
        "One time",
        "real patient data: Yes",
        f"There is an existing legal basis for the Data Transfer ('{dtr.legal_basis}')",
        settings.CONFIG.notification.ticket_mail,  # pylint: disable=no-member
    ):
        assert s in body, f"{s} not in {body}."

    dtr.purpose = DataTransfer.TEST
    dtr.max_packages = -1
    dtr.save()
    notification = create_data_transfer_creation_notification(dtr)[1]
    assert "legal basis" not in notification
    for s in (
        "Recurring",
        "real patient data: No",
    ):
        assert s in notification
