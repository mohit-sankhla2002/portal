from ..models.project import ProjectRole, ProjectUserRole
from ..permissions import IsAuthenticated, SAFE_METHODS


class DataTransferPermission(IsAuthenticated):
    def has_permission(self, request, view):
        user = request.user
        method = request.method
        project_id = request.data.get("project")

        return super().has_permission(request, view) and (
            method in SAFE_METHODS
            or user.is_staff
            or (
                project_id is not None
                and ProjectUserRole.objects.filter(
                    user__id=user.id,
                    project_id=project_id,
                    role=ProjectRole.DM.value,
                ).exists()
                and method == "POST"
            )
        )

    def has_object_permission(self, request, view, obj):
        return request.method in SAFE_METHODS or request.user.is_staff
