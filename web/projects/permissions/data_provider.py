from rest_framework.permissions import BasePermission, SAFE_METHODS, IsAuthenticated

from . import UPDATE_METHODS
from ..models.data_provider import (
    has_any_data_provider_permissions,
    has_data_provider_admin_data_providers,
    has_data_provider_admin_permission,
    has_dp_viewer_role,
)
from ..models.node import has_any_node_permissions
from ..models.project import is_data_manager


class IsDataProviderAdmin(BasePermission):
    """Data Provider Admin can modify a data provider.

    Some fields are not-editable with this permission:
    - code
    - node
    """

    def has_permission(self, request, view):
        user = request.user
        return (
            bool(user)
            and user.is_authenticated
            and request.method in UPDATE_METHODS
            and has_data_provider_admin_data_providers(user)
        )

    def has_object_permission(self, request, view, obj):
        return has_data_provider_admin_permission(request.user, obj)


class CanViewDataProviderRoles(BasePermission):
    def has_permission(self, request, view):
        return (
            bool(request.user)
            and request.user.is_authenticated
            and request.method in SAFE_METHODS
        )

    def has_object_permission(self, request, view, obj):
        return (
            has_any_node_permissions(request.user)
            or has_dp_viewer_role(request.user, obj)
            or has_data_provider_admin_permission(request.user, obj)
        )


class ViewDataProvider(IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and (
            has_any_node_permissions(request.user)
            or has_any_data_provider_permissions(request.user)
            or is_data_manager(request.user)
        )
