from rest_framework.permissions import IsAuthenticated, BasePermission, SAFE_METHODS
from ..models.project import ProjectUserRole, ProjectRole


class SSHPublicKeyPermission(IsAuthenticated, BasePermission):
    """Normal users in projects are allowed to add/modify their own public ssh key entry"""

    def has_permission(self, request, view):
        """This is one of the first methods being called by DRF.
        - GET requests are generally allowed (and filtered accordingly in the ViewClass)
        - POST/PATCH/PUT methods are only allowed for the users's own ssh public key in this project
        - DELETE request need to be allowed in general, as the has_object_permission does the detail check
        and only if they are members of the project"""
        if request.method in SAFE_METHODS:
            return True
        if request.user.is_staff:
            return True
        if request.method == "DELETE":
            # will be handled by the has_object_permission method below
            return True
        if request.method == "POST":
            if request.data.get("user"):
                return request.user.id == request.data.get(
                    "user"
                ) and self.is_project_member(
                    project_id=request.data.get("project"), user=request.user
                )
            return False
        return True

    def is_project_member(self, project_id, user):
        return ProjectUserRole.objects.filter(project=project_id, user=user.id).exists()

    def is_project_leader(self, project_id, user):
        return ProjectUserRole.objects.filter(
            project=project_id, user=user.id, role__gt=ProjectRole.USER.value
        ).exists()

    def has_object_permission(self, request, view, obj):
        """Used in GET, PUT and PATCH methods and single instances. GET is always allowed,
        PUT/PATCH is only allowed if the user is modifying her own ssh public key"""
        if request.method in SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the ssh public key.
        return obj.user == request.user and self.is_project_member(
            project_id=obj.project.id, user=request.user
        )
