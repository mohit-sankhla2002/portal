from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from guardian.admin import GuardedModelAdmin
from simple_history.admin import SimpleHistoryAdmin

from .models.approval import DataProviderApproval, NodeApproval, GroupApproval
from .models.data_provider import DataProvider
from .models.data_transfer import DataTransfer, DataPackage, DataPackageTrace
from .models.feed import Feed
from .models.custom_affiliation import CustomAffiliation
from .models.flag import Flag
from .models.message import Message
from .models.node import Node
from .models.pgp import PgpKeyInfo
from .models.project import Project, ProjectIPAddresses, Resource, Service
from .models.tile import QuickAccessTile
from .models.user import Profile, UserNamespace

User = get_user_model()


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "Profile"


@admin.register(User)
class UserAdmin(BaseUserAdmin, SimpleHistoryAdmin):
    list_display = BaseUserAdmin.list_display + ("get_flags",)
    inlines = (ProfileInline,)

    # Users should be created only via the RESTful API
    def has_add_permission(self, _):
        return False

    def get_flags(self, obj):
        return list(obj.flags.all())

    get_flags.short_description = "Flags"


@admin.register(Project)
class ProjectAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = ("name", "gid")


@admin.register(ProjectIPAddresses)
class ProjectIPAddressesAdmin(SimpleHistoryAdmin):
    list_display = ("ip_and_mask", "project")
    list_filter = ("project",)

    def ip_and_mask(self, obj):
        return str(obj)


@admin.register(Resource)
class ResourceAdmin(SimpleHistoryAdmin):
    list_display = ("name", "location", "project", "contact")
    list_filter = ("project",)


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ("name", "project")
    list_filter = ("project",)


@admin.register(Message)
class MessageAdmin(SimpleHistoryAdmin):
    list_display = ("title", "user", "status", "created")
    list_filter = ("user", "status", "created")


@admin.register(UserNamespace)
class UserNamespaceAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Node)
class NodeAdmin(GuardedModelAdmin):
    list_display = ("name", "code", "node_status")


@admin.register(DataProvider)
class DataProviderAdmin(GuardedModelAdmin):
    list_display = ("name", "code", "node")


@admin.register(DataPackage)
class DataPackageAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = (
        "metadata_hash",
        "data_transfer",
        "file_name",
        "file_size",
        "metadata",
        "purpose",
    )


@admin.register(NodeApproval)
class NodeApprovalAdmin(SimpleHistoryAdmin):
    list_display = ("data_transfer", "node", "status")


@admin.register(DataProviderApproval)
class DataProviderApprovalAdmin(SimpleHistoryAdmin):
    list_display = ("data_transfer", "data_provider", "status")


@admin.register(GroupApproval)
class GroupApprovalAdmin(SimpleHistoryAdmin):
    list_display = ("data_transfer", "group", "status")


@admin.register(DataPackageTrace)
class DataPackageTraceAdmin(GuardedModelAdmin):
    list_display = ("data_package", "node", "timestamp", "status")


@admin.register(DataTransfer)
class DataTransferAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = (
        "id",
        "project",
        "max_packages",
        "status",
        "data_provider",
        "requestor",
        "purpose",
    )


@admin.register(Flag)
class FlagAdmin(GuardedModelAdmin, SimpleHistoryAdmin):
    list_display = ("code", "description")


@admin.register(Feed)
class FeedAdmin(GuardedModelAdmin):
    list_display = ("label", "title", "message", "from_date", "until_date")


@admin.register(CustomAffiliation)
class CustomAffiliationAdmin(admin.ModelAdmin):
    list_display = ("code",)


@admin.register(QuickAccessTile)
class QuickAccessTileAdmin(admin.ModelAdmin):
    list_display = ("title", "description", "url", "image", "flag", "order")


@admin.register(PgpKeyInfo)
class PgpKeyInfoAdmin(SimpleHistoryAdmin):
    list_display = ("user", "key_user_id", "key_email", "fingerprint", "status")
