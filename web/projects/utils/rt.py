from typing import TypeVar
from collections.abc import Iterable, Callable
import re
import urllib.parse
import urllib.request
from urllib.error import HTTPError
from urllib.parse import urljoin, urlencode
from http.client import HTTPResponse


class RTSession:
    def __init__(self, host: str, username: str, password: str, **kwargs):
        self.post_args = {
            "session_cookie": get_session_cookie(host, username, password),
            "host": host,
            **kwargs,
        }

    def create_ticket(self, **kwargs) -> int:
        return create_ticket(**{**self.post_args, **kwargs})


def get_session_cookie(host: str, username: str, password: str):
    return post(
        host=host,
        endpoint="/NoAuth/Login.html",
        data={"user": username, "pass": password},
        extractor=extract_cookie,
    )


def create_ticket(
    host: str,
    session_cookie,
    subject: str,
    queue: str,
    body: str,
    requestor_mail: str | None = None,
    owner: str | None = None,
    parent: int | None = None,
    cc: Iterable[str] = (),
    custom_fields: dict[str, str] | None = None,
) -> int:
    fields = (
        ("id", "ticket/new"),
        ("Queue", queue),
        ("Requestor", requestor_mail),
        ("Subject", subject),
        ("Cc", ",".join(cc) or None),
        ("Owner", owner),
        # Multiple body should be indented
        ("Text", body.replace("\n", "\n ")),
    )
    data = "\n".join(f"{key}: {val}" for key, val in fields if val is not None)
    if custom_fields:
        data += "\n" + "\n".join(
            f"CF-{key}: {value}" for key, value in custom_fields.items()
        )
    response = post(
        host, "REST/1.0/ticket/new", data={"content": data}, cookie=session_cookie
    )
    response_pattern = re.compile(r"# Ticket ([0-9]*) created.")
    m = response_pattern.search(response)
    if m is None:
        raise RuntimeError(f"Could not read ticket number from response: {response}")
    created_id = int(m.group(1))
    if parent is not None:
        create_dependency(host, session_cookie, from_id=parent, to_id=created_id)
    return created_id


def create_dependency(host: str, session_cookie, from_id: int, to_id: int):
    data = f"""DependsOn: {to_id}
HasMember: {to_id}
"""
    post(
        host,
        f"REST/1.0/ticket/{from_id}/links",
        data={"content": data},
        cookie=session_cookie,
    )


def extract_cookie(response: HTTPResponse) -> str:
    return response.headers["Set-Cookie"].split(";")[0]


def extract_body(response: HTTPResponse) -> str:
    return response.read().decode()


T = TypeVar("T")

Extractor = Callable[[HTTPResponse], T]


def post(
    host: str,
    endpoint: str,
    data: dict,
    cookie: str | None = None,
    extractor: Extractor[str] = extract_body,
    debuglevel=0,
) -> str:
    headers = {
        "Host": host.replace("https://", "").replace("http://", ""),
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    if cookie:
        headers["Cookie"] = cookie
    request = urllib.request.Request(
        urljoin(host, endpoint),
        urlencode(data).encode(),
        method="POST",
        headers=headers,
    )
    opener = urllib.request.build_opener(
        urllib.request.HTTPSHandler(debuglevel=debuglevel)
    )
    try:
        with opener.open(request) as response:
            return extractor(response)
    except HTTPError as e:
        if e.code == 400:
            raise RuntimeError(f"{e} ({e.read().decode()})") from e
        raise e
