import dataclasses
import enum

import requests
import sett_rs

from ..models.pgp import KeyAlgorithm

PGP_BEGIN_BLOCK = b"-----BEGIN PGP PUBLIC KEY BLOCK-----"
PGP_END_BLOCK = b"-----END PGP PUBLIC KEY BLOCK-----"


class KeyDownloadError(Exception):
    """Error class that displays an error message when a PGP key cannot be
    downloaded from the specified keyserver.

    :param keyserver: URL of keyserver.
    :param msg: optional additional message to append to the error.
    """

    def __init__(self, keyserver: str, msg: str | None = None):
        super().__init__(
            f"Key download from [{keyserver}] failed{f': {msg}' if msg else '.'}"
        )


class KeyNotFoundError(KeyDownloadError):
    """Error class that displays an error message when a PGP key cannot be
    found on the specified keyserver.

    :param keyserver: URL of keyserver.
    """

    def __init__(self, keyserver: str):
        super().__init__(keyserver, "Key not found.")


class KeyNotVerifiedError(KeyDownloadError):
    """Error class that displays an error message when a PGP key can be
    downloaded from the specified keyserver but has not been verified by the owner.

    :param keyserver: URL of keyserver.
    """

    def __init__(self, keyserver: str):
        super().__init__(keyserver, "Key is not verified by user.")


class KeyRevokedError(KeyDownloadError):
    """Error class that displays an error message when a PGP key can be
    downloaded from the specified keyserver but has been revoked by the owner.

    :param keyserver: URL of keyserver.
    """

    def __init__(self, keyserver: str):
        super().__init__(keyserver, "Key has been revoked.")


class KeyStatus(enum.Enum):
    """Status of a public PGP key on a verifying keyserver.

    Verified keys have their associated email verified by the keyserver (i.e.
    the user must prove they have access to the email).
    Only verified keys have the user ID information (name, email) published by
    the verifying keyserver. Non-verified and revoked keys are stripped of
    this info.
    """

    CONDEMNED = "C"  # Key is marked as condemned by keyserver (most probably SHA-1).
    VERIFIED = "V"  # Key email was verified by user.
    NONVERIFIED = (
        "D"  # Key email is not verified, or user "deleted" key from keyserver.
    )
    REVOKED = "R"  # Key was revoked by user.


@dataclasses.dataclass
class KeyMetadata:
    """Metadata extracted from a public PGP key. Does not contain the key
    itself.
    """

    status: KeyStatus
    fingerprint: str | None = None
    user_id: str | None = None
    email: str | None = None
    length: int | None = None
    algorithm: KeyAlgorithm | None = None


def download_ascii_armored_public_pgp_key(
    fingerprint: str, keyserver_url: str
) -> bytes:
    """Downloads the specified PGP key in ASCII armored format from a Verifying
    KeyServer (VKS) interface, e.g. https://keys.openpgp.org.

    The input fingerprint must be 40 hexadecimal characters long and not
    prefixed with "0x". See also https://keys.openpgp.org/about/api

    :param fingerprint: fingerprint of the PGP key to retrieve.
    :param keyserver_url: base URL of the keyserver.
    :returns: encoded ascii-armored public key.
    :raises KeyDownloadError: if key is not found, is invalid, or keyserver
        cannot be reached.
    """
    # Query the keyserver for the specified key fingerprint.
    query_url = f"{keyserver_url}/vks/v1/by-fingerprint/{fingerprint.upper()}"
    try:
        response = requests.get(url=query_url, timeout=4)
    except requests.exceptions.RequestException as e:
        raise KeyDownloadError(keyserver_url, "unable to connect to keyserver.") from e

    # Make sure the key was found on the keyserver.
    if response.status_code == 404:
        raise KeyNotFoundError(keyserver_url)
    if not response.ok:
        raise KeyDownloadError(
            keyserver_url, f"HTTP response code: {response.status_code}."
        )

    # Verify the structure of the PGP key block.
    key_bloc = response.content
    if key_bloc.startswith(PGP_BEGIN_BLOCK) and key_bloc.endswith(
        PGP_END_BLOCK + b"\n"
    ):
        return key_bloc

    raise KeyDownloadError(
        keyserver_url,
        "unrecognized key format:\n"
        f"Expected:\n{PGP_BEGIN_BLOCK.decode()}\n...\n...\n{PGP_END_BLOCK.decode()}\n"
        f"Actual:\n{key_bloc.decode()}",
    )


def extract_public_key_metadata(key_bloc: bytes, end_relax: int) -> KeyMetadata:
    """Extract the PGP key metadata from an encoded public key PGP key bloc.
    PGP public key blocs are expected to have the following format:

        -----BEGIN PGP PUBLIC KEY BLOCK-----
        ...
        ...
        -----END PGP PUBLIC KEY BLOCK-----

    :param key_bloc: ascii armored public key bloc, in encoded format (bytes).
    :param end_relax: unix timesamp until which the key is considered valid even though it is using a deprecated algorithm.
    :returns: key metadata object. If the key could not be imported, a key
        metadata object with status "KeyStatus.REVOKED" is returned.
    """

    cert = sett_rs.cert.CertInfo.from_bytes(key_bloc, end_relax)

    match cert.validity:
        case sett_rs.cert.Validity.Valid:
            status = KeyStatus.VERIFIED
        case sett_rs.cert.Validity.Revoked:
            status = KeyStatus.REVOKED
        case sett_rs.cert.Validity.Condemned:
            status = KeyStatus.CONDEMNED
        case _:
            status = KeyStatus.NONVERIFIED

    return KeyMetadata(
        status=status,
        fingerprint=cert.fingerprint,
        user_id=cert.uid
        and cert.uid.name
        and (cert.uid.name + (f" ({cert.uid.comment})" if cert.uid.comment else "")),
        email=cert.email,
        length=cert.primary_key.length,
        algorithm=KeyAlgorithm(cert.primary_key.pub_key_algorithm),
    )


def download_key_metadata(
    fingerprint: str, keyserver_url: str, end_relax: int
) -> KeyMetadata:
    """Retrieves the PGP key user ID and email for the specified PGP key (via
    its fingerprint) from the specified keyserver.

    The input fingerprint must be 40 hexadecimal characters long and not
    prefixed with "0x".

    :param fingerprint: fingerprint (40 hexadecimal chars) of the key for which
        the metadata should be downloaded.
    :param keyserver_url: URL of keyserver from where to retrieve the key (and
        thus its metadata).
    :param end_relax: unix timestamp or datetime until which the key is considered
        valid even though it is using a deprecated algorithm.
    :raises KeyDownloadError: if the key cannot be found on the keyserver or
        is otherwise considered unfit for usage.
    """
    key_bloc = download_ascii_armored_public_pgp_key(fingerprint, keyserver_url)
    key_metadata = extract_public_key_metadata(key_bloc, end_relax)
    # Verify that the fingerprint extracted from the downloaded key matches
    # the fingerprint given as input argument.
    if key_metadata.fingerprint != fingerprint.upper():
        raise KeyDownloadError(
            keyserver_url,
            f"mismatch in fingerprint: expected '{fingerprint}', "
            f"received '{key_metadata.fingerprint}'.",
        )
    return key_metadata


def assert_key_verified(key_metadata: KeyMetadata, keyserver_url: str):
    match key_metadata.status:
        case KeyStatus.NONVERIFIED:
            raise KeyNotVerifiedError(keyserver_url)
        case KeyStatus.REVOKED:
            raise KeyRevokedError(keyserver_url)


def assert_key_algorithm_valid(key_metadata: KeyMetadata, keyserver_url: str):
    """Verify that the key is either ECC, or RSA with length >= 4096."""
    if key_metadata.algorithm not in (KeyAlgorithm.RSA, KeyAlgorithm.ECC):
        raise KeyDownloadError(
            keyserver_url,
            "unauthorized key algorithm: only RSA and ECC keys are accepted.",
        )
    if key_metadata.algorithm == KeyAlgorithm.RSA and key_metadata.length < 4096:
        raise KeyDownloadError(
            keyserver_url,
            "unauthorized key length: key length for RSA algorithm must be >= 4096",
        )


def download_key_metadata_with_validation(
    fingerprint: str, keyserver_url: str, end_relax: int
) -> KeyMetadata:
    """Download key from keyserver, validate it, and extract its metadata."""
    key_metadata = download_key_metadata(
        fingerprint=fingerprint, keyserver_url=keyserver_url, end_relax=end_relax
    )
    assert_key_verified(key_metadata, keyserver_url)
    assert_key_algorithm_valid(key_metadata, keyserver_url)
    return key_metadata
