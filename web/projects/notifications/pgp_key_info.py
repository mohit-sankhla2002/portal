from typing import Tuple, List

from django.contrib.auth import get_user_model

from portal import settings
from ..models.pgp import PgpKeyInfo

User = get_user_model()


def create_pgp_key_info_new_approval_request_notification(
    pgp_key_info: PgpKeyInfo,
) -> Tuple[str, str]:
    user = pgp_key_info.user
    subject = f"PGP key approval request from {user.profile.display_name}"
    body = f"""A new approval request has just been submitted by {user.profile.display_name}.

Fingerprint:    {pgp_key_info.fingerprint}
User ID:        {pgp_key_info.key_user_id}
Email:          {pgp_key_info.key_email}
    """
    return subject, body


def pgp_key_info_status_updated(
    pgp_key_info: PgpKeyInfo,
) -> Tuple[str, str]:
    subject = f"Status of PGP key '{pgp_key_info.fingerprint}' has changed"
    body = f"""Dear {pgp_key_info.key_user_id},

Following key (available at {settings.CONFIG.pgp.keyserver}) can NO longer be used in BioMedIT:

Fingerprint:    {pgp_key_info.fingerprint}
User ID:        {pgp_key_info.key_user_id}
New status:     {pgp_key_info.status}

Either this change is expected, or you have to generate a new key and submit a new approval request.

Please contact {settings.CONFIG.notification.ticket_mail} if you have any questions.

Kind Regards,

BioMedIT Team"""
    return subject, body


def pgp_key_info_status_all_updated(
    keys: List[PgpKeyInfo],
) -> Tuple[str, str]:
    fingerprints = "- " + "\n- ".join(f"{key.fingerprint} ({key})" for key in keys)
    subject = f"Status of {len(keys)} PGP key(s) has changed"
    body = f"""Following keys have been synchronized with '{settings.CONFIG.pgp.keyserver}'.
Please check the status of each key:

{fingerprints}"""
    return subject, body


def create_pgp_key_info_approved_or_rejected_notification(
    pgp_key_info: PgpKeyInfo,
) -> Tuple[str, str]:
    if not pgp_key_info.status in [
        PgpKeyInfo.Status.APPROVED,
        PgpKeyInfo.Status.REJECTED,
    ]:
        raise ValueError("pgp_key_info status has to be APPROVED or REJECTED")

    outcome = (
        "approved" if pgp_key_info.status == PgpKeyInfo.Status.APPROVED else "rejected"
    )
    subject = f"Your PGP key approval request was {outcome}"
    body = f"""Your PGP key has been {outcome}.

Fingerprint:    {pgp_key_info.fingerprint}
User ID:        {pgp_key_info.key_user_id}
Email:          {pgp_key_info.key_email}
    """
    return subject, body
