from django.contrib.auth import get_user_model

from ..models.flag import Flag

User = get_user_model()


def affiliation_change_body(
    user: User, old_affiliation: str, new_affiliation: str
) -> str:
    def display_affiliation(affiliation: str):
        return ", ".join(term.strip() for term in affiliation.split(","))

    def display_flags(user: User):
        return ", ".join(flag.code for flag in Flag.objects.filter(users=user))

    body = (f"Display name: {user.profile.display_name}",)
    body += (f"Old affiliation: {display_affiliation(old_affiliation)}",)
    body += (f"New affiliation: {display_affiliation(new_affiliation)}",)
    body += (f"Flags: {display_flags(user)}",)
    return "\n".join(body)
