import itertools
from abc import ABC, abstractmethod
from urllib.parse import urljoin

from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.email import sendmail

from ..models.approval import BaseApproval
from ..models.data_transfer import DataTransfer
from ..models.message import Message
from ..models.node import get_node_admins
from ..notifications.approval import (
    create_approve_notification,
    create_final_approval_notification,
    create_reject_notification,
)
from ..notifications.data_transfer import create_data_transfer_creation_notification

User = get_user_model()


class DtrNotification(ABC):
    def __init__(self, dtr: DataTransfer, base_url: str) -> None:
        self.dtr = dtr
        self.base_url = base_url
        self.dtr_path = f"data-transfers/{dtr.id}"
        # List of unique nodes
        self.nodes = frozenset(approval.node for approval in dtr.node_approvals.all())
        # List of unique node admins
        self.node_admins = frozenset(
            itertools.chain.from_iterable(get_node_admins(node) for node in self.nodes)
        )
        self.dp_coordinators = list(dtr.data_provider.coordinators)
        self.legal_approval_group_users = (
            self.dtr.project.legal_approval_group.user_set.all()
            if self.dtr.project.legal_approval_group
            else ()
        )

    @abstractmethod
    def create_notification(self) -> tuple[str, str]:
        """Generate subject and body for the notification"""

    def send_messages(self) -> None:
        """Creates messages in portal."""
        subject, body = self.create_notification()
        Message.objects.bulk_create(
            Message(
                user=recipient,
                title=subject,
                # carriage return `\r` is necessary for the frontend to correctly
                # render new lines
                body=body.replace("\n", "\r\n")
                + f"\r\n[Go to the DTR]({self.dtr_path})",
            )
            for recipient in self.recipients()
        )

    def recipients(self) -> frozenset[User]:
        """Returns default list of recipients.

        By default, it does NOT include the legal approval group.
        """
        return frozenset().union(
            self.node_admins,
            self.dp_coordinators,
            (self.dtr.requestor,),
        )

    def email_recipients(self) -> tuple[str, ...]:
        """Generate list of DTR notification email recipients.

        As the recipients are already unique, we just pick up the email of each.
        """
        return tuple(recipient.email for recipient in self.recipients())

    def sendmail(self) -> None:
        subject, body = self.create_notification()
        sendmail(
            subject=subject,
            body=body + f"({urljoin(self.base_url, self.dtr_path)})",
            bcc=self.email_recipients(),
            # pylint: disable=no-member
            email_cfg=settings.CONFIG.email,
            reply_to=(settings.CONFIG.notification.ticket_mail,),
        )


class DtrCreatedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_data_transfer_creation_notification(self.dtr)

    def recipients(self) -> frozenset[User]:
        recipients = super().recipients()
        if self.dtr.purpose == DataTransfer.PRODUCTION:
            # Legal approval group only gets notified in case of a `PRODUCTION`
            # DTR and when a DTR gets created
            return frozenset(recipients).union(self.legal_approval_group_users)
        return recipients

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )


class DtrApprovalNotification(DtrNotification):
    """Abstract class for approval handling"""

    def __init__(
        self,
        base_url: str,
        approval: BaseApproval,
        user: User,
    ) -> None:
        super().__init__(approval.data_transfer, base_url)
        self.approval = approval
        self.user = user


class DtrApprovedNotification(DtrApprovalNotification):
    """Approval has been approved."""

    def create_notification(self) -> tuple[str, str]:
        return create_approve_notification(self.approval, self.user)


class DtrRejectedNotification(DtrApprovalNotification):
    """Approval has been rejected."""

    def create_notification(self) -> tuple[str, str]:
        return create_reject_notification(self.approval, self.user)

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )


class DtrAuthorizedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_final_approval_notification(self.dtr)

    def email_recipients(self) -> tuple[str, ...]:
        return (
            super().email_recipients()
            + tuple(node.ticketing_system_email for node in self.nodes)
            + (settings.CONFIG.notification.ticket_mail,)  # pylint: disable=no-member
        )
