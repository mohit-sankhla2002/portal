from django.contrib.auth import get_user_model
from rest_framework.filters import BaseFilterBackend

from ..models.node import get_node_viewer_nodes, get_node_admin_nodes

User = get_user_model()


class NodePermissionsFilter(BaseFilterBackend):
    """Filters out all nodes the user has permission to"""

    param = "username"

    def filter_queryset(self, request, queryset, view):
        username = request.query_params.get(self.param)
        if username:
            user = User.objects.get(username=username)
            return get_node_viewer_nodes(user) | get_node_admin_nodes(user)
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Only returns nodes which the user has permission to view or edit.",
                "schema": {"type": "string"},
            },
        )
