from rest_framework.filters import BaseFilterBackend

from .common import BaseLookupFilter, QueryParam
from ..models.project import ProjectRole


class ProjectsPermissionFilter(BaseFilterBackend):
    """Filter projects by user permissions."""

    def filter_queryset(self, request, queryset, view):
        f = {}
        username = request.query_params.get("username")
        if username:
            f["users__user__username"] = username
            f["users__role__in"] = (
                ProjectRole.USER.value,
                ProjectRole.DM.value,
                ProjectRole.PM.value,
                ProjectRole.PL.value,
            )
        return queryset.filter(**f).distinct()

    def get_schema_operation_parameters(self, view):
        params = []
        for name, description in (("username", "Username for the User Role"),):
            params.append(
                {
                    "name": name,
                    "required": False,
                    "in": "query",
                    "description": description,
                    "schema": {"type": "string"},
                }
            )
        return params


class ProjectsLookupFilter(BaseLookupFilter):
    """Filters projects by `destination`, `code` and `name`"""

    query_params = [
        QueryParam(
            "destination",
            "destination__code",
            "Includes only projects with specified destination",
        ),
        QueryParam(
            "code",
            "code",
            "Includes only the project identified by specified code",
        ),
        QueryParam(
            "name",
            "name__iexact",
            "Includes only the project identified by specified name",
        ),
    ]


class ProjectUserRoleHistoryLookupFilter(BaseLookupFilter):
    query_params = [
        QueryParam(
            "from",
            "created__gte",
            "Includes only entries created after specified date",
            "date",
        ),
        QueryParam(
            "to",
            "created__lte",
            "Includes only entries created before specified date",
            "date",
        ),
        QueryParam(
            "project",
            "project_str__regex",
            "Includes only entries whose project code matches the specified regex",
        ),
    ]
