from .common import BaseLookupFilter, QueryParam


class CustomAffiliationLookupFilter(BaseLookupFilter):
    """Filters custom affiliations by `code`"""

    query_params = [
        QueryParam(
            "code",
            "code__iexact",
            "Includes only custom affiliations with specified code",
        ),
    ]
