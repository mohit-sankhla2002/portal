from dataclasses import dataclass

from django.db.models.constants import LOOKUP_SEP
from django.db.models.functions import Lower
from rest_framework.filters import BaseFilterBackend, OrderingFilter
from rest_framework.request import Request


@dataclass
class QueryParam:
    name: str
    lookup: str
    description: str
    type: str = "string"


class BaseLookupFilter(BaseFilterBackend):
    """
    Filters entity using just lookups.
    Inheriting class should initialize `query_params`.
    """

    query_params: list[QueryParam]

    def _get_lookups(self, request: Request):
        def get_lookup(param: QueryParam):
            name = param.name
            value: str = request.query_params.get(name)
            if param.type == "string" or value != "":
                return param.lookup, value
            last_index = param.lookup.rfind(LOOKUP_SEP)
            return param.lookup[:last_index] + LOOKUP_SEP + "isnull", True

        return dict(
            get_lookup(param)
            for param in self.query_params
            if request.query_params.get(param.name, None) is not None
        )

    def filter_queryset(self, request, queryset, view):
        if lookups := self._get_lookups(request):
            return queryset.filter(**lookups).distinct()
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return [
            {
                "name": param.name,
                "required": False,
                "in": "query",
                "description": param.description,
                "schema": {"type": param.type},
            }
            for param in self.query_params
        ]


class CaseInsensitiveOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)
        if ordering:
            return queryset.order_by(
                *[
                    Lower(f[1:]).desc() if f.startswith("-") else Lower(f)
                    for f in ordering
                ]
            )
        return queryset
