import logging
from typing import Any

from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.email import sendmail

from projects.models.user import Profile
from projects.notifications import affiliation_change_body

User = get_user_model()

logger = logging.getLogger(__name__)

SEP = ","


def get_or_create_user(username: str, email: str | None) -> tuple[User, bool]:
    """Fetch an existing user or create a new one.

    Lookup happens via either email address or username (case-insensitive). In
    principle, a search by username/email should yield (at most) a single
    result.

    :param username: User name. Can be empty.
    :param email: email address. Can be empty.
    :return: tuple containing the retrieved or created user object, and a
        boolean indicating whether user was created (True) or already existed
        (False).
    """
    if email:
        try:
            return User.objects.get(email__iexact=email), False
        except User.DoesNotExist:
            pass

    return User.objects.get_or_create(username=username)


def affiliations_to_domains(affs: str, separator: str = SEP):
    """Extract the university domain(s) from the linkedAffiliation claim, e.g.
    'member@affiliation.ch,staff@affiliation.ch' -> ['affiliation.ch']
    Original OIDC claim from SWITCH edu-ID: swissEduIDLinkedAffiliationUniqueID"""
    if affs:
        # fmt: off
        return sorted(
            {
                aff.split("@")[1]
                for aff in affs.split(separator)
                if "@" in aff
            }
        )
        # fmt: on
    return []


def user_from_claims(username_claim: str, claims: dict[str, Any]):
    try:
        username = claims[username_claim]
    except KeyError:
        username = None

    if not username:
        return None
    mapper = settings.CONFIG.oidc.mapper
    user, created = get_or_create_user(username, claims.get(mapper.email, None))
    user.first_name = claims.get(mapper.first_name, user.first_name)
    user.last_name = claims.get(mapper.last_name, user.last_name)
    Profile.objects.get_or_create(user=user)
    old_affiliation = user.profile.affiliation
    new_affiliation = SEP.join(claims.get(mapper.affiliation, []))
    new_affiliation_id = SEP.join(claims.get(mapper.affiliation_id, []))
    old_domains = affiliations_to_domains(old_affiliation)
    new_domains = affiliations_to_domains(new_affiliation)
    user.profile.affiliation = new_affiliation
    user.profile.affiliation_id = new_affiliation_id
    new_emails = []
    if mapper.additional_emails is not None:
        for val in mapper.additional_emails:
            new_emails.extend(
                [emails] if isinstance(emails := claims.get(val, []), str) else emails
            )
    user.profile.emails = SEP.join(new_emails)

    if not created:
        if user.username != username:
            logger.info(
                "Local user with email '%s' found. Changing username from '%s' to '%s'.",
                user.email,
                user.username,
                username,
            )
            user.set_unusable_password()
            user.username = username

        if old_domains != new_domains:
            logger.info(
                "Affiliations for user (username: %s) have changed from '%s' to '%s'. ",
                user.username,
                old_affiliation,
                new_affiliation,
            )
            send_affiliation_change_email(user, old_affiliation, new_affiliation, True)

        if not user.email in new_emails:
            logger.info(
                "Current email '%s' for user (username: %s) not found in '%s'. Resetting...",
                user.email,
                user.username,
                new_emails,
            )
            user.email = None

    user.profile.save()
    user.save()

    return user


def send_affiliation_change_email(
    user: User, old_affiliation: str, new_affiliation: str, on_login: bool = False
):
    sendmail(
        subject=f"Affiliation change {'on login ' if on_login else ''}of user '{user.username}'",
        body=affiliation_change_body(user, old_affiliation, new_affiliation),
        recipients=(settings.CONFIG.notification.ticket_mail,),
        email_cfg=settings.CONFIG.email,
    )


def send_no_affiliation_consent_email(user: User, old_affiliation: str):
    if settings.CONFIG.notification.send_no_affiliation_consent_email:
        sendmail(
            subject=f"Affiliation of user '{user.username}' may have changed, but consent is missing!",
            body=affiliation_change_body(
                user, old_affiliation, "unknown (no consent given)"
            ),
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,
        )
