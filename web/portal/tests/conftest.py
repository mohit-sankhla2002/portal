import pytest
from django_drf_utils.config import OidcMapper

# pylint: disable=unused-import
from projects.tests.conftest import user_factory, profile_factory


@pytest.fixture
def apply_oidc_mapper_settings(settings):
    settings.CONFIG.oidc.mapper = OidcMapper(
        first_name="given_name",
        last_name="family_name",
        email="email",
        affiliation="linkedAffiliation",
        affiliation_id="linkedAffiliationUniqueID",
        additional_emails=[
            "swissEduIDAssociatedMail",
            "swissEduIDLinkedAffiliationMail",
        ],
    )  # nosec
    return settings
