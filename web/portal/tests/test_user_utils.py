from unittest.mock import patch

import pytest

from portal import user_utils
from portal.user_utils import (
    user_from_claims,
    get_or_create_user,
    affiliations_to_domains,
)

CREATION_CLAIMS = {
    "preferred_username": "906726174130@identity.ch",
    "given_name": "Bob",
    "family_name": "Smith",
    "swissEduIDAssociatedMail": ["bob@example.org"],
}
CREATION_CLAIMS_HOSPITAL = CREATION_CLAIMS | {
    "swissEduIDLinkedAffiliationMail": "bob@hospital.org",
    "linkedAffiliation": ["member@hospital.ch", "staff@hospital.ch"],
    "linkedAffiliationUniqueID": ["1234567@affiliation.ch", "7654321@basel.ch"],
}
CREATION_CLAIMS_SCIENCES = CREATION_CLAIMS | {
    "swissEduIDLinkedAffiliationMail": "bob@sciences.org",
    "linkedAffiliation": ["member@sciences.ch", "staff@sciences.ch"],
    "linkedAffiliationUniqueID": ["987321@sciences.ch", "24680@basel.ch"],
}

BOB_LOCAL = {"username": "bob", "email": "bob@example.org"}
BOB = {
    "username": "906726174130@identity.ch",
    "first_name": "Bob",
    "last_name": "Smith",
}
BOB_PROFILE_HOSPITAL = {
    "emails": "bob@example.org,bob@hospital.org",
    "affiliation": "member@hospital.ch,staff@hospital.ch",
    "affiliation_id": "1234567@affiliation.ch,7654321@basel.ch",
}
BOB_PROFILE_SCIENCES = {
    "emails": "bob@example.org,bob@sciences.org",
    "affiliation": "member@sciences.ch,staff@sciences.ch",
    "affiliation_id": "987321@sciences.ch,24680@basel.ch",
}
BOB_UNAFFILIATED = BOB | {
    "email": None,
    "profile": {"emails": "bob@example.org", "affiliation": "", "affiliation_id": ""},
}
BOB_HOSPITAL = BOB | {"email": "bob@hospital.org", "profile": BOB_PROFILE_HOSPITAL}
BOB_SCIENCES = BOB | {"email": None, "profile": BOB_PROFILE_SCIENCES}


@pytest.mark.parametrize(
    "existing_user_data, email, username, expected_created",
    (
        (BOB_LOCAL, None, BOB_LOCAL["username"], False),
        (BOB_LOCAL, BOB_LOCAL["email"], "", False),
        (None, BOB_LOCAL["email"], BOB_LOCAL["username"], True),
        ({"username": "bob"}, BOB_LOCAL["email"], BOB_LOCAL["username"], False),
    ),
)
def test_get_or_create_user(
    existing_user_data, email, username, expected_created, user_factory
):
    if existing_user_data:
        user_factory(**existing_user_data)

    user, created = get_or_create_user(username, email)

    if username:
        assert user.username == username

    assert created is expected_created


@pytest.mark.parametrize(
    "existing_user_data, existing_profile_data, claims, expected_user, notification_sent",
    (
        (None, None, {"wrong_username": "bob"}, None, False),
        (BOB_LOCAL, None, CREATION_CLAIMS_SCIENCES, BOB_SCIENCES, False),
        (
            BOB_HOSPITAL,
            BOB_PROFILE_HOSPITAL,
            CREATION_CLAIMS_HOSPITAL,
            BOB_HOSPITAL,
            False,
        ),
        (None, None, CREATION_CLAIMS_SCIENCES, BOB_SCIENCES, False),
        (None, None, CREATION_CLAIMS, BOB_UNAFFILIATED, False),
        (
            BOB_HOSPITAL,
            BOB_PROFILE_HOSPITAL,
            CREATION_CLAIMS_SCIENCES,
            BOB_SCIENCES,
            True,
        ),
    ),
)
def test_user_from_claims(
    apply_oidc_mapper_settings,  # pylint: disable=unused-argument
    existing_user_data,
    existing_profile_data,
    claims,
    expected_user,
    notification_sent,
    user_factory,
    profile_factory,
):
    if existing_user_data:
        user = user_factory(**existing_user_data)

        if existing_profile_data:
            profile_factory(user=user, **existing_profile_data)

    with patch.object(user_utils, "sendmail") as mock_method:
        user = user_from_claims("preferred_username", claims)

        if expected_user:
            assert user.first_name == expected_user["first_name"]
            assert user.last_name == expected_user["last_name"]
            assert user.profile.affiliation == expected_user["profile"]["affiliation"]
            assert (
                user.profile.affiliation_id
                == expected_user["profile"]["affiliation_id"]
            )
            assert user.profile.emails == expected_user["profile"]["emails"]
            assert user.username == expected_user["username"]
            assert user.email == expected_user["email"]
            assert notification_sent == mock_method.called
        else:
            assert user is None


@pytest.mark.parametrize(
    "affiliations, expected_domains",
    (
        ("member@example.org,staff@example.org", ["example.org"]),
        (
            "member@hospital.ch,staff@hospital.ch,member@university.ch,staff@university.ch",
            ["hospital.ch", "university.ch"],
        ),
        ("member,staff", []),
        ("", []),
    ),
)
def test_affililations_to_domains(affiliations: str, expected_domains: list):
    domains = affiliations_to_domains(affiliations)
    assert domains == expected_domains
