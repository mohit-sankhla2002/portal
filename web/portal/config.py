import logging
from dataclasses import dataclass, field

from django_drf_utils.config import BaseConfig, Email, Logging, Oidc, Session

logger = logging.getLogger(__name__)


@dataclass
class TicketingApi(BaseConfig):
    host: str
    username: str
    password: str
    queue: str


@dataclass
class Notification(BaseConfig):
    contact_form_recipient: str
    ticket_mail: str
    send_no_affiliation_consent_email: bool = False


@dataclass
class Uid(BaseConfig):
    min: int
    max: int


@dataclass
class LocalUsername(BaseConfig):
    min_length: int = 6
    max_length: int = 64


@dataclass
class User(BaseConfig):
    default_namespace: str = "ch"
    uid: Uid | None = field(default_factory=lambda: Uid(min=1000000, max=1500000 - 1))
    gid: Uid | None = field(default_factory=lambda: Uid(min=1000000, max=1500000 - 1))
    local_username: LocalUsername = field(default_factory=LocalUsername)


@dataclass
class Project(BaseConfig):
    gid: Uid | None = field(default_factory=lambda: Uid(min=1500000, max=2000000 - 1))


@dataclass
class Pgp(BaseConfig):
    keyserver: str
    minimal_key_length: int = 4096
    end_relax: int = 1706745600  # 02/01/2024, 12:00:00 am GMT/UTC+0


@dataclass
class SwitchNotification(BaseConfig):
    # portal username to use in basic authentication when switch is making a request to portal API
    auth_username: str
    # url to Switch SCIM API affiliation URL with trailing slash, affiliations ID will be appended
    scim_api_affiliations_url: str
    scim_api_username: str
    scim_api_password: str


@dataclass
class Flags(BaseConfig):
    authorized_user: str | None = None


@dataclass
class JWTKey(BaseConfig):
    private_path: str = ""
    public_path: str = ""


@dataclass
class STS(BaseConfig):
    duration_seconds: int = 3600  # 1 hour


@dataclass
class OAuth(BaseConfig):
    issuer: str = ""
    jwt_key: JWTKey = field(default_factory=JWTKey)
    sts: STS = field(default_factory=STS)
    alg: str = "RS256"
    expiration_time_seconds: int = 3600  # 1 hour


@dataclass
class Config(BaseConfig):
    notification: Notification
    oidc: Oidc
    pgp: Pgp
    oauth: OAuth = field(default_factory=OAuth)
    logging: Logging = field(default_factory=Logging)
    session: Session = field(default_factory=Session)
    email: Email | None = None
    user: User | None = field(default_factory=User)
    project: Project | None = field(default_factory=Project)
    switch: SwitchNotification | None = None
    flags: Flags | None = None
