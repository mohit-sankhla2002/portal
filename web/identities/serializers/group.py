import logging

from django.contrib.auth.models import Group, Permission
from django.db.transaction import atomic
from django_drf_utils.serializers.utils import get_request_username
from guardian.models import GroupObjectPermission
from rest_framework import serializers

from identities.models import GroupProfile, User

logger = logging.getLogger(__name__)

VALIDATION_ERROR_NON_STAFF_FIELDS = (
    "Only staff are allowed to change fields other than `users`"
)


class GroupObjectPermissionSerializer(
    serializers.Serializer
):  # pylint: disable=abstract-method
    permission = serializers.IntegerField()
    objects = serializers.ListField(child=serializers.IntegerField(min_value=1))

    def validate_permission(self, value):
        try:
            return Permission.objects.get(pk=value)
        except Permission.DoesNotExist as e:
            raise serializers.ValidationError(
                f"Permission {value} does not exist"
            ) from e

    def validate(self, attrs):
        data = super().validate(attrs)
        model = data["permission"].content_type.model_class()
        for obj in data["objects"]:
            try:
                model.objects.get(pk=obj)
            except model.DoesNotExist as e:
                raise serializers.ValidationError(
                    f"Object {obj} does not exist for model {model.__name__}"
                ) from e
        return data


class GroupObjectPermissionListSerializer(
    serializers.ListSerializer
):  # pylint: disable=abstract-method
    def to_representation(self, data):
        output = {}
        for perm in data.all():
            output.setdefault(perm.permission.id, []).append(perm.object_pk)
        return [
            GroupObjectPermissionSerializer({"permission": p, "objects": o}).data
            for p, o in output.items()
        ]


class GroupProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupProfile
        read_only_fields = ("role_entity_type", "role_entity_id")
        fields = read_only_fields + (
            "description",
            "role",
        )  # `role` can be set on creation only

    role_entity_type = serializers.SerializerMethodField()

    def validate(self, attrs):
        data = super().validate(attrs)
        user = get_request_username(self)
        if not user.is_staff:
            # Only staff can set `role` field
            attrs.pop("role")
        return data

    @staticmethod
    def get_role_entity_type(profile: GroupProfile) -> str | None:
        if profile.role_entity:
            return profile.role_entity_type.model

        return None


class GroupShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("name",)


class GroupUserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(
        required=True,
        label="User ID",
        help_text="User's technical unique identifier",
    )
    display_name = serializers.CharField(
        source="profile.display_name",
        read_only=True,
        label="Display name",
        help_text="User's display name",
    )

    class Meta:
        model = User
        read_only_fields = (
            "username",
            "email",
            "last_name",
            "first_name",
            "display_name",
        )
        fields = read_only_fields + ("id",)

    def to_internal_value(self, data: dict):
        if data is not None and (not isinstance(data, dict) or "id" not in data):
            raise serializers.ValidationError(
                "User data must be a dictionary containing the `id` field"
            )
        try:
            return User.objects.get(pk=data["id"])
        except User.DoesNotExist as e:
            raise serializers.ValidationError(f"User {data} does not exist") from e


class GroupSerializer(serializers.ModelSerializer):
    permissions_object = GroupObjectPermissionListSerializer(
        child=GroupObjectPermissionSerializer(),
        required=False,
        source="groupobjectpermission_set",
    )
    users = GroupUserSerializer(
        source="user_set",
        many=True,
        label="Users",
        help_text="Users associated with this group",
    )
    profile = GroupProfileSerializer(
        required=False,
        label="Profile",
        help_text="Contains more attributes about the group",
    )

    class Meta:
        model = Group
        fields = (
            "id",
            "name",
            "profile",
            "users",
            "permissions",
            "permissions_object",
        )

    def _log_action(self, action: str, queryset):
        if queryset:
            logger.info("%s %s %s", get_request_username(self), action, queryset)

    @staticmethod
    def _transform_object_permissions(permissions):
        return (
            (perm_grp["permission"].content_type, str(obj), perm_grp["permission"])
            for perm_grp in permissions
            for obj in perm_grp["objects"]
        )

    def _create_object_permissions(self, obj_perm, instance: Group):
        created = GroupObjectPermission.objects.bulk_create(
            GroupObjectPermission(
                group=instance, content_type=t, object_pk=o, permission=p
            )
            for t, o, p in obj_perm
        )
        self._log_action("created", created)

    @atomic
    def create(self, validated_data):
        obj_perm = self._transform_object_permissions(
            validated_data.pop("groupobjectpermission_set", ())
        )
        profile = validated_data.pop("profile", None)
        users = validated_data.pop("user_set", ())
        group = super().create(validated_data)
        if profile:
            GroupProfile.objects.create(group=group, **profile)
        self._create_object_permissions(obj_perm, group)
        group.user_set.set(users)
        return group

    def _validate_non_staff_changes(self, instance, validated_data, new_perm, old_perm):
        user = get_request_username(self)
        current_permissions = list(instance.permissions.all())
        has_different_name = validated_data.get("name", instance.name) != instance.name
        profile_data = validated_data.get("profile", None)
        has_different_description = False
        if profile_data:
            try:
                has_different_description = (
                    profile_data.get("description", instance.profile.description)
                    != instance.profile.description
                )
            except GroupProfile.DoesNotExist:
                # If `Group` does NOT have `profile`, then we have a mismatch
                has_different_description = True
        has_different_permissions = (
            validated_data.get("permissions", current_permissions)
            != current_permissions
        )
        has_different_group_object_permissions = set(old_perm.keys()) != new_perm
        if not user.is_staff and (
            has_different_name
            or has_different_description
            or has_different_permissions
            or has_different_group_object_permissions
        ):
            raise serializers.ValidationError(VALIDATION_ERROR_NON_STAFF_FIELDS)

    @atomic
    def update(self, instance, validated_data):
        new_perm = set(
            self._transform_object_permissions(
                validated_data.pop("groupobjectpermission_set", ())
            )
        )
        instance.user_set.set(validated_data.pop("user_set", ()))
        old_perm = {
            (p.content_type, p.object_pk, p.permission): p.pk
            for p in GroupObjectPermission.objects.filter(group=instance)
        }
        self._validate_non_staff_changes(instance, validated_data, new_perm, old_perm)
        self._create_object_permissions(new_perm - set(old_perm.keys()), instance)
        to_be_removed = GroupObjectPermission.objects.filter(
            pk__in=[
                v for k, v in old_perm.items() if k in set(old_perm.keys()) - new_perm
            ]
        )
        self._log_action("deleted", list(to_be_removed))
        to_be_removed.delete()
        profile_data = validated_data.pop("profile", None)
        if profile_data:
            try:
                profile = instance.profile
                profile.description = profile_data.get(
                    "description", profile.description
                )
                profile.save()
            # `profile` does NOT exist on `Group` yet
            except GroupProfile.DoesNotExist:
                GroupProfile.objects.create(group=instance, **profile_data)
        return super().update(instance, validated_data)
