import pytest

from django.test import RequestFactory

from projects.models.project import ProjectRole
from projects.tests.factories import (
    make_group_manager,
    make_node_viewer,
    make_project_user,
    make_data_provider_viewer,
    UserFactory,
)

from ..permissions import (
    IsDataManager,
    IsDpPersonnel,
    IsGroupManager,
    IsNodePersonnel,
    IsProjectLeader,
    IsProjectManager,
)


def test_is_group_manager(user_factory, group_factory):
    managed_group, other_group = group_factory.create_batch(2)
    group_manager = make_group_manager(managed_group)
    permission_object = IsGroupManager()

    for user, allowed in ((group_manager, True), (user_factory(), False)):
        request = RequestFactory().request()
        request.user = user
        assert permission_object.has_permission(request, None) is allowed
        assert (
            permission_object.has_object_permission(request, None, managed_group)
            is allowed
        )
        assert (
            permission_object.has_object_permission(request, None, other_group) is False
        )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "permission_class, factory, allowed",
    (
        (IsNodePersonnel, make_node_viewer, True),
        (IsNodePersonnel, UserFactory, False),
        (IsDpPersonnel, make_data_provider_viewer, True),
        (IsDpPersonnel, UserFactory, False),
        (IsProjectLeader, (lambda: make_project_user(role=ProjectRole.PL)), True),
        (IsProjectLeader, UserFactory, False),
        (IsDataManager, (lambda: make_project_user(role=ProjectRole.DM)), True),
        (IsDataManager, UserFactory, False),
        (IsProjectManager, (lambda: make_project_user(role=ProjectRole.PM)), True),
        (IsProjectManager, UserFactory, False),
    ),
)
def test_user_role_classes(permission_class, factory, allowed):
    permission_object = permission_class()

    request = RequestFactory().request()
    request.user = factory()
    assert permission_object.has_permission(request, None) is allowed
