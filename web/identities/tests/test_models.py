import pytest
from django.core.exceptions import ValidationError

from identities.models import validate_grant_type, OAuth2Client


def test_tokens(oauth2_token_factory):
    token = oauth2_token_factory()
    assert token.check_client(token.client)
    assert not token.is_expired()


@pytest.mark.parametrize(
    "scope,expected",
    (
        ("openid", "openid"),
        ("email", ""),
        ("profile", "profile"),
        ("openid profile", "openid profile"),
        ("profile openid", "profile openid"),
        ("openid email", "openid"),
    ),
)
def test_client_get_allowed_scope(oauth2_client_factory, scope, expected):
    client = oauth2_client_factory()
    assert client.get_allowed_scope(scope) == expected


@pytest.mark.parametrize(
    "redirect_uri,expected",
    (
        ("https://www.google.com/", False),
        (None, False),
        ("", False),
        ("http://localhost:9001/oauth_callback", True),
    ),
)
def test_client_check_redirect_uri(oauth2_client_factory, redirect_uri, expected):
    client = oauth2_client_factory()
    assert client.check_redirect_uri(redirect_uri) == expected


@pytest.mark.parametrize(
    "value,expected_exception",
    (
        (None, True),
        ("", True),
        (OAuth2Client.GrantType.AUTHORIZATION_CODE.value, False),
        ("does_not_exist", True),
        (
            OAuth2Client.GrantType.AUTHORIZATION_CODE.value
            + " "
            + OAuth2Client.GrantType.CLIENT_CREDENTIALS.value,
            False,
        ),
    ),
)
def test_validate_grant_type(value, expected_exception):
    if expected_exception:
        with pytest.raises(ValidationError):
            validate_grant_type(value)
    else:
        validate_grant_type(value)
