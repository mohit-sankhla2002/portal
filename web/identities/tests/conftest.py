import pytest

# pylint: disable=unused-import
from projects.tests.conftest import (
    basic_user_auth,
    flag_factory,
    group_factory,
    group_profile_factory,
    node_factory,
    project_factory,
    project_user_role_factory,
    apply_notification_settings,
    user_factory,
)
from .factories import OAuth2TokenFactory, OAuth2ClientFactory


@pytest.fixture
def oauth2_token_factory(db):  # pylint: disable=unused-argument
    return OAuth2TokenFactory


@pytest.fixture
def oauth2_client_factory(db):  # pylint: disable=unused-argument
    return OAuth2ClientFactory
