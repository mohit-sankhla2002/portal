import pytest

from identities.serializers import AnyObjectSerializer
from identities.serializers.group import GroupProfileSerializer


def test_generic_object_serializer(group_factory):
    group = group_factory(name="New Group")
    assert AnyObjectSerializer(group).data == {"id": group.id, "name": str(group)}


@pytest.mark.parametrize("has_role_entity, expected", ((True, "node"), (False, None)))
def test_group_profile_get_role_entity_type(
    has_role_entity, expected, group_profile_factory, node_factory
):
    role_entity = node_factory() if has_role_entity else None
    group_profile = group_profile_factory(role_entity=role_entity)
    assert GroupProfileSerializer(group_profile).data["role_entity_type"] == expected
