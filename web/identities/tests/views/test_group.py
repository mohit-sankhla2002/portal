import logging

import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from guardian.models import GroupObjectPermission
from rest_framework import status

from identities.apps import APP_NAME
from identities.models import GroupProfile
from identities.serializers.group import VALIDATION_ERROR_NON_STAFF_FIELDS
from identities.tests import APPLICATION_JSON
from projects.models.const import PERM_NODE_ADMIN
from projects.models.node import Node
from projects.models.project import Project, ProjectRole
from projects.tests.factories import (
    NodeFactory,
    UserFactory,
    USER_PASSWORD,
    make_group_manager,
)
from projects.tests.factories import (
    make_data_provider_viewer,
    make_legal_approval_group,
    make_node_viewer,
    make_project_user,
)

User = get_user_model()

VIEW_NAME = f"{APP_NAME}:group"
URL_LIST = reverse(f"{VIEW_NAME}-list")
GROUP_NAME = "New Group"


def _dict_for_user(user: User):
    return {
        attr: getattr(user, attr)
        for attr in dir(user)
        if attr in ("email", "first_name", "id", "last_name", "username")
    }


def test_get_groups_anonymous(client):
    assert client.get(URL_LIST).status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_get_groups_authenticated(client):
    assert client.get(URL_LIST).status_code == status.HTTP_200_OK


def test_get_groups_permissions(admin_client, group_factory):
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 0

    group_factory.create()
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1


def test_get_groups_by_permission(admin_client, client, group_factory, project_factory):
    total_groups = 5
    (
        managed_group,
        manager_group,
        dp_group,
        node_group,
        legal_approval_group,
    ) = group_factory.create_batch(total_groups)

    group_manager = make_group_manager(managed_group, manager_group=manager_group)
    dp_user = make_data_provider_viewer(group=dp_group)
    node_user = make_node_viewer(group=node_group)
    project = project_factory()
    project_user = make_project_user(project, ProjectRole.USER)
    project_leader = make_project_user(project, ProjectRole.PL)
    make_legal_approval_group(group=legal_approval_group)

    # staff can see all groups
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == total_groups
    admin_client.logout()

    for user, group, length in [
        (group_manager, managed_group, 2),
        (dp_user, dp_group, 2),
        (node_user, node_group, 2),
        (project_leader, legal_approval_group, 1),
        (project_user, legal_approval_group, 1),
    ]:
        client.login(username=user.username, password=USER_PASSWORD)
        response = client.get(URL_LIST)
        assert response.status_code == status.HTTP_200_OK
        response_json = response.json()
        assert len(response_json) == length
        assert {obj["name"] for obj in response_json}.issubset(
            {
                group.name,
                legal_approval_group.name,
            }
        )
        client.logout()


def test_get_groups_sorted(admin_client, group_factory):
    group_factory(name="Test Node 1 Node Admin")
    group_factory(name="dataprovider 1 DP Manager")
    group_factory(name="dataprovider 1 DP Coordinator")
    response = admin_client.get(URL_LIST)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 3
    original_list = [group["name"].lower() for group in response.json()]
    assert sorted(original_list) == original_list


def test_get_managed_groups(client, group_factory):
    managed_group = group_factory()
    group_manager = make_group_manager(managed_group)
    make_legal_approval_group()
    client.login(username=group_manager.username, password=USER_PASSWORD)
    for url, expected in (
        (URL_LIST, 2),
        (f"{URL_LIST}?managed=true", 1),
        (f"{URL_LIST}?managed=1", 1),
        (f"{URL_LIST}?managed", 2),
    ):
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == expected
    client.logout()


def test_create_group(admin_client, admin_user, user_factory, project_factory, caplog):
    caplog.set_level(logging.INFO)
    user1, user2 = user_factory.create_batch(2)
    project1, project2 = project_factory.create_batch(2)
    perm_add_node = Permission.objects.get(codename="add_node")
    perm_change_project = Permission.objects.get(codename="change_project")
    perm_delete_project = Permission.objects.get(codename="delete_project")
    group_name = GROUP_NAME

    data = {
        "name": group_name,
        "profile": {"description": "This is the group description", "role": "ELSI"},
        "users": [_dict_for_user(user1), _dict_for_user(user2)],
        "permissions": [perm_add_node.id],
        "permissions_object": [
            {
                "permission": perm_change_project.id,
                "objects": [project1.id, project2.id],
            },
            {"permission": perm_delete_project.id, "objects": [project2.id]},
        ],
    }
    response = admin_client.post(URL_LIST, data, content_type=APPLICATION_JSON)
    assert response.status_code == status.HTTP_201_CREATED
    group = Group.objects.get(name=group_name)
    assert group.profile.description == data["profile"]["description"]
    assert group.profile.role == GroupProfile.Role.ELSI
    assert len(caplog.record_tuples) == 2
    assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
    assert caplog.record_tuples[1][2].startswith(f"{str(admin_user)} created")
    for match in (
        "change_project",
        "delete_project",
        GROUP_NAME,
        project1.code,
        project2.code,
    ):
        assert (
            match in caplog.record_tuples[1][2]
        ), f"{match} not in {caplog.record_tuples[1][2]}"
    object_permissions = GroupObjectPermission.objects.filter(group=group)
    assert len(object_permissions) == sum(
        len(x["objects"]) for x in data["permissions_object"]
    )
    assert_permissions(
        object_permissions,
        group,
        (
            (perm_change_project, project1),
            (perm_change_project, project2),
            (perm_delete_project, project2),
        ),
    )


def test_create_group_target_does_not_exist(admin_client, project_factory):
    project = project_factory()
    perm_change_project = Permission.objects.get(codename="change_project")
    non_existing_pk = 1_000_000
    initial_group_count = Group.objects.count()
    # Non-existing object
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": perm_change_project.id,
                    "objects": [non_existing_pk],
                },
            ],
        },
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {
            "non_field_errors": [
                f"Object {non_existing_pk} does not exist for model "
                f"{project._meta.model.__name__}"
            ]
        }
    ]
    # Non-existing permission
    response = admin_client.post(
        URL_LIST,
        {
            "name": GROUP_NAME,
            "users": [],
            "permissions_object": [
                {
                    "permission": non_existing_pk,
                    "objects": [project.id],
                },
            ],
        },
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["permissions_object"] == [
        {"permission": [f"{Permission.__name__} {non_existing_pk} does not exist"]}
    ]
    assert Group.objects.count() == initial_group_count


def test_update_group(admin_client, project_factory, caplog, group_factory):
    caplog.set_level(logging.INFO)
    project1, project2, project3 = project_factory.create_batch(3)
    perm_change_project = Permission.objects.get(codename="change_project")
    perm_delete_project = Permission.objects.get(codename="delete_project")
    group = group_factory.create()
    GroupObjectPermission.objects.bulk_create(
        GroupObjectPermission(
            group=group,
            content_type=ContentType.objects.get_for_model(Project),
            object_pk=str(proj.pk),
            permission=perm,
        )
        for perm, proj in (
            (perm_change_project, project1),
            (perm_change_project, project2),
            (perm_delete_project, project3),
        )
    )
    initial_group_perms = GroupObjectPermission.objects.filter(
        permission=perm_change_project
    )
    initial_group_perm_count = GroupObjectPermission.objects.count()

    url = reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    data = {
        "name": group.name,
        "profile": {"description": "This is a changed description!", "role": "NV"},
        "users": [],
        "permissions": [],
        "permissions_object": [
            {
                "permission": perm_change_project.id,
                "objects": [project3.id, project2.id],
            },
        ],
    }
    response = admin_client.put(url, data, content_type=APPLICATION_JSON)
    assert response.status_code == status.HTTP_200_OK
    group = Group.objects.get(name=group.name)
    assert group.profile.description == data["profile"]["description"]
    assert group.profile.role == GroupProfile.Role.NODE_VIEWER
    # one log message for created, another for deleted
    assert len(caplog.record_tuples) == 3
    assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
    object_permissions = GroupObjectPermission.objects.filter(
        permission=perm_change_project
    )
    assert object_permissions.difference(initial_group_perms).count() == 0
    assert_permissions(
        object_permissions,
        group,
        (
            (perm_change_project, project2),
            (perm_change_project, project3),
        ),
    )
    # Add new permission without deleting anything
    caplog.clear()
    admin_client.patch(
        url,
        {
            "permissions_object": [
                {
                    "permission": perm_change_project.id,
                    "objects": [project3.id, project2.id],
                },
                {
                    "permission": perm_delete_project.id,
                    "objects": [project1.id],
                },
            ]
        },
        content_type=APPLICATION_JSON,
    )
    assert len(caplog.record_tuples) == 2
    assert {tup[1] for tup in caplog.record_tuples} == {logging.INFO}
    assert GroupObjectPermission.objects.count() == initial_group_perm_count


def assert_permissions(object_permissions, group, tup):
    assert set(
        (x.group, x.permission, x.content_object) for x in object_permissions
    ) == {(group, perm, proj) for perm, proj in tup}


def test_delete_group(admin_client, project_factory, group_factory):
    group = group_factory.create()
    GroupObjectPermission.objects.create(
        group=group,
        content_type=ContentType.objects.get_for_model(Project),
        object_pk=str(project_factory().pk),
        permission=Permission.objects.get(codename="change_project"),
    )
    initial_group_perm_count = GroupObjectPermission.objects.count()
    initial_group_count = Group.objects.count()
    response = admin_client.delete(
        reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert GroupObjectPermission.objects.count() == initial_group_perm_count - 1
    assert Group.objects.count() == initial_group_count - 1


@pytest.mark.django_db
class TestGroupManagerUpdate:
    @staticmethod
    def get_url(group: Group):
        return reverse(f"{VIEW_NAME}-detail", kwargs={"pk": group.pk})

    @pytest.fixture(autouse=True)
    def setup(self, client, group_factory):
        # pylint: disable=attribute-defined-outside-init
        self.managed_group, self.unmanaged_group = group_factory.create_batch(2)
        self.managed_node, self.unmanaged_node = NodeFactory.create_batch(2)
        self.group_manager = make_group_manager(self.managed_group)
        self.user = UserFactory()
        self.perm_node_admin = Permission.objects.get(codename=PERM_NODE_ADMIN)
        self.perm_change_node = Permission.objects.get(codename="change_node")
        GroupObjectPermission.objects.bulk_create(
            GroupObjectPermission(
                group=group,
                content_type=ContentType.objects.get_for_model(Node),
                object_pk=str(node.pk),
                permission=self.perm_node_admin,
            )
            for group, node in (
                (self.managed_group, self.managed_node),
                (self.unmanaged_group, self.unmanaged_node),
            )
        )

        self.data = {
            "name": self.managed_group.name,
            "users": [{"id": self.user.id}],
            "permissions": [],
            "permissions_object": [
                {
                    "permission": self.perm_node_admin.id,
                    "objects": [self.managed_node.id],
                },
            ],
        }
        self.url = self.get_url(self.managed_group)
        self.client = client
        # pylint: enable=attribute-defined-outside-init

        self.client.login(username=self.group_manager.username, password=USER_PASSWORD)
        yield
        # teardown
        self.client.logout()

    def assert_error_response(self):
        error_response = self.client.put(
            self.url, self.data, content_type=APPLICATION_JSON
        )
        assert error_response.status_code == status.HTTP_400_BAD_REQUEST
        assert error_response.json() == [VALIDATION_ERROR_NON_STAFF_FIELDS]

    def test_group_manager_can_update_own_users(self):
        # `self.data` only contains user ID, not full user object
        response = self.client.put(self.url, self.data, content_type=APPLICATION_JSON)
        assert response.status_code == status.HTTP_200_OK
        users = response.json()["users"]
        assert len(users) == 1

    def test_whole_user_is_returned_for_updated_group(self):
        # `self.data` only contains user ID, not full user object
        response = self.client.put(self.url, self.data, content_type=APPLICATION_JSON)
        user = response.json()["users"][0]
        # Remove `display_name` as `UserFactory` does not provide it
        user.pop("display_name")
        # Returned, updated group contains full user objects
        assert user == _dict_for_user(self.user)

    def test_cannot_update_name(self):
        # group manager CANNOT update `name`
        self.data["name"] = "different"
        self.assert_error_response()

    def test_cannot_update_permissions(self):
        # group manager CANNOT update `permissions`
        self.data["permissions"] = [self.perm_change_node.id]
        self.assert_error_response()

    def test_cannot_update_permission_in_permissions_object(self):
        # group manager CANNOT update `permission` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["permission"] = self.perm_change_node.id
        self.assert_error_response()

    def test_cannot_update_objects_in_permissions_object(self):
        # group manager CANNOT update `objects` on an existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"] = [self.unmanaged_node.id]
        self.assert_error_response()

    def test_cannot_add_object_to_objects_in_permissions_object(self):
        # group manager CANNOT add an object to `objects` on existing object in `permissions_object`
        self.data["permissions_object"][0]["objects"].append(self.unmanaged_node.id)
        self.assert_error_response()

    def test_cannot_add_new_object_to_permissions_object(self):
        # group manager CANNOT add a new object to `permissions_object`
        self.data["permissions_object"].append(
            {
                "permission": self.perm_node_admin.id,
                "objects": [self.unmanaged_node.id],
            }
        )
        self.assert_error_response()

    def test_cannot_update_unmanaged_group_users(self):
        # group manager CANNOT update `users` on groups they DO NOT manage
        url = self.get_url(self.unmanaged_group)
        self.data["name"] = self.unmanaged_group.name
        response = self.client.put(url, self.data, content_type=APPLICATION_JSON)
        assert response.status_code == status.HTTP_404_NOT_FOUND
