from guardian.models import GroupObjectPermission
from guardian.shortcuts import assign_perm

PERM_GROUP_VIEW = "view_group"


def test_delete_group_permissions_when_group_object_is_deleted(group_factory):
    group_with_permission, *group_objects = group_factory.create_batch(3)
    for permission_object in group_objects:
        assign_perm(PERM_GROUP_VIEW, group_with_permission, permission_object)
    assert GroupObjectPermission.objects.all().count() == 2
    group_objects[0].delete()
    assert GroupObjectPermission.objects.all().count() == 1
    group_objects[1].delete()
    assert GroupObjectPermission.objects.all().count() == 0
