import factory
from django_drf_utils.tests.factories import DjangoModelFactoryNoSignals

from identities.models import OAuth2Token, OAuth2Client


class OAuth2ClientFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = OAuth2Client

    client_id = "minio"
    client_name = "minio"
    redirect_uri = "http://localhost:9001/oauth_callback"
    scope = "openid openid profile"
    claims = "policy,name,groups,email,does_not_exist"


class OAuth2TokenFactory(DjangoModelFactoryNoSignals):
    class Meta:
        model = OAuth2Token

    client = factory.SubFactory(OAuth2ClientFactory)
    token_type = "Bearer"  # nosec
    expires_in = 864_000  # 10d
    scope = "openid openid"
    access_token = "cesRL91BCnWQhkral9rHRY3noBZCVxVXDULAZBjo29"  # nosec
