from django.contrib.auth.models import Group
from django.db.models.functions import Lower
from django_drf_utils.permissions import IsUpdate, IsRead
from django_drf_utils.views.utils import unique_check
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.filters import BaseFilterBackend

from identities.filters.group import GroupLookupFilter
from identities.permissions import (
    IsGroupManager,
    get_group_manager_groups,
)
from identities.serializers import GroupSerializer
from projects.models.data_provider import get_data_provider_personnel_groups
from projects.models.node import get_node_personnel_groups
from projects.models.project import get_project_user_groups
from projects.permissions import IsStaff


class GroupManagedFilter(BaseFilterBackend):
    """Filter groups by `auth.change_group` permission for current user."""

    param = "managed"

    def filter_queryset(self, request, queryset, view):
        if (
            request.method == "GET"
            and request.user
            and bool(request.query_params.get(self.param, False))
        ):
            return get_group_manager_groups(request.user)
        return queryset.all()

    def get_schema_operation_parameters(self, view):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": "Returns only groups managed by the user",
                "schema": {"type": "boolean"},
            },
        )


@unique_check((IsStaff,))
class GroupViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    serializer_class = GroupSerializer
    # pylint: disable=unsupported-binary-operation
    permission_classes = (
        IsStaff | (IsUpdate & IsGroupManager) | (permissions.IsAuthenticated & IsRead),
    )
    filter_backends = (GroupLookupFilter, GroupManagedFilter)

    def get_queryset(self):
        requestor = self.request.user
        queryset = (
            Group.objects.all()
            if requestor.is_staff
            else (
                get_group_manager_groups(requestor)
                | get_node_personnel_groups(requestor)
                | get_data_provider_personnel_groups(requestor)
                | get_project_user_groups()
            )
        )

        return queryset.order_by(Lower("name"))
