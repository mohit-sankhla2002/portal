from django.contrib.auth.models import Group
from django.contrib.auth.signals import (
    user_logged_in,
    user_logged_out,
    user_login_failed,
)
from django.contrib.contenttypes.models import ContentType
from django.db.models import signals, Q
from django.dispatch import receiver

from guardian.models import GroupObjectPermission

from ..models import AuthenticationLog


@receiver(user_logged_in, dispatch_uid="log_logged_in")
def log_logged_in(sender, request, user, **_):
    AuthenticationLog.objects.create(
        user=user,
        username=user.username,
        sender=sender,
        action=AuthenticationLog.Action.LOGIN.value,
    )


@receiver(user_logged_out, dispatch_uid="log_logged_out")
def log_logged_out(sender, request, user, **_):
    # user is `None` when the `logout` endpoint is
    # accessed by an anonymous user, e.g. when the
    # user's session is expired
    if user:
        AuthenticationLog.objects.create(
            user=user,
            username=user.username,
            sender=sender,
            action=AuthenticationLog.Action.LOGOUT.value,
        )


@receiver(user_login_failed, dispatch_uid="log_login_failed")
def log_login_failed(sender, credentials, request, **_):
    AuthenticationLog.objects.create(
        user=None,
        username=credentials.get("username", ""),
        sender=sender,
        action=AuthenticationLog.Action.FAILED.value,
    )


# pylint: disable=unused-argument
@receiver(signals.pre_delete, sender=Group)
def delete_group_permissions_when_group_object_is_deleted(sender, instance, **_):
    filters = Q(
        content_type=ContentType.objects.get_for_model(instance), object_pk=instance.pk
    )
    GroupObjectPermission.objects.filter(filters).delete()
