from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import QuerySet
from guardian.shortcuts import get_objects_for_user
from rest_framework import permissions

from projects.models.node import has_any_node_permissions
from projects.models.data_provider import has_any_data_provider_permissions
from projects.models.project import (
    is_data_manager,
    is_project_leader,
    is_project_manager,
)

User = get_user_model()

perm_group_manager = "auth.change_group"
perm_change_user = "identities.change_user"


def has_change_user_permission(user: User) -> bool:
    return user.has_perm(perm_change_user)


def get_group_manager_groups(user: User) -> QuerySet[Group]:
    return get_objects_for_user(user, perm_group_manager)


def has_group_manager_groups(user: User) -> bool:
    return get_group_manager_groups(user).exists()


def has_group_manager_permission(user: User, group: Group | None = None) -> bool:
    return user.has_perm(perm_group_manager, group)


class IsNodePersonnel(permissions.IsAuthenticated):
    """To see node users, node personnel need to access groups."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and (
            has_any_node_permissions(request.user)
        )


class IsDpPersonnel(permissions.IsAuthenticated):
    """To see dp users, dp personnel need to access groups."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and (
            has_any_data_provider_permissions(request.user)
        )


class IsProjectLeader(permissions.IsAuthenticated):
    """To edit a project, project leaders need to access groups."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and is_project_leader(request.user)


class IsDataManager(permissions.IsAuthenticated):
    """To edit a project, data managers need to access groups."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and is_data_manager(request.user)


class IsProjectManager(permissions.IsAuthenticated):
    """To edit a project, project managers need to access groups."""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and is_project_manager(
            request.user
        )


class IsGroupManager(permissions.IsAuthenticated):
    """Group Manager has permission on groups"""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and has_group_manager_groups(
            request.user
        )

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view) and has_group_manager_permission(
            request.user, obj
        )
