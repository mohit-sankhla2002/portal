import secrets
import string
from datetime import timedelta
from enum import Enum

from authlib.oauth2.rfc6749 import scope_to_list, list_to_scope
from authlib.oauth2.rfc6749.models import (
    ClientMixin,
    TokenMixin,
    AuthorizationCodeMixin,
)
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django_drf_utils.models import CreatedMixin, EmailField
from guardian.mixins import GuardianUserMixin
from simple_history.models import HistoricalRecords


class User(AbstractUser, GuardianUserMixin):
    email = EmailField(
        blank=True,
        null=True,
        max_length=254,
        unique=True,
        verbose_name="email address",
    )
    history = HistoricalRecords()

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} ({self.username})"


def get_anonymous_user_instance(UserModel):
    return UserModel(username="AnonymousUser", email="anonymous_user@localhost")


class AuthenticationLog(models.Model):
    class Action(models.TextChoices):
        LOGIN = "I"
        LOGOUT = "O"
        FAILED = "F"

    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, related_name="+"
    )
    username = models.CharField(
        max_length=150,  # the same value as in `AbstractUser`
        validators=(UnicodeUsernameValidator(),),
        null=False,
        blank=True,
    )
    sender = models.CharField(max_length=64, null=False, blank=False)
    action = models.CharField(max_length=1, choices=Action.choices)

    def __str__(self) -> str:
        return f"{self.Action(self.action).label}({self.timestamp}): {self.username}"


class GroupProfile(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=["role_entity_type", "role_entity_id"]),
        ]

    class RoleEntityType(Enum):
        NODE = "node"
        DATA_PROVIDER = "dataprovider"

    class Role(models.TextChoices):
        NODE_ADMIN = "NA", "Node Admin"
        NODE_VIEWER = "NV", "Node Viewer"
        NODE_MANAGER = "NM", "Node Manager"
        DATA_PROVIDER_TECHNICAL_ADMIN = "DPTA", "Data Provider Technical Admin"
        DATA_PROVIDER_VIEWER = "DPW", "Data Provider Viewer"
        DATA_PROVIDER_COORDINATOR = "DPC", "Data Provider Coordinator"
        DATA_PROVIDER_DATA_ENGINEER = "DPDE", "Data Provider Data Engineer"
        DATA_PROVIDER_SECURITY_OFFICER = "DPSO", "Data Provider Security Officer"
        DATA_PROVIDER_MANAGER = "DPM", "Data Provider Manager"
        ELSI = "ELSI", "Legal Approval"

    description = models.TextField(
        blank=True,
        help_text="Group description",
    )
    group = models.OneToOneField(
        Group,
        related_name="profile",
        on_delete=models.CASCADE,
    )
    role = models.CharField(max_length=4, choices=Role.choices, blank=True)
    role_entity_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, blank=True, null=True
    )
    role_entity_id = models.PositiveIntegerField(blank=True, null=True)
    role_entity = GenericForeignKey("role_entity_type", "role_entity_id")

    def __str__(self):
        return f"GroupProfile ({self.group})"


INTERNAL_LEGAL_APPROVAL_GROUP_NAME = "ELSI Help Desk"


def make_random_password():
    return "".join(
        secrets.choice(string.ascii_letters + string.digits) for i in range(48)
    )


def validate_grant_type(value):
    if not value:
        raise ValidationError(
            f"No grant type specified. Valid values are: {OAuth2Client.GrantType.values}"
        )
    for grant_type in value.split():
        if grant_type not in OAuth2Client.GrantType.values:
            raise ValidationError(
                f"Invalid grant type: '{value}'. Valid values are: {OAuth2Client.GrantType.values}"
            )


class OAuth2Client(CreatedMixin, ClientMixin):
    class GrantType(models.TextChoices):
        AUTHORIZATION_CODE = "authorization_code"
        CLIENT_CREDENTIALS = "client_credentials"

    class TokenEndpointAuthMethod(models.TextChoices):
        CLIENT_SECRET_BASIC = "client_secret_basic"  # nosec
        CLIENT_SECRET_POST = "client_secret_post"  # nosec
        NONE = "none"

    client_id = models.CharField(max_length=48, unique=True, db_index=True)
    client_secret = models.CharField(max_length=48, default=make_random_password)
    client_name = models.CharField(
        max_length=120,
        help_text="Helps to uniquely identify the client by name in the list of all clients. "
        "It is NOT an unique identifier.",
        verbose_name="Client name",
    )
    redirect_uri = models.CharField(
        max_length=255,
        verbose_name="Redirect URI",
        help_text="Redirection URI for use in redirect-based flows such as the authorization code and implicit"
        "flows (typically 'http://localhost:9001/oauth_callback' using a minio local instance).",
    )
    # `openid` is a required scope. All others – `profile`, `email`, `address`, ... and custom ones – are optional.
    scope = models.CharField(
        max_length=120,
        blank=False,
        help_text="Provides you a logical grouping of claims. An application can request one or "
        "more scopes (space separated) and specify what access privileges are being requested "
        "by the client application.",
        default="openid",
    )
    claims = models.CharField(
        max_length=120,
        blank=False,
        help_text="Provides you information as they are found in tokens. "
        "They are specific attributes from the User model (comma separated).",
        default="policy",
    )
    response_type = models.CharField(
        max_length=48,
        help_text="Actual set of response types are `code`, `token`, `id_token`, `none`. "
        "A combination (space separated) is possible.",
        default="code",
    )
    grant_type = models.CharField(
        max_length=48,
        help_text=f"Refers to the way the app gets the access token. "
        f"Possible values (space separated) are [{', '.join(GrantType.values)}].",
        validators=[validate_grant_type],
        default=GrantType.AUTHORIZATION_CODE,
    )
    token_endpoint_auth_method = models.CharField(
        blank=False,
        choices=TokenEndpointAuthMethod.choices,
        max_length=120,
        default=TokenEndpointAuthMethod.CLIENT_SECRET_BASIC,
    )

    def get_client_id(self):
        return self.client_id

    def get_default_redirect_uri(self):
        return self.redirect_uri

    def get_allowed_scope(self, scope):
        allowed = set(scope_to_list(self.scope))
        return list_to_scope([s for s in scope.split() if s in allowed])

    def check_redirect_uri(self, redirect_uri):
        return bool(redirect_uri) and (redirect_uri == self.redirect_uri)

    def check_client_secret(self, client_secret):
        return self.client_secret == client_secret

    def check_endpoint_auth_method(self, method, endpoint):
        return endpoint != "token" or self.token_endpoint_auth_method == method

    def check_response_type(self, response_type):
        return response_type in self.response_type.split()

    def check_grant_type(self, grant_type):
        return grant_type in self.grant_type.split()

    class Meta:
        verbose_name = "OAuth2 client"

    history = HistoricalRecords()

    def __str__(self) -> str:
        return f"OAuth2 client '{self.client_name}' (ID: '{self.client_id}')"


class OAuth2Token(TokenMixin, CreatedMixin):
    client = models.ForeignKey(
        OAuth2Client, to_field="client_id", db_column="client", on_delete=models.CASCADE
    )
    token_type = models.CharField(max_length=40)
    access_token = models.CharField(max_length=255, unique=True, null=False)
    scope = models.CharField(max_length=120)
    expires_in = models.IntegerField(
        null=False, default=0
    )  # It applies to the token response and must not be confused with token `exp`

    def get_client_id(self):
        return self.client.client_id

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_expires_at(self):
        return self.created + timedelta(seconds=self.expires_in)

    def check_client(self, client):
        return self.client_id == self.get_client_id()

    def is_expired(self):
        return self.get_expires_at() < timezone.now()

    def is_revoked(self):
        return False


class AuthorizationCode(AuthorizationCodeMixin, CreatedMixin):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    client = models.ForeignKey(
        OAuth2Client, to_field="client_id", db_column="client", on_delete=models.CASCADE
    )
    code = models.CharField(max_length=120, unique=True)
    redirect_uri = models.CharField(max_length=255, null=True)
    response_type = models.CharField(max_length=48)
    scope = models.CharField(max_length=120, null=True)
    nonce = models.CharField(max_length=120, null=True)

    def is_expired(self):
        return self.created + timedelta(seconds=300) < timezone.now()

    def get_redirect_uri(self):
        return self.redirect_uri

    def get_scope(self):
        return self.scope

    def get_auth_time(self):
        return self.created.timestamp()

    def get_nonce(self):
        return self.nonce
