from projects.filters.common import BaseLookupFilter, QueryParam


class GroupLookupFilter(BaseLookupFilter):
    """Filters groups by `role` and `role_entity_id`"""

    query_params = [
        QueryParam(
            "role",
            "profile__role__iexact",
            "Includes only groups with specified role",
        ),
        QueryParam(
            "role_entity_id",
            "profile__role_entity_id__iexact",
            "Includes only groups with specified role_entity_id",
        ),
        QueryParam(
            "role_entity_type",
            "profile__role_entity_type__model__iexact",
            "Includes only groups with specified role_entity_type",
        ),
    ]
