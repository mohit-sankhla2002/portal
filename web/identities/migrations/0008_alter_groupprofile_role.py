# Generated by Django 4.0.7 on 2022-11-16 13:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("identities", "0007_groupprofile_role_groupprofile_role_entity_id_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="groupprofile",
            name="role",
            field=models.CharField(
                blank=True,
                choices=[
                    ("NA", "Node Admin"),
                    ("NV", "Node Viewer"),
                    ("NM", "Node Manager"),
                    ("DPTA", "Data Provider Technical Admin"),
                    ("DPW", "Data Provider Viewer"),
                    ("DPC", "Data Provider Coordinator"),
                    ("DPDE", "Data Provider Data Engineer"),
                    ("DPSO", "Data Provider Security Officer"),
                    ("DPM", "Data Provider Manager"),
                ],
                max_length=4,
                null=True,
            ),
        ),
    ]
