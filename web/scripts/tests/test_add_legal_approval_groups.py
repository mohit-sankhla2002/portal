import pytest

from django.contrib.auth.models import Group

from identities.models import GroupProfile
from projects.tests.factories import make_legal_approval_group
from scripts.add_legal_approval_groups import run, get_node_legal_approval_group_name


@pytest.mark.django_db
@pytest.mark.parametrize("groups_exist", (False, True))
def test_add_legal_approval_groups(groups_exist, node_factory):
    existing_nodes = 3
    nodes = node_factory.create_batch(existing_nodes)
    if groups_exist:
        make_legal_approval_group()
        for node in nodes:
            make_legal_approval_group(get_node_legal_approval_group_name(node), node)

    run()

    groups = Group.objects.all()
    assert len(groups) == (existing_nodes + 1)
    assert all(group.profile.role == GroupProfile.Role.ELSI for group in groups)
    assert set(nodes).issubset(group.profile.role_entity for group in groups)
