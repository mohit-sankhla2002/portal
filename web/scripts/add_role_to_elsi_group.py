import logging

from django.contrib.auth.models import Group
from django.db.transaction import atomic


from identities.models import GroupProfile, INTERNAL_LEGAL_APPROVAL_GROUP_NAME


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    try:
        group = Group.objects.get(
            name=INTERNAL_LEGAL_APPROVAL_GROUP_NAME, profile__role=None
        )
        group_profile, _ = GroupProfile.objects.get_or_create(group=group)
        group_profile.role = GroupProfile.Role.ELSI
        group_profile.save()

        logger.info("Updated group %s", group.name)

    except Group.DoesNotExist:
        logger.info("Skipping %s", INTERNAL_LEGAL_APPROVAL_GROUP_NAME)

    return 0
